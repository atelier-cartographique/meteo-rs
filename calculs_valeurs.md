# Calculs sous-tendants la page forecast:

## Eolien

### Production onshore et offshore (voir SQL):
*production_corrigée / nb jours* (1, 3 ou 7 en fonction du choix) pour avoir une moyenne,
où: 

  - production_corrigée = production / monitored_capacity * max (monitored_capacity, val_apere), avec:
  - production  = sum(most_recent_forecast)/4 où la somme se fait sur la période donnée.
  - monitored_capacity = moyenne de cette colonne sur la période donnée
  - val_apere est la somme de "eol_onshore_rw_power_apere" et "eol_onshore_rf_power_apere" introduites par l'Apere dans la db.

### Nombre d'éolienne (onsh/offsh):
nombre issu de la db (*introduit par l'Apere*)
  -> sql: prend la valeur à la date la plus proche de la date donnée

### Production totale:
*somme des productions* onshore et offshore

### Puissance installée (monitored capacity)
*max(puissance installée, puissance donnée par l'apere)*, (voir render_forecast.rs, l.316)
où la puissance installée est la moyenne sur la période considérée, des valeurs d'Elia (voir SQL)


### Nombre d'équivalents logement:
*nombre d'équivalents logement/ nombre de jours* (1, 3 ou 7 en fonction du choix) pour avoir une moyenne,
où 

  - nb d'équivalents logement = production/3.5 * 365 * nb_jours (voir wind_indicators l.251)

### Pourcentage d'équivalents logement:
*nombre d'équivalent logement / tot_logements \* 100*
où:

  - tot_logements est complété par l'Apere dans la db

### Taux de production (jauge)
*loadfactor* (voir render_forecast, l.359), où

  - loadfactor = moyenne(most_recent_forecast)/ moyenne(monitored_capacity) (voir SQL)

### Valeurs du graphe (production au quart d'heure) (render_forecast.rs, l.297):
*most_recent_forecast / monitored_capacity * max(monitored_capacity, val_apere)*
où 

  - val_apere est la somme de "eol_onshore_rw_power_apere" et "eol_onshore_rf_power_apere" introduites par l'Apere dans la db.


## Photovoltaique

### Production totale
*production_corrigée / nb jours* (1, 3 ou 7 selon le choix de l'utilisateur) où

  - production_corrigée (voir SQL) = production / monitored_capacity * max (monitored_capacity, pv_power_apere), avec:
  - production  = sum(most_recent_forecast)/4 où la somme se fait sur la période donnée.
  - monitored_capacity = moyenne de cette colonne sur la période donnée
  - pv_power_apere est introduite par l'Apere dans la db.

### Nombres de systèmes photovoltaiques
Valeur introduite par l'Apere (utilisation de la dernière ajoutée, voir SQL).

### Puissance installée
*max(monitored_capacity, pv_power_apere)*
où 

  - "pv_power_apere" est introduite par l'Apere dans la db.

### Nombre d'équivalents logement
*nombre d'équivalent logement / nombre de jours* (1, 3 ou 7 en fonction du choix) pour avoir une moyenne
où 

  - nb d'équivalents logement = production/3.5 * 365 * nb_jours (voir solar_indicators.rs l.243)

### Pourcentage d'équivalents logement
*nb d'équivalents logement / tot_logements* (< db, complété par l'Apere) * 100

### Taux de production (jauge)
*loadfactor / 0.85* (render_forecast, l.205)
où 
  - loadfactor = moyenne(most_recent_forecast/monitored_capacity)*100, où la moyenne est calculée sur les valeurs entre 13h (inclu) et 14h (exclu).

### Valeurs de la carte
loadfactors pour chaque région pendant l'heure donnée (moyenne sur l'heure), ajustés comme le taux de production (/0.85)
