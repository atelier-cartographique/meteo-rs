# Base de données APERE

## Tables complétées par l'APERE

- corrections (corrections des capacités controlées et des quantités d'installations pv et éoliennes)
- hydro (1 valeur par mois)
- hydro_corrections (normalement peu modifiées, 1 valeur par mois)

## Tables complétées automatiquement à partir du web service ELIA:

### Tables pour les prévisions

- solar, solar_bxl, solar_lie, etc.
- wind, wind_onshore, wind_offshore
- load

### Tables pour la production passée

- solar_past, solar_bxl_prod, solar_lie_prod, etc.
- solar_daily_past (pour la productivité journalière depuis 2009)
- wind_past, wind_past_onshore, wind_past_offshore
- load_past

## Tables complétées automatiquement à partir du web-service ENTSOe

- biomass
- waste

## Autres tables

- fetch_events: indique les imports de données dans la db, ainsi que suppressions d'anciennes données pour les mettre à jour ("Delete...")
  - "ts" indique la date de l'action (ajout, suppression de données ou erreur),
  - "begin" indique la date dans la db à partir de laquelle il y a modification

### Remarque

Les tables liées aux données solaires sont soit pour la Belgique, soit par province (bxl = Bruxelles, hai = Hainaut, o_vl = Oost Vlaanderen, br_w = Brabant Wallon, w_vl = West Vlaanderen, vl_b = Vlaams Brabant, lie = Liège, lux = Luxembourg, lim = Limbourg, ant = Antwerpen, nam = Namur)

### Acces

https://meteoer.atelier-cartographique.be/pgadmin4/browser/

- Dans le navigateur (à gauche), aller dans server -> Databases -> meteo -> Schemas -> public -> tables
- Choisir une table puis cliquer tout en haut sur l'icone "View Data" (ou "Afficher les données").
- Modifier les données existantes dans le tableau ou ajouter des nouvelles données dans la ligne vierge tout en bas du tableau.
- Fermer la table (croix en haut à droite), une fenêtre apparait pour vous demander si vous voulez enregistrer les nouvelles données.
