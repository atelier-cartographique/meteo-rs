#!/bin/bash

names=(bxl ant hai lim lie lux nam o_vl vl_b br_w w_vl)
Names=(Bxl Ant Hai Lim Lie Lux Nam OVl VlB BrW WVl)

for i in {9..10}
do 
# echo "
#     // -- forecast -- ${names[i]}
#     Name::InsertSolar${Names[i]} => include_str!(\"sql/solar/forecast/insert_solar_${names[i]}.sql\"),
#     Name::SelectSolarForecast${Names[i]} => {
#         include_str!(\"sql/solar/forecast/select_solar_${names[i]}_forecast.sql\")
#     }
#     Name::SelectSolarLoadFactorForecast${Names[i]} => {
#         include_str!(\"sql/solar/forecast/select_solar_${names[i]}_loadfactor.sql\")
#     }
#     Name::SelectSolarLastDate${Names[i]} => {
#         include_str!(\"sql/solar/forecast/select_solar_${names[i]}_last_date.sql\")
#     }
#     // -- production -- ${names[i]}
#     Name::InsertSolarProd${Names[i]} => include_str!(\"sql/solar/production/insert_solar_${names[i]}_prod.sql\"),
#     Name::SelectSolarProduction${Names[i]} => {
#         include_str!(\"sql/solar/production/select_solar_${names[i]}_prod.sql\")
#     }
#     Name::SelectSolarMonitoredCapacity${Names[i]} => {
#         include_str!(\"sql/solar/production/select_solar_${names[i]}_prod_monitored_cap.sql\")
#     }
#     Name::SelectSolarLoadFactor${Names[i]} => {
#         include_str!(\"sql/solar/production/select_solar_${names[i]}_prod_loadfactor.sql\")
#     }
#     Name::SelectSolarProdLastDate${Names[i]} => {
#         include_str!(\"sql/solar/production/select_solar_${names[i]}_prod_last_date.sql\")
#     }
# "

echo "
    // -- forecast ${names[i]}
    InsertSolar${Names[i]},
    SelectSolarForecast${Names[i]},
    SelectSolarLoadFactorForecast${Names[i]},
    SelectSolarLastDate${Names[i]},
    DeleteSolarBxl${Names[i]},
    HourOfMaxBxl${Names[i]},
    // -- production ${names[i]}
    InsertSolarProd${Names[i]},
    SelectSolarProduction${Names[i]},
    SelectSolarMonitoredCapacity${Names[i]},
    SelectSolarLoadFactor${Names[i]},
    SelectSolarProdLastDate${Names[i]},
    DeleteSolarProdBxl${Names[i]},
"
echo "
    //statements for SOLAR ${names[i]}
    // -- forecast -- ${names[i]}
            statements.insert(
                Name::InsertSolar${Names[i]},
                client.prepare(sql(Name::InsertSolar${Names[i]})).await?,
            );

            statements.insert(
                Name::SelectSolarForecast${Names[i]},
                client.prepare(sql(Name::SelectSolarForecast${Names[i]})).await?,
            );
            statements.insert(
                Name::SelectSolarLoadFactorForecast${Names[i]},
                client
                    .prepare(sql(Name::SelectSolarLoadFactorForecast${Names[i]}))
                    .await?,
            );
            statements.insert(
                Name::SelectSolarLastDate${Names[i]},
                client.prepare(sql(Name::SelectSolarLastDate${Names[i]})).await?,
            );
            statements.insert(
            Name::DeleteSolar${Names[i]},
            client.prepare(sql(Name::DeleteSolar${Names[i]})).await?,
            );
            statements.insert(
                Name::HourOfMax${Names[i]},
                client.prepare(sql(Name::HourOfMax${Names[i]})).await?,
            );

    // -- production -- ${names[i]}
            statements.insert(
                Name::InsertSolarProd${Names[i]},
                client.prepare(sql(Name::InsertSolarProd${Names[i]})).await?,
            );
            statements.insert(
                Name::SelectSolarProduction${Names[i]},
                client.prepare(sql(Name::SelectSolarProduction${Names[i]})).await?,
            );
            statements.insert(
                Name::SelectSolarMonitoredCapacity${Names[i]},
                client
                    .prepare(sql(Name::SelectSolarMonitoredCapacity${Names[i]}))
                    .await?,
            );
            statements.insert(
                Name::SelectSolarLoadFactor${Names[i]},
                client.prepare(sql(Name::SelectSolarLoadFactor${Names[i]})).await?,
            );
            statements.insert(
                Name::SelectSolarProdLastDate${Names[i]},
                client.prepare(sql(Name::SelectSolarProdLastDate${Names[i]})).await?,
            );
            statements.insert(
                Name::DeleteSolarProd${Names[i]},
                client.prepare(sql(Name::DeleteSolarProd${Names[i]})).await?,
            );
            
"
done