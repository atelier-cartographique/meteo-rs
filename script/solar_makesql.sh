#!/bin/bash

tables=(solar_bxl solar_ant solar_hai solar_lim solar_lie solar_lux solar_nam solar_o_vl solar_vl_b solar_br_w solar_w_vl)

for TABLE in ${tables[@]}

do
# echo "
#   DROP TABLE ${TABLE};
# "
# echo "
#   CREATE TABLE IF NOT EXISTS $TABLE (
#     -- table Anvers/Antwerpen (5)
#     id serial PRIMARY KEY,
#     day_ahead_forecast double precision,
#     monitored_capacity double precision,
#     most_recent_forecast double precision,
#     ts timestamp with time zone,
#     week_ahead_forecast double precision
# );
#   CREATE INDEX ${TABLE}_ts_index
#     ON public.${TABLE} (ts);
# " 
# >> create_${TABLE}.sql

# echo "
#   INSERT INTO $TABLE (
#     day_ahead_forecast,
#     monitored_capacity,
#     most_recent_forecast,
#     ts,
#     week_ahead_forecast
#   )
#   VALUES
#     (\$1, \$2, \$3, \$4, \$5);
# " >> insert_${TABLE}.sql
	
# echo "
#   SELECT 
#     ROUND(SUM(most_recent_forecast) / 4) 
#   FROM $TABLE 
#   WHERE ts > \$1 AND ts < \$2;
# " >> select_${TABLE}_forecast.sql

# echo "
#   SELECT 
      # ROUND(
      #   AVG(most_recent_forecast) /(AVG(monitored_capacity)) * 10000
      # ) / 100
#   FROM $TABLE
#   WHERE
#     ts > \$1
#     AND ts < \$2
#     AND most_recent_forecast >= 0;
# " >> select_${TABLE}_loadfactor.sql

# echo "
#   SELECT
#     ts :: TIMESTAMP WITH TIME ZONE
#   FROM $TABLE
#   ORDER BY
#     ts DESC
#   LIMIT
#     1;
# " >> select_${TABLE}_last_date.sql
 
echo "
    SELECT ts
  FROM solar_bxl
  WHERE DATE_TRUNC('day', ts) = DATE_TRUNC('day', \$1::TIMESTAMP WITH TIME ZONE)
  ORDER BY most_recent_forecast DESC
  LIMIT 1;
" >> max_hour_${TABLE}.sql
done


