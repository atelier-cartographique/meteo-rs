#!/bin/bash

tables=(solar_bxl_prod solar_ant_prod solar_hai_prod solar_lim_prod solar_lie_prod solar_lux_prod solar_nam_prod solar_o_vl_prod solar_vl_b_prod solar_br_w_prod solar_w_vl_prod)

for TABLE in ${tables[@]}
do
# echo "
# DROP TABLE ${TABLE};"
# echo "
#   CREATE TABLE IF NOT EXISTS $TABLE (
#     id serial PRIMARY KEY,
#     load_factor double precision,
#     monitored_capacity double precision,
#     real_time double precision,
#     ts timestamp with time zone
#   );
#     CREATE INDEX ${TABLE}_ts_index
#     ON public.${TABLE} (ts);
# " >> create_${TABLE}.sql

# echo "
#   INSERT INTO $TABLE (
#     load_factor,
#     monitored_capacity,
#     real_time,
#     ts
#   )
#   VALUES
#     (\$1, \$2, \$3, \$4);
# " >> insert_${TABLE}.sql

# echo "
#   SELECT
#     ROUND(SUM(real_time) / 4)
#   FROM $TABLE
#   WHERE
#     ts > \$1
#     AND ts < \$2
#     AND real_time >= 0;
# " >> select_${TABLE}.sql

# echo "
#   SELECT
#     ROUND(monitored_capacity)
#   FROM $TABLE
#   WHERE
#     date_trunc('hour', ts) = date_trunc('hour', \$1 :: TIMESTAMP WITH TIME ZONE);
# " >> select_${TABLE}_monitored_cap.sql

# echo "
#   SELECT
#     ROUND(AVG(load_factor))
#   FROM $TABLE
#   WHERE
#     ts > \$1
#     AND ts < \$2
#     AND load_factor >= 0;
# " >> select_${TABLE}_loadfactor.sql

# echo "
#   SELECT
#     ts :: TIMESTAMP WITH TIME ZONE
#   FROM $TABLE
#   ORDER BY
#     ts DESC
#   LIMIT
#     1;
# " >> select_${TABLE}_last_date.sql

echo "
  DELETE FROM $TABLE
  WHERE ts > \$1;
" >> delete_${TABLE}.sql


done
