use proc_macro::{self, TokenStream};
use proc_macro_error::{abort_call_site, proc_macro_error};
use quote::quote;
use syn::{
    parse::{Parse, ParseStream},
    parse_macro_input, DeriveInput, Expr, FieldsNamed, FieldsUnnamed, Ident, Type,
};

const COPY_TYPES: [&str; 15] = [
    "bool", "char", "f32", "f64", "i8", "i16", "i32", "i64", "i128", "isize", "u8", "u16", "u32",
    "u64", "u128",
];

fn is_copy(ty: &Type) -> bool {
    if let Type::Path(tp) = ty {
        for ident in COPY_TYPES.iter() {
            if tp.path.is_ident(ident) {
                return true;
            }
        }
    };
    false
}

// fn wrap_unnamed_nocopy(ident: Ident, out_type: &Type) -> TokenStream {
//     let output = quote! {
//     impl #ident {
//         pub fn new(arg: #out_type) -> #ident {
//             #ident(arg)
//         }

//         pub fn value(&self) -> &#out_type {
//             &self.0
//         }
//     }

//     impl From<#out_type> for #ident {
//         fn from(source: #out_type) -> Self {
//             #ident::new(source)
//         }
//     }

//     impl Into<#out_type> for #ident {
//         fn into(self) -> #out_type {
//             self.0
//         }
//     }

//     };
//     return output.into();
// }

fn wrap_unnamed(ident: Ident, fields: FieldsUnnamed) -> TokenStream {
    if let Some(field) = fields.unnamed.first() {
        let out_type = field.ty.clone();
        let value_fn = if is_copy(&out_type) == false {
            quote! {
                pub fn value(&self) -> &#out_type {
                    &self.0
                }
            }
        } else {
            quote! {
                pub fn value(&self) -> #out_type {
                    self.0
                }
            }
        };

        let output = quote! {
        impl #ident {
            pub fn new(arg: #out_type) -> #ident {
                #ident(arg)
            }

            #value_fn
        }


        impl From<#out_type> for #ident {
            fn from(source: #out_type) -> Self {
                #ident::new(source)
            }
        }

        impl Into<#out_type> for #ident {
            fn into(self) -> #out_type {
                self.0
            }
        }

        };
        return output.into();
    } else {
        abort_call_site!("An empty struct cannot be wrapped")
    }
}

fn wrap_named(ident: Ident, fields: FieldsNamed) -> TokenStream {
    let simple_fields: Vec<(Ident, Type)> = fields
        .named
        .iter()
        .filter_map(|f| f.ident.clone().map(|i| (i.clone(), f.ty.clone())))
        .collect();

    let idents: Vec<Ident> = simple_fields.iter().map(|(i, _)| i.clone()).collect();

    let types: Vec<Type> = simple_fields.iter().map(|(_, t)| t.clone()).collect();

    let indices: Vec<syn::Index> = (0..idents.len()).map(syn::Index::from).collect();

    let output = quote! {
    impl #ident {
        pub fn new(arg: (#(#types),*)) -> #ident {
            #ident {
                #( #idents : arg.#indices ),*
            }
        }

        #( pub fn #idents(&self) -> &#types {
            &self.#idents
        } )*

    }


    impl From<(#(#types),*)> for #ident {
        fn from(source: (#(#types),*)) -> Self {
            #ident::new(source)
        }
    }

    impl Into<(#(#types),*)> for #ident {
        fn into(self) ->  (#(#types),*) {
            (
                #( self.#idents ),*
            )
        }
    }

    };

    // eprintln!(
    //     "== output ====================================\n{}",
    //     output.clone()
    // );
    return output.into();
}

#[proc_macro_derive(Wrapper0)]
#[proc_macro_error]
pub fn wrapper0(input: TokenStream) -> TokenStream {
    let DeriveInput { ident, data, .. } = parse_macro_input!(input);

    if let syn::Data::Struct(s) = data {
        match s.fields {
            syn::Fields::Unnamed(fields) => return wrap_unnamed(ident, fields),
            syn::Fields::Named(fields) => return wrap_named(ident, fields),
            syn::Fields::Unit => abort_call_site!("Err! we don't wrap unit"),
        }
    } else {
        abort_call_site!("I need a struct")
    }
}

fn parse_sql(expr: Expr) -> Expr {
    match expr {
        Expr::Lit(_) => expr,
        Expr::Macro(_) => expr,
        Expr::MethodCall(_) => expr,
        Expr::Path(_) => expr,
        _ => abort_call_site!(
            "The sql parameter shall be a literal, method call or macro invocation"
        ),
    }
}

#[derive(Debug)]
struct ReadModelDesc {
    base_ty: Ident,
    sql: Expr,
    from_row_block: Expr,
}

impl Parse for ReadModelDesc {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        let base_ty: Ident = input.parse()?;
        let sql = Expr::parse_without_eager_brace(input)?;
        let from_row_block: Expr = input.parse()?;
        Ok(ReadModelDesc {
            base_ty,
            sql,
            from_row_block,
        })
        // Ok(ReadModelDesc {
        //     base_ty: input.parse()?,
        //     sql: input.parse()?,
        //     from_row_block: input.parse()?,
        // })
    }
}

#[proc_macro]
#[proc_macro_error]
pub fn read_model(input: TokenStream) -> TokenStream {
    let stream = input.clone();
    let parsed = parse_macro_input!(stream as ReadModelDesc);
    // eprintln!(
    //     "== input ====================================\n{}",
    //     input.clone()
    // );
    // eprintln!(
    //     "== parsed ===================================\n{:?}",
    //     parsed
    // );
    // eprintln!("=============================================");
    let ReadModelDesc {
        base_ty,
        sql,
        from_row_block,
    } = parsed;

    let sql = parse_sql(sql);

    quote! {
    // auto-generated
    impl crate::model::WithSQL for #base_ty {
        fn sql() -> &'static str {
            #sql
        }
    }
    impl crate::model::FromRow for #base_ty {
        fn from_row(
            row: &tokio_postgres::Row,
        ) -> Result<Self, crate::model::ModelError> {
            #from_row_block
        }
    }
    }
    .into()
}

// #[derive(Debug)]
// struct WriteModelDesc {
//     base_ty: Type,
//     sql: Lit,
//     to_params_block: Block,
// }

// impl Parse for WriteModelDesc {
//     fn parse(input: ParseStream) -> syn::Result<Self> {
//         Ok(WriteModelDesc {
//             base_ty: input.parse()?,
//             sql: input.parse()?,
//             to_params_block: input.parse()?,
//         })
//     }
// }
// #[proc_macro]
// pub fn write_model(input: TokenStream) -> TokenStream {
//     let stream = input.clone();
//     let parsed = parse_macro_input!(stream as WriteModelDesc);
//     let WriteModelDesc {
//         base_ty,
//         sql,
//         to_params_block,
//     } = parsed;

//     quote! {
//     // auto-generated
//     impl crate::model::WithSQL for #base_ty {
//         fn sql() -> &'static str {
//             #sql
//         }
//     }
//     impl crate::model::ToParams for #base_ty {
//         fn params(&self) -> Vec<Box<dyn ToSql + Sync>> {
//             #to_params_block
//         }
//     }
//     }
//     .into()
// }
