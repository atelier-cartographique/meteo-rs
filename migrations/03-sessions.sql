CREATE TABLE "sessions" (
    username text PRIMARY KEY,
    token text not null,
    created_at timestamp with time zone not null
);