CREATE TABLE news (
    id SERIAL PRIMARY KEY,
    pub_date timestamp with time zone,
    summary text not null,
    content text not null,
    "status" integer default 1
);