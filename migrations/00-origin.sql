--
-- PostgreSQL database dump
--

-- Dumped from database version 13.7 (Debian 13.7-0+deb11u1)
-- Dumped by pg_dump version 13.7 (Debian 13.7-0+deb11u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: biomass; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.biomass (
    id integer NOT NULL,
    ts timestamp with time zone,
    production double precision DEFAULT 0
);


--
-- Name: biomass_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.biomass_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: biomass_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.biomass_id_seq OWNED BY public.biomass.id;


--
-- Name: corrections; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.corrections (
    id integer NOT NULL,
    ts timestamp with time zone,
    pv_power_apere double precision DEFAULT 4861,
    autoconsommation double precision DEFAULT 1,
    eol_offshore_power_apere double precision DEFAULT 1556,
    eol_onshore_rw_power_apere double precision DEFAULT 1036,
    eol_onshore_rf_power_apere double precision DEFAULT 1242,
    nb_onshore integer DEFAULT 982,
    nb_offshore integer DEFAULT 359,
    tot_logements integer DEFAULT 11431406,
    nb_syst_pv integer DEFAULT 601000
);


--
-- Name: corrections_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.corrections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: corrections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.corrections_id_seq OWNED BY public.corrections.id;


--
-- Name: fetch_events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fetch_events (
    id integer NOT NULL,
    ts timestamp with time zone,
    description text,
    begin timestamp with time zone,
    url text,
    error text
);


--
-- Name: fetch_events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.fetch_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fetch_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.fetch_events_id_seq OWNED BY public.fetch_events.id;


--
-- Name: hydro; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hydro (
    id integer NOT NULL,
    month integer,
    year integer,
    production double precision DEFAULT 0
);


--
-- Name: hydro_correction; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hydro_correction (
    id integer NOT NULL,
    month integer,
    correction double precision DEFAULT 1
);


--
-- Name: hydro_correction_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.hydro_correction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hydro_correction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.hydro_correction_id_seq OWNED BY public.hydro_correction.id;


--
-- Name: hydro_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.hydro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hydro_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.hydro_id_seq OWNED BY public.hydro.id;


--
-- Name: load; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.load (
    id integer NOT NULL,
    cipu_load double precision,
    cipu_load_da_forecast double precision,
    cipu_load_forecast double precision,
    cipu_load_id_forecast double precision,
    ts timestamp with time zone,
    tick integer,
    total_load double precision,
    total_load_da_forecast double precision,
    total_load_da_forecast_p10 double precision,
    total_load_da_forecast_p90 double precision,
    total_load_forecast double precision,
    total_load_forecast_p10 double precision,
    total_load_forecast_p90 double precision,
    total_load_id_forecast double precision,
    total_load_id_forecast_p10 double precision,
    total_load_id_forecast_p90 double precision,
    total_load_wa_forecast double precision,
    total_load_wa_forecast_p10 double precision,
    total_load_wa_forecast_p90 double precision
);


--
-- Name: load_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.load_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: load_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.load_id_seq OWNED BY public.load.id;


--
-- Name: load_past; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.load_past (
    id integer NOT NULL,
    cipu_load double precision,
    ts timestamp with time zone,
    tick integer,
    total_load double precision
);


--
-- Name: load_past_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.load_past_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: load_past_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.load_past_id_seq OWNED BY public.load_past.id;


--
-- Name: solar; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar (
    id integer NOT NULL,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision
);


--
-- Name: solar_ant; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_ant (
    id integer NOT NULL,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision
);


--
-- Name: solar_ant_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_ant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_ant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_ant_id_seq OWNED BY public.solar_ant.id;


--
-- Name: solar_ant_prod; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_ant_prod (
    id integer NOT NULL,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: solar_ant_prod_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_ant_prod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_ant_prod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_ant_prod_id_seq OWNED BY public.solar_ant_prod.id;


--
-- Name: solar_br_w; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_br_w (
    id integer NOT NULL,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision
);


--
-- Name: solar_br_w_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_br_w_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_br_w_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_br_w_id_seq OWNED BY public.solar_br_w.id;


--
-- Name: solar_br_w_prod; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_br_w_prod (
    id integer NOT NULL,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: solar_br_w_prod_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_br_w_prod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_br_w_prod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_br_w_prod_id_seq OWNED BY public.solar_br_w_prod.id;


--
-- Name: solar_bxl; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_bxl (
    id integer NOT NULL,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision
);


--
-- Name: solar_bxl_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_bxl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_bxl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_bxl_id_seq OWNED BY public.solar_bxl.id;


--
-- Name: solar_bxl_prod; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_bxl_prod (
    id integer NOT NULL,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: solar_bxl_prod_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_bxl_prod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_bxl_prod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_bxl_prod_id_seq OWNED BY public.solar_bxl_prod.id;


--
-- Name: solar_hai; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_hai (
    id integer NOT NULL,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision
);


--
-- Name: solar_hai_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_hai_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_hai_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_hai_id_seq OWNED BY public.solar_hai.id;


--
-- Name: solar_hai_prod; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_hai_prod (
    id integer NOT NULL,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: solar_hai_prod_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_hai_prod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_hai_prod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_hai_prod_id_seq OWNED BY public.solar_hai_prod.id;


--
-- Name: solar_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_id_seq OWNED BY public.solar.id;


--
-- Name: solar_lie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_lie (
    id integer NOT NULL,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision
);


--
-- Name: solar_lie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_lie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_lie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_lie_id_seq OWNED BY public.solar_lie.id;


--
-- Name: solar_lie_prod; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_lie_prod (
    id integer NOT NULL,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: solar_lie_prod_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_lie_prod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_lie_prod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_lie_prod_id_seq OWNED BY public.solar_lie_prod.id;


--
-- Name: solar_lim; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_lim (
    id integer NOT NULL,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision
);


--
-- Name: solar_lim_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_lim_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_lim_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_lim_id_seq OWNED BY public.solar_lim.id;


--
-- Name: solar_lim_prod; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_lim_prod (
    id integer NOT NULL,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: solar_lim_prod_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_lim_prod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_lim_prod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_lim_prod_id_seq OWNED BY public.solar_lim_prod.id;


--
-- Name: solar_lux; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_lux (
    id integer NOT NULL,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision
);


--
-- Name: solar_lux_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_lux_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_lux_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_lux_id_seq OWNED BY public.solar_lux.id;


--
-- Name: solar_lux_prod; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_lux_prod (
    id integer NOT NULL,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: solar_lux_prod_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_lux_prod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_lux_prod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_lux_prod_id_seq OWNED BY public.solar_lux_prod.id;


--
-- Name: solar_nam; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_nam (
    id integer NOT NULL,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision
);


--
-- Name: solar_nam_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_nam_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_nam_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_nam_id_seq OWNED BY public.solar_nam.id;


--
-- Name: solar_nam_prod; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_nam_prod (
    id integer NOT NULL,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: solar_nam_prod_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_nam_prod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_nam_prod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_nam_prod_id_seq OWNED BY public.solar_nam_prod.id;


--
-- Name: solar_o_vl; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_o_vl (
    id integer NOT NULL,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision
);


--
-- Name: solar_o_vl_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_o_vl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_o_vl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_o_vl_id_seq OWNED BY public.solar_o_vl.id;


--
-- Name: solar_o_vl_prod; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_o_vl_prod (
    id integer NOT NULL,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: solar_o_vl_prod_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_o_vl_prod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_o_vl_prod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_o_vl_prod_id_seq OWNED BY public.solar_o_vl_prod.id;


--
-- Name: solar_past; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_past (
    id integer NOT NULL,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: solar_past_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_past_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_past_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_past_id_seq OWNED BY public.solar_past.id;


--
-- Name: solar_vl_b; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_vl_b (
    id integer NOT NULL,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision
);


--
-- Name: solar_vl_b_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_vl_b_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_vl_b_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_vl_b_id_seq OWNED BY public.solar_vl_b.id;


--
-- Name: solar_vl_b_prod; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_vl_b_prod (
    id integer NOT NULL,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: solar_vl_b_prod_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_vl_b_prod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_vl_b_prod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_vl_b_prod_id_seq OWNED BY public.solar_vl_b_prod.id;


--
-- Name: solar_w_vl; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_w_vl (
    id integer NOT NULL,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision
);


--
-- Name: solar_w_vl_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_w_vl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_w_vl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_w_vl_id_seq OWNED BY public.solar_w_vl.id;


--
-- Name: solar_w_vl_prod; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solar_w_vl_prod (
    id integer NOT NULL,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: solar_w_vl_prod_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solar_w_vl_prod_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solar_w_vl_prod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solar_w_vl_prod_id_seq OWNED BY public.solar_w_vl_prod.id;


--
-- Name: waste; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.waste (
    id integer NOT NULL,
    ts timestamp with time zone,
    production double precision DEFAULT 0
);


--
-- Name: waste_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.waste_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: waste_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.waste_id_seq OWNED BY public.waste.id;


--
-- Name: wind; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.wind (
    id integer NOT NULL,
    bid integer,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision,
    day_ahead_confidence10 double precision,
    day_ahead_confidence90 double precision,
    most_recent_confidence10 double precision,
    most_recent_confidence90 double precision,
    week_ahead_confidence10 double precision,
    week_ahead_confidence90 double precision
);


--
-- Name: wind_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.wind_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wind_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.wind_id_seq OWNED BY public.wind.id;


--
-- Name: wind_offshore; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.wind_offshore (
    id integer NOT NULL,
    bid integer,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision,
    day_ahead_confidence10 double precision,
    day_ahead_confidence90 double precision,
    most_recent_confidence10 double precision,
    most_recent_confidence90 double precision,
    week_ahead_confidence10 double precision,
    week_ahead_confidence90 double precision
);


--
-- Name: wind_offshore_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.wind_offshore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wind_offshore_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.wind_offshore_id_seq OWNED BY public.wind_offshore.id;


--
-- Name: wind_onshore; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.wind_onshore (
    id integer NOT NULL,
    bid integer,
    day_ahead_forecast double precision,
    monitored_capacity double precision,
    most_recent_forecast double precision,
    ts timestamp with time zone,
    week_ahead_forecast double precision,
    day_ahead_confidence10 double precision,
    day_ahead_confidence90 double precision,
    most_recent_confidence10 double precision,
    most_recent_confidence90 double precision,
    week_ahead_confidence10 double precision,
    week_ahead_confidence90 double precision
);


--
-- Name: wind_onshore_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.wind_onshore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wind_onshore_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.wind_onshore_id_seq OWNED BY public.wind_onshore.id;


--
-- Name: wind_past; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.wind_past (
    id integer NOT NULL,
    bid integer,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: wind_past_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.wind_past_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wind_past_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.wind_past_id_seq OWNED BY public.wind_past.id;


--
-- Name: wind_past_offshore; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.wind_past_offshore (
    id integer NOT NULL,
    bid integer,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: wind_past_offshore_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.wind_past_offshore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wind_past_offshore_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.wind_past_offshore_id_seq OWNED BY public.wind_past_offshore.id;


--
-- Name: wind_past_onshore; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.wind_past_onshore (
    id integer NOT NULL,
    bid integer,
    load_factor double precision,
    monitored_capacity double precision,
    real_time double precision,
    ts timestamp with time zone
);


--
-- Name: wind_past_onshore_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.wind_past_onshore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wind_past_onshore_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.wind_past_onshore_id_seq OWNED BY public.wind_past_onshore.id;


--
-- Name: biomass id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.biomass ALTER COLUMN id SET DEFAULT nextval('public.biomass_id_seq'::regclass);


--
-- Name: corrections id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.corrections ALTER COLUMN id SET DEFAULT nextval('public.corrections_id_seq'::regclass);


--
-- Name: fetch_events id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fetch_events ALTER COLUMN id SET DEFAULT nextval('public.fetch_events_id_seq'::regclass);


--
-- Name: hydro id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hydro ALTER COLUMN id SET DEFAULT nextval('public.hydro_id_seq'::regclass);


--
-- Name: hydro_correction id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hydro_correction ALTER COLUMN id SET DEFAULT nextval('public.hydro_correction_id_seq'::regclass);


--
-- Name: load id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.load ALTER COLUMN id SET DEFAULT nextval('public.load_id_seq'::regclass);


--
-- Name: load_past id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.load_past ALTER COLUMN id SET DEFAULT nextval('public.load_past_id_seq'::regclass);


--
-- Name: solar id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar ALTER COLUMN id SET DEFAULT nextval('public.solar_id_seq'::regclass);


--
-- Name: solar_ant id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_ant ALTER COLUMN id SET DEFAULT nextval('public.solar_ant_id_seq'::regclass);


--
-- Name: solar_ant_prod id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_ant_prod ALTER COLUMN id SET DEFAULT nextval('public.solar_ant_prod_id_seq'::regclass);


--
-- Name: solar_br_w id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_br_w ALTER COLUMN id SET DEFAULT nextval('public.solar_br_w_id_seq'::regclass);


--
-- Name: solar_br_w_prod id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_br_w_prod ALTER COLUMN id SET DEFAULT nextval('public.solar_br_w_prod_id_seq'::regclass);


--
-- Name: solar_bxl id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_bxl ALTER COLUMN id SET DEFAULT nextval('public.solar_bxl_id_seq'::regclass);


--
-- Name: solar_bxl_prod id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_bxl_prod ALTER COLUMN id SET DEFAULT nextval('public.solar_bxl_prod_id_seq'::regclass);


--
-- Name: solar_hai id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_hai ALTER COLUMN id SET DEFAULT nextval('public.solar_hai_id_seq'::regclass);


--
-- Name: solar_hai_prod id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_hai_prod ALTER COLUMN id SET DEFAULT nextval('public.solar_hai_prod_id_seq'::regclass);


--
-- Name: solar_lie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_lie ALTER COLUMN id SET DEFAULT nextval('public.solar_lie_id_seq'::regclass);


--
-- Name: solar_lie_prod id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_lie_prod ALTER COLUMN id SET DEFAULT nextval('public.solar_lie_prod_id_seq'::regclass);


--
-- Name: solar_lim id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_lim ALTER COLUMN id SET DEFAULT nextval('public.solar_lim_id_seq'::regclass);


--
-- Name: solar_lim_prod id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_lim_prod ALTER COLUMN id SET DEFAULT nextval('public.solar_lim_prod_id_seq'::regclass);


--
-- Name: solar_lux id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_lux ALTER COLUMN id SET DEFAULT nextval('public.solar_lux_id_seq'::regclass);


--
-- Name: solar_lux_prod id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_lux_prod ALTER COLUMN id SET DEFAULT nextval('public.solar_lux_prod_id_seq'::regclass);


--
-- Name: solar_nam id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_nam ALTER COLUMN id SET DEFAULT nextval('public.solar_nam_id_seq'::regclass);


--
-- Name: solar_nam_prod id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_nam_prod ALTER COLUMN id SET DEFAULT nextval('public.solar_nam_prod_id_seq'::regclass);


--
-- Name: solar_o_vl id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_o_vl ALTER COLUMN id SET DEFAULT nextval('public.solar_o_vl_id_seq'::regclass);


--
-- Name: solar_o_vl_prod id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_o_vl_prod ALTER COLUMN id SET DEFAULT nextval('public.solar_o_vl_prod_id_seq'::regclass);


--
-- Name: solar_past id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_past ALTER COLUMN id SET DEFAULT nextval('public.solar_past_id_seq'::regclass);


--
-- Name: solar_vl_b id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_vl_b ALTER COLUMN id SET DEFAULT nextval('public.solar_vl_b_id_seq'::regclass);


--
-- Name: solar_vl_b_prod id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_vl_b_prod ALTER COLUMN id SET DEFAULT nextval('public.solar_vl_b_prod_id_seq'::regclass);


--
-- Name: solar_w_vl id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_w_vl ALTER COLUMN id SET DEFAULT nextval('public.solar_w_vl_id_seq'::regclass);


--
-- Name: solar_w_vl_prod id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_w_vl_prod ALTER COLUMN id SET DEFAULT nextval('public.solar_w_vl_prod_id_seq'::regclass);


--
-- Name: waste id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.waste ALTER COLUMN id SET DEFAULT nextval('public.waste_id_seq'::regclass);


--
-- Name: wind id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wind ALTER COLUMN id SET DEFAULT nextval('public.wind_id_seq'::regclass);


--
-- Name: wind_offshore id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wind_offshore ALTER COLUMN id SET DEFAULT nextval('public.wind_offshore_id_seq'::regclass);


--
-- Name: wind_onshore id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wind_onshore ALTER COLUMN id SET DEFAULT nextval('public.wind_onshore_id_seq'::regclass);


--
-- Name: wind_past id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wind_past ALTER COLUMN id SET DEFAULT nextval('public.wind_past_id_seq'::regclass);


--
-- Name: wind_past_offshore id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wind_past_offshore ALTER COLUMN id SET DEFAULT nextval('public.wind_past_offshore_id_seq'::regclass);


--
-- Name: wind_past_onshore id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wind_past_onshore ALTER COLUMN id SET DEFAULT nextval('public.wind_past_onshore_id_seq'::regclass);


--
-- Name: biomass biomass_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.biomass
    ADD CONSTRAINT biomass_pkey PRIMARY KEY (id);


--
-- Name: corrections corrections_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.corrections
    ADD CONSTRAINT corrections_pkey PRIMARY KEY (id);


--
-- Name: fetch_events fetch_events_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fetch_events
    ADD CONSTRAINT fetch_events_pkey PRIMARY KEY (id);


--
-- Name: hydro_correction hydro_correction_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hydro_correction
    ADD CONSTRAINT hydro_correction_pkey PRIMARY KEY (id);


--
-- Name: hydro hydro_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hydro
    ADD CONSTRAINT hydro_pkey PRIMARY KEY (id);


--
-- Name: load_past load_past_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.load_past
    ADD CONSTRAINT load_past_pkey PRIMARY KEY (id);


--
-- Name: load load_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.load
    ADD CONSTRAINT load_pkey PRIMARY KEY (id);


--
-- Name: solar_ant solar_ant_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_ant
    ADD CONSTRAINT solar_ant_pkey PRIMARY KEY (id);


--
-- Name: solar_ant_prod solar_ant_prod_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_ant_prod
    ADD CONSTRAINT solar_ant_prod_pkey PRIMARY KEY (id);


--
-- Name: solar_br_w solar_br_w_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_br_w
    ADD CONSTRAINT solar_br_w_pkey PRIMARY KEY (id);


--
-- Name: solar_br_w_prod solar_br_w_prod_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_br_w_prod
    ADD CONSTRAINT solar_br_w_prod_pkey PRIMARY KEY (id);


--
-- Name: solar_bxl solar_bxl_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_bxl
    ADD CONSTRAINT solar_bxl_pkey PRIMARY KEY (id);


--
-- Name: solar_bxl_prod solar_bxl_prod_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_bxl_prod
    ADD CONSTRAINT solar_bxl_prod_pkey PRIMARY KEY (id);


--
-- Name: solar_hai solar_hai_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_hai
    ADD CONSTRAINT solar_hai_pkey PRIMARY KEY (id);


--
-- Name: solar_hai_prod solar_hai_prod_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_hai_prod
    ADD CONSTRAINT solar_hai_prod_pkey PRIMARY KEY (id);


--
-- Name: solar_lie solar_lie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_lie
    ADD CONSTRAINT solar_lie_pkey PRIMARY KEY (id);


--
-- Name: solar_lie_prod solar_lie_prod_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_lie_prod
    ADD CONSTRAINT solar_lie_prod_pkey PRIMARY KEY (id);


--
-- Name: solar_lim solar_lim_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_lim
    ADD CONSTRAINT solar_lim_pkey PRIMARY KEY (id);


--
-- Name: solar_lim_prod solar_lim_prod_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_lim_prod
    ADD CONSTRAINT solar_lim_prod_pkey PRIMARY KEY (id);


--
-- Name: solar_lux solar_lux_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_lux
    ADD CONSTRAINT solar_lux_pkey PRIMARY KEY (id);


--
-- Name: solar_lux_prod solar_lux_prod_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_lux_prod
    ADD CONSTRAINT solar_lux_prod_pkey PRIMARY KEY (id);


--
-- Name: solar_nam solar_nam_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_nam
    ADD CONSTRAINT solar_nam_pkey PRIMARY KEY (id);


--
-- Name: solar_nam_prod solar_nam_prod_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_nam_prod
    ADD CONSTRAINT solar_nam_prod_pkey PRIMARY KEY (id);


--
-- Name: solar_o_vl solar_o_vl_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_o_vl
    ADD CONSTRAINT solar_o_vl_pkey PRIMARY KEY (id);


--
-- Name: solar_o_vl_prod solar_o_vl_prod_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_o_vl_prod
    ADD CONSTRAINT solar_o_vl_prod_pkey PRIMARY KEY (id);


--
-- Name: solar_past solar_past_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_past
    ADD CONSTRAINT solar_past_pkey PRIMARY KEY (id);


--
-- Name: solar solar_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar
    ADD CONSTRAINT solar_pkey PRIMARY KEY (id);


--
-- Name: solar_vl_b solar_vl_b_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_vl_b
    ADD CONSTRAINT solar_vl_b_pkey PRIMARY KEY (id);


--
-- Name: solar_vl_b_prod solar_vl_b_prod_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_vl_b_prod
    ADD CONSTRAINT solar_vl_b_prod_pkey PRIMARY KEY (id);


--
-- Name: solar_w_vl solar_w_vl_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_w_vl
    ADD CONSTRAINT solar_w_vl_pkey PRIMARY KEY (id);


--
-- Name: solar_w_vl_prod solar_w_vl_prod_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solar_w_vl_prod
    ADD CONSTRAINT solar_w_vl_prod_pkey PRIMARY KEY (id);


--
-- Name: waste waste_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.waste
    ADD CONSTRAINT waste_pkey PRIMARY KEY (id);


--
-- Name: wind_offshore wind_offshore_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wind_offshore
    ADD CONSTRAINT wind_offshore_pkey PRIMARY KEY (id);


--
-- Name: wind_onshore wind_onshore_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wind_onshore
    ADD CONSTRAINT wind_onshore_pkey PRIMARY KEY (id);


--
-- Name: wind_past_offshore wind_past_offshore_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wind_past_offshore
    ADD CONSTRAINT wind_past_offshore_pkey PRIMARY KEY (id);


--
-- Name: wind_past_onshore wind_past_onshore_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wind_past_onshore
    ADD CONSTRAINT wind_past_onshore_pkey PRIMARY KEY (id);


--
-- Name: wind_past wind_past_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wind_past
    ADD CONSTRAINT wind_past_pkey PRIMARY KEY (id);


--
-- Name: wind wind_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wind
    ADD CONSTRAINT wind_pkey PRIMARY KEY (id);


--
-- Name: biomass_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX biomass_ts_index ON public.biomass USING btree (ts);


--
-- Name: corrections_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX corrections_ts_index ON public.corrections USING btree (ts);


--
-- Name: fetch_events_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fetch_events_ts_index ON public.fetch_events USING btree (ts);


--
-- Name: solar_ant_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX solar_ant_ts_index ON public.solar_ant USING btree (ts);


--
-- Name: solar_br_w_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX solar_br_w_ts_index ON public.solar_br_w USING btree (ts);


--
-- Name: solar_bxl_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX solar_bxl_ts_index ON public.solar_bxl USING btree (ts);


--
-- Name: solar_hai_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX solar_hai_ts_index ON public.solar_hai USING btree (ts);


--
-- Name: solar_lie_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX solar_lie_ts_index ON public.solar_lie USING btree (ts);


--
-- Name: solar_lim_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX solar_lim_ts_index ON public.solar_lim USING btree (ts);


--
-- Name: solar_lux_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX solar_lux_ts_index ON public.solar_lux USING btree (ts);


--
-- Name: solar_nam_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX solar_nam_ts_index ON public.solar_nam USING btree (ts);


--
-- Name: solar_o_vl_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX solar_o_vl_ts_index ON public.solar_o_vl USING btree (ts);


--
-- Name: solar_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX solar_ts_index ON public.solar USING btree (ts);


--
-- Name: solar_vl_b_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX solar_vl_b_ts_index ON public.solar_vl_b USING btree (ts);


--
-- Name: solar_w_vl_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX solar_w_vl_ts_index ON public.solar_w_vl USING btree (ts);


--
-- Name: waste_ts_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX waste_ts_index ON public.waste USING btree (ts);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: -
--

GRANT USAGE ON SCHEMA public TO energiecommune;


--
-- Name: TABLE biomass; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.biomass TO energiecommune;


--
-- Name: TABLE corrections; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.corrections TO energiecommune;


--
-- Name: TABLE fetch_events; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.fetch_events TO energiecommune;


--
-- Name: TABLE hydro; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.hydro TO energiecommune;


--
-- Name: TABLE hydro_correction; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.hydro_correction TO energiecommune;


--
-- Name: TABLE load; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.load TO energiecommune;


--
-- Name: TABLE load_past; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.load_past TO energiecommune;


--
-- Name: TABLE solar; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar TO energiecommune;


--
-- Name: TABLE solar_ant; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_ant TO energiecommune;


--
-- Name: TABLE solar_ant_prod; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_ant_prod TO energiecommune;


--
-- Name: TABLE solar_br_w; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_br_w TO energiecommune;


--
-- Name: TABLE solar_br_w_prod; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_br_w_prod TO energiecommune;


--
-- Name: TABLE solar_bxl; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_bxl TO energiecommune;


--
-- Name: TABLE solar_bxl_prod; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_bxl_prod TO energiecommune;


--
-- Name: TABLE solar_hai; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_hai TO energiecommune;


--
-- Name: TABLE solar_hai_prod; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_hai_prod TO energiecommune;


--
-- Name: TABLE solar_lie; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_lie TO energiecommune;


--
-- Name: TABLE solar_lie_prod; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_lie_prod TO energiecommune;


--
-- Name: TABLE solar_lim; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_lim TO energiecommune;


--
-- Name: TABLE solar_lim_prod; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_lim_prod TO energiecommune;


--
-- Name: TABLE solar_lux; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_lux TO energiecommune;


--
-- Name: TABLE solar_lux_prod; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_lux_prod TO energiecommune;


--
-- Name: TABLE solar_nam; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_nam TO energiecommune;


--
-- Name: TABLE solar_nam_prod; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_nam_prod TO energiecommune;


--
-- Name: TABLE solar_o_vl; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_o_vl TO energiecommune;


--
-- Name: TABLE solar_o_vl_prod; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_o_vl_prod TO energiecommune;


--
-- Name: TABLE solar_past; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_past TO energiecommune;


--
-- Name: TABLE solar_vl_b; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_vl_b TO energiecommune;


--
-- Name: TABLE solar_vl_b_prod; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_vl_b_prod TO energiecommune;


--
-- Name: TABLE solar_w_vl; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_w_vl TO energiecommune;


--
-- Name: TABLE solar_w_vl_prod; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.solar_w_vl_prod TO energiecommune;


--
-- Name: TABLE waste; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.waste TO energiecommune;


--
-- Name: TABLE wind; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.wind TO energiecommune;


--
-- Name: TABLE wind_offshore; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.wind_offshore TO energiecommune;


--
-- Name: TABLE wind_onshore; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.wind_onshore TO energiecommune;


--
-- Name: TABLE wind_past; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.wind_past TO energiecommune;


--
-- Name: TABLE wind_past_offshore; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.wind_past_offshore TO energiecommune;


--
-- Name: TABLE wind_past_onshore; Type: ACL; Schema: public; Owner: -
--

GRANT ALL ON TABLE public.wind_past_onshore TO energiecommune;


--
-- PostgreSQL database dump complete
--

