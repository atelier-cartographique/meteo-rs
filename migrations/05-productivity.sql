--
-- Name: solar_daily_past; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE public.solar_daily_past (
-- id integer NOT NULL,
date date,
production_obs_kwh double precision,
productivite_kwh_by_kwc double precision
);
CREATE UNIQUE INDEX CONCURRENTLY IF NOT EXISTS solar_daily_past_idx ON solar_daily_past (date);
BEGIN;
-- Insert from Energie Commune data (csv)
COPY solar_daily_past(
    date,
    production_obs_kwh,
    productivite_kwh_by_kwc
)
FROM '/prodPVSolar.csv' DELIMITER ',' CSV HEADER;
-- Insert from actual Elia datas (in solar_past)
INSERT INTO solar_daily_past (
        date,
        production_obs_kwh,
        productivite_kwh_by_kwc
    )
SELECT ts::timestamp::date as date,
    sum(most_recent_forecast / 4000) as production_obs_kwh,
    sum(most_recent_forecast / monitored_capacity) / 4 as productivite_kwh_by_kwc
FROM solar
WHERE ts >= '2021-01-01 02:00:00+02'
GROUP BY date;
COMMIT;