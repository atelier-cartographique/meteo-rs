CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    username text not null unique,
    password text not null
);