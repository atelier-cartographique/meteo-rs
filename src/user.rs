use model_wrapper::Wrapper0;
use pbkdf2::{
    password_hash::{rand_core::OsRng, PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Pbkdf2,
};
use tokio::runtime::Runtime;

use crate::{
    model::{params, FromRow, ModelError, ToParams, WithSQL},
    store::{Store, StoreError},
};

#[derive(Wrapper0, Clone, Debug)]
pub struct UserRecord {
    pub id: i32,
    pub username: String,
    pub password: String,
}

fn from_row(row: &tokio_postgres::Row) -> Result<UserRecord, crate::model::ModelError> {
    Ok(UserRecord {
        id: row
            .try_get("id")
            .map_err(|err| ModelError::Wrapped(format!("id: {err}")))?,
        username: row
            .try_get("username")
            .map_err(|err| ModelError::Wrapped(format!("username: {err}")))?,
        password: row
            .try_get("password")
            .map_err(|err| ModelError::Wrapped(format!("password: {err}")))?,
    })
}

// GET

#[derive(Wrapper0, Clone, Debug)]
pub struct UserGet(UserRecord);

impl WithSQL for UserGet {
    fn sql() -> &'static str {
        "
        SELECT id, username, password
        FROM users
        WHERE id = $1;
        "
    }
}
impl FromRow for UserGet {
    fn from_row(row: &tokio_postgres::Row) -> Result<Self, crate::model::ModelError>
    where
        Self: Sized,
    {
        from_row(row).map(Self)
    }
}

// FIND

#[derive(Wrapper0, Clone, Debug)]
pub struct UserFind(UserRecord);

impl WithSQL for UserFind {
    fn sql() -> &'static str {
        "
        SELECT id, username, password
        FROM users
        WHERE username = $1;
        "
    }
}

impl FromRow for UserFind {
    fn from_row(row: &tokio_postgres::Row) -> Result<Self, crate::model::ModelError>
    where
        Self: Sized,
    {
        from_row(row).map(Self)
    }
}

// CREATE

#[derive(Wrapper0, Clone, Debug)]
pub struct UserCreate(UserRecord);

impl WithSQL for UserCreate {
    fn sql() -> &'static str {
        "INSERT INTO  users (username, password) VALUES ($1, $2) RETURNING *; "
    }
}

impl FromRow for UserCreate {
    fn from_row(row: &tokio_postgres::Row) -> Result<Self, crate::model::ModelError>
    where
        Self: Sized,
    {
        from_row(row).map(Self)
    }
}

impl ToParams for UserCreate {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        (params() + self.0.username() + self.0.password()).inner()
    }
}

// UPDATE PASSWORD

#[derive(Wrapper0, Clone, Debug)]
pub struct UserChange(UserRecord);

impl WithSQL for UserChange {
    fn sql() -> &'static str {
        "UPDATE  users SET password = $2 WHERE id = $1; "
    }
}

impl FromRow for UserChange {
    fn from_row(row: &tokio_postgres::Row) -> Result<Self, crate::model::ModelError>
    where
        Self: Sized,
    {
        from_row(row).map(Self)
    }
}

impl ToParams for UserChange {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        (params() + self.0.id() + self.0.password()).inner()
    }
}

impl Store {
    pub async fn get_user(&self, id: i32) -> Result<UserGet, StoreError> {
        self.fetch_one(&[&id]).await
    }

    pub async fn find_user(&self, username: &str) -> Result<UserFind, StoreError> {
        self.fetch_one(&[&username]).await
    }

    pub async fn create_user(
        &self,
        username: &str,
        password: &str,
    ) -> Result<UserCreate, StoreError> {
        self.create(&[&username, &password]).await
    }

    pub async fn change_user(&self, model: &UserChange) -> Result<u64, StoreError> {
        self.save(model).await
    }
}

fn encode_password(password: &str) -> Result<String, String> {
    let salt = SaltString::generate(&mut OsRng);
    let password_hash = Pbkdf2
        .hash_password(password.as_bytes(), &salt)
        .map_err(|err| format!("[HashError] {err}"))?;
    Ok(password_hash.to_string())
}

pub fn check_password(password: &str, hash: &str) -> bool {
    if let Ok(parsed_hash) = PasswordHash::new(hash) {
        Pbkdf2
            .verify_password(password.as_bytes(), &parsed_hash)
            .is_ok()
    } else {
        false
    }
}

async fn make_user_inner(dsn: &str, username: &str, password: &str) {
    match Store::new(dsn).await {
        Err(err) => println!("oops {:?}", err),

        Ok(store) => {
            if let Ok(hash) = encode_password(password) {
                if let Ok(user) = store.find_user(username).await {
                    let record = user.value();
                    let change = UserChange::new(UserRecord {
                        id: *record.id(),
                        username: record.username().clone(),
                        password: hash,
                    });
                    match store.save(&change).await {
                        Err(err) => println!("OOPS {err}"),
                        Ok(_) => println!("OK"),
                    }
                } else {
                    match store.create_user(username, &hash).await {
                        Err(err) => println!("OOPS {err}"),
                        Ok(_) => println!("OK"),
                    }
                }
            } else {
                println!("OOPS hash did not work")
            }
        }
    }
}

pub fn make_user(dsn: &str, username: &str, password: &str) {
    let runtime = Runtime::new().expect("Failed to build a Tokio runtime, that's bad");
    runtime.block_on(make_user_inner(dsn, username, password));
}
