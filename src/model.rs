// use sqlx::{Row, query};

use std::ops::Add;

use tokio_postgres::{types::ToSql, Row};

use crate::store::StoreError;

#[derive(Debug)]
pub enum ModelError {
    Unknown,
    Wrapped(String),
    // Convert(String),
}

impl From<tokio_postgres::Error> for ModelError {
    fn from(err: tokio_postgres::Error) -> Self {
        ModelError::Wrapped(format!("[tokio_postgres::Error] {err}"))
    }
}

impl From<StoreError> for ModelError {
    fn from(err: StoreError) -> Self {
        ModelError::Wrapped(format!("[StoreError] {err}"))
    }
}

pub trait WithSQL {
    fn sql() -> &'static str;
}

pub trait FromRow: WithSQL {
    fn from_row(row: &Row) -> Result<Self, ModelError>
    where
        Self: Sized;
}
pub trait ToParams: WithSQL {
    // fn params(&self) -> Box<[&(dyn ToSql + Sync)]>;
    fn params<'a>(&'a self) -> Vec<Box<dyn ToSql + Sync + Send + 'a>>;
}

// macro_rules! model {
//     ($Model:ty, $sql:expr, $row_ident:ident, $from_row:block) => {
//         impl crate::model::WithSQL for $Model {
//             fn sql() -> &'static str {
//                 $sql
//             }
//         }
//         impl crate::model::FromRow for $Model {
//             fn from_row(
//                 $row_ident: &tokio_postgres::Row,
//             ) -> Result<Self, crate::model::ModelError> {
//                 $from_row
//             }
//         }
//     };
// }
// pub use model;

pub struct Params<'a> {
    inner: Vec<Box<dyn ToSql + Sync + Send + 'a>>,
}

impl<'a> Params<'a> {
    pub fn new() -> Params<'a> {
        Params { inner: Vec::new() }
    }

    pub fn inner(self) -> Vec<Box<dyn ToSql + Sync + Send + 'a>> {
        self.inner
    }

    pub fn push<Rhs>(mut self, other: Rhs) -> Self
    where
        Rhs: ToSql + Sync + Send + 'a,
    {
        self.inner.push(Box::new(other));
        Self { inner: self.inner }
    }
}

pub fn params<'a>() -> Params<'a> {
    Params::new()
}

impl<'a, Rhs> Add<Rhs> for Params<'a>
where
    Rhs: ToSql + Sync + Send + 'a,
{
    type Output = Self;

    fn add(self, other: Rhs) -> Self {
        // self.inner.push(Box::new(other));
        // Self { inner: self.inner }
        self.push(other)
    }
}

// #[cfg(test)]
// mod test {
//     use crate::model::*;
//     #[test]
//     fn model_write() {
//         struct Test {
//             phrase: String,
//             number: i32,
//         }

//         impl WithSQL for Test {
//             fn sql() -> &'static str {
//                 "Yopla"
//             }
//         }

//         impl ToParams for Test {
//             fn params(&self) -> Vec<Box<dyn ToSql + Sync>> {
//                 (params() + &self.phrase + &self.number).inner()
//             }
//         }

//         let store = tokio_test::block_on(crate::store::Store::new("test")).unwrap();

//         let model = Test {
//             phrase: String::from("Test"),
//             number: 42,
//         };

//         let res = tokio_test::block_on(store.save(&model)).unwrap();

//         ()
//     }
// }
