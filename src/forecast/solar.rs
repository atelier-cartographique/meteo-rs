use chrono::{DateTime, Datelike, Utc};
use std::f64;
use warp::{Rejection, Reply};

use crate::{
    forecast::jauges::solar_jauge_svg,
    html::*,
    map::solar_map_legend,
    render::{render_error, render_item},
    store::{ArcStore, Region},
    util::{format_datetime, format_family_nb, get_last_del_date},
};

pub async fn image_legend() -> Result<impl Reply, Rejection> {
    Ok(solar_map_legend())
}

fn render_solar_values(
    _forecast: f64,
    family_nb: f64,
    _mon_cap: f64,
    _nb_syst: f64,
    tot_log: f64,
) -> Element {
    div(vec![render_item(
        &format_family_nb(family_nb),
        "équivalent-logements",
        &format!(" ({}%)", (family_nb / tot_log * 100.0).round()),
    )])
    .class("wrapper__values")
}
pub fn render_solar_jauge_succes(solar_loadfactor: f64) -> Element {
    div(Node::from(solar_jauge_svg(solar_loadfactor))).class("jauge")
}
async fn render_solar_map(tomorrow: &DateTime<Utc>) -> Element {
    //TODO: adapt for 3 and 7 days
    div(vec![img()
        .set(
            "src",
            format!(
                "/image/{}/{}/{}/{}",
                tomorrow.date().year(),
                tomorrow.date().month(),
                tomorrow.date().day(),
                13
            ),
        )
        .class("map")])
    .class("map-wrapper")
}

pub async fn render_solar_forecast(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    // date_title: &str,
) -> Result<Element, Rejection> {
    let fnb = store
        .solar_familly_nb_forecast(start, end, Region::Belgique)
        .await;

    let sf = store.solar_forecast(start, end, Region::Belgique).await;
    let mc = store.solar_monitored_capacity_forecast(start, end).await;
    let mc_corr = store.solar_capacity_correction(end).await;
    let nb_syst = store.solar_nb_syst_pv(end).await;
    let fam_tot = store.tot_housing().await;

    let nb_days = (*end - *start).num_days();

    let solar_values = match (sf, fnb, mc, fam_tot) {
        (Ok(sf), Ok(fnb), Ok(mc), Ok(tot)) => render_solar_values(
            sf.value() / nb_days as f64,
            // fnb / nb_days as f64,
            fnb.value() / nb_days as f64,
            mc.value().max(mc_corr.map(|r| r.value()).unwrap_or(0.0)),
            nb_syst.map(|r| r.value()).unwrap_or(0.0),
            tot.value(),
        ),
        (_, _, _, _) => render_error("Solar forecast values are missing"),
    };

    //take avg of loadfactors between 13h and 14h (see SQL), for all days between start and end
    let solar_jauge = match store
        .solar_loadfactor_forecast(start, end, Region::Belgique)
        .await
    {
        Ok(lf) => render_solar_jauge_succes(lf.value()),
        Err(_) => render_error("Solar loadfactors are missing for the selected dates"),
    };

    let jauge_title = div("Taux de production").class("main__title");

    let solar_info =
        div(vec![jauge_title.append(solar_jauge), solar_values]).class("wrapper__info");

    let solar_map =
        div(vec![solar_info, render_solar_map(start).await]).class("wrapper wrapper--forecast");

    let solar_update_date = get_last_del_date(store, "InsertSolar").await;

    let render_solar_update = match solar_update_date {
        Ok(d) => div(format!(
            "Données photovoltaiques mises à jour le {}",
            format_datetime(&d)
        ))
        .class("update-info"),
        Err(_) => div(""),
    };

    // let energy_type = EnergyType::Solar;
    // let mut links = render_days_links(nb_days, &energy_type);
    // if start.date() == Utc::now().date() {
    //     links = render_days_links(0, &energy_type)
    // }

    Ok(div([solar_map, render_solar_update]).class("content--body"))
}

// pub async fn render_solar_map(
//     store: &ArcStore,
//     start: &DateTime<Utc>,
//     end: &DateTime<Utc>,
// ) -> Result<Element, Rejection> {
//     let solar_update_date = get_last_del_date(store, "DeleteSolar").await;

//     let render_solar_update = match solar_update_date {
//         Ok(d) => div(format!(
//             "Données photovoltaiques mises à jour le {}",
//             format_datetime(&d)
//         ))
//         .class("update-info"),
//         Err(_) => div(""),
//     };

//     Ok(div(vec![
//         div("Électricité photovoltaïque").class("main__title"),
//         render_solar_forecast(store, start, end, nb_days).await,
//         render_solar_update,
//     ])
//     .class("content--body"))
// }
