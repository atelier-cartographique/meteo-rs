use chrono::{DateTime, NaiveDate};
use chrono::{Datelike, Duration, Utc};
use std::f64;
use svg::{node::element::SVG, Document};
use warp::Rejection;

use crate::{
    forecast::jauges::wind_jauge_svg,
    html::*,
    models::wind_indicators::{WindOffshoreCapacityCorrection, WindOnshoreCapacityCorrection},
    plot::linechart::wind_linechart,
    render::{render_error, render_item, WindData},
    store::{ArcStore, Offshore},
    util::{begin_of_day, format_datetime, format_family_nb, get_last_del_date},
};

struct WindValues {
    pub offshore: f64,
    pub onshore: f64,
    pub _nb_offshore: f64,
    pub _nb_onshore: f64,
    pub _monitored_cap: f64,
    pub family_nb: f64,
    pub family_percent: f64,
}

fn render_wind_values(wv: WindValues) -> Element {
    let _total_forecast = wv.offshore + wv.onshore;

    div(vec![
        // render_item(
        //     "Production totale",
        //     "",
        //     &format!("{} MWh", format_nb(total_forecast)),
        // ),
        // render_item(
        //     "Puissance installée",
        //     "",
        //     &format!("{} MW", format_nb(wv.monitored_cap)),
        // ),
        // render_item(
        //     "Production onshore",
        //     &format!("\n ({} éoliennes)", wv.nb_onshore),
        //     &format!("{} MWh", format_nb(wv.onshore)),
        // ),
        // render_item(
        //     "Production offshore",
        //     &format!("\n ({} éoliennes)", wv.nb_offshore),
        //     &format!("{} MWh", format_nb(wv.offshore)),
        // ),
        render_item(
            &format!(
                "{} ({}%)",
                format_family_nb(wv.family_nb),
                wv.family_percent.round()
            ),
            "équivalent-logements",
            "",
        ),
    ])
    .class("wrapper__values")
}

pub fn render_wind_jauge(loadfactor: f64) -> Element {
    div(Node::from(wind_jauge_svg(loadfactor))).class("jauge")
}

pub async fn wind_linechart_svg(
    year: i32,
    month: u32,
    day: u32,
    nb_days: u32,
    store: &ArcStore,
) -> Document {
    let start_n = NaiveDate::from_ymd(year, month, day);
    let start_day = DateTime::from_utc(start_n.and_hms(0, 0, 0), Utc);
    let end_day = start_day + Duration::days(nb_days.into());
    let start = &begin_of_day(&start_day);
    let end = &begin_of_day(&end_day);
    let off_data = store.wind_forecast_list(start, end, Offshore::True).await;

    let on_data = store.wind_forecast_list(start, end, Offshore::False).await;

    let off_mc = store
        .wind_monitored_capacity_forecast(start, end, Offshore::True)
        .await;
    let on_mc = store
        .wind_monitored_capacity_forecast(start, end, Offshore::False)
        .await;
    let off_correction = store
        .wind_offshore_capacity_correction(start)
        .await
        .unwrap_or(WindOffshoreCapacityCorrection::new(0.0));
    let on_correction = store
        .wind_onshore_capacity_correction(start)
        .await
        .unwrap_or(WindOnshoreCapacityCorrection::new(0.0));

    match (off_data, on_data, off_mc, on_mc) {
        (Ok(off), Ok(on), Ok(offmc), Ok(onmc)) => {
            let offshore_data_corrected = off
                .iter()
                .map(|item| {
                    (
                        *item.ts(),
                        item.value() / offmc.value() * offmc.value().max(off_correction.value()),
                    )
                })
                .collect::<Vec<_>>();
            let onshore_data_corrected = on
                .iter()
                .map(|item| {
                    (
                        *item.ts(),
                        item.value() / onmc.value() * onmc.value().max(on_correction.value()),
                    )
                })
                .collect::<Vec<_>>();
            let wd = WindData {
                offshore: offshore_data_corrected,
                onshore: onshore_data_corrected,
                monitored_capacity: Some(offmc.value() + onmc.value()),
            };
            wind_linechart(700., 300., &wd.offshore, &wd.onshore)
        }
        (_, _, _, _) => SVG::new(),
    }
}
async fn max_wind_mon_cap(store: &ArcStore, start: &DateTime<Utc>, mon_cap: f64) -> f64 {
    let mc_off_corr = store
        .wind_offshore_capacity_correction(start)
        .await
        .unwrap_or(WindOffshoreCapacityCorrection::new(0.0));
    let mc_on_corr = store
        .wind_onshore_capacity_correction(start)
        .await
        .unwrap_or(WindOnshoreCapacityCorrection::new(0.0));
    mon_cap.max(mc_off_corr.value() + mc_on_corr.value())
}

async fn render_wind_forecast(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    // date_title: &str,
) -> Element {
    let off_val = store.wind_forecast(start, end, Offshore::True).await;
    let on_val = store.wind_forecast(start, end, Offshore::False).await;
    let mon_cap = store
        .wind_monitored_capacity_forecast(start, end, Offshore::None)
        .await;
    let fam_nb = store
        .wind_familly_nb_forecast(start, end, Offshore::None)
        .await;
    let fam_tot = store.tot_housing().await;

    let nb_on = store.nb_onshore(end).await;
    let nb_off = store.nb_offshore(end).await;

    let nb_days = (*end - *start).num_days();

    let values = match (off_val, on_val, nb_on, nb_off, mon_cap, fam_nb, fam_tot) {
        (Ok(offshore), Ok(onshore), Ok(nb_onshore), Ok(nb_offshore), Ok(mc), Ok(fnb), Ok(tot)) => {
            // let fnb = fam_nb / nb_days as f64;
            let wv = WindValues {
                //nb days to get the average by day
                onshore: onshore.value() / nb_days as f64,
                offshore: offshore.value() / nb_days as f64,
                _nb_onshore: nb_onshore.value(),
                _nb_offshore: nb_offshore.value(),
                _monitored_cap: max_wind_mon_cap(store, start, mc.value()).await,
                family_nb: fnb,
                family_percent: fnb / tot.value() * 100.,
            };
            render_wind_values(wv)
        }
        (_, _, _, _, _, _, _) => render_error("Wind values are missing"),
    };

    let jauge = match store
        .wind_loadfactor_forecast(start, end, Offshore::None)
        .await
    {
        Ok(lf) => render_wind_jauge(lf.value().round()),
        Err(_) => render_error("Wind loadfactor is missing"),
    };
    let jauge_title = div("Taux de production").class("main__title");

    let wind_info = div(vec![jauge_title.append(jauge), values]).class("wrapper__info");
    div(vec![
        wind_info,
        div(Node::from(
            wind_linechart_svg(
                start.date().year(),
                start.date().month(),
                start.date().day(),
                nb_days as u32,
                store,
            )
            .await,
        ))
        .class("wrapper__graph"),
    ])
    .class("wrapper wrapper--forecast")
}

pub async fn render_wind_linechart(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
) -> Result<Element, Rejection> {
    let wind_update_date = get_last_del_date(store, "DeleteWindForecast").await;

    let render_wind_update = match wind_update_date {
        Ok(d) => div(format!(
            "Données éoliennes mises à jour le {}",
            format_datetime(&d)
        ))
        .class("update-info"),
        Err(_) => div(""),
    };
    Ok(div([
        render_wind_forecast(store, start, end).await,
        render_wind_update,
    ])
    .class("content--body"))
}
