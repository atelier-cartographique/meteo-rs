use chrono::{Duration, Utc};
use std::{f64, path::Path};
use svg::{
    node::{
        element::Image as SvgImage,
        element::{Group, Rectangle, Text},
        Text as TextNode,
    },
    Document,
};
use warp::Rejection;

use crate::{
    forecast::solar::render_solar_jauge_succes,
    forecast::wind::render_wind_jauge,
    html::{anchor, body, div, head, html, link, meta, span, Element},
    plot::base::{SOLAR_COLOR, SOLAR_EMPTY_COLOR, TEXT_COLOR, WIND_COLOR, WIND_EMPTY_COLOR},
    raster::dynimg_to_svgimg,
    render::{render_error, EnergyType, MeteoImage, FONT_REGULAR},
    render_forecast::ForecastQuery,
    store::{ArcStore, Offshore, Region},
    util::begin_of_day,
};

// const SOLAR_ICONE: &'static str = include_str!("../medias/sun.svg");
// const WIND_ICONE: &'static str = include_str!("../medias/wind.svg");

fn solar_icone() -> SvgImage {
    let icone = image::open(Path::new("medias/sun.png")).unwrap();
    dynimg_to_svgimg(MeteoImage(icone))
        .set("width", 12)
        .set("height", 12)
        .set("x", 5)
        .set("y", 1)
}
fn wind_icone() -> SvgImage {
    let icone = image::open(Path::new("medias/wind.png")).unwrap();
    dynimg_to_svgimg(MeteoImage(icone))
        .set("width", 12)
        .set("height", 12)
        .set("x", 5)
        .set("y", 1)
}

fn jauge_svg(strong_color: &str, weak_color: &str, factor: f64) -> Document {
    let rect_width = 50;
    let rect_height = 10;
    // let icone_path = Path::new()
    //     .set("stroke", TEXT_COLOR)
    //     .set("fill", TEXT_COLOR)
    //     .set("x", 0)
    //     .set("y", 10)
    //     .set("d", icone);
    let text = Text::new()
        .add(TextNode::new(format!("{}%", factor)))
        .set("x", rect_width / 2 - 3)
        .set("y", 12)
        .set("text-anchor", "begin")
        .set("font-size", "1em")
        .set("fill", TEXT_COLOR)
        .set("font-family", FONT_REGULAR);

    let dist = rect_height + 1;
    let mut g = Group::new();
    let nb_filled = (factor / 10.).round() as usize;
    let x = 0;
    let mut y = 15;
    // let mut y = 0;
    for _i in 0..(10 - nb_filled) {
        let rect = Rectangle::new()
            .set("x", x)
            .set("y", y)
            .set("rx", 2)
            .set("width", rect_width)
            .set("height", rect_height)
            .set("fill", weak_color);
        g = g.clone().add(rect);
        y += dist;
    }
    for _i in 0..nb_filled {
        let rect = Rectangle::new()
            .set("x", x)
            .set("y", y)
            .set("rx", 2)
            .set("width", rect_width)
            .set("height", rect_height)
            .set("fill", strong_color);
        y += dist;
        g = g.clone().add(rect);
    }

    Document::new()
        .set("viewBox", (0, 0, rect_width, dist * 10 + 15))
        // .set("viewBox", (0, 0, rect_width, dist * 10))
        .set("xmlns:xlink", "http://www.w3.org/1999/xlink")
        .add(g)
        .add(text)
}

pub fn solar_jauge_svg(loadfactor: f64) -> Document {
    // let svg = svg::read(SOLAR_ICONE.as_bytes()).unwrap_or(svg::new());
    jauge_svg(SOLAR_COLOR, SOLAR_EMPTY_COLOR, loadfactor).add(solar_icone())
}

pub fn wind_jauge_svg(loadfactor: f64) -> Document {
    jauge_svg(WIND_COLOR, WIND_EMPTY_COLOR, loadfactor).add(wind_icone())
}

fn make_document(elem: Element) -> Element {
    html([
        head([
            meta().set("charset", "UTF-8"),
            meta().set("main__title", "Taux de production"),
            link()
                .set("rel", "stylesheet")
                .set("type", "text/css")
                .set("href", format!("/static/style/forecast.css")),
        ]),
        // div(title).class("sub__title"),
        body(elem).class("content--body"),
        // div(vec![
        //     span("Energie Commune - "),
        //     anchor("www.energiecommune.be")
        //         .set("href", "https://energiecommune.be")
        //         .class("page-link"),
        //     span(" - "),
        //     anchor("info@energiecommune.be")
        //         .set("href", "mailto:info@energiecommune.be")
        //         .class("page-link"),
        // ])
        // .class("footer footer--prod"),
        div(vec![
            span("Valeurs calculées à partir de données de "),
            anchor("Elia").set("href", "https://www.elia.be/"),
            span(", de "),
            anchor("ENTSOe").set("href", "https://transparency.entsoe.eu/"),
            span(" et de "),
            anchor("Energie Commune.").set("href", "https://energiecommune.be"),
            span("Taux de production solaire calculé à 13h. Taux de production éolien journalier."),
        ])
        .class("source"),
    ])
}

pub async fn render_jauges_document(
    store: &ArcStore,
    nb_days: i32,
    energy_type: EnergyType,
) -> Element {
    let first_day = Utc::now();
    let last_day = first_day + Duration::days(nb_days.into());
    let start = &begin_of_day(&first_day);
    let end = &begin_of_day(&last_day);

    let wind = match store
        .wind_loadfactor_forecast(start, end, Offshore::None)
        .await
    {
        Ok(lf) => render_wind_jauge(lf.value().round()),
        Err(_) => render_error("Wind loadfactor is missing"),
    };

    //take avg of loadfactors between 13h and 14h (see SQL), for all days between start and end
    let solar = match store
        .solar_loadfactor_forecast(start, end, Region::Belgique)
        .await
    {
        Ok(lf) => render_solar_jauge_succes(lf.value()),
        Err(_) => render_error("Solar loadfactors are missing for the selected dates"),
    };

    // let title = Text::new()
    //     .add(TextNode::new("Taux de production du jour"))
    //     .set("x", 0)
    //     .set("y", 25)
    //     .set("text-anchor", "begin")
    //     .set("classname", "jauge-title")
    //     .set("fill", TEXT_COLOR)
    //     .set("font-family", FONT_REGULAR);
    let title = div("Taux de production du jour").class("main__title");

    match energy_type {
        EnergyType::All => {
            let jauges = div(vec![wind, solar]).class("wrapper__jauge");
            make_document(div(vec![title, jauges]))
        }
        EnergyType::Solar => make_document(div(solar).class("wrapper__jauge")),
        EnergyType::Wind => make_document(div(wind).class("wrapper__jauge")),
    }
}

pub async fn jauges_reply(store: ArcStore, q: ForecastQuery) -> Result<Element, Rejection> {
    Ok(render_jauges_document(&store, q.nb_days, q.energy_type).await)
}

pub async fn jauges_default_reply(store: ArcStore) -> Result<Element, Rejection> {
    Ok(render_jauges_document(&store, 1, EnergyType::All).await)
}
