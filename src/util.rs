use crate::fetch::FetchResult;

use crate::store::{ArcStore, Store, StoreError};
use chrono::{DateTime, Datelike, Duration, NaiveDate, TimeZone, Utc};
use chrono_tz::Europe::Brussels;
use model_wrapper::{read_model, Wrapper0};
use num_format::{Locale, ToFormattedString};
use paste::paste;
use tokio::time;
use tokio_postgres::types::FromSql;

#[derive(Wrapper0)]
pub struct LastDelDate(Option<DateTime<Utc>>);
read_model!(
    LastDelDate
    "
    SELECT ts
    FROM fetch_events
    WHERE description LIKE '%' || $1
    ORDER BY ts DESC
    LIMIT 1;
    "
    {
        get_nullable_datetime(0, row).map(LastDelDate::new).map_err(|e| e.into())
    }
);

impl Store {
    pub async fn last_del_date(&self, description: &str) -> Result<LastDelDate, StoreError> {
        self.fetch_one(&[&description]).await
    }
}
pub async fn get_last_del_date(store: &ArcStore, description: &str) -> FetchResult<DateTime<Utc>> {
    let last_delete_opt = store.last_del_date(description).await?;
    match last_delete_opt.value() {
        Some(date) => Ok(*date),
        None => Ok(Utc::now() - Duration::weeks(9)), // to make sure we do it the first time
    }
}

pub fn begin_of_day(day: &DateTime<Utc>) -> DateTime<Utc> {
    day.date().and_hms(0, 0, 0)
}

pub fn begin_of_month(date: DateTime<Utc>) -> DateTime<Utc> {
    let year = date.year();
    let month = date.month();
    Utc.ymd(year, month, 1).and_hms(0, 0, 0)
}
pub fn end_of_day(day: &DateTime<Utc>) -> DateTime<Utc> {
    day.date().and_hms(23, 59, 0)
}

pub fn get_value<'a, T, D>(
    idx: usize,
    row: &'a tokio_postgres::row::Row,
    default: D,
) -> Result<T, StoreError>
where
    T: FromSql<'a>,
    D: Into<T>,
{
    match row.try_get::<usize, T>(idx) {
        Err(_) => Ok(default.into()),
        Ok(v) => Ok(v),
    }
}

pub fn get_f64(idx: usize, row: &tokio_postgres::row::Row) -> Result<f64, StoreError> {
    match row.try_get::<usize, f64>(idx) {
        Err(e) => Err(e.into()),
        Ok(v) => Ok(v),
    }
}

macro_rules! clamp {
    ($source:ty, $target:ty) => {
        paste! {
            pub fn [<$source _to_ $target>] (u: $source) -> $target {
                // println!("clamp source: {}", u);
                // println!("target min in target type: {}", $target::MIN);
                let min = $target::MIN as $source;
                // println!("target min: {}", min);
                let max = $target::MAX as $source;
                if u < min {
                    $target::MIN
                } else if u > max {
                    $target::MAX
                }
                else {
                    u as $target
                }
            }
        }
    };
}

clamp!(i64, f64);
clamp!(usize, f64);
// clamp!(u32, i32); !!! does not work properly! "$target::MIN as $source" just remove de sign and don't get a real min.

// pub fn i64_to_f64(i: i64) -> f64 {
//     let tmp: u32 = i
//         .try_into()
//         .expect("i64_to_f64 cannot deal with numbers over u32::MAX");
//     tmp.into()
// }

// pub fn usize_to_f64(i: usize) -> f64 {
//     let tmp: u32 = i
//         .try_into()
//         .expect("usize_to_f64 cannot deal with numbers over u32::MAX");
//     tmp.into()
// }

pub fn str_to_f64(s: &str) -> Option<f64> {
    match s.parse::<f64>() {
        Ok(v) => Some(v),
        Err(_) => None,
    }
}
pub fn str_to_i32(s: &str) -> Option<i32> {
    match s.parse::<i32>() {
        Ok(v) => Some(v),
        Err(_) => None,
    }
}
pub fn str_to_utc(s: &str) -> Option<DateTime<Utc>> {
    match s.parse::<DateTime<Utc>>() {
        Ok(v) => Some(v),
        Err(_) => None,
    }
}

// pub fn get_datetime(
//     idx: usize,
//     row: &tokio_postgres::row::Row,
// ) -> Result<DateTime<Utc>, StoreError> {
//     match row.try_get::<usize, DateTime<Utc>>(idx) {
//         Err(e) => Err(e.into()),
//         Ok(v) => Ok(v),
//     }
// }
pub fn get_nullable_datetime(
    idx: usize,
    row: &tokio_postgres::row::Row,
) -> Result<Option<DateTime<Utc>>, StoreError> {
    match row.try_get::<usize, Option<DateTime<Utc>>>(idx) {
        Err(e) => Err(e.into()),
        Ok(v) => Ok(v),
    }
}

pub fn format_nb(nb: f64) -> String {
    (nb.round() as i64).to_formatted_string(&Locale::fr)
}

pub fn format_family_nb<N>(nb: N) -> String
where
    N: Into<f64>,
{
    format_nb((nb.into() / 1000.).round() * 1000.)
}

pub fn format_date_iso(dt: &DateTime<Utc>) -> String {
    format!("{}", dt.with_timezone(&Brussels).format("%Y-%m-%d"))
}

pub fn format_date(dt: &DateTime<Utc>) -> String {
    format!("{}", dt.with_timezone(&Brussels).format("%d.%m.%Y"))
}

pub fn format_datetime(dt: &DateTime<Utc>) -> String {
    // format!("{}", dt.with_timezone(&Brussels).format("%d.%m.%Y %H:%M"))
    dt.with_timezone(&Brussels).to_string()
}

pub fn format_time(dt: &DateTime<Utc>) -> String {
    format!("{}", dt.with_timezone(&Brussels).format("%Hh"))
}

pub async fn wait_start_time(st: chrono::NaiveTime) {
    use std::convert::TryFrom;
    let seconds = st
        .signed_duration_since(chrono::Utc::now().time())
        .num_seconds();
    let wait_time = if seconds < 0 {
        std::time::Duration::from_secs(u64::try_from(86400 + seconds).unwrap())
    } else {
        std::time::Duration::from_secs(u64::try_from(seconds).unwrap())
    };
    println!("Fetcher will start in {} seconds", wait_time.as_secs());
    time::sleep(wait_time).await;
}

pub fn last_month(month: u32) -> u32 {
    if month == 1 {
        return 12;
    } else {
        return month - 1;
    }
}

pub fn year_of_last_month(month: u32, year: u32) -> u32 {
    if month > 1 {
        return year;
    } else {
        return year - 1;
    }
}

pub fn begin_year(year: i32) -> DateTime<Utc> {
    let start_naive = NaiveDate::from_ymd(year, 1, 1);
    DateTime::from_utc(start_naive.and_hms(0, 0, 0), Utc)
}

pub fn end_year(year: i32) -> DateTime<Utc> {
    let start_naive = NaiveDate::from_ymd(year, 12, 31);
    DateTime::from_utc(start_naive.and_hms(23, 59, 59), Utc)
}
