#![recursion_limit = "256"]

extern crate tokio_postgres;

use crate::fetch::fetch_all;
use clap::{Args, Parser, Subcommand};
use rtbf::{uploader, FTPConfig};
use std::thread;
use tokio::runtime::Runtime;
use user::make_user;

mod auth;
mod fetch;
mod forecast;
mod form;
mod home;
mod html;
mod map;
mod model;
mod models;
mod news;
mod period_select;
mod plot;
mod production;
mod raster;
mod render;
mod render_forecast;
mod render_onepage;
mod render_production;
mod rtbf;
mod session;
mod store;
mod user;
mod util;
mod widget;

#[derive(Args)]
struct ServerArgs {
    #[clap(long, value_parser)]
    dsn: String,

    #[clap(long, value_parser)]
    address: String,

    #[clap(long, value_parser, default_value = "600")]
    interval: u64,

    #[clap(long, value_parser, help = "Fetch start time (UTC)")]
    start_time: String,

    #[clap(long, value_parser, help = "Security token for ENTSOe")]
    token: String,

    #[clap(long, value_parser)]
    ftp_host: Option<String>,

    #[clap(long, value_parser, default_value = "21")]
    ftp_port: u32,

    #[clap(long, value_parser)]
    ftp_username: Option<String>,

    #[clap(long, value_parser)]
    ftp_password: Option<String>,

    #[clap(long, value_parser)]
    ftp_directory: Option<String>,

    #[clap(long, value_parser)]
    ftp_time_of_day: Option<String>,

    #[clap(long, value_parser, default_value = ".")]
    static_directory: String,
}

#[derive(Args)]
struct UserArgs {
    #[clap(long, value_parser)]
    dsn: String,

    #[clap(long, value_parser)]
    username: String,

    #[clap(long, value_parser)]
    password: String,
}

#[derive(Subcommand)]
enum Commands {
    Serve(ServerArgs),
    User(UserArgs),
}

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
#[clap(propagate_version = true)]
struct Cli {
    #[clap(subcommand)]
    command: Commands,
}

fn start_server(args: ServerArgs) {
    if let (Some(host), Some(username), Some(password), Some(directory), Some(time_of_day)) = (
        args.ftp_host,
        args.ftp_username,
        args.ftp_password,
        args.ftp_directory,
        args.ftp_time_of_day,
    ) {
        let time_of_day = chrono::NaiveTime::parse_from_str(&time_of_day, "%H:%M:%S")
            .expect("ftp time of day must be formated as hh:mm:ss");
        let ftp_config = FTPConfig {
            host,
            port: args.ftp_port,
            username,
            password,
            directory,
            time_of_day,
        };

        let dsn = args.dsn.clone();
        let _uploader = thread::spawn(move || uploader(dsn.clone(), ftp_config));
    }

    let st = chrono::NaiveTime::parse_from_str(&args.start_time, "%H:%M:%S")
        .expect("start time must be formated as hh:mm:ss");
    let interval = args.interval;
    let token = args.token.clone();
    let dsn = args.dsn.clone();
    let _fetcher = thread::spawn(move || fetch_all(dsn, interval, st, token));

    let runtime = Runtime::new().expect("Failed to build a Tokio runtime, that's bad");
    runtime.block_on(render::serve(
        args.dsn,
        &args.address,
        &args.static_directory,
    ));
}

fn start_user(args: UserArgs) {
    make_user(&args.dsn, &args.username, &args.password)
}

fn main() {
    let cli = Cli::parse();

    match cli.command {
        Commands::Serve(args) => start_server(args),
        Commands::User(args) => start_user(args),
    }
}
