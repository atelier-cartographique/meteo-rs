use chrono::{DateTime, Datelike, Utc};

use crate::{
    html::{form, input, option, select, Element},
    plot::base::first_year,
    render::EnergyType,
};

// fn select_with_options(name: &str, options: Vec<&str>) -> Element {
//     let o: Vec<Element> = options
//         .iter()
//         .map(|opt| option(opt.to_string()).set("value", opt.to_string()))
//         .collect();
//     select(o).set("name", name)
// }

// fn days(current_day: u32, current_month: u32) -> Vec<Element> {
//     let long_months: Vec<u32> = vec![1, 3, 5, 7, 8, 10, 12];
//     let february: u32 = 2;
//     let mut list: Vec<u32> = vec![];

//     if long_months.contains(&current_month) {
//         list = (1u32..31).collect::<Vec<_>>();
//     } else if current_month == february {
//         list = (1u32..29).collect::<Vec<_>>();
//     } else {
//         list = (1u32..30).collect::<Vec<_>>();
//     }
//     list.iter()
//         .map(|d| option(d.to_string()).set_bool("selected", d == &current_day))
//         .collect()
// }

fn months(current_month: u32) -> Vec<Element> {
    [
        "Janvier",
        "Février",
        "Mars",
        "Avril",
        "Mai",
        "Juin",
        "Juillet",
        "Août",
        "Septembre",
        "Octobre",
        "Novembre",
        "Décembre",
    ]
    .iter()
    .enumerate()
    .map(|(i, m)| {
        option(m.to_string())
            .set_num("value", i + 1)
            .set_bool("selected", i + 1 == current_month as usize)
    })
    .collect()
}

fn years(current_year: i32, energy_type: &EnergyType) -> Vec<Element> {
    let first_year = first_year(energy_type);
    (first_year..=Utc::now().year())
        // .iter()
        .map(|y| {
            option(y.to_string())
                .set_num("value", y)
                .set_bool("selected", y == current_year)
        })
        .collect()
}

pub struct PositiveYear(u32);
impl From<u32> for PositiveYear {
    fn from(y: u32) -> Self {
        PositiveYear(y)
    }
}

impl From<i32> for PositiveYear {
    fn from(y: i32) -> Self {
        if y < 0 {
            PositiveYear(0)
        } else {
            PositiveYear(y as u32)
        }
    }
}

impl From<PositiveYear> for i32 {
    fn from(positive_year: PositiveYear) -> Self {
        if positive_year.0 >= u32::MAX / 2 {
            i32::MAX
        } else {
            positive_year.0 as i32
        }
    }
}

// pub fn render_select_day_month_year<Y>(
//     day: u32,
//     month: u32,
//     year: Y,
//     energy_type: EnergyType,
//     path: &str,
// ) -> Element
// where
//     Y: Into<PositiveYear>,
// {
//     let positive_year: PositiveYear = year.into();

//     form(vec![
//         select(days(day, month))
//             .set("type", "number")
//             .set("key", "day")
//             .set("name", "day"),
//         select(months(month))
//             .set("type", "number")
//             .set("key", "month")
//             .set("name", "month"),
//         select(years(positive_year.into()))
//             .set("type", "number")
//             .set("key", "year")
//             .set("name", "year"),
//         input().set("type", "submit").set("value", "Sélectionner"),
//     ])
//     .set("action", path)
//     .set("method", "get")
//     .append(
//         input()
//             .set("type", "hidden")
//             .set("name", "energy_type")
//             .set("value", energy_type.to_string()),
//     )
// }
pub fn render_date_input(start: &DateTime<Utc>, end: &DateTime<Utc>) -> Element {
    form(vec![
        input()
            .set("type", "date")
            .set("key", "start-date")
            .set("name", "start")
            .set("min", "2020-01-01")
            .set("max", Utc::now().format("%Y-%m-%d").to_string())
            .set("value", start.format("%Y-%m-%d").to_string()),
        input()
            .set("type", "date")
            .set("key", "end-date")
            .set("name", "end")
            .set("min", "2020-01-01")
            .set("max", Utc::now().format("%Y-%m-%d").to_string())
            .set("value", end.format("%Y-%m-%d").to_string()),
        input().set("type", "submit").set("value", "Sélectionner"),
    ])
    .set("action", "/conso_piechart")
    .set("method", "get")
}

pub fn render_select_month_year<Y>(
    month: u32,
    year: Y,
    energy_type: &EnergyType,
    path: &str,
) -> Element
where
    Y: Into<PositiveYear>,
{
    let positive_year: PositiveYear = year.into();

    form(vec![
        select(months(month))
            .set("type", "number")
            .set("key", "month")
            .set("name", "month"),
        select(years(positive_year.into(), energy_type))
            .set("type", "number")
            .set("key", "year")
            .set("name", "year"),
        input().set("type", "submit").set("value", "Sélectionner"),
    ])
    .set("action", path)
    .set("method", "get")
    .append(
        input()
            .set("type", "hidden")
            .set("name", "energy_type")
            .set("value", energy_type.to_string()),
    )
}

pub fn render_select_year<Y>(year: Y, energy_type: &EnergyType) -> Element
where
    Y: Into<PositiveYear>,
{
    let positive_year: PositiveYear = year.into();

    form(vec![
        select(years(positive_year.into(), energy_type))
            .set("type", "number")
            .set("key", "year")
            .set("name", "year"),
        input().set("type", "submit").set("value", "Sélectionner"),
    ])
    .set("action", "/year_production")
    .set("method", "get")
    .append(
        input()
            .set("type", "hidden")
            .set("name", "month")
            .set("value", "1"),
    )
    .append(
        input()
            .set("type", "hidden")
            .set("name", "energy_type")
            .set("value", energy_type.to_string()),
    )
}
