extern crate spade;
use crate::plot::base::TEXT_COLOR;
use crate::raster::dynimg_to_svgimg;
use crate::render::MeteoImage;
use crate::store::{ArcStore, Region, StoreError};
use crate::util::{begin_of_day, end_of_day, format_time};
use chrono::DateTime;
use chrono::{TimeZone, Utc};
use image::{DynamicImage, ImageBuffer, Rgb, Rgba};
use palette::rgb::Rgb as Rgb_palette;
use palette::{Hsv, RgbHue};
use spade::delaunay::{IntDelaunayTriangulation, VertexHandle};
use spade::{PointN, TwoDimensional};
use std::fmt::Debug;
use std::path::Path;
use svg::node::element::path::Data;
use svg::node::element::Image as SvgImage;
use svg::node::element::{
    Definitions, Group, LinearGradient, Path as SvgPath, Rectangle, Stop, Text,
};
use svg::node::Text as TextNode;
use svg::Document;

const WIDTH: u32 = 1280;
const HEIGHT: u32 = 960;
const ARROW_MAP_COLOR: &str = "#595959";
const HUE_MAX: f32 = 60.; //yellow (and hue_min is 0 - red)

#[derive(Debug, Copy, Clone)]
pub struct Barycentric {
    pub lam0: f32,
    pub lam1: f32,
    pub lam2: f32,
}

#[derive(Debug, Copy, Clone)]
pub struct Coord {
    x: i64,
    y: i64,
}

#[derive(Clone, Debug, PartialEq)]
struct Point {
    x: i64,
    y: i64,
    color: Hsv,
}

impl Point {
    fn new(x: i64, y: i64, color: Hsv) -> Point {
        Point { x, y, color }
    }
}

impl PointN for Point {
    /// The points's internal scalar type.
    type Scalar = i64;

    /// The (fixed) number of dimensions of this point type.
    fn dimensions() -> usize {
        2
    }

    /// Creates a new point with all components set to a certain value.
    fn from_value(value: Self::Scalar) -> Self {
        Point {
            x: value,
            y: value,
            color: Hsv::new(0.0, 0.0, 0.0),
        }
    }
    /// Returns the nth element of this point.
    fn nth(&self, index: usize) -> &Self::Scalar {
        if index == 0 {
            &self.x
        } else {
            &self.y
        }
    }

    /// Returns a mutable reference to the nth element of this point.
    fn nth_mut(&mut self, index: usize) -> &mut Self::Scalar {
        if index == 0 {
            &mut self.x
        } else {
            &mut self.y
        }
    }
}

impl TwoDimensional for Point {}

// the double area of the triangle abc
fn double_area(a: &Coord, b: &Coord, c: &Coord) -> i64 {
    (c.x - a.x) * (b.y - a.y) - (c.y - a.y) * (b.x - a.x)
}

// barycentric coordinates of a point p in the triangle abc
fn barycentric(a: &Coord, b: &Coord, c: &Coord, p: &Coord) -> Barycentric {
    let total_double_area = double_area(a, b, c) as f32;
    let (x, y, z) = (
        double_area(b, c, p) as f32 / total_double_area,
        double_area(c, a, p) as f32 / total_double_area,
        double_area(a, b, p) as f32 / total_double_area,
    );
    Barycentric {
        lam0: x,
        lam1: y,
        lam2: z,
    }
}

// multiply Hsv components by a given factor
fn multiply_hsv(hsv: Hsv, lam: f32) -> Hsv {
    Hsv::new(
        RgbHue::from_radians((hsv.hue).to_radians() * lam),
        (hsv.saturation) * lam,
        (hsv.value) * lam,
    )
}

//sum elements of 3 arrays, term by term
fn add_3_arrays(col0: Hsv, col1: Hsv, col2: Hsv) -> Hsv {
    Hsv::new(
        col0.hue + col1.hue + col2.hue,
        col0.saturation + col1.saturation + col2.saturation,
        col0.value + col1.value + col2.value,
    )
}

//merge of 3 colors, balanced by barycentric_coeficients
fn merge_3_colors(color1: Hsv, color2: Hsv, color3: Hsv, barycentric_coef: Barycentric) -> Hsv {
    add_3_arrays(
        multiply_hsv(color1, barycentric_coef.lam0),
        multiply_hsv(color2, barycentric_coef.lam1),
        multiply_hsv(color3, barycentric_coef.lam2),
    )
}
fn hsv_to_rgb(hsv: Hsv) -> Rgba<u8> {
    let srgb: Rgb_palette = hsv.into();
    let (r, g, b) = srgb.into_format::<u8>().into_components();
    Rgba([r, g, b, std::u8::MAX])
}

//the color of a point p which is the average (balanced by his barycentrics coordinates) of the colors of the 3 tops of the triangle abc
fn color_in_triangle(a: &Point, b: &Point, c: &Point, p: &Coord) -> Rgba<u8> {
    let coord_a = Coord { x: a.x, y: a.y };
    let coord_b = Coord { x: b.x, y: b.y };
    let coord_c = Coord { x: c.x, y: c.y };
    let coef = barycentric(&coord_a, &coord_b, &coord_c, p);
    hsv_to_rgb(merge_3_colors(a.color, b.color, c.color, coef))
}

//check if a point is inside a given triangle
fn is_inside(triangle: [VertexHandle<'_, Point>; 3], p: &Coord) -> bool {
    let a = &Coord {
        x: triangle[0].x,
        y: triangle[0].y,
    };
    let b = &Coord {
        x: triangle[1].x,
        y: triangle[1].y,
    };
    let c = &Coord {
        x: triangle[2].x,
        y: triangle[2].y,
    };
    double_area(a, c, p) >= 0 && double_area(c, b, p) >= 0 && double_area(b, a, p) >= 0
}

// the color for a given loadfactor, with a hue scale from 0 to 230.
fn color_of_prod(loadfactor: f64) -> Hsv {
    let loadf = loadfactor as f32;
    let h = RgbHue::to_positive_degrees(RgbHue::from_degrees(HUE_MAX - (HUE_MAX * loadf / 100.0)));
    Hsv::new(h, 1.0, 1.0)
}

// belgian map with colors in function of solar loadfactors of the 11 regions
pub fn solar_map(loadfactor_forecast: &Vec<f64>) -> MeteoImage {
    let imgx = WIDTH;
    let imgy = HEIGHT;

    if loadfactor_forecast.len() < 12 {
        println!("err map display: we miss some regions values");
        return MeteoImage(DynamicImage::ImageRgb8(ImageBuffer::new(imgx, imgy)));
    }
    // Create a new ImgBuf with width: imgx and height: imgy
    let mut imgbuf = ImageBuffer::new(imgx, imgy);

    let hsv_4 = color_of_prod(loadfactor_forecast[1]); //1
    let hsv_5 = color_of_prod(loadfactor_forecast[2]); //2
    let hsv_6 = color_of_prod(loadfactor_forecast[3]); //3
    let hsv_7 = color_of_prod(loadfactor_forecast[4]); //4
    let hsv_8 = color_of_prod(loadfactor_forecast[5]); //5
    let hsv_9 = color_of_prod(loadfactor_forecast[6]); //6
    let hsv_10 = color_of_prod(loadfactor_forecast[7]); //7
    let hsv_11 = color_of_prod(loadfactor_forecast[8]); //8
    let hsv_12 = color_of_prod(loadfactor_forecast[9]); //9
    let hsv_13 = color_of_prod(loadfactor_forecast[10]); //10
    let hsv_14 = color_of_prod(loadfactor_forecast[11]); //11

    let points = [
        Point::new(597, 321, hsv_4),  //bruxelles
        Point::new(702, 124, hsv_5),  //antwerpen
        Point::new(0, 960, hsv_5),    // down-left corner
        Point::new(459, 472, hsv_6),  //hainaut
        Point::new(1280, 0, hsv_7),   // up-rigth corner
        Point::new(930, 240, hsv_7),  //limburg
        Point::new(1025, 460, hsv_8), //liège
        Point::new(950, 740, hsv_9),  //luxembourg
        Point::new(1280, 960, hsv_9), // down-rigth corner
        Point::new(775, 570, hsv_10), //namur
        Point::new(427, 225, hsv_11), //oost vlaanderen
        Point::new(710, 285, hsv_12), //vlaams brabant
        Point::new(670, 401, hsv_13), //brabant wallon
        Point::new(0, 0, hsv_14),     // up-left corner
        Point::new(190, 228, hsv_14), //west vlaanderen
    ];

    // Make the triangulation between the given points
    let mut delaunay = IntDelaunayTriangulation::with_walk_locate();
    for p in points.iter() {
        delaunay.insert(p.clone());
    }
    let triangles = delaunay.triangles().collect::<Vec<_>>();

    // Draw the borders of Belgium
    let borders = image::open(Path::new("medias/regions-white.png")).unwrap();
    let border_buf = borders.into_rgb8();

    for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
        let p = Coord {
            x: x as i64,
            y: y as i64,
        };
        let p_border = border_buf.get_pixel(x, y);
        for triangle in triangles.iter() {
            let t = triangle.as_triangle();
            if *p_border == Rgb([0, 0, 0]) {
                *pixel = Rgba([0, 0, 0, 0]);
                // *pixel = Rgb([248, 248, 248]);
                // imgbuf.put_pixel(x, y, Rgb([248, 248, 248]));
                break;
            } else if is_inside(t, &p) {
                // imgbuf.put_pixel(x, y, color_in_triangle(&t[0], &t[1], &t[2], &p));
                *pixel = color_in_triangle(&t[0], &t[1], &t[2], &p);
                break;
            }
        }
    }
    let dyimg = DynamicImage::ImageRgba8(imgbuf);
    MeteoImage(dyimg)
}

pub fn solar_map_legend() -> MeteoImage {
    let imgx = 100;
    let imgy = 400;
    let mut imgbuf = ImageBuffer::new(imgx, imgy);

    for (_x, y, p) in imgbuf.enumerate_pixels_mut() {
        // *p = hsv_to_rgb(grad.get(y as f32 * hue2 / imgy as f32));
        *p = hsv_to_rgb(Hsv::new(y as f32 * HUE_MAX / imgy as f32, 1., 1.));
    }
    let dynimg = DynamicImage::ImageRgba8(imgbuf);
    MeteoImage(dynimg)
}

#[derive(Debug)]
struct HourPeak {
    peak_moment: Result<Option<DateTime<Utc>>, StoreError>,
    coord: Coord,
}

async fn map_texts_download(date: &DateTime<Utc>, store: &ArcStore) -> Group {
    let (h_vl, h_bxl, h_wal) = futures::future::join3(
        store.solar_max_hour(Region::OVlaanderen, date),
        store.solar_max_hour(Region::Bruxelles, date),
        store.solar_max_hour(Region::Luxembourg, date),
    )
    .await;
    let peaks = [
        HourPeak {
            peak_moment: h_vl.map(|x| x.into()),
            coord: Coord { x: 403, y: 216 },
        },
        HourPeak {
            peak_moment: h_bxl.map(|x| x.into()),
            coord: Coord { x: 591, y: 316 },
        },
        HourPeak {
            peak_moment: h_wal.map(|x| x.into()),
            coord: Coord { x: 904, y: 705 },
        },
    ];

    let rect_width = 120;
    let rect_height = 50;
    let font_size = "4em";
    let mut group = Group::new();
    for p in peaks.iter() {
        group = group
            .clone()
            .add(
                Rectangle::new()
                    .set("x", p.coord.x - rect_width / 2)
                    .set("y", p.coord.y - rect_height / 2)
                    .set("rx", 10)
                    .set("width", rect_width)
                    .set("height", rect_height)
                    .set("fill", "#b3b3b3")
                    .set("opacity", 0.5),
            )
            .add(match p.peak_moment {
                Ok(Some(pm)) => Text::new()
                    .add(TextNode::new(format_time(&pm)))
                    .set("x", p.coord.x)
                    .set("y", p.coord.y)
                    .set("dy", ".4em")
                    .set("text-anchor", "middle")
                    .set("font-size", font_size)
                    .set("fill", TEXT_COLOR),
                _ => Text::new(),
            });
    }
    group
}
async fn map_texts_app(date: &DateTime<Utc>, store: &ArcStore, regions: &Vec<Region>) -> Group {
    let coords = [
        Coord { x: 597, y: 321 },  //bruxelles
        Coord { x: 702, y: 124 },  //antwerpen
        Coord { x: 459, y: 472 },  //hainaut
        Coord { x: 930, y: 240 },  //limburg
        Coord { x: 1025, y: 460 }, //liège
        Coord { x: 950, y: 740 },  //luxembourg
        Coord { x: 775, y: 570 },  //namur
        Coord { x: 190, y: 228 },  //west vlaanderen
        Coord { x: 710, y: 285 },  //vlaams brabant
        Coord { x: 670, y: 401 },  //brabant wallon
        Coord { x: 427, y: 225 },  //oost vlaanderen
    ];
    let mut peaks = Vec::new();
    for (r, c) in regions.iter().zip(coords.iter()) {
        let peak_moment = store.solar_max_hour(*r, date).await;
        peaks.push(HourPeak {
            peak_moment: peak_moment.map(|x| x.into()),
            coord: *c,
        });
    }

    let rect_width = 120;
    let rect_height = 50;
    let font_size = "4em";
    let mut group = Group::new();
    for p in peaks.iter() {
        group = group
            .clone()
            .add(
                Rectangle::new()
                    .set("x", p.coord.x - rect_width / 2)
                    .set("y", p.coord.y - rect_height / 2)
                    .set("rx", 10)
                    .set("width", rect_width)
                    .set("height", rect_height)
                    .set("fill", "#b3b3b3")
                    .set("opacity", 0.5),
            )
            .add(match p.peak_moment {
                Ok(Some(pm)) => Text::new()
                    .add(TextNode::new(format_time(&pm)))
                    .set("x", p.coord.x)
                    .set("y", p.coord.y)
                    .set("dy", ".4em")
                    .set("text-anchor", "middle")
                    .set("font-size", font_size)
                    .set("fill", TEXT_COLOR),
                _ => Text::new(),
            });
    }
    group
}

fn map_legend_svg(loadfactor: f64) -> Group {
    let width: f64 = 50.;
    let height: f64 = 300.;
    let defs = Definitions::new().add(
        LinearGradient::new()
            .set("id", "Gradient")
            .set("x1", 1)
            .set("y2", 1)
            .add(Stop::new().set("stop-color", "red").set("offset", "0%"))
            .add(
                Stop::new()
                    .set("stop-color", "yellow")
                    .set("offset", "100%"),
            ), // .add(
               //     Stop::new()
               //         .set("stop-color", "#9cff00")
               //         .set("offset", "100%"),
               // ),
    );
    let x_rect: f64 = 70.;
    let y_rect: f64 = HEIGHT as f64 - height - 20.;
    let rect = Rectangle::new()
        .set("x", x_rect)
        .set("y", y_rect)
        .set("rx", 10)
        .set("width", width)
        .set("height", height)
        .set("fill", "url(#Gradient)");
    // .set("id", "legend-rectangle");
    let text_up = Text::new()
        .add(TextNode::new("+"))
        .set("x", x_rect + width / 2.)
        .set("y", y_rect + 30.)
        .set("text-anchor", "middle")
        .set("font-size", "2em")
        .set("fill", TEXT_COLOR);
    let text_down = Text::new()
        .add(TextNode::new("-"))
        .set("x", x_rect + width / 2.)
        .set("y", y_rect + height - 10.)
        .set("text-anchor", "middle")
        .set("font-size", "2em")
        .set("fill", TEXT_COLOR);

    let y = (1. - loadfactor / 100.) * height + y_rect;
    let x = x_rect - 5.;
    let arrow_data = Data::new()
        .move_to((x - 10., y - 6.))
        .line_to((x, y))
        .line_to((x - 10., y + 6.))
        .move_to((x, y))
        .line_to((50, y));

    let arrow = SvgPath::new()
        .set("d", arrow_data)
        .set("fill", "none")
        .set("stroke-width", 3.)
        .set("stroke", ARROW_MAP_COLOR);
    // let arrow = Text::new()
    //     .add(TextNode::new("→"))
    //     .set("x", x)
    //     .set("y", y)
    //     .set("text-anchor", "end")
    //     .set("font-size", "4em")
    //     .set("fill", TEXT_COLOR);
    Group::new()
        .add(defs)
        .add(rect)
        .add(arrow)
        .add(text_up)
        .add(text_down)
}
fn logo_apere_map() -> SvgImage {
    let width = 243. / 2.75;
    let height = 177. / 2.75;
    let logo = image::open(Path::new("medias/logo-ec.png")).unwrap();
    dynimg_to_svgimg(MeteoImage(logo))
        .set("width", width)
        .set("height", height)
        .set("x", WIDTH - width as u32 - 20)
        .set("y", 10)
}

// pub fn solar_map_svg(img: MeteoImage, h_vl: u32, h_bxl: u32, h_wal: u32) -> Document {
pub async fn solar_map_svg_download(
    year: i32,
    month: u32,
    day: u32,
    hour: u32,
    store: &ArcStore,
) -> Document {
    let date = Utc.ymd(year, month, day).and_hms(hour, 0, 0);
    let begin_day = begin_of_day(&date);
    let end_day = end_of_day(&date);
    let regions: Vec<Region> = vec![
        Region::Belgique,
        Region::Bruxelles,
        Region::Antw,
        Region::Hainaut,
        Region::Limburg,
        Region::Liege,
        Region::Luxembourg,
        Region::Namur,
        Region::WVlaanderen,
        Region::VlBrabant,
        Region::BrabantW,
        Region::OVlaanderen,
    ];
    let mut loadfactor_forecast: Vec<f64> = Vec::new();
    for r in &regions {
        let r_lf = store
            .solar_loadfactor_forecast(&begin_day, &end_day, *r)
            .await;
        match r_lf {
            Ok(lf) => loadfactor_forecast.push(lf.value()),
            Err(_) => loadfactor_forecast.push(0.),
        }
    }
    let texts = map_texts_download(&date, store).await;
    let svg_map = dynimg_to_svgimg(solar_map(&loadfactor_forecast));
    let lf = loadfactor_forecast.get(0);
    let legend = match lf {
        Some(lf) => map_legend_svg(*lf),
        None => Group::new(),
    };

    Document::new()
        .set("xmlns:xlink", "http://www.w3.org/1999/xlink")
        .set("viewBox", (0, 0, WIDTH, HEIGHT))
        .add(svg_map)
        .add(texts)
        .add(legend)
        .add(logo_apere_map())
}
pub async fn solar_map_svg_app(
    year: i32,
    month: u32,
    day: u32,
    hour: u32,
    store: &ArcStore,
) -> Document {
    let date = Utc.ymd(year, month, day).and_hms(hour, 0, 0);
    let begin_day = begin_of_day(&date);
    let end_day = end_of_day(&date);
    let regions: Vec<Region> = vec![
        Region::Belgique,
        Region::Bruxelles,
        Region::Antw,
        Region::Hainaut,
        Region::Limburg,
        Region::Liege,
        Region::Luxembourg,
        Region::Namur,
        Region::WVlaanderen,
        Region::VlBrabant,
        Region::BrabantW,
        Region::OVlaanderen,
    ];
    let mut loadfactor_forecast: Vec<f64> = Vec::new();
    for r in &regions {
        let r_lf = store
            .solar_loadfactor_forecast(&begin_day, &end_day, *r)
            .await;
        match r_lf {
            Ok(lf) => loadfactor_forecast.push(lf.value()),
            Err(_) => loadfactor_forecast.push(0.),
        }
    }
    let texts = map_texts_app(&date, store, &regions).await;
    let svg_map = dynimg_to_svgimg(solar_map(&loadfactor_forecast));
    let lf = loadfactor_forecast.get(0);
    let legend = match lf {
        Some(lf) => map_legend_svg(*lf),
        None => Group::new(),
    };

    Document::new()
        .set("xmlns:xlink", "http://www.w3.org/1999/xlink")
        .set("viewBox", (0, 0, WIDTH, HEIGHT))
        .add(svg_map)
        .add(texts)
        .add(legend)
        .add(logo_apere_map())
}
