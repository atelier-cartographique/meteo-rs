// use warp::{
//     reject::{not_found, Reject},
//     Rejection,
// };

// use crate::{render::EnergyType, render_forecast::ForecastQuery};
// use serde::Deserialize;

// enum WidgetType {
//     News,
//     Prod,
//     Forecast, // SolarProd,
//               // WindProd,
//               // JaugesForecast,
//               // PieProd,
//               // SolarForecast,
//               // WindForecast,
// }

// #[derive(Deserialize, Debug)]
// #[serde(tag = "wid")]
// #[serde(rename_all = "lowercase")]
// pub enum WidgetQuery {
//     None,
//     Forecast(ForecastQuery),
//     Prod {
//         month: u32,
//         year: u32,
//         energy_type: EnergyType,
//     },
// }

// fn name_to_widget(widget_name: String) -> Result<WidgetType, Rejection> {
//     match widget_name.as_str() {
//         "news" => Ok(WidgetType::News),
//         "production" => Ok(WidgetType::Prod),
//         "forecast" => Ok(WidgetType::Forecast),
//         &_ => Err(not_found()),
//     }
// }

// #[derive(Debug)]
// struct WrongArgument;
// impl Reject for WrongArgument {}

// // pub async fn render_widget(
// //     store: ArcStore,
// //     widget_name: String,
// //     query: WidgetQuery,
// // ) -> Result<impl Reply, Rejection> {
// //     let root = html(head([
// //         meta().set("charset", "UTF-8"),
// //         link()
// //             .set("rel", "stylesheet")
// //             .set("type", "text/css")
// //             .set("href", "/static/style/widget.css"),
// //     ]));

// //     let default_prod_query = default_prod_query();
// //     let widget_type = name_to_widget(widget_name)?;
// //     let widget_node: Element = match (widget_type, query) {
// //         (WidgetType::News, _) => render_latest_news(&store, None)
// //             .await
// //             .map_err(warp::reject::custom)?,
// //         // (WidgetType::Forecast, WidgetQuery::Forecast(query)) => {
// //         //     render_jauges(&store, query.nb_days, query.energy_type).await
// //         // }
// //         (
// //             WidgetType::Prod,
// //             WidgetQuery::Prod {
// //                 month,
// //                 year,
// //                 energy_type,
// //             },
// //         ) => {
// //             let query = ProdQuery {
// //                 month,
// //                 year,
// //                 energy_type,
// //             };
// //             production(&store, &query.start(), &query.end(), EnergyType::Solar).await
// //         }
// //         // (WidgetType::Prod, WidgetQuery::Prod(query)) => {
// //         //     production(&store, &query.start(), &query.end(), EnergyType::Wind).await
// //         // }
// //         // // (WidgetType::PieProd, WidgetQuery::Prod(query)) => {
// //         // //     render_prod_piechart(&store, &query.start(), &query.end()).await?
// //         // // }
// //         // (WidgetType::Prod, WidgetQuery::Prod(query)) => {
// //         //     production(&store, &query.start(), &query.end(), EnergyType::All).await
// //         // }
// //         (_, _) => {
// //             production(
// //                 &store,
// //                 &default_prod_query.start(),
// //                 &default_prod_query.end(),
// //                 default_prod_query.energy_type,
// //             )
// //             .await
// //         } // (WidgetType::SolarForecast, _) => {
// //           //     forecast(
// //           //         &store,
// //           //         ForecastQuery {
// //           //             nb_days: 1,
// //           //             energy_type: "solar".to_string(),
// //           //         },
// //           //     )
// //           //     .await?
// //           // }
// //     };
// //     Ok(root.append(body(widget_node)))
// // }
