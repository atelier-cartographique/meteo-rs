use deadpool_postgres::{Manager, ManagerConfig, Pool, PoolError, RecyclingMethod};
use std::str::FromStr;
use std::{fmt};
use tokio_postgres::{types::ToSql, Config, NoTls, Row};
use warp::{reject::Reject};

use crate::model::{FromRow, ModelError, ToParams};

#[derive(Debug)]
pub enum StoreError {
    Postgres(PoolError),
    Model(ModelError),
    MissingRow,
}

impl fmt::Display for StoreError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            StoreError::Postgres(err) => write!(f, "Failed to open connection to {}", err),
            StoreError::Model(err) => write!(f, "Model Failed: {:?}", err),
            StoreError::MissingRow => write!(f, "Missing Row"),
        }
    }
}

impl std::error::Error for StoreError {}

impl std::convert::From<tokio_postgres::error::Error> for StoreError {
    fn from(err: tokio_postgres::error::Error) -> Self {
        StoreError::Postgres(err.into())
    }
}

impl std::convert::From<ModelError> for StoreError {
    fn from(err: ModelError) -> Self {
        StoreError::Model(err)
    }
}

impl std::convert::From<PoolError> for StoreError {
    fn from(err: PoolError) -> Self {
        StoreError::Postgres(err)
    }
}

impl Reject for StoreError {}

// impl From<StoreError> for Rejection {
//     fn from(err: StoreError) -> Self {
//         warp::reject::custom(e)
//     }
// }

#[derive(Clone, Copy)]
pub enum Offshore {
    True,
    False,
    None,
}
#[derive(Clone, Copy, Debug)]
pub enum Region {
    Belgique,
    Bruxelles,
    Antw,
    Hainaut,
    Limburg,
    Liege,
    Luxembourg,
    Namur,
    WVlaanderen,
    VlBrabant,
    BrabantW,
    OVlaanderen,
}

pub struct Store {
    pool: Pool,
}

impl Store {
    pub async fn new(dsn: &str) -> Result<Store, StoreError> {
        let pg_config =
            Config::from_str(dsn).expect("Could not parse DSN into a proper PostgreSQL config");

        let mgr_config = ManagerConfig {
            recycling_method: RecyclingMethod::Fast,
        };
        let mgr = Manager::from_config(pg_config, NoTls, mgr_config);
        let pool = Pool::builder(mgr).max_size(8).build().unwrap();

        Ok(Store { pool })
    }

    async fn exec(&self, sql: &str, params: &[&(dyn ToSql + Sync)]) -> Result<u64, StoreError> {
        let client = self.pool.get().await?;
        let stmt = client.prepare_cached(sql).await?;
        client
            .execute(&stmt, params)
            .await
            .map_err(|e| StoreError::Postgres(e.into()))
    }

    async fn query(
        &self,
        sql: &str,
        params: &[&(dyn ToSql + Sync)],
    ) -> Result<Vec<Row>, StoreError> {
        let client = self.pool.get().await?;
        let stmt = client.prepare_cached(sql).await?;
        client
            .query(&stmt, params)
            .await
            .map_err(|e| StoreError::Postgres(e.into()))
    }

    async fn query_one(
        &self,
        sql: &str,
        params: &[&(dyn ToSql + Sync)],
    ) -> Result<Row, StoreError> {
        let client = self.pool.get().await?;
        let stmt = client.prepare_cached(sql).await?;
        client
            .query_one(&stmt, params)
            .await
            .map_err(|e| StoreError::Postgres(e.into()))
    }

    pub async fn fetch<M>(&self, params: &[&(dyn ToSql + Sync)]) -> Result<Vec<M>, StoreError>
    where
        M: FromRow,
    {
        let rows = self.query(M::sql(), params).await?;
        let records: Vec<M> = rows
            .iter()
            .filter_map(|row| M::from_row(row).ok())
            .collect();
        Ok(records)
    }

    pub async fn fetch_one<M>(&self, params: &[&(dyn ToSql + Sync)]) -> Result<M, StoreError>
    where
        M: FromRow,
    {
        let row = self.query_one(M::sql(), params).await?;
        M::from_row(&row).map_err(StoreError::Model)
    }

    pub async fn create<M>(&self, params: &[&(dyn ToSql + Sync)]) -> Result<M, StoreError>
    where
        M: FromRow,
    {
        let result = self.query_one(M::sql(), params).await?;
        M::from_row(&result).map_err(StoreError::Model)
    }

    pub async fn save<M>(&self, model: &M) -> Result<u64, StoreError>
    where
        M: ToParams,
    {
        let raw_params = model.params();
        let params: Vec<_> = raw_params
            .iter()
            .map(|x| x.as_ref() as &(dyn ToSql + Sync))
            .collect();
        self.exec(M::sql(), params.as_slice()).await
    }

    pub async fn delete<M>(&self, model: &M) -> Result<u64, StoreError>
    where
        M: ToParams,
    {
        let raw_params = model.params();
        let params: Vec<_> = raw_params
            .iter()
            .map(|x| x.as_ref() as &(dyn ToSql + Sync))
            .collect();
        self.exec(M::sql(), params.as_slice()).await
    }
}

pub type ArcStore = ::std::sync::Arc<Store>;
