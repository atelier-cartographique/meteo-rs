
use crate::store::{Store, StoreError};
use crate::util::*;
use chrono::{DateTime, Utc};
use model_wrapper::{read_model, Wrapper0};

#[derive(Wrapper0)]
pub struct WateLastPastUpdate(Option<DateTime<Utc>>);

read_model!(
    WateLastPastUpdate
    "SELECT ts::TIMESTAMP WITH TIME ZONE
    FROM waste
    ORDER BY ts DESC
    LIMIT 1;"
    {
        get_nullable_datetime(0, row)
            .map(WateLastPastUpdate::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WasteProduction(f64);

read_model!(
    WasteProduction
    "SELECT SUM(production)
    FROM waste
    WHERE ts > $1
        AND ts < $2;"
    {
        get_f64(0, row)
            .map(WasteProduction::new)
            .map_err(|e| e.into())
    }
);

impl Store {
    pub async fn waste_last_past_update(&self) -> Result<WateLastPastUpdate, StoreError> {
        self.fetch_one(&[]).await
    }

    pub async fn waste_production(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
    ) -> Result<WasteProduction, StoreError> {
        self.fetch_one(&[start, end]).await
    }
}
