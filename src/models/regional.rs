use crate::{
    store::{Region, Store, StoreError},
    util::{get_f64, get_nullable_datetime},
};
use chrono::{DateTime, Utc};
use const_format::concatcp;
use model_wrapper::{read_model, Wrapper0};
use paste::paste;

pub const fn table_name(region: Region) -> &'static str {
    match region {
        Region::Belgique => "solar",
        Region::Bruxelles => "solar_bxl",
        Region::Antw => "solar_ant",
        Region::Hainaut => "solar_hai",
        Region::Limburg => "solar_lim",
        Region::Liege => "solar_lie",
        Region::Luxembourg => "solar_lux",
        Region::Namur => "solar_nam",
        Region::OVlaanderen => "solar_o_vl",
        Region::BrabantW => "solar_br_w",
        Region::WVlaanderen => "solar_w_vl",
        Region::VlBrabant => "solar_vl_b",
    }
}

macro_rules! region_fetch_one {
    ($store:ident, $region:ident, $prefix:ident, $params:expr) => {
        paste! {
          match $region {
            Region::Belgique => $store
            .fetch_one::<[< $prefix Belgique>]>($params)
            .await
            .map(|r| r.into()),
        Region::Bruxelles => $store
            .fetch_one::<[< $prefix Bruxelles>]>($params)
            .await
            .map(|r| r.into()),
        Region::Antw => $store
            .fetch_one::<[< $prefix Antw>]>($params)
            .await
            .map(|r| r.into()),
        Region::Hainaut => $store
            .fetch_one::<[< $prefix Hainaut>]>($params)
            .await
            .map(|r| r.into()),
        Region::Limburg => $store
            .fetch_one::<[< $prefix Limburg>]>($params)
            .await
            .map(|r| r.into()),
        Region::Liege => $store
            .fetch_one::<[< $prefix Liege>]>($params)
            .await
            .map(|r| r.into()),
        Region::Luxembourg => $store
            .fetch_one::<[< $prefix Luxembourg>]>($params)
            .await
            .map(|r| r.into()),
        Region::Namur => $store
            .fetch_one::<[< $prefix Namur>]>($params)
            .await
            .map(|r| r.into()),
        Region::OVlaanderen => $store
            .fetch_one::<[< $prefix Ovlaanderen>]>($params)
            .await
            .map(|r| r.into()),
        Region::BrabantW => $store
            .fetch_one::<[< $prefix Brabantw>]>($params)
            .await
            .map(|r| r.into()),
        Region::WVlaanderen => $store
            .fetch_one::<[< $prefix Wvlaanderen>]>($params)
            .await
            .map(|r| r.into()),
        Region::VlBrabant => $store
            .fetch_one::<[< $prefix Vlbrabant>]>($params)
            .await
            .map(|r| r.into()),
            }
          }
    };
}

#[derive(Wrapper0)]
pub struct SelectForecast(f64);

#[derive(Wrapper0)]
struct SelectSolarForecastBelgique(f64);

read_model!(
    SelectSolarForecastBelgique
    "
WITH s_base AS (
  SELECT MIN(ts) AS ts, (SUM(most_recent_forecast) / 4) AS val, AVG(monitored_capacity) AS mc
  FROM solar
  WHERE ts > $1 AND ts < $2
),
corr AS (
  SELECT ts, c.pv_power_apere AS corr_val
  FROM corrections AS c
  WHERE ts > $1 AND ts < $2
)
SELECT sb.val / sb.mc * (
    CASE
      WHEN sb.mc < corr_val THEN corr_val
      ELSE sb.mc
    END
  ) AS most_recent_forecast
FROM s_base sb
  LEFT JOIN corr c ON (
    EXTRACT(
      MONTH
      FROM sb.ts
    ) = EXTRACT(
      MONTH
      FROM c.ts
    )
    AND EXTRACT(
      YEAR
      FROM sb.ts
    ) = EXTRACT(
      YEAR
      FROM c.ts
    )
  )
LIMIT 1;
    "
    {
        get_f64(0, row) .map(SelectSolarForecastBelgique::new) .map_err(|e| e.into())
    }
);

impl From<SelectSolarForecastBelgique> for SelectForecast {
    fn from(source: SelectSolarForecastBelgique) -> Self {
        SelectForecast(source.into())
    }
}

const SOLAR_FORECAST_BRUXELLES: &str = concatcp!(
    "SELECT ROUND(SUM(most_recent_forecast) / 4) FROM ",
    table_name(Region::Bruxelles),
    " WHERE ts > $1 AND ts < $2;"
);
const SOLAR_FORECAST_ANTW: &str = concatcp!(
    "SELECT ROUND(SUM(most_recent_forecast) / 4) FROM ",
    table_name(Region::Antw),
    " WHERE ts > $1 AND ts < $2;"
);
const SOLAR_FORECAST_HAINAUT: &str = concatcp!(
    "SELECT ROUND(SUM(most_recent_forecast) / 4) FROM ",
    table_name(Region::Hainaut),
    " WHERE ts > $1 AND ts < $2;"
);
const SOLAR_FORECAST_LIMBURG: &str = concatcp!(
    "SELECT ROUND(SUM(most_recent_forecast) / 4) FROM ",
    table_name(Region::Limburg),
    " WHERE ts > $1 AND ts < $2;"
);
const SOLAR_FORECAST_LIEGE: &str = concatcp!(
    "SELECT ROUND(SUM(most_recent_forecast) / 4) FROM ",
    table_name(Region::Liege),
    " WHERE ts > $1 AND ts < $2;"
);
const SOLAR_FORECAST_LUXEMBOURG: &str = concatcp!(
    "SELECT ROUND(SUM(most_recent_forecast) / 4) FROM ",
    table_name(Region::Luxembourg),
    " WHERE ts > $1 AND ts < $2;"
);
const SOLAR_FORECAST_NAMUR: &str = concatcp!(
    "SELECT ROUND(SUM(most_recent_forecast) / 4) FROM ",
    table_name(Region::Namur),
    " WHERE ts > $1 AND ts < $2;"
);
const SOLAR_FORECAST_OVLAANDEREN: &str = concatcp!(
    "SELECT ROUND(SUM(most_recent_forecast) / 4) FROM ",
    table_name(Region::OVlaanderen),
    " WHERE ts > $1 AND ts < $2;"
);
const SOLAR_FORECAST_BRABANTW: &str = concatcp!(
    "SELECT ROUND(SUM(most_recent_forecast) / 4) FROM ",
    table_name(Region::BrabantW),
    " WHERE ts > $1 AND ts < $2;"
);
const SOLAR_FORECAST_WVLAANDEREN: &str = concatcp!(
    "SELECT ROUND(SUM(most_recent_forecast) / 4) FROM ",
    table_name(Region::WVlaanderen),
    " WHERE ts > $1 AND ts < $2;"
);
const SOLAR_FORECAST_VLBRABANT: &str = concatcp!(
    "SELECT ROUND(SUM(most_recent_forecast) / 4) FROM ",
    table_name(Region::VlBrabant),
    " WHERE ts > $1 AND ts < $2;"
);

macro_rules! select_forecast {
    ($model:ident, $sql_ident:ident) => {
      #[derive(Wrapper0)]
      struct $model(f64);

      read_model!(
        $model
        $sql_ident
        get_f64(0, row).map($model::new).map_err(|e| e.into())
      );

      impl From<$model> for SelectForecast {
          fn from(source: $model) -> Self {
              SelectForecast(source.into())
          }
      }
    };
}

select_forecast!(SelectSolarForecastBruxelles, SOLAR_FORECAST_BRUXELLES);
select_forecast!(SelectSolarForecastAntw, SOLAR_FORECAST_ANTW);
select_forecast!(SelectSolarForecastHainaut, SOLAR_FORECAST_HAINAUT);
select_forecast!(SelectSolarForecastLimburg, SOLAR_FORECAST_LIMBURG);
select_forecast!(SelectSolarForecastLiege, SOLAR_FORECAST_LIEGE);
select_forecast!(SelectSolarForecastLuxembourg, SOLAR_FORECAST_LUXEMBOURG);
select_forecast!(SelectSolarForecastNamur, SOLAR_FORECAST_NAMUR);
select_forecast!(SelectSolarForecastOvlaanderen, SOLAR_FORECAST_OVLAANDEREN);
select_forecast!(SelectSolarForecastBrabantw, SOLAR_FORECAST_BRABANTW);
select_forecast!(SelectSolarForecastWvlaanderen, SOLAR_FORECAST_WVLAANDEREN);
select_forecast!(SelectSolarForecastVlbrabant, SOLAR_FORECAST_VLBRABANT);

pub async fn solar_forecast_region(
    store: &Store,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    region: Region,
) -> Result<SelectForecast, StoreError> {
    region_fetch_one!(store, region, SelectSolarForecast, &[start, end])
}

// MAX HOUR

#[derive(Wrapper0)]
pub struct SelectForecastMaxHour(Option<DateTime<Utc>>);

macro_rules! solar_forecast_max_hour_sql {
    ($region:path) => {
        concatcp!(
            "SELECT ts FROM ",
            table_name($region),
            "  WHERE DATE_TRUNC('day', ts) = DATE_TRUNC('day', $1::TIMESTAMP WITH TIME ZONE) ORDER BY most_recent_forecast DESC LIMIT 1;"
        )
    };
}

const SOLAR_FORECAST_MAX_HOUR_BELGIQUE: &str = solar_forecast_max_hour_sql!(Region::Belgique);
const SOLAR_FORECAST_MAX_HOUR_BRUXELLES: &str = solar_forecast_max_hour_sql!(Region::Bruxelles);
const SOLAR_FORECAST_MAX_HOUR_ANTW: &str = solar_forecast_max_hour_sql!(Region::Antw);
const SOLAR_FORECAST_MAX_HOUR_HAINAUT: &str = solar_forecast_max_hour_sql!(Region::Hainaut);
const SOLAR_FORECAST_MAX_HOUR_LIMBURG: &str = solar_forecast_max_hour_sql!(Region::Limburg);
const SOLAR_FORECAST_MAX_HOUR_LIEGE: &str = solar_forecast_max_hour_sql!(Region::Liege);
const SOLAR_FORECAST_MAX_HOUR_LUXEMBOURG: &str = solar_forecast_max_hour_sql!(Region::Luxembourg);
const SOLAR_FORECAST_MAX_HOUR_NAMUR: &str = solar_forecast_max_hour_sql!(Region::Namur);
const SOLAR_FORECAST_MAX_HOUR_OVLAANDEREN: &str = solar_forecast_max_hour_sql!(Region::OVlaanderen);
const SOLAR_FORECAST_MAX_HOUR_BRABANTW: &str = solar_forecast_max_hour_sql!(Region::BrabantW);
const SOLAR_FORECAST_MAX_HOUR_WVLAANDEREN: &str = solar_forecast_max_hour_sql!(Region::WVlaanderen);
const SOLAR_FORECAST_MAX_HOUR_VLBRABANT: &str = solar_forecast_max_hour_sql!(Region::VlBrabant);

macro_rules! select_forecast_max_hour {
  ($model:ident, $sql_ident:ident) => {
    #[derive(Wrapper0)]
    struct $model(Option<DateTime<Utc>>);

    read_model!(
      $model
      $sql_ident
      get_nullable_datetime(0, row).map($model::new).map_err(|e| e.into())
    );

    impl From<$model> for SelectForecastMaxHour {
        fn from(source: $model) -> Self {
          SelectForecastMaxHour(source.into())
        }
    }
  };
}

select_forecast_max_hour!(
    SelectSolarForecastMaxHourBelgique,
    SOLAR_FORECAST_MAX_HOUR_BELGIQUE
);
select_forecast_max_hour!(
    SelectSolarForecastMaxHourBruxelles,
    SOLAR_FORECAST_MAX_HOUR_BRUXELLES
);
select_forecast_max_hour!(SelectSolarForecastMaxHourAntw, SOLAR_FORECAST_MAX_HOUR_ANTW);
select_forecast_max_hour!(
    SelectSolarForecastMaxHourHainaut,
    SOLAR_FORECAST_MAX_HOUR_HAINAUT
);
select_forecast_max_hour!(
    SelectSolarForecastMaxHourLimburg,
    SOLAR_FORECAST_MAX_HOUR_LIMBURG
);
select_forecast_max_hour!(
    SelectSolarForecastMaxHourLiege,
    SOLAR_FORECAST_MAX_HOUR_LIEGE
);
select_forecast_max_hour!(
    SelectSolarForecastMaxHourLuxembourg,
    SOLAR_FORECAST_MAX_HOUR_LUXEMBOURG
);
select_forecast_max_hour!(
    SelectSolarForecastMaxHourNamur,
    SOLAR_FORECAST_MAX_HOUR_NAMUR
);
select_forecast_max_hour!(
    SelectSolarForecastMaxHourOvlaanderen,
    SOLAR_FORECAST_MAX_HOUR_OVLAANDEREN
);
select_forecast_max_hour!(
    SelectSolarForecastMaxHourBrabantw,
    SOLAR_FORECAST_MAX_HOUR_BRABANTW
);
select_forecast_max_hour!(
    SelectSolarForecastMaxHourWvlaanderen,
    SOLAR_FORECAST_MAX_HOUR_WVLAANDEREN
);
select_forecast_max_hour!(
    SelectSolarForecastMaxHourVlbrabant,
    SOLAR_FORECAST_MAX_HOUR_VLBRABANT
);

pub async fn solar_forecast_max_hour_region(
    store: &Store,
    date: &DateTime<Utc>,
    region: Region,
) -> Result<SelectForecastMaxHour, StoreError> {
    region_fetch_one!(store, region, SelectSolarForecastMaxHour, &[date])
}

// MONITORED CAPACITY

// #[derive(Wrapper0)]
// pub struct SelectMonCap(f64);

// const fn solar_mon_cap_sql(region: Region) -> String {
//     let table = table_name(region);
//     format!(
//         "
//         SELECT
//         ROUND(AVG(monitored_capacity))
//     FROM
//         {table}
//     WHERE
//         ts >= $ 1
//         AND ts <= $ 2;
//       "
//     )
// }
// const SOLAR_MON_CAP_BELGIQUE: &str = &solar_mon_cap_sql(Region::Belgique);
// const SOLAR_MON_CAP_BRUXELLES: &str = &solar_mon_cap_sql(Region::Bruxelles);
// const SOLAR_MON_CAP_ANTW: &str = &solar_mon_cap_sql(Region::Antw);
// const SOLAR_MON_CAP_HAINAUT: &str = &solar_mon_cap_sql(Region::Hainaut);
// const SOLAR_MON_CAP_LIMBURG: &str = &solar_mon_cap_sql(Region::Limburg);
// const SOLAR_MON_CAP_LIEGE: &str = &solar_mon_cap_sql(Region::Liege);
// const SOLAR_MON_CAP_LUXEMBOURG: &str = &solar_mon_cap_sql(Region::Luxembourg);
// const SOLAR_MON_CAP_NAMUR: &str = &solar_mon_cap_sql(Region::Namur);
// const SOLAR_MON_CAP_OVLAANDEREN: &str = &solar_mon_cap_sql(Region::OVlaanderen);
// const SOLAR_MON_CAP_BRABANTW: &str = &solar_mon_cap_sql(Region::BrabantW);
// const SOLAR_MON_CAP_WVLAANDEREN: &str = &solar_mon_cap_sql(Region::WVlaanderen);
// const SOLAR_MON_CAP_VLBRABANT: &str = &solar_mon_cap_sql(Region::VlBrabant);

// macro_rules! select_mon_cap {
//   ($model:ident, $sql_ident:ident) => {
//     #[derive(Wrapper0)]
//     struct $model(f64);

//     read_model!(
//       $model
//       $sql_ident
//       get_f64(0, row).map($model::new).map_err(|e| e.into())
//     );

//     impl From<$model> for SelectMonCap {
//         fn from(source: $model) -> Self {
//           SelectMonCap(source.into())
//         }
//     }
//   };
// }

// select_mon_cap!(SelectSolarMonCapBelgique, SOLAR_MON_CAP_BELGIQUE);
// select_mon_cap!(SelectSolarMonCapBruxelles, SOLAR_MON_CAP_BRUXELLES);
// select_mon_cap!(SelectSolarMonCapAntw, SOLAR_MON_CAP_ANTW);
// select_mon_cap!(SelectSolarMonCapHainaut, SOLAR_MON_CAP_HAINAUT);
// select_mon_cap!(SelectSolarMonCapLimburg, SOLAR_MON_CAP_LIMBURG);
// select_mon_cap!(SelectSolarMonCapLiege, SOLAR_MON_CAP_LIEGE);
// select_mon_cap!(SelectSolarMonCapLuxembourg, SOLAR_MON_CAP_LUXEMBOURG);
// select_mon_cap!(SelectSolarMonCapNamur, SOLAR_MON_CAP_NAMUR);
// select_mon_cap!(SelectSolarMonCapOvlaanderen, SOLAR_MON_CAP_OVLAANDEREN);
// select_mon_cap!(SelectSolarMonCapBrabantw, SOLAR_MON_CAP_BRABANTW);
// select_mon_cap!(SelectSolarMonCapWvlaanderen, SOLAR_MON_CAP_WVLAANDEREN);
// select_mon_cap!(SelectSolarMonCapVlbrabant, SOLAR_MON_CAP_VLBRABANT);

// pub async fn solar_mon_cap_region(
//     store: &Store,
//     start: &DateTime<Utc>,
//     end: &DateTime<Utc>,
//     region: Region,
// ) -> Result<SelectMonCap, StoreError> {
//     region_fetch_one!(store, region, SelectSolarMonCap, &[start, end])
// }

// PRODUCTION

#[derive(Wrapper0, Debug)]
pub struct SelectProduction(f64);

#[derive(Wrapper0)]
struct SelectSolarProductionBelgique(f64);

read_model!(
    SelectSolarProductionBelgique
    "
    WITH base AS (
        SELECT sp.ts AS ts,
          CASE
            WHEN sp.real_time < 0 THEN s.most_recent_forecast
            ELSE sp.real_time
          END AS realtime
        FROM solar_past sp
          LEFT JOIN solar s ON sp.ts = s.ts
        WHERE sp.ts > $1
          AND sp.ts < $2
      )
      SELECT ROUND(SUM(base.realtime))
      FROM base;
    "
    {
        get_f64(0, row) .map(SelectSolarProductionBelgique::new) .map_err(|e| e.into())
    }
);

impl From<SelectSolarProductionBelgique> for SelectProduction {
    fn from(source: SelectSolarProductionBelgique) -> Self {
        SelectProduction(source.into())
    }
}

macro_rules! solar_production_sql {
    ($region:path) => {
        concatcp!(
            "SELECT ROUND(SUM(real_time) / 4) FROM ",
            table_name($region),
            "  WHERE ts > $1 AND ts < $2 AND real_time >= 0;"
        )
    };
}

const SOLAR_PRODUCTION_BRUXELLES: &str = solar_production_sql!(Region::Bruxelles);
const SOLAR_PRODUCTION_ANTW: &str = solar_production_sql!(Region::Antw);
const SOLAR_PRODUCTION_HAINAUT: &str = solar_production_sql!(Region::Hainaut);
const SOLAR_PRODUCTION_LIMBURG: &str = solar_production_sql!(Region::Limburg);
const SOLAR_PRODUCTION_LIEGE: &str = solar_production_sql!(Region::Liege);
const SOLAR_PRODUCTION_LUXEMBOURG: &str = solar_production_sql!(Region::Luxembourg);
const SOLAR_PRODUCTION_NAMUR: &str = solar_production_sql!(Region::Namur);
const SOLAR_PRODUCTION_OVLAANDEREN: &str = solar_production_sql!(Region::OVlaanderen);
const SOLAR_PRODUCTION_BRABANTW: &str = solar_production_sql!(Region::BrabantW);
const SOLAR_PRODUCTION_WVLAANDEREN: &str = solar_production_sql!(Region::WVlaanderen);
const SOLAR_PRODUCTION_VLBRABANT: &str = solar_production_sql!(Region::VlBrabant);

macro_rules! select_forecast {
    ($model:ident, $sql_ident:ident) => {
      #[derive(Wrapper0)]
      struct $model(f64);

      read_model!(
        $model
        $sql_ident
        get_f64(0, row).map($model::new).map_err(|e| e.into())
      );

      impl From<$model> for SelectProduction {
          fn from(source: $model) -> Self {
              SelectProduction(source.into())
          }
      }
    };
}

select_forecast!(SelectSolarProductionBruxelles, SOLAR_PRODUCTION_BRUXELLES);
select_forecast!(SelectSolarProductionAntw, SOLAR_PRODUCTION_ANTW);
select_forecast!(SelectSolarProductionHainaut, SOLAR_PRODUCTION_HAINAUT);
select_forecast!(SelectSolarProductionLimburg, SOLAR_PRODUCTION_LIMBURG);
select_forecast!(SelectSolarProductionLiege, SOLAR_PRODUCTION_LIEGE);
select_forecast!(SelectSolarProductionLuxembourg, SOLAR_PRODUCTION_LUXEMBOURG);
select_forecast!(SelectSolarProductionNamur, SOLAR_PRODUCTION_NAMUR);
select_forecast!(
    SelectSolarProductionOvlaanderen,
    SOLAR_PRODUCTION_OVLAANDEREN
);
select_forecast!(SelectSolarProductionBrabantw, SOLAR_PRODUCTION_BRABANTW);
select_forecast!(
    SelectSolarProductionWvlaanderen,
    SOLAR_PRODUCTION_WVLAANDEREN
);
select_forecast!(SelectSolarProductionVlbrabant, SOLAR_PRODUCTION_VLBRABANT);

pub async fn solar_production_region(
    store: &Store,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    region: Region,
) -> Result<SelectProduction, StoreError> {
    region_fetch_one!(store, region, SelectSolarProduction, &[start, end])
}

// LOAD FACTOR

#[derive(Wrapper0, Debug)]
pub struct SelectLoadFactor(f64);

impl SelectLoadFactor {
    pub fn corrected(&self) -> SelectLoadFactor {
        // original comment: // correction: 85% de capacité correspond à 100% de prod
        //TODO my take (pm): shouldn't it be self.value() * 0.85 ? //nw response: no, it is correct ;)
        SelectLoadFactor((self.value() / 0.85).round())
    }
}

#[derive(Wrapper0)]
struct SelectSolarLoadFactorBelgique(f64);

read_model!(
    SelectSolarLoadFactorBelgique
    "
    SELECT ROUND(
        AVG(most_recent_forecast / monitored_capacity) * 100
      )
    FROM solar
    WHERE ts > $1
      AND ts < $2
      AND EXTRACT(
        HOUR
        FROM ts
      ) >= 13
      AND EXTRACT(
        HOUR
        FROM ts
      ) < 14
      AND most_recent_forecast >= 0;    
    "
    {
        get_f64(0, row) .map(SelectSolarLoadFactorBelgique::new) .map_err(|e| e.into())
    }
);

impl From<SelectSolarLoadFactorBelgique> for SelectLoadFactor {
    fn from(source: SelectSolarLoadFactorBelgique) -> Self {
        SelectLoadFactor(source.into())
    }
}

macro_rules! solar_loadfactor_sql {
    ($region:path) => {
        concatcp!(
            "SELECT ROUND(
                AVG(most_recent_forecast) /(AVG(monitored_capacity)) * 10000
              ) / 100
            FROM ",
            table_name($region),
            "  WHERE ts > $1
            AND ts < $2
            AND EXTRACT(
              HOUR
              FROM ts
            ) >= 13
            AND EXTRACT(
              HOUR
              FROM ts
            ) < 14
            AND most_recent_forecast >= 0;"
        )
    };
}

const SOLAR_LOADFACTOR_BRUXELLES: &str = solar_loadfactor_sql!(Region::Bruxelles);
const SOLAR_LOADFACTOR_ANTW: &str = solar_loadfactor_sql!(Region::Antw);
const SOLAR_LOADFACTOR_HAINAUT: &str = solar_loadfactor_sql!(Region::Hainaut);
const SOLAR_LOADFACTOR_LIMBURG: &str = solar_loadfactor_sql!(Region::Limburg);
const SOLAR_LOADFACTOR_LIEGE: &str = solar_loadfactor_sql!(Region::Liege);
const SOLAR_LOADFACTOR_LUXEMBOURG: &str = solar_loadfactor_sql!(Region::Luxembourg);
const SOLAR_LOADFACTOR_NAMUR: &str = solar_loadfactor_sql!(Region::Namur);
const SOLAR_LOADFACTOR_OVLAANDEREN: &str = solar_loadfactor_sql!(Region::OVlaanderen);
const SOLAR_LOADFACTOR_BRABANTW: &str = solar_loadfactor_sql!(Region::BrabantW);
const SOLAR_LOADFACTOR_WVLAANDEREN: &str = solar_loadfactor_sql!(Region::WVlaanderen);
const SOLAR_LOADFACTOR_VLBRABANT: &str = solar_loadfactor_sql!(Region::VlBrabant);

macro_rules! select_forecast {
    ($model:ident, $sql_ident:ident) => {
      #[derive(Wrapper0)]
      struct $model(f64);

      read_model!(
        $model
        $sql_ident
        get_f64(0, row).map($model::new).map_err(|e| e.into())
      );

      impl From<$model> for SelectLoadFactor {
          fn from(source: $model) -> Self {
              SelectLoadFactor(source.into())
          }
      }
    };
}

select_forecast!(SelectSolarLoadFactorBruxelles, SOLAR_LOADFACTOR_BRUXELLES);
select_forecast!(SelectSolarLoadFactorAntw, SOLAR_LOADFACTOR_ANTW);
select_forecast!(SelectSolarLoadFactorHainaut, SOLAR_LOADFACTOR_HAINAUT);
select_forecast!(SelectSolarLoadFactorLimburg, SOLAR_LOADFACTOR_LIMBURG);
select_forecast!(SelectSolarLoadFactorLiege, SOLAR_LOADFACTOR_LIEGE);
select_forecast!(SelectSolarLoadFactorLuxembourg, SOLAR_LOADFACTOR_LUXEMBOURG);
select_forecast!(SelectSolarLoadFactorNamur, SOLAR_LOADFACTOR_NAMUR);
select_forecast!(
    SelectSolarLoadFactorOvlaanderen,
    SOLAR_LOADFACTOR_OVLAANDEREN
);
select_forecast!(SelectSolarLoadFactorBrabantw, SOLAR_LOADFACTOR_BRABANTW);
select_forecast!(
    SelectSolarLoadFactorWvlaanderen,
    SOLAR_LOADFACTOR_WVLAANDEREN
);
select_forecast!(SelectSolarLoadFactorVlbrabant, SOLAR_LOADFACTOR_VLBRABANT);

pub async fn solar_loadfactor_region(
    store: &Store,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    region: Region,
) -> Result<SelectLoadFactor, StoreError> {
    region_fetch_one!(store, region, SelectSolarLoadFactor, &[start, end])
}

// LAST DATE

#[derive(Wrapper0)]
pub struct SelectForcastLastDate(Option<DateTime<Utc>>);

#[derive(Wrapper0)]
pub struct SelectForcastLastDateBelgique(Option<DateTime<Utc>>);

read_model!(
    SelectForcastLastDateBelgique
    "
    SELECT
    ts :: TIMESTAMP WITH TIME ZONE
    FROM solar
    ORDER BY
    ts DESC
    LIMIT
    1;
    "
    {
        get_nullable_datetime(0, row).map(SelectForcastLastDateBelgique::new).map_err(|e| e.into())
    }
);

impl From<SelectForcastLastDateBelgique> for SelectForcastLastDate {
    fn from(source: SelectForcastLastDateBelgique) -> Self {
        SelectForcastLastDate(source.into())
    }
}

macro_rules! solar_forecast_last_date_sql {
    ($region:path) => {
        concatcp!(
            "SELECT
            ts :: TIMESTAMP WITH TIME ZONE
            FROM ",
            table_name($region),
            "  ORDER BY
            ts DESC
            LIMIT
            1;"
        )
    };
}

const SOLAR_FORECAST_LAST_DATE_BELGIQUE: &str = solar_forecast_last_date_sql!(Region::Belgique);
const SOLAR_FORECAST_LAST_DATE_BRUXELLES: &str = solar_forecast_last_date_sql!(Region::Bruxelles);
const SOLAR_FORECAST_LAST_DATE_ANTW: &str = solar_forecast_last_date_sql!(Region::Antw);
const SOLAR_FORECAST_LAST_DATE_HAINAUT: &str = solar_forecast_last_date_sql!(Region::Hainaut);
const SOLAR_FORECAST_LAST_DATE_LIMBURG: &str = solar_forecast_last_date_sql!(Region::Limburg);
const SOLAR_FORECAST_LAST_DATE_LIEGE: &str = solar_forecast_last_date_sql!(Region::Liege);
const SOLAR_FORECAST_LAST_DATE_LUXEMBOURG: &str = solar_forecast_last_date_sql!(Region::Luxembourg);
const SOLAR_FORECAST_LAST_DATE_NAMUR: &str = solar_forecast_last_date_sql!(Region::Namur);
const SOLAR_FORECAST_LAST_DATE_OVLAANDEREN: &str =
    solar_forecast_last_date_sql!(Region::OVlaanderen);
const SOLAR_FORECAST_LAST_DATE_BRABANTW: &str = solar_forecast_last_date_sql!(Region::BrabantW);
const SOLAR_FORECAST_LAST_DATE_WVLAANDEREN: &str =
    solar_forecast_last_date_sql!(Region::WVlaanderen);
const SOLAR_FORECAST_LAST_DATE_VLBRABANT: &str = solar_forecast_last_date_sql!(Region::VlBrabant);

macro_rules! select_forecast_last_date {
  ($model:ident, $sql_ident:ident) => {
    #[derive(Wrapper0)]
    struct $model(Option<DateTime<Utc>>);

    read_model!(
      $model
      $sql_ident
      get_nullable_datetime(0, row).map($model::new).map_err(|e| e.into())
    );

    impl From<$model> for SelectForcastLastDate {
        fn from(source: $model) -> Self {
          SelectForcastLastDate(source.into())
        }
    }
  };
}

select_forecast_last_date!(
    SelectSolarForcastLastDateBelgique,
    SOLAR_FORECAST_LAST_DATE_BELGIQUE
);
select_forecast_last_date!(
    SelectSolarForcastLastDateBruxelles,
    SOLAR_FORECAST_LAST_DATE_BRUXELLES
);
select_forecast_last_date!(
    SelectSolarForcastLastDateAntw,
    SOLAR_FORECAST_LAST_DATE_ANTW
);
select_forecast_last_date!(
    SelectSolarForcastLastDateHainaut,
    SOLAR_FORECAST_LAST_DATE_HAINAUT
);
select_forecast_last_date!(
    SelectSolarForcastLastDateLimburg,
    SOLAR_FORECAST_LAST_DATE_LIMBURG
);
select_forecast_last_date!(
    SelectSolarForcastLastDateLiege,
    SOLAR_FORECAST_LAST_DATE_LIEGE
);
select_forecast_last_date!(
    SelectSolarForcastLastDateLuxembourg,
    SOLAR_FORECAST_LAST_DATE_LUXEMBOURG
);
select_forecast_last_date!(
    SelectSolarForcastLastDateNamur,
    SOLAR_FORECAST_LAST_DATE_NAMUR
);
select_forecast_last_date!(
    SelectSolarForcastLastDateOvlaanderen,
    SOLAR_FORECAST_LAST_DATE_OVLAANDEREN
);
select_forecast_last_date!(
    SelectSolarForcastLastDateBrabantw,
    SOLAR_FORECAST_LAST_DATE_BRABANTW
);
select_forecast_last_date!(
    SelectSolarForcastLastDateWvlaanderen,
    SOLAR_FORECAST_LAST_DATE_WVLAANDEREN
);
select_forecast_last_date!(
    SelectSolarForcastLastDateVlbrabant,
    SOLAR_FORECAST_LAST_DATE_VLBRABANT
);

pub async fn solar_forecast_last_date_region(
    store: &Store,
    region: Region,
) -> Result<SelectForcastLastDate, StoreError> {
    region_fetch_one!(store, region, SelectSolarForcastLastDate, &[])
}

// LAST PROD DATE

const fn table_name_prod(region: Region) -> &'static str {
    match region {
        Region::Belgique => "solar_past",
        Region::Bruxelles => "solar_bxl_prod",
        Region::Antw => "solar_ant_prod",
        Region::Hainaut => "solar_hai_prod",
        Region::Limburg => "solar_lim_prod",
        Region::Liege => "solar_lie_prod",
        Region::Luxembourg => "solar_lux_prod",
        Region::Namur => "solar_nam_prod",
        Region::OVlaanderen => "solar_o_vl_prod",
        Region::BrabantW => "solar_br_w_prod",
        Region::WVlaanderen => "solar_w_vl_prod",
        Region::VlBrabant => "solar_vl_b_prod",
    }
}

#[derive(Wrapper0)]
pub struct SelectProductionLastDate(Option<DateTime<Utc>>);

// #[derive(Wrapper0)]
// pub struct SelectProductionLastDateBelgique(Option<DateTime<Utc>>);

// read_model!(
//     SelectProductionLastDateBelgique
//     "
//     SELECT
//         ts :: TIMESTAMP WITH TIME ZONE
//     FROM solar_past
//     ORDER BY
//         ts DESC
//     LIMIT
//         1;
//     "
//     {
//         get_nullable_datetime(0, row).map(SelectProductionLastDateBelgique::new).map_err(|e| e.into())
//     }
// );

// impl From<SelectProductionLastDateBelgique> for SelectProductionLastDate {
//     fn from(source: SelectProductionLastDateBelgique) -> Self {
//         SelectProductionLastDate(source.into())
//     }
// }

macro_rules! solar_production_last_date_sql {
    ($region:path) => {
        concatcp!(
            "SELECT
            ts :: TIMESTAMP WITH TIME ZONE
          FROM ",
            table_name_prod($region),
            "  ORDER BY
            ts DESC
          LIMIT
            1;"
        )
    };
}

const SOLAR_PRODUCTION_LAST_DATE_BELGIQUE: &str = solar_production_last_date_sql!(Region::Belgique);
const SOLAR_PRODUCTION_LAST_DATE_BRUXELLES: &str =
    solar_production_last_date_sql!(Region::Bruxelles);
const SOLAR_PRODUCTION_LAST_DATE_ANTW: &str = solar_production_last_date_sql!(Region::Antw);
const SOLAR_PRODUCTION_LAST_DATE_HAINAUT: &str = solar_production_last_date_sql!(Region::Hainaut);
const SOLAR_PRODUCTION_LAST_DATE_LIMBURG: &str = solar_production_last_date_sql!(Region::Limburg);
const SOLAR_PRODUCTION_LAST_DATE_LIEGE: &str = solar_production_last_date_sql!(Region::Liege);
const SOLAR_PRODUCTION_LAST_DATE_LUXEMBOURG: &str =
    solar_production_last_date_sql!(Region::Luxembourg);
const SOLAR_PRODUCTION_LAST_DATE_NAMUR: &str = solar_production_last_date_sql!(Region::Namur);
const SOLAR_PRODUCTION_LAST_DATE_OVLAANDEREN: &str =
    solar_production_last_date_sql!(Region::OVlaanderen);
const SOLAR_PRODUCTION_LAST_DATE_BRABANTW: &str = solar_production_last_date_sql!(Region::BrabantW);
const SOLAR_PRODUCTION_LAST_DATE_WVLAANDEREN: &str =
    solar_production_last_date_sql!(Region::WVlaanderen);
const SOLAR_PRODUCTION_LAST_DATE_VLBRABANT: &str =
    solar_production_last_date_sql!(Region::VlBrabant);

macro_rules! select_production_last_date {
  ($model:ident, $sql_ident:ident) => {
    #[derive(Wrapper0)]
    struct $model(Option<DateTime<Utc>>);

    read_model!(
      $model
      $sql_ident
      get_nullable_datetime(0, row).map($model::new).map_err(|e| e.into())
    );

    impl From<$model> for SelectProductionLastDate {
        fn from(source: $model) -> Self {
          SelectProductionLastDate(source.into())
        }
    }
  };
}

select_production_last_date!(
    SelectSolarProductionLastDateBelgique,
    SOLAR_PRODUCTION_LAST_DATE_BELGIQUE
);
select_production_last_date!(
    SelectSolarProductionLastDateBruxelles,
    SOLAR_PRODUCTION_LAST_DATE_BRUXELLES
);
select_production_last_date!(
    SelectSolarProductionLastDateAntw,
    SOLAR_PRODUCTION_LAST_DATE_ANTW
);
select_production_last_date!(
    SelectSolarProductionLastDateHainaut,
    SOLAR_PRODUCTION_LAST_DATE_HAINAUT
);
select_production_last_date!(
    SelectSolarProductionLastDateLimburg,
    SOLAR_PRODUCTION_LAST_DATE_LIMBURG
);
select_production_last_date!(
    SelectSolarProductionLastDateLiege,
    SOLAR_PRODUCTION_LAST_DATE_LIEGE
);
select_production_last_date!(
    SelectSolarProductionLastDateLuxembourg,
    SOLAR_PRODUCTION_LAST_DATE_LUXEMBOURG
);
select_production_last_date!(
    SelectSolarProductionLastDateNamur,
    SOLAR_PRODUCTION_LAST_DATE_NAMUR
);
select_production_last_date!(
    SelectSolarProductionLastDateOvlaanderen,
    SOLAR_PRODUCTION_LAST_DATE_OVLAANDEREN
);
select_production_last_date!(
    SelectSolarProductionLastDateBrabantw,
    SOLAR_PRODUCTION_LAST_DATE_BRABANTW
);
select_production_last_date!(
    SelectSolarProductionLastDateWvlaanderen,
    SOLAR_PRODUCTION_LAST_DATE_WVLAANDEREN
);
select_production_last_date!(
    SelectSolarProductionLastDateVlbrabant,
    SOLAR_PRODUCTION_LAST_DATE_VLBRABANT
);

pub async fn solar_production_last_date_region(
    store: &Store,
    region: Region,
) -> Result<SelectProductionLastDate, StoreError> {
    region_fetch_one!(store, region, SelectSolarProductionLastDate, &[])
}
