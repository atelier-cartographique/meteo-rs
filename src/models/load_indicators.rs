
use crate::store::{Store, StoreError};
use crate::util::{get_f64, get_nullable_datetime};
use chrono::{DateTime, Utc};
use model_wrapper::{read_model, Wrapper0};

#[derive(Wrapper0)]
pub struct LoadPast(f64);

read_model!(
    LoadPast
    "SELECT
  ROUND(SUM(total_load))
FROM load_past
WHERE
  ts > $1
  AND ts < $2;"
    {
        get_f64(0, row)
            .map(LoadPast::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct LoadLastUpdate(Option<DateTime<Utc>>);

read_model!(
    LoadLastUpdate
    "SELECT
  ts :: TIMESTAMP WITH TIME ZONE
FROM load
ORDER BY
  ts DESC
LIMIT
  1;"
    {
        get_nullable_datetime(0, row)
            .map(LoadLastUpdate::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct LoadLastPastUpdate(Option<DateTime<Utc>>);

read_model!(
    LoadLastPastUpdate
    "
    SELECT
  ts :: TIMESTAMP WITH TIME ZONE
FROM load_past
ORDER BY
  ts DESC
LIMIT
  1;
    "
    {
        get_nullable_datetime(0, row)
            .map(LoadLastPastUpdate::new)
            .map_err(|e| e.into())
    }
);

impl Store {
    pub async fn load_last_update(&self) -> Result<LoadLastUpdate, StoreError> {
        self.fetch_one(&[]).await
    }

    pub async fn load_last_past_update(&self) -> Result<LoadLastPastUpdate, StoreError> {
        self.fetch_one(&[]).await
    }

    pub async fn load_past(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
    ) -> Result<LoadPast, StoreError> {
        self.fetch_one(&[start, end]).await
    }
}
