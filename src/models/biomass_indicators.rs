
use crate::store::{Store, StoreError};
use crate::util::*;
use chrono::{DateTime, Utc};
use model_wrapper::{read_model, Wrapper0};

#[derive(Wrapper0)]
pub struct BiomassProduction(f64);

#[derive(Wrapper0)]
pub struct BiomassLastPastUpdate(Option<DateTime<Utc>>);

read_model!(
    BiomassProduction
    "SELECT SUM(production) FROM biomass WHERE ts > $1 AND ts < $2;"
    get_f64(0, row).map(BiomassProduction::new).map_err(|e| e.into())
);

read_model!(
    BiomassLastPastUpdate

    "SELECT ts::TIMESTAMP WITH TIME ZONE
FROM biomass
ORDER BY ts DESC
LIMIT 1;"

    {
        get_nullable_datetime(0, row)
            .map(BiomassLastPastUpdate::new)
            .map_err(|e| e.into())
    }
);

impl Store {
    pub async fn biomass_last_past_update(&self) -> Result<BiomassLastPastUpdate, StoreError> {
        self.fetch_one(&[]).await
    }

    pub async fn biomass_production(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
    ) -> Result<BiomassProduction, StoreError> {
        self.fetch_one(&[start, end]).await
    }
}
