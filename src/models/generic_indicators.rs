use model_wrapper::{read_model, Wrapper0};

use crate::{
    store::{Store, StoreError},
    util::get_f64,
};

#[derive(Wrapper0)]
pub struct TotHousing(f64);

read_model!(
TotHousing
"SELECT tot_logements::double precision
    FROM corrections
    WHERE tot_logements IS NOT NULL
    ORDER BY ts DESC
    LIMIT 1;"
{
get_f64(0, row)
    .map(TotHousing::new)
    .map_err(|e| e.into())
});

impl Store {
    pub async fn tot_housing(&self) -> Result<TotHousing, StoreError> {
        self.fetch_one(&[]).await
    }
}
