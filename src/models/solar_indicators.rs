use super::regional::{
    solar_forecast_last_date_region, solar_forecast_max_hour_region, solar_forecast_region,
    solar_loadfactor_region, solar_production_last_date_region, solar_production_region,
    SelectForcastLastDate, SelectForecast, SelectForecastMaxHour, SelectLoadFactor,
    SelectProduction, SelectProductionLastDate,
};

use crate::store::{Region, Store, StoreError};
use crate::util::*;
use chrono::{DateTime, NaiveDate, Utc};
use model_wrapper::{read_model, Wrapper0};

#[derive(Wrapper0)]
pub struct SolarMonitoredCapacityForecast(f64);

impl SolarMonitoredCapacityForecast {
    pub fn with_correction(&self, corr: SolarCapacityCorrection) -> Self {
        SolarMonitoredCapacityForecast(self.value().max(corr.value()))
    }
}

read_model!(
   SolarMonitoredCapacityForecast
   "SELECT ROUND(AVG(monitored_capacity)) FROM solar WHERE ts >= $1 AND ts <= $2;"
   get_f64(0, row).map(SolarMonitoredCapacityForecast::new).map_err(|e| e.into())
);

#[derive(Wrapper0, Debug)]
pub struct SolarCapacityCorrection(f64);

read_model!(
    SolarCapacityCorrection
   "
SELECT ROUND(pv_power_apere * autoconsommation)
FROM corrections
WHERE 
    EXTRACT( EPOCH FROM ts ) <= EXTRACT( EPOCH FROM $1::TIMESTAMP WITH TIME ZONE )
    AND pv_power_apere IS NOT NULL
ORDER BY ts DESC LIMIT 1;
"
get_value(0, row, 0.0).map(SolarCapacityCorrection::new).map_err(|e| e.into())
);

#[derive(Wrapper0)]
pub struct SolarNumSysPV(f64);

read_model!(
    SolarNumSysPV
   "
   SELECT nb_syst_pv::double precision
   FROM corrections
   WHERE nb_syst_pv IS NOT NULL
   ORDER BY (ts::date - $1::timestamp with time zone::date) ASC
   LIMIT 1;
"
get_value(0, row, 0.0).map(SolarNumSysPV::new).map_err(|e| e.into())
);

#[derive(Wrapper0, Debug)]
pub struct SolarMonCap(f64);

read_model!(
    SolarMonCap
   "
    SELECT ROUND(AVG(monitored_capacity))
    FROM solar
    WHERE ts >= $1 AND ts <= $2;
"
get_value(0, row, 0.0).map(SolarMonCap::new).map_err(|e| e.into())
);

#[derive(Wrapper0)]
pub struct SolarProductionListItem {
    pub ts: DateTime<Utc>,
    pub value: f64,
}

read_model!(
    SolarProductionListItem
   "
   SELECT sp.ts,
    CASE
        WHEN sp.real_time < 0 THEN s.most_recent_forecast
        ELSE sp.real_time
    END
    FROM solar_past sp
    LEFT JOIN solar s ON sp.ts = s.ts
    WHERE sp.ts >= $1
    AND sp.ts <= $2
    ORDER BY sp.ts;
"
row.try_get("ts").and_then(|ts| row.try_get("real_time").map(|value| SolarProductionListItem::new((ts,value)))).map_err(|e| e.into())
);

#[derive(Wrapper0)]
pub struct SolarDailyProdListItem {
    pub date: NaiveDate,
    pub value: f64,
}

read_model!(
    SolarDailyProdListItem
   "
   SELECT date, productivite_kwh_by_kwc as prod
    FROM solar_daily_past
    WHERE date >= $1
    AND date <= $2
    ORDER BY date;
"
row.try_get("date")
    .and_then(|date| {
        row.try_get("prod").map(|value| SolarDailyProdListItem::new((date,value)))
    })
    .map_err(|e| e.into())
);

#[derive(Wrapper0)]
pub struct SolarFamilyNbForecast(f64);

impl Store {
    pub async fn solar_forecast(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
        region: Region,
    ) -> Result<SelectForecast, StoreError> {
        solar_forecast_region(self, start, end, region).await
    }

    pub async fn solar_monitored_capacity_forecast(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
    ) -> Result<SolarMonitoredCapacityForecast, StoreError> {
        self.fetch_one(&[start, end]).await
    }

    pub async fn solar_capacity_correction(
        &self,
        date: &DateTime<Utc>,
    ) -> Result<SolarCapacityCorrection, StoreError> {
        self.fetch_one(&[date]).await
    }

    pub async fn solar_nb_syst_pv(
        &self,
        date: &DateTime<Utc>,
    ) -> Result<SolarNumSysPV, StoreError> {
        self.fetch_one(&[date]).await
    }

    pub async fn solar_max_hour(
        &self,
        region: Region,
        date: &DateTime<Utc>,
    ) -> Result<SelectForecastMaxHour, StoreError> {
        solar_forecast_max_hour_region(self, date, region).await
    }

    pub async fn solar_monitored_capacity(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
    ) -> Result<SolarMonCap, StoreError> {
        self.fetch_one(&[start, end]).await
    }

    pub async fn solar_production(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
        region: Region,
    ) -> Result<SelectProduction, StoreError> {
        solar_production_region(self, start, end, region).await
    }

    pub async fn solar_production_list(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
    ) -> Result<Vec<SolarProductionListItem>, StoreError> {
        self.fetch(&[start, end]).await
    }

    pub async fn solar_daily_prod_list(
        &self,
        start: &NaiveDate,
        end: &NaiveDate,
    ) -> Result<Vec<SolarDailyProdListItem>, StoreError> {
        self.fetch(&[start, end]).await
    }

    pub async fn solar_loadfactor_forecast(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
        region: Region,
    ) -> Result<SelectLoadFactor, StoreError> {
        solar_loadfactor_region(self, start, end, region)
            .await
            .map(|lf| lf.corrected())
    }

    pub async fn solar_familly_nb_forecast(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
        region: Region,
    ) -> Result<SolarFamilyNbForecast, StoreError> {
        let td = i64_to_f64(end.signed_duration_since(*start).num_days());
        self.solar_forecast(start, end, region).await.map(|r| {
            if td > 1. {
                SolarFamilyNbForecast(r.value() / (3.5 / 365.0 * td))
            } else {
                SolarFamilyNbForecast(r.value() / (3.5 / 365.0))
            }
        })
    }

    pub async fn solar_last_forecast_date(
        &self,
        region: Region,
    ) -> Result<SelectForcastLastDate, StoreError> {
        solar_forecast_last_date_region(self, region).await
    }
    pub async fn solar_last_prod_date(
        &self,
        region: Region,
    ) -> Result<SelectProductionLastDate, StoreError> {
        solar_production_last_date_region(self, region).await
    }
}
