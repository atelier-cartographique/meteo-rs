use crate::{
    model::ModelError,
    store::{Store, StoreError},
};
use chrono::{DateTime, Utc};
use model_wrapper::{read_model, Wrapper0};

#[derive(Wrapper0)]
pub struct HydroProduction {
    ts: DateTime<Utc>,
    value: f64,
}

read_model!(
    HydroProduction

    "WITH base AS (
        SELECT to_timestamp(
                hydro.year || '-' || hydro.month || '-' || '01 00:00:00',
                'YYYY-MM-DD hh24:mi:ss'
            ) AS ts,
            production
        FROM hydro
    )
    SELECT base.ts ts,
        production
    FROM base
    WHERE ts >= date_trunc('month', $1::TIMESTAMP WITH TIME ZONE)
        AND ts < $2;"

    {
        row.try_get("ts")
            .and_then(|ts| row.try_get("production").map(|p| (ts, p)))
            .map(HydroProduction::new)
            .map_err(|_| ModelError::Unknown)
    }
);

#[derive(Wrapper0)]
pub struct HydroCorrection {
    month: i32,
    value: f64,
}

read_model!(
    HydroCorrection

    r#"SELECT "month",
    correction
FROM hydro_correction
WHERE "month" >= EXTRACT(
        MONTH
        FROM $1::TIMESTAMP WITH TIME ZONE
    )
    OR "month" <= EXTRACT(
        MONTH
        FROM $2::TIMESTAMP WITH TIME ZONE
    );"#

    {
        row.try_get("month")
            .and_then(|month| {
                row.try_get("correction")
                    .map(|correction| (month, correction))
            })
            .map(HydroCorrection::new)
            .map_err(|_| ModelError::Unknown)
    }
);

impl Store {
    pub async fn hydro_production(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
    ) -> Result<Vec<HydroProduction>, StoreError> {
        self.fetch(&[start, end]).await
    }

    pub async fn hydro_correction(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
    ) -> Result<Vec<HydroCorrection>, StoreError> {
        self.fetch(&[start, end]).await
    }
}

// #[cfg(test)]
// mod test {
//     use crate::models::hydro_indicators::*;
//     use chrono::Utc;
//     #[test]
//     fn hydro_prod_manip() {
//         let mut t = (Utc::now(), 1.23f64);
//         let hp = HydroProduction::from(t);
//         t = hp.into();

//     }
// }
