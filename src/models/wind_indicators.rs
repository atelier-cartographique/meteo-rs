use crate::model::ModelError;
use crate::store::{Offshore, Store, StoreError};
use crate::util::*;
use chrono::{DateTime, Utc};
use model_wrapper::{read_model, Wrapper0};

use super::shore::{
    wind_forcastlist, wind_forecast, wind_forecastlastdate, wind_loadfactorforecast,
    wind_monitoredcapacityforecast, wind_monitoredcapacityproduction, wind_prodlist,
    wind_production, wind_productionlastdate, WindForcastListItem, WindForecast,
    WindForecastLastDate, WindLoadFactorForecast, WindMonitoredCapacityForecast,
    WindMonitoredCapacityProduction, WindProdListItem, WindProduction, WindProductionLastDate,
};

#[derive(Wrapper0, Debug)]
pub struct WindOffshoreCapacityCorrection(f64);

read_model!(
    WindOffshoreCapacityCorrection
   "
    SELECT ROUND(AVG(eol_offshore_power_apere))
    FROM corrections
    WHERE EXTRACT(
            MONTH
            FROM ts
        ) = EXTRACT(
            MONTH
            FROM $1 :: TIMESTAMP WITH TIME ZONE
        )
        AND EXTRACT(
            YEAR
            FROM ts
        ) = EXTRACT(
            YEAR
            FROM $1 :: TIMESTAMP WITH TIME ZONE
        );
   "
   get_f64(0, row).map(WindOffshoreCapacityCorrection::new).map_err(|e| ModelError::Wrapped(format!("<wrapped> {e}")))
);

#[derive(Wrapper0)]
pub struct WindOnshoreCapacityCorrection(f64);

read_model!(
    WindOnshoreCapacityCorrection
   "
   SELECT ROUND(
        AVG(
            eol_onshore_rw_power_apere + eol_onshore_rf_power_apere
        )
    )
    FROM corrections
    WHERE EXTRACT(
        MONTH
        FROM ts
    ) = EXTRACT(
        MONTH
        FROM $1::TIMESTAMP WITH TIME ZONE
    )
    AND EXTRACT(
        YEAR
        FROM ts
    ) = EXTRACT(
        YEAR
        FROM $1::TIMESTAMP WITH TIME ZONE
    );
   "
   get_f64(0, row).map(WindOnshoreCapacityCorrection::new).map_err(|e| e.into())
);

#[derive(Wrapper0)]
pub struct WindOffshoreQuant(f64);

read_model!(
    WindOffshoreQuant
   "
   SELECT nb_offshore::double precision
   FROM corrections
   WHERE nb_offshore IS NOT NULL
   ORDER BY (ts::date - $1::timestamp with time zone::date) ASC
   LIMIT 1;
   "
   get_f64(0, row).map(WindOffshoreQuant::new).map_err(|e| e.into())
);

#[derive(Wrapper0)]
pub struct WindOnshoreQuant(f64);

read_model!(
    WindOnshoreQuant
   "
   SELECT nb_onshore::double precision
   FROM corrections
   WHERE nb_onshore IS NOT NULL
   ORDER BY (ts::date - $1::timestamp with time zone::date) ASC
   LIMIT 1;
   "
   get_f64(0, row).map(WindOnshoreQuant::new).map_err(|e| e.into())
);

impl Store {
    pub async fn wind_forecast(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
        is_offshore: Offshore,
    ) -> Result<WindForecast, StoreError> {
        wind_forecast(self, start, end, is_offshore).await
    }

    pub async fn wind_forecast_list(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
        is_offshore: Offshore,
    ) -> Result<Vec<WindForcastListItem>, StoreError> {
        wind_forcastlist(self, start, end, is_offshore).await
    }

    pub async fn wind_monitored_capacity_forecast(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
        is_offshore: Offshore,
    ) -> Result<WindMonitoredCapacityForecast, StoreError> {
        wind_monitoredcapacityforecast(self, start, end, is_offshore).await
    }

    pub async fn wind_prod_list(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
        is_offshore: Offshore,
    ) -> Result<Vec<WindProdListItem>, StoreError> {
        wind_prodlist(self, start, end, is_offshore).await
    }

    pub async fn wind_monitored_capacity(
        &self,
        start: &DateTime<Utc>,
        is_offshore: Offshore,
    ) -> Result<WindMonitoredCapacityProduction, StoreError> {
        wind_monitoredcapacityproduction(self, start, is_offshore).await
    }

    pub async fn wind_offshore_capacity_correction(
        &self,
        start: &DateTime<Utc>,
    ) -> Result<WindOffshoreCapacityCorrection, StoreError> {
        self.fetch_one(&[start]).await
    }
    pub async fn wind_onshore_capacity_correction(
        &self,
        start: &DateTime<Utc>,
    ) -> Result<WindOnshoreCapacityCorrection, StoreError> {
        self.fetch_one(&[start]).await
    }

    pub async fn wind_production(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
        is_offshore: Offshore,
    ) -> Result<WindProduction, StoreError> {
        wind_production(self, start, end, is_offshore).await
    }

    pub async fn wind_loadfactor_forecast(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
        is_offshore: Offshore,
    ) -> Result<WindLoadFactorForecast, StoreError> {
        wind_loadfactorforecast(self, start, end, is_offshore).await
    }

    pub async fn wind_forecast_last_date(
        &self,
        is_offshore: Offshore,
    ) -> Result<WindForecastLastDate, StoreError> {
        wind_forecastlastdate(self, is_offshore).await
    }

    pub async fn wind_production_last_date(
        &self,
        is_offshore: Offshore,
    ) -> Result<WindProductionLastDate, StoreError> {
        wind_productionlastdate(self, is_offshore).await
    }

    // nb logements couverts par éolien = production belge/ conso moyenne
    // où conso moyenne =   3,5* nb jours / 365
    pub async fn wind_familly_nb_forecast(
        &self,
        start: &DateTime<Utc>,
        end: &DateTime<Utc>,
        is_offshore: Offshore,
    ) -> Result<f64, StoreError> {
        let td = i64_to_f64(end.signed_duration_since(*start).num_days()); // count until next day
        let r = self.wind_forecast(start, end, is_offshore).await?;
        if td > 1. {
            Ok(r.value() / (3.5 / 365.0 * td))
        } else {
            Ok(r.value() * 365.0 / 3.5)
        }
    }

    pub async fn nb_onshore(&self, end: &DateTime<Utc>) -> Result<WindOnshoreQuant, StoreError> {
        self.fetch_one(&[end]).await
    }
    pub async fn nb_offshore(&self, end: &DateTime<Utc>) -> Result<WindOffshoreQuant, StoreError> {
        self.fetch_one(&[end]).await
    }
}
