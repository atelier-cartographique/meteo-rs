use crate::store::{Store, StoreError};
use crate::util::*;
use crate::{store::Offshore};
use chrono::{DateTime, Utc};
use model_wrapper::{read_model, Wrapper0};
use paste::paste;

macro_rules! shore_fetch_one {
    ($store:ident, $shore:ident, $prefix:ident, $params:expr) => {
        paste! {
            match $shore {
                Offshore::None => $store.fetch_one::<[< $prefix >]>($params).await,
                Offshore::True => {
                    $store.fetch_one::<[< $prefix Offshore>]>($params)
                        .await
                        .map(|r| r.into())
                }
                Offshore::False => $store.fetch_one::<[< $prefix Onshore>]>($params).await
                .map(|r| r.into()),
            }
        }
    };
}

macro_rules! shore_fetch {
    ($store:ident, $shore:ident, $prefix:ident, $params:expr) => {
        paste! {
            match $shore {
                Offshore::None => $store.fetch::<[< $prefix >]>($params).await,
                Offshore::True => {
                    $store.fetch::<[< $prefix Offshore>]>($params)
                        .await
                        .map(|v| v.into_iter().map(|r|$prefix::new(r.into())).collect::<Vec<_>>())
                }
                Offshore::False => $store.fetch::<[< $prefix Onshore>]>($params).await
                .map(|v| v.into_iter().map(|r|$prefix::new(r.into())).collect::<Vec<_>>()),
            }
        }
    };
}

// WIND_FORECAST

#[derive(Wrapper0)]
pub struct WindForecast(f64);

read_model!(
    WindForecast
    "
    WITH w_base AS (
        SELECT MIN(ts) AS ts,
          (SUM(most_recent_forecast) / 4) AS val,
          AVG(monitored_capacity) AS mc
        FROM wind
        WHERE ts > $1
          AND ts < $2
      ),
      corr AS (
        SELECT ts,
          c.eol_offshore_power_apere + c.eol_onshore_rw_power_apere + c.eol_onshore_rf_power_apere AS corr_val
        FROM corrections AS c
        WHERE ts > $1
          AND ts < $2
      )
      SELECT wb.val / wb.mc * (
          CASE
            WHEN wb.mc < corr_val THEN corr_val
            ELSE wb.mc
          END
        ) AS most_recent_forecast
      FROM w_base wb
        LEFT JOIN corr c ON (
          EXTRACT(
            MONTH
            FROM wb.ts
          ) = EXTRACT(
            MONTH
            FROM c.ts
          )
          AND EXTRACT(
            YEAR
            FROM wb.ts
          ) = EXTRACT(
            YEAR
            FROM c.ts
          )
        )
      LIMIT 1;
    "
    {
        get_f64(0, row)
            .map(WindForecast::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindForecastOffshore(f64);

impl From<WindForecastOffshore> for WindForecast {
    fn from(source: WindForecastOffshore) -> Self {
        WindForecast(source.value())
    }
}

read_model!(
    WindForecastOffshore
    "
    WITH w_base AS (
        SELECT MIN(ts) AS ts,
          (SUM(most_recent_forecast) / 4) AS val,
          AVG(monitored_capacity) AS mc
        FROM wind_offshore
        WHERE ts > $1
          AND ts < $2
      ),
      corr AS (
        SELECT ts,
          c.eol_offshore_power_apere AS corr_val
        FROM corrections AS c
        WHERE ts > $1
          AND ts < $2
      )
      SELECT wb.val / wb.mc * (
          CASE
            WHEN wb.mc < corr_val THEN corr_val
            ELSE wb.mc
          END
        ) AS most_recent_forecast
      FROM w_base wb
        LEFT JOIN corr c ON (
          EXTRACT(
            MONTH
            FROM wb.ts
          ) = EXTRACT(
            MONTH
            FROM c.ts
          )
          AND EXTRACT(
            YEAR
            FROM wb.ts
          ) = EXTRACT(
            YEAR
            FROM c.ts
          )
        )
      LIMIT 1;
    "
    {
        get_f64(0, row)
            .map(WindForecastOffshore::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindForecastOnshore(f64);

impl From<WindForecastOnshore> for WindForecast {
    fn from(source: WindForecastOnshore) -> Self {
        WindForecast(source.value())
    }
}
read_model!(
    WindForecastOnshore
    "
    WITH w_base AS (
        SELECT MIN(ts) AS ts,
          (SUM(most_recent_forecast) / 4) AS val,
          AVG(monitored_capacity) AS mc
        FROM wind_onshore
        WHERE ts >= $1::TIMESTAMP WITH TIME ZONE
          AND ts < $2::TIMESTAMP WITH TIME ZONE
      ),
      corr AS (
        SELECT ts,
          (
            eol_onshore_rw_power_apere + eol_onshore_rf_power_apere
          ) AS corr_val
        FROM corrections AS c
        WHERE EXTRACT(
            EPOCH
            FROM ts
          ) < EXTRACT(
            EPOCH
            FROM $2
          )
          AND eol_onshore_rw_power_apere IS NOT NULL
          AND eol_onshore_rf_power_apere IS NOT NULL
        ORDER BY ts DESC
        LIMIT 1
      )
      SELECT ROUND(
          wb.val / wb.mc * (
            CASE
              WHEN wb.mc < c.corr_val THEN c.corr_val
              ELSE wb.mc
            END
          )
        ) AS most_recent_forecast
      FROM w_base wb
        LEFT JOIN corr c ON (
          EXTRACT(
            MONTH
            FROM wb.ts
          ) = EXTRACT(
            MONTH
            FROM c.ts
          )
          AND EXTRACT(
            YEAR
            FROM wb.ts
          ) = EXTRACT(
            YEAR
            FROM c.ts
          )
        )
      LIMIT 1;
    "
    {
        get_f64(0, row)
            .map(WindForecastOnshore::new)
            .map_err(|e| e.into())
    }
);

pub async fn wind_forecast(
    store: &Store,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    shore: Offshore,
) -> Result<WindForecast, StoreError> {
    shore_fetch_one!(store, shore, WindForecast, &[start, end])
}

// WIND_FORECAST_LIST

#[derive(Wrapper0)]
pub struct WindForcastListItem {
    ts: DateTime<Utc>,
    value: f64,
}

read_model!(
    WindForcastListItem
    "
    SELECT ts,
    most_recent_forecast
    FROM wind
    WHERE ts > $1
        AND ts < $2
    ORDER BY ts ASC;
    "
    {
        row.try_get("ts")
        .and_then(|ts| row.try_get("most_recent_forecast")
        .map(|value| WindForcastListItem::new((ts,value))))
        .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindForcastListItemOffshore {
    ts: DateTime<Utc>,
    value: f64,
}

impl From<WindForcastListItemOffshore> for WindForcastListItem {
    fn from(source: WindForcastListItemOffshore) -> Self {
        WindForcastListItem::new(source.into())
    }
}

read_model!(
    WindForcastListItemOffshore
    "
    SELECT ts,
  most_recent_forecast
FROM wind_offshore
WHERE ts > $1
  AND ts < $2
ORDER BY ts ASC;
    "
    {
        row.try_get("ts")
        .and_then(|ts| row.try_get("most_recent_forecast")
        .map(|value| WindForcastListItemOffshore::new((ts,value))))
        .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindForcastListItemOnshore {
    ts: DateTime<Utc>,
    value: f64,
}

impl From<WindForcastListItemOnshore> for WindForcastListItem {
    fn from(source: WindForcastListItemOnshore) -> Self {
        WindForcastListItem::new(source.into())
    }
}

read_model!(
    WindForcastListItemOnshore
    "
    SELECT ts,
    most_recent_forecast
  FROM wind_onshore
  WHERE ts > $1
    AND ts < $2
  ORDER BY ts ASC;
    "
    {
        row.try_get("ts")
        .and_then(|ts| row.try_get("most_recent_forecast")
        .map(|value| WindForcastListItemOnshore::new((ts,value))))
        .map_err(|e| e.into())
    }
);

pub async fn wind_forcastlist(
    store: &Store,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    shore: Offshore,
) -> Result<Vec<WindForcastListItem>, StoreError> {
    shore_fetch!(store, shore, WindForcastListItem, &[start, end])
}

// WIND_MONITORED_CAPACITY_FORECAST

#[derive(Wrapper0)]
pub struct WindMonitoredCapacityForecast(f64);

read_model!(
    WindMonitoredCapacityForecast
    "
    SELECT ROUND(AVG(monitored_capacity))
    FROM wind
    WHERE 
      ts >= $1
      AND ts <= $2;
    "
    {
        get_f64(0, row)
            .map(WindMonitoredCapacityForecast::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindMonitoredCapacityForecastOffshore(f64);

impl From<WindMonitoredCapacityForecastOffshore> for WindMonitoredCapacityForecast {
    fn from(source: WindMonitoredCapacityForecastOffshore) -> Self {
        WindMonitoredCapacityForecast(source.value())
    }
}

read_model!(
    WindMonitoredCapacityForecastOffshore
    "
    SELECT ROUND(AVG(monitored_capacity))
    FROM wind_offshore
    WHERE 
      ts >= $1
      AND ts <= $2;
    "
    {
        get_f64(0, row)
            .map(WindMonitoredCapacityForecastOffshore::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindMonitoredCapacityForecastOnshore(f64);

impl From<WindMonitoredCapacityForecastOnshore> for WindMonitoredCapacityForecast {
    fn from(source: WindMonitoredCapacityForecastOnshore) -> Self {
        WindMonitoredCapacityForecast(source.value())
    }
}
read_model!(
    WindMonitoredCapacityForecastOnshore
    "
    SELECT ROUND(AVG(monitored_capacity))
    FROM wind_onshore
    WHERE 
      ts >= $1
      AND ts <= $2;
    "
    {
        get_f64(0, row)
            .map(WindMonitoredCapacityForecastOnshore::new)
            .map_err(|e| e.into())
    }
);

pub async fn wind_monitoredcapacityforecast(
    store: &Store,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    shore: Offshore,
) -> Result<WindMonitoredCapacityForecast, StoreError> {
    shore_fetch_one!(store, shore, WindMonitoredCapacityForecast, &[start, end])
}

// WIND_PROD_LIST

#[derive(Wrapper0)]
pub struct WindProdListItem {
    ts: DateTime<Utc>,
    value: f64,
}

read_model!(
    WindProdListItem
    "
    SELECT ts,
    real_time
  FROM wind_past
  WHERE ts >= $1
    AND ts <= $2;
    "
    {
        row.try_get("ts")
        .and_then(|ts| row.try_get("real_time")
        .map(|value| WindProdListItem::new((ts,value))))
        .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindProdListItemOffshore {
    ts: DateTime<Utc>,
    value: f64,
}

impl From<WindProdListItemOffshore> for WindProdListItem {
    fn from(source: WindProdListItemOffshore) -> Self {
        WindProdListItem::new(source.into())
    }
}

read_model!(
    WindProdListItemOffshore
    "
    SELECT wp.ts,
    CASE
      WHEN wp.real_time IS NULL THEN w.most_recent_forecast
      ELSE wp.real_time
    END
  FROM wind_past_offshore wp
    LEFT JOIN wind_offshore w ON wp.ts = w.ts
  WHERE wp.ts >= $1
    AND wp.ts <= $2
  ORDER BY wp.ts;
    "
    {
        row.try_get("ts")
        .and_then(|ts| row.try_get("real_time")
        .map(|value| WindProdListItemOffshore::new((ts,value))))
        .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindProdListItemOnshore {
    ts: DateTime<Utc>,
    value: f64,
}

impl From<WindProdListItemOnshore> for WindProdListItem {
    fn from(source: WindProdListItemOnshore) -> Self {
        WindProdListItem::new(source.into())
    }
}

read_model!(
    WindProdListItemOnshore
    "
    SELECT wp.ts,
    CASE
      WHEN wp.real_time IS NULL THEN w.most_recent_forecast
      ELSE wp.real_time
    END
  FROM wind_past_onshore wp
    LEFT JOIN wind_onshore w ON wp.ts = w.ts
  WHERE wp.ts >= $1
    AND wp.ts <= $2
  ORDER BY wp.ts;
    "
    {
        row.try_get("ts")
        .and_then(|ts| row.try_get("real_time")
        .map(|value| WindProdListItemOnshore::new((ts,value))))
        .map_err(|e| e.into())
    }
);

pub async fn wind_prodlist(
    store: &Store,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    shore: Offshore,
) -> Result<Vec<WindProdListItem>, StoreError> {
    shore_fetch!(store, shore, WindProdListItem, &[start, end])
}

// WIND_MONITORED_CAPACITY

#[derive(Wrapper0, Debug)]
pub struct WindMonitoredCapacityProduction(f64);

read_model!(
    WindMonitoredCapacityProduction
    "
    SELECT
  ROUND(AVG(monitored_capacity))
FROM wind_past
WHERE
  date_trunc('hour', ts) = date_trunc('hour', $1 :: TIMESTAMP WITH TIME ZONE);
    "
    {
        get_f64(0, row)
            .map(WindMonitoredCapacityProduction::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindMonitoredCapacityProductionOffshore(f64);

impl From<WindMonitoredCapacityProductionOffshore> for WindMonitoredCapacityProduction {
    fn from(source: WindMonitoredCapacityProductionOffshore) -> Self {
        WindMonitoredCapacityProduction(source.value())
    }
}

read_model!(
    WindMonitoredCapacityProductionOffshore
    "
    SELECT
  ROUND(AVG(monitored_capacity))
FROM wind_past_offshore
WHERE
  date_trunc('hour', ts) = date_trunc('hour', $1 :: TIMESTAMP WITH TIME ZONE);
    "
    {
        get_f64(0, row)
            .map(WindMonitoredCapacityProductionOffshore::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindMonitoredCapacityProductionOnshore(f64);

impl From<WindMonitoredCapacityProductionOnshore> for WindMonitoredCapacityProduction {
    fn from(source: WindMonitoredCapacityProductionOnshore) -> Self {
        WindMonitoredCapacityProduction(source.value())
    }
}
read_model!(
    WindMonitoredCapacityProductionOnshore
    "
    SELECT
    ROUND(AVG(monitored_capacity))
  FROM wind_past_onshore
  WHERE
    date_trunc('hour', ts) = date_trunc('hour', $1 :: TIMESTAMP WITH TIME ZONE);
    "
    {
        get_f64(0, row)
            .map(WindMonitoredCapacityProductionOnshore::new)
            .map_err(|e| e.into())
    }
);

pub async fn wind_monitoredcapacityproduction(
    store: &Store,
    start: &DateTime<Utc>,
    shore: Offshore,
) -> Result<WindMonitoredCapacityProduction, StoreError> {
    shore_fetch_one!(store, shore, WindMonitoredCapacityProduction, &[start])
}

// WIND_PRODUCTION

#[derive(Wrapper0, Debug)]
pub struct WindProduction(f64);

read_model!(
    WindProduction
    "
    SELECT
  ROUND(SUM(real_time))
FROM wind_past
WHERE
  ts > $1
  AND ts < $2;
    "
    {
        get_f64(0, row)
            .map(WindProduction::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindProductionOffshore(f64);

impl From<WindProductionOffshore> for WindProduction {
    fn from(source: WindProductionOffshore) -> Self {
        WindProduction(source.value())
    }
}

read_model!(
    WindProductionOffshore
    "
    SELECT
  ROUND(SUM(real_time))
FROM wind_past_offshore
WHERE
  ts > $1
  AND ts < $2;
    "
    {
        get_f64(0, row)
            .map(WindProductionOffshore::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindProductionOnshore(f64);

impl From<WindProductionOnshore> for WindProduction {
    fn from(source: WindProductionOnshore) -> Self {
        WindProduction(source.value())
    }
}
read_model!(
    WindProductionOnshore
    "
    SELECT
  ROUND(SUM(real_time))
FROM wind_past_onshore
WHERE
  ts > $1
  AND ts < $2;
    "
    {
        get_f64(0, row)
            .map(WindProductionOnshore::new)
            .map_err(|e| e.into())
    }
);

pub async fn wind_production(
    store: &Store,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    shore: Offshore,
) -> Result<WindProduction, StoreError> {
    shore_fetch_one!(store, shore, WindProduction, &[start, end])
}

// WIND_LOADFACTOR_FORECAST

#[derive(Wrapper0)]
pub struct WindLoadFactorForecast(f64);

read_model!(
    WindLoadFactorForecast
    "
    SELECT
  ROUND(
    AVG(most_recent_forecast) /(AVG(monitored_capacity)) * 100
  )
FROM wind
WHERE
  ts > $1
  AND ts < $2;
    "
    {
        get_f64(0, row)
            .map(WindLoadFactorForecast::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindLoadFactorForecastOffshore(f64);

impl From<WindLoadFactorForecastOffshore> for WindLoadFactorForecast {
    fn from(source: WindLoadFactorForecastOffshore) -> Self {
        WindLoadFactorForecast(source.value())
    }
}

read_model!(
    WindLoadFactorForecastOffshore
    "
    SELECT
    ROUND(
      AVG(most_recent_forecast) /(AVG(monitored_capacity)) * 100
    )
  FROM wind_offshore
  WHERE
    ts > $1
    AND ts < $2;
    "
    {
        get_f64(0, row)
            .map(WindLoadFactorForecastOffshore::new)
            .map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindLoadFactorForecastOnshore(f64);

impl From<WindLoadFactorForecastOnshore> for WindLoadFactorForecast {
    fn from(source: WindLoadFactorForecastOnshore) -> Self {
        WindLoadFactorForecast(source.value())
    }
}
read_model!(
    WindLoadFactorForecastOnshore
    "
    SELECT
    ROUND(
      AVG(most_recent_forecast) /(AVG(monitored_capacity)) * 100
    )
  FROM wind_onshore
  WHERE
    ts > $1
    AND ts < $2;
    "
    {
        get_f64(0, row)
            .map(WindLoadFactorForecastOnshore::new)
            .map_err(|e| e.into())
    }
);

pub async fn wind_loadfactorforecast(
    store: &Store,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    shore: Offshore,
) -> Result<WindLoadFactorForecast, StoreError> {
    shore_fetch_one!(store, shore, WindLoadFactorForecast, &[start, end])
}

// WIND_LAST_DATE

#[derive(Wrapper0)]
pub struct WindForecastLastDate(Option<DateTime<Utc>>);

read_model!(
    WindForecastLastDate
    "
    SELECT
  ts :: TIMESTAMP WITH TIME ZONE
FROM wind
ORDER BY
  ts DESC
LIMIT
  1;
    "
    {
        get_nullable_datetime(0, row).map(WindForecastLastDate::new).map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindForecastLastDateOffshore(Option<DateTime<Utc>>);

impl From<WindForecastLastDateOffshore> for WindForecastLastDate {
    fn from(source: WindForecastLastDateOffshore) -> Self {
        WindForecastLastDate(source.into())
    }
}

read_model!(
    WindForecastLastDateOffshore
    "
    SELECT
  ts :: TIMESTAMP WITH TIME ZONE
FROM wind_offshore
ORDER BY
  ts DESC
LIMIT
  1;
    "
    {
        get_nullable_datetime(0, row).map(WindForecastLastDateOffshore::new).map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindForecastLastDateOnshore(Option<DateTime<Utc>>);

impl From<WindForecastLastDateOnshore> for WindForecastLastDate {
    fn from(source: WindForecastLastDateOnshore) -> Self {
        WindForecastLastDate(source.into())
    }
}
read_model!(
    WindForecastLastDateOnshore
    "
    SELECT
  ts :: TIMESTAMP WITH TIME ZONE
FROM wind_onshore
ORDER BY
  ts DESC
LIMIT
  1;
    "
    {
        get_nullable_datetime(0, row).map(WindForecastLastDateOnshore::new).map_err(|e| e.into())
    }
);

pub async fn wind_forecastlastdate(
    store: &Store,
    shore: Offshore,
) -> Result<WindForecastLastDate, StoreError> {
    shore_fetch_one!(store, shore, WindForecastLastDate, &[])
}

// WIND_LAST_PROD_DATE

#[derive(Wrapper0)]
pub struct WindProductionLastDate(Option<DateTime<Utc>>);

read_model!(
    WindProductionLastDate
    "
    SELECT
  ts :: TIMESTAMP WITH TIME ZONE
FROM wind_past
ORDER BY
  ts DESC
LIMIT
  1;
    "
    {
        get_nullable_datetime(0, row).map(WindProductionLastDate::new).map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindProductionLastDateOffshore(Option<DateTime<Utc>>);

impl From<WindProductionLastDateOffshore> for WindProductionLastDate {
    fn from(source: WindProductionLastDateOffshore) -> Self {
        WindProductionLastDate(source.into())
    }
}

read_model!(
    WindProductionLastDateOffshore
    "
    SELECT
  ts :: TIMESTAMP WITH TIME ZONE
FROM wind_past_offshore
ORDER BY
  ts DESC
LIMIT
  1;
    "
    {
        get_nullable_datetime(0, row).map(WindProductionLastDateOffshore::new).map_err(|e| e.into())
    }
);

#[derive(Wrapper0)]
pub struct WindProductionLastDateOnshore(Option<DateTime<Utc>>);

impl From<WindProductionLastDateOnshore> for WindProductionLastDate {
    fn from(source: WindProductionLastDateOnshore) -> Self {
        WindProductionLastDate(source.into())
    }
}
read_model!(
    WindProductionLastDateOnshore
    "
    SELECT
  ts :: TIMESTAMP WITH TIME ZONE
FROM wind_past_onshore
ORDER BY
  ts DESC
LIMIT
  1;
    "
    {
        get_nullable_datetime(0, row).map(WindProductionLastDateOnshore::new).map_err(|e| e.into())
    }
);

pub async fn wind_productionlastdate(
    store: &Store,
    shore: Offshore,
) -> Result<WindProductionLastDate, StoreError> {
    shore_fetch_one!(store, shore, WindProductionLastDate, &[])
}
