use chrono::{DateTime, Utc};
use warp::Rejection;

use crate::{
    html::Node,
    models::wind_indicators::{WindOffshoreCapacityCorrection, WindOnshoreCapacityCorrection},
    plot::piechart::prod_pie_chart,
    production::hydro::{hydro_value, mw_to_gwh},
    render::PastProduction,
    store::{ArcStore, Offshore, Region},
};

fn check_divider_null(val: f64, div: f64) -> f64 {
    if div > 0. {
        val / div
    } else {
        0.
    }
}

async fn past_prod(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
) -> Result<PastProduction, Rejection> {
    // println!("PRODUCTION DATES s:{:?}, e:{:?}", start, end);
    //pie chart data
    let wind_off_prod = store.wind_production(start, end, Offshore::True).await;
    let wind_on_prod = store.wind_production(start, end, Offshore::False).await;
    let solar_prod = store.solar_production(start, end, Region::Belgique).await;

    let biomass_prod = (store.biomass_production(start, end).await)
        .map(|l| l.value())
        .unwrap_or(0.);
    let waste_prod = (store.waste_production(start, end).await)
        .map(|w| w.value())
        .unwrap_or(0.);

    let load = (store.load_past(start, end).await)
        .map(|l| l.value())
        .unwrap_or(0.);

    let solar_mon_capacity = store.solar_monitored_capacity(start, end).await;
    let solar_correction = store.solar_capacity_correction(start).await;

    let solar_prod_corrected = match (solar_prod, solar_mon_capacity, solar_correction) {
        (Ok(sp), Ok(smc), Ok(sc)) => {
            check_divider_null(sp.value(), smc.value()) * smc.value().max(sc.value())
        }
        (_, _, _) => 0.,
    };

    let offshore_correction = store
        .wind_offshore_capacity_correction(start)
        .await
        .unwrap_or(WindOffshoreCapacityCorrection::new(0.0));
    let onshore_correction = store
        .wind_onshore_capacity_correction(start)
        .await
        .unwrap_or(WindOnshoreCapacityCorrection::new(0.0));

    let offshore_mon_capacity = store.wind_monitored_capacity(start, Offshore::True).await;

    let onshore_mon_capacity = store.wind_monitored_capacity(start, Offshore::False).await;

    let wind_on_month_prod_corrected = match (wind_on_prod, onshore_mon_capacity) {
        (Ok(wop), Ok(omc)) => {
            check_divider_null(wop.value(), omc.value())
                * omc.value().max(onshore_correction.value())
        }
        (_, _) => 0.,
    };

    let wind_off_month_prod_corrected = match (wind_off_prod, offshore_mon_capacity) {
        (Ok(wop), Ok(omc)) => {
            check_divider_null(wop.value(), omc.value())
                * omc.value().max(offshore_correction.value())
        }
        (_, _) => 0.,
    };

    Ok(PastProduction {
        wind_off: mw_to_gwh(wind_off_month_prod_corrected),
        wind_on: mw_to_gwh(wind_on_month_prod_corrected),
        solar: mw_to_gwh(solar_prod_corrected),
        hydro: hydro_value(store, &start, &end).await,
        biomass: biomass_prod / 1000., //to get GWh in place of MWh
        waste: waste_prod / 1000.,
        total_load: mw_to_gwh(load),
    })
}

pub async fn render_prod_piechart(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
) -> Result<Node, Rejection> {
    let prod = past_prod(store, start, end).await?;
    Ok(Node::from(prod_pie_chart(prod)))
    // match  {
    //     Ok(prod) => Node::from(prod_pie_chart(prod, 200.)),
    //     _ => Node::from(render_error("piechart error")),
    // }
}
