use chrono::{DateTime, Datelike, NaiveDate, Utc};
use std::f64;
use warp::Rejection;

use crate::{
    html::*,
    plot::{
        base::{first_year, VIEWBOX_HEIGHT, VIEWBOX_WIDTH},
        histogram::{month_histogram, solar_histogram},
    },
    render::EnergyType,
    store::ArcStore,
    util::{begin_year, end_year},
};

async fn solar_prod(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
) -> Result<Vec<(DateTime<Utc>, f64)>, Rejection> {
    let mon_capacity = store.solar_monitored_capacity(start, end).await;

    let correction = store.solar_capacity_correction(start).await;
    let data = store.solar_production_list(start, end).await;

    let data_corrected = match (data, mon_capacity) {
        (Ok(data), Ok(mc)) => match correction {
            Ok(c) => data
                .iter()
                .map(|item| {
                    (
                        *item.ts(),
                        item.value() / mc.value() * (mc.value().max(c.value())),
                    )
                })
                .collect::<Vec<_>>(),
            Err(_) => data
                .iter()
                .map(|item| (*item.ts(), *item.value()))
                .collect::<Vec<_>>(),
        },
        (_, _) => Vec::new(),
    };

    Ok(data_corrected)
}

async fn solar_daily_productivity(
    store: &ArcStore,
    start: &NaiveDate,
    end: &NaiveDate,
) -> Result<Vec<(NaiveDate, f64)>, Rejection> {
    match store.solar_daily_prod_list(start, end).await {
        Ok(data) => Ok(data
            .iter()
            .map(|item| (*item.date(), *item.value()))
            .collect::<Vec<_>>()),
        Err(_) => Ok(Vec::new()),
    }
}

pub async fn render_solar_histogram(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
) -> Result<Element, Rejection> {
    let solar_data = solar_prod(store, start, end).await?;
    let solar_histogram_node =
        Node::from(solar_histogram(VIEWBOX_WIDTH, VIEWBOX_HEIGHT, &solar_data));

    Ok(div(solar_histogram_node).class("line-chart"))
}

// This function uses productivity and not production
async fn month_data_for_year(store: &ArcStore, year: i32) -> Result<[f64; 12], Rejection> {
    let start = &begin_year(year).date_naive();
    let end = &end_year(year).date_naive();
    let solar_data = solar_daily_productivity(store, start, end)
        .await?
        .iter()
        .fold([0.; 12], |mut acc, (date, val)| {
            acc[date.month() as usize - 1] += val;
            acc
        });
    Ok(solar_data)
}

pub async fn render_solar_year_histogram(
    store: &ArcStore,
    year: i32, // selected year, to which we want to compare current year
) -> Result<Element, Rejection> {
    let current_year = Utc::now().year();
    let mut all_years: Vec<(i32, [f64; 12])> = Vec::new();
    for y in first_year(&EnergyType::Solar)..current_year + 1 {
        all_years.push((y, month_data_for_year(store, y).await?));
    }

    let solar_histogram_month_node = Node::from(month_histogram(
        VIEWBOX_WIDTH,
        VIEWBOX_HEIGHT,
        &all_years,
        &EnergyType::Solar,
        year,
    ));

    Ok(div(solar_histogram_month_node).class("line-chart"))
}
