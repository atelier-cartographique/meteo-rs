use chrono::{DateTime, Datelike, Utc};
use warp::Rejection;

use crate::{
    html::*,
    models::wind_indicators::{WindOffshoreCapacityCorrection, WindOnshoreCapacityCorrection},
    plot::{
        base::{first_year, VIEWBOX_HEIGHT, VIEWBOX_WIDTH},
        histogram::{month_histogram, wind_histogram},
    },
    render::{EnergyType, WindData},
    store::{ArcStore, Offshore},
    util::{begin_year, end_year},
};

async fn wind_prod(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
) -> Result<WindData, Rejection> {
    let offshore_data = store.wind_prod_list(start, end, Offshore::True).await;

    let onshore_data = store.wind_prod_list(start, end, Offshore::False).await;

    // Monitored capacity stay stable - there is no point to choose start or end date - but the value is missing for end of the current year
    let offshore_monitored_capacity = store.wind_monitored_capacity(start, Offshore::True).await;
    let onshore_monitored_capacity = store.wind_monitored_capacity(start, Offshore::False).await;
    let offshore_capacity_correction = store
        .wind_offshore_capacity_correction(start)
        .await
        .unwrap_or(WindOffshoreCapacityCorrection::new(0.0));
    let onshore_capacity_correction = store
        .wind_onshore_capacity_correction(start)
        .await
        .unwrap_or(WindOnshoreCapacityCorrection::new(0.0));

    let (onshore_data_corrected, on_mon_cap) = match (onshore_data, onshore_monitored_capacity) {
        (Ok(ond), Ok(onmc)) => {
            let cap = onmc.value().max(onshore_capacity_correction.value());
            let d = ond
                .iter()
                .map(|item| (*item.ts(), item.value() / onmc.value() * cap))
                .collect::<Vec<_>>();
            (d, Some(cap))
        }
        (_, _) => (Vec::new(), None),
    };

    let (offshore_data_corrected, off_mon_cap) = match (offshore_data, offshore_monitored_capacity)
    {
        (Ok(offd), Ok(offmc)) => {
            let cap = offmc.value().max(offshore_capacity_correction.value());

            let d = offd
                .iter()
                .map(|item| (*item.ts(), item.value() / offmc.value() * cap))
                .collect::<Vec<_>>();
            (d, Some(cap))
        }
        (_, _) => (Vec::new(), None),
    };

    Ok(WindData {
        offshore: offshore_data_corrected,
        onshore: onshore_data_corrected,
        monitored_capacity: on_mon_cap.and_then(|omc| off_mon_cap.map(|ofmc| omc + ofmc)),
    })
}

pub async fn render_wind_histogram(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
) -> Result<Element, Rejection> {
    let wind_data = wind_prod(store, start, end).await?;
    let wind_histogram_node = Node::from(wind_histogram(VIEWBOX_WIDTH, VIEWBOX_HEIGHT, &wind_data));

    Ok(div(wind_histogram_node).class("line-chart"))
}

fn render_plot_error() -> Element {
    div("Une erreur est survenue. Veuillez nous en excuser.")
}

// This function uses productivity and not production
async fn month_data_for_year(store: &ArcStore, year: i32) -> Option<[f64; 12]> {
    let start = &begin_year(year);
    let end = &end_year(year);

    let wind_prod = wind_prod(store, start, end).await.ok()?;
    let onshore = wind_prod.onshore;
    let offshore = wind_prod.offshore;
    let monitor_cap = wind_prod.monitored_capacity;
    let wind_data = onshore.iter().zip(offshore.iter());
    let wind = monitor_cap.map(|mc| {
        wind_data.fold([0.; 12], |mut acc, ((date1, val1), (date2, val2))| {
            acc[date1.month() as usize - 1] += val1 / (4. * mc);
            acc[date2.month() as usize - 1] += val2 / (4. * mc);
            acc
        })
    });
    wind // .ok_or(warp::reject::not_found())
}

pub async fn render_wind_year_histogram(
    store: &ArcStore,
    year: i32, // selected year, to which we want to compare current year
) -> Result<Element, Rejection> {
    let current_year = Utc::now().year();
    let mut all_years: Vec<(i32, [f64; 12])> = Vec::new();
    for y in first_year(&EnergyType::Wind)..current_year + 1 {
        match month_data_for_year(store, y).await {
            None => return Ok(render_plot_error()),
            Some(val) => all_years.push((y, val)),
        }
    }

    let wind_histogram_month_node = Node::from(month_histogram(
        VIEWBOX_WIDTH,
        VIEWBOX_HEIGHT,
        &all_years,
        &EnergyType::Wind,
        year,
    ));

    Ok(div(wind_histogram_month_node).class("line-chart"))
}
