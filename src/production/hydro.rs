use chrono::{DateTime, Datelike, Duration, Utc};
use std::f64;

use crate::{
    render_production::{
        corr_for_month, nb_days_in_month, nb_days_included, next_month, TimePlace,
    },
    store::ArcStore,
};

async fn hydro_prod_calc(
    list: Vec<(DateTime<Utc>, f64)>,
    corr: Vec<(i32, f64)>,
    begin: &DateTime<Utc>,
    end: &DateTime<Utc>,
) -> f64 {
    let n = list.len();
    let mut total = 0.;
    let mut start = *begin;
    let start_month = start.month();
    if n > 1 {
        let end_month = end.month();
        total = nb_days_included(&start, TimePlace::Begin) as f64
            / nb_days_in_month(start_month) as f64
            * list[0].1
            / corr_for_month(&corr, start_month).await;

        for i in 1..n - 1 {
            start = next_month(&start);
            total += list[i].1 / corr_for_month(&corr, start.month()).await;
        }
        total += nb_days_included(end, TimePlace::End) as f64 / nb_days_in_month(end_month) as f64
            * list[n - 1].1
            / corr_for_month(&corr, end_month).await;
    } else if n > 0 {
        let diff = *end - start;
        let nb_days = Duration::num_days(&diff);
        total = (nb_days as f64) / (nb_days_in_month(start_month) as f64) * list[0].1
            / corr_for_month(&corr, start_month).await;
    }
    total / 1000000. //to get MWh (same unit as other energy sources)
}

pub fn mw_to_gwh(val: f64) -> f64 {
    val / 4000. // /4 to get MWh and by 1000 to get GWh.
}

pub async fn hydro_value(store: &ArcStore, start: &DateTime<Utc>, end: &DateTime<Utc>) -> f64 {
    let hydro_list = store.hydro_production(start, end).await;
    let hydro_corr = store.hydro_correction(start, end).await;

    match (hydro_list, hydro_corr) {
        (Ok(hl), Ok(hc)) => {
            hydro_prod_calc(
                hl.into_iter().map(|h| h.into()).collect(),
                hc.into_iter().map(|h| h.into()).collect(),
                start,
                end,
            )
            .await
        }
        (_, _) => 0.,
    }
}
