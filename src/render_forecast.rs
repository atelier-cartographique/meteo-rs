use chrono::{DateTime, Duration, Utc};
use serde::Deserialize;
use warp::Rejection;

use crate::{
    forecast::{solar::render_solar_forecast, wind::render_wind_linechart},
    html::*,
    render::{render_error, EnergyType},
    store::ArcStore,
    util::{begin_of_day, format_date},
};

#[derive(Deserialize, Debug)]
pub struct ForecastQuery {
    pub nb_days: i32,
    pub energy_type: EnergyType,
}

impl ForecastQuery {
    pub fn new(nb_days: i32, energy_type: EnergyType) -> ForecastQuery {
        ForecastQuery {
            nb_days,
            energy_type,
        }
    }
    pub fn start(&self) -> DateTime<Utc> {
        let mut first_day = Utc::now();
        if self.nb_days != 0 {
            first_day = Utc::now() + Duration::days(1);
        }
        begin_of_day(&first_day)
    }
    pub fn end(&self) -> DateTime<Utc> {
        let first_day = self.start();
        let last_day = first_day + Duration::days(self.nb_days.into());
        if self.nb_days == 0 {
            return begin_of_day(&(last_day + Duration::days(1)));
        }
        begin_of_day(&last_day)
    }
}

pub fn render_days_links(choice: i64, energy_type: &EnergyType) -> Element {
    let mut index = choice as usize;
    println!("{}", choice);
    if choice == 5 {
        index = 2;
    } else if choice == 7 {
        index = 3;
    }

    let choice0 = div(anchor("Aujourd'hui").set(
        "href",
        format!(
            "/forecast?nb_days={}&energy_type={}",
            0,
            energy_type.to_string()
        ),
    ))
    .class("day");
    let choice1 = div(anchor("Demain").set(
        "href",
        format!(
            "/forecast?nb_days={}&energy_type={}",
            1,
            energy_type.to_string()
        ),
    ))
    .class("day");

    let choice5 = div(vec![
        anchor("5 jours").set(
            "href",
            format!(
                "/forecast?nb_days={}&energy_type={}",
                5,
                energy_type.to_string()
            ),
        ),
        // div("moyenne journalière").class("choice__subtitle"),
    ])
    .class("day");
    let choice7 = div(vec![
        anchor("7 jours").set(
            "href",
            format!(
                "/forecast?nb_days={}&energy_type={}",
                7,
                energy_type.to_string()
            ),
        ),
        // div("moyenne journalière").class("choice__subtitle"),
    ])
    .class("day");
    let mut links = vec![choice0, choice1, choice5, choice7];
    let highlighted = links.remove(index);

    links.insert(index, highlighted.class("day highlighted"));
    div(div(links).class("days")).class("days__wrapper")
}

// pub async fn forecast_all_reply(store: ArcStore, q: ForecastQuery) -> Result<Element, Rejection> {
//     forecast_all(&store, q).await
// }

//Choice is 0 for the forecast of today, 1 for tomorrow, 3 for the three days to come and 7 for the seven days to come
// pub async fn forecast_all(store: &ArcStore, q: ForecastQuery) -> Result<Element, Rejection> {
//     let choice = q.nb_days;
//     let solar_update_date = get_last_del_date(store, "DeleteSolar").await;
//     let wind_update_date = get_last_del_date(store, "DeleteWindForecast").await;

//     let mut first_day = Utc::now();
//     // let mut is_today = true;
//     let mut nb_days = 1;
//     if choice != 0 {
//         first_day = Utc::now() + Duration::days(1);
//         // is_today = false;
//         nb_days = choice;
//     }
//     let last_day = first_day + Duration::days(nb_days.into());
//     let start = &begin_of_day(&first_day);
//     let end = &begin_of_day(&last_day);
//     // let today_str = match is_today {
//     //     true => "d'aujourd'hui",
//     //     false => "de demain",
//     // };
//     // let sub_title = match choice > 1 {
//     //     true => "(en moyenne journalière)",
//     //     false => "",
//     // };

//     let mut date_title = format!("du {}", format_date(&first_day));
//     if choice > 1 {
//         date_title = format!(
//             "du {} au {}",
//             format_date(&first_day),
//             format_date(&(last_day - Duration::days(1))),
//         );
//     };

//     let header = head([
//         meta().set("charset", "UTF-8"),
//         link()
//             .set("rel", "stylesheet")
//             .set("type", "text/css")
//             .set("href", "/static/style/forecast.css"),
//     ]);

//     let forecast_header = div(vec![
//         div(vec![
//             anchor(img().set(
//                 "src",
//                 "https://atelier-cartographique.be/storage/apere/logo-ec.png",
//             ))
//             .set("href", "https://energiecommune.be")
//             .class("logo--link"),
//             anchor("Voir les données passées >>")
//                 .set("href", "/production")
//                 .class("page-link"),
//         ])
//         .class("header--top"),
//         div(vec![
//             div(format!("Prévision {}", date_title)).class("sub__title"),
//             render_days_links(choice),
//         ])
//         .class("header--top"),
//     ])
//     .class("header header--prod");

//     let forecast_footer = div(vec![
//         span("Energie Commune - "),
//         anchor("www.energiecommune.be")
//             .set("href", "https://energiecommune.be")
//             .class("page-link"),
//         span(" - "),
//         anchor("info@energiecommune.be")
//             .set("href", "mailto:info@energiecommune.be")
//             .class("page-link"),
//     ])
//     .class("footer footer--prod");

//     let render_solar_update = match solar_update_date {
//         Ok(d) => div(format!(
//             "Données photovoltaiques mises à jour le {}",
//             format_datetime(&d)
//         ))
//         .class("update-info"),
//         Err(_) => div(""),
//     };
//     let render_wind_update = match wind_update_date {
//         Ok(d) => div(format!(
//             "Données éoliennes mises à jour le {}",
//             format_datetime(&d)
//         ))
//         .class("update-info"),
//         Err(_) => div(""),
//     };
//     let render_source = div(vec![
//         span("Valeurs calculées à partir de données de "),
//         anchor("Elia").set("href", "https://www.elia.be/"),
//         span(" et de "),
//         anchor("Energie Commune").set("href", "https://energiecommune.be"),
//     ])
//     .class("source");

//     let forecast_content_body = div(vec![
//         div("Électricité éolienne").class("main__title"),
//         render_wind_forecast(store, start, end, nb_days).await,
//         div("Électricité photovoltaïque").class("main__title"),
//         render_solar_forecast(store, start, end, nb_days).await,
//         render_wind_update,
//         render_solar_update,
//         render_source,
//     ])
//     .class("content--body");

//     let content = body(vec![div(vec![
//         forecast_header,
//         div(forecast_content_body).class("content"), // TO DO: hour as parameter
//         forecast_footer,
//     ])
//     .class("main")]);

//     Ok(html([header, content]))
// }

fn forecast_header(
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    energy_type: &EnergyType,
) -> Element {
    let nb_days = (*end - *start).num_days();
    let mut date_title = format!("du {}", format_date(&start));
    if nb_days > 1 {
        date_title = format!(
            "du {} au {}",
            format_date(start),
            format_date(&(*end - Duration::days(1))),
        );
    };
    let mut links = render_days_links(nb_days, &energy_type);
    if start.date() == Utc::now().date() {
        links = render_days_links(0, &energy_type)
    }
    div([
        div(vec![
            anchor(img().set(
                "src",
                "https://atelier-cartographique.be/storage/apere/logo-ec.png",
            ))
            .set("href", "https://energiecommune.be")
            .class("logo--link"),
            anchor("Voir les données passées >>")
                .set("href", "/production")
                .class("page-link"),
        ])
        .class("header--top"),
        div(format!("Prévision {}", date_title)).class("sub__title"),
        links, // div(vec![
               //     div(format!("Prévision {}", date_title)).class("sub__title"),
               //     render_days_links(nb_days, energy_type),
               // ])
               // .class("header--top"),
    ])
    .class("header header--prod")
}

// fn forecast_footer() -> Element {
//     div(vec![
//         span("Energie Commune - "),
//         anchor("www.energiecommune.be")
//             .set("href", "https://energiecommune.be")
//             .class("page-link"),
//         span(" - "),
//         anchor("info@energiecommune.be")
//             .set("href", "mailto:info@energiecommune.be")
//             .class("page-link"),
//     ])
//     .class("footer footer--prod")
// }

fn forecast_for(
    rendered: Result<Element, Rejection>,
    // nb_days: i64,
    // energy_type: &EnergyType,
    title: &str,
    error_message: &str,
) -> Element {
    let title = div(title).class("main__title");
    match rendered {
        Ok(element) => div([title, element]).class("forecast"),
        Err(err) => render_error(&format!("{}: {:?}", error_message, err)),
    }
}

//Choice is 0 for the forecast of today, 1 for tomorrow, 3 for the three days to come and 7 for the seven days to come
pub async fn forecast(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    energy_type: &EnergyType,
) -> Element {
    let nb_days = (*end - *start).num_days();
    let mut date_title = format!("du {}", format_date(&start));
    if nb_days > 1 {
        date_title = format!(
            "du {} au {}",
            format_date(start),
            format_date(&(*end - Duration::days(1))),
        );
    };
    let mut links = render_days_links(nb_days, &energy_type);
    if start.date() == Utc::now().date() {
        links = render_days_links(0, &energy_type)
    }

    match energy_type {
        EnergyType::Solar => {
            let solar = forecast_for(
                render_solar_forecast(store, start, end).await,
                // nb_days,
                // energy_type,
                "Électricité photovoltaïque",
                "Error: solar map",
            );
            links.append(solar)
        }
        EnergyType::Wind => {
            let wind = forecast_for(
                render_wind_linechart(store, start, end).await,
                // nb_days,
                // energy_type,
                "Électricité éolienne",
                "Error: wind linechart",
            );
            links.append(wind)
        }
        EnergyType::All => {
            let solar = forecast_for(
                render_solar_forecast(store, start, end).await,
                // nb_days,
                // energy_type,
                "Électricité photovoltaïque",
                "Error: solar map",
            );
            let wind = forecast_for(
                render_wind_linechart(store, start, end).await,
                "Électricité éolienne",
                "Error: wind linechart",
            );
            forecast_header(start, end, &EnergyType::All)
                .append(wind)
                .append(solar)
        }
    }
}

fn make_document(elem: Element, title: &str) -> Element {
    html([
        head([
            meta().set("charset", "UTF-8"),
            meta().set("title", title),
            link()
                .set("rel", "stylesheet")
                .set("type", "text/css")
                .set("href", format!("/static/style/forecast.css")),
        ]),
        body(elem).class("content--body"),
        div(vec![
            span("Valeurs calculées à partir de données de "),
            anchor("Elia").set("href", "https://www.elia.be/"),
            // span(", de "),
            // anchor("ENTSOe").set("href", "https://transparency.entsoe.eu/"),
            span(" et de "),
            anchor("Energie Commune").set("href", "https://energiecommune.be"),
        ])
        .class("source"),
    ])
}

pub async fn forecast_document(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    energy_type: &EnergyType,
) -> Element {
    make_document(forecast(store, start, end, energy_type).await, "Prévisions")
}

pub async fn forecast_reply(store: ArcStore, q: ForecastQuery) -> Result<Element, Rejection> {
    let start = q.start();
    let end = q.end();
    Ok(forecast_document(&store, &start, &end, &q.energy_type).await)
}
// //Choice is 0 for the forecast of today, 1 for tomorrow, 3 for the three days to come and 7 for the seven days to come
// pub async fn forecast(store: &ArcStore, q: ForecastQuery) -> Result<Element, Rejection> {
//     let choice = q.nb_days;
//     let energy = EnergyType::try_from(q.energy_type);
//     let solar_update_date = get_last_del_date(store, "DeleteSolar").await;
//     let wind_update_date = get_last_del_date(store, "DeleteWindForecast").await;

//     let mut first_day = Utc::now();
//     // let mut is_today = true;
//     let mut nb_days = 1;
//     if choice != 0 {
//         first_day = Utc::now() + Duration::days(1);
//         // is_today = false;
//         nb_days = choice;
//     }
//     let last_day = first_day + Duration::days(nb_days.into());
//     let start = &begin_of_day(&first_day);
//     let end = &begin_of_day(&last_day);
//     // let today_str = match is_today {
//     //     true => "d'aujourd'hui",
//     //     false => "de demain",
//     // };
//     // let sub_title = match choice > 1 {
//     //     true => "(en moyenne journalière)",
//     //     false => "",
//     // };

//     let mut date_title = format!("du {}", format_date(&first_day));
//     if choice > 1 {
//         date_title = format!(
//             "du {} au {}",
//             format_date(&first_day),
//             format_date(&(last_day - Duration::days(1))),
//         );
//     };

//     let header = head([
//         meta().set("charset", "UTF-8"),
//         link()
//             .set("rel", "stylesheet")
//             .set("type", "text/css")
//             .set("href", "/static/style/forecast.css"),
//     ]);

//     let _forecast_header = div(vec![
//         div(vec![
//             anchor(img().set(
//                 "src",
//                 "https://atelier-cartographique.be/storage/apere/logo-ec.png",
//             ))
//             .set("href", "https://energiecommune.be")
//             .class("logo--link"),
//             anchor("Voir les données passées >>")
//                 .set("href", "/production")
//                 .class("page-link"),
//         ])
//         .class("header--top"),
//         div(vec![
//             div(format!("Prévision {}", date_title)).class("sub__title"),
//             render_days_links(choice),
//         ])
//         .class("header--top"),
//     ])
//     .class("header header--prod");

//     let _forecast_footer = div(vec![
//         span("Energie Commune - "),
//         anchor("www.energiecommune.be")
//             .set("href", "https://energiecommune.be")
//             .class("page-link"),
//         span(" - "),
//         anchor("info@energiecommune.be")
//             .set("href", "mailto:info@energiecommune.be")
//             .class("page-link"),
//     ])
//     .class("footer footer--prod");

//     let render_solar_update = match solar_update_date {
//         Ok(d) => div(format!(
//             "Données photovoltaiques mises à jour le {}",
//             format_datetime(&d)
//         ))
//         .class("update-info"),
//         Err(_) => div(""),
//     };
//     let render_wind_update = match wind_update_date {
//         Ok(d) => div(format!(
//             "Données éoliennes mises à jour le {}",
//             format_datetime(&d)
//         ))
//         .class("update-info"),
//         Err(_) => div(""),
//     };
//     let render_source = div(vec![
//         span("Valeurs calculées à partir de données de "),
//         anchor("Elia").set("href", "https://www.elia.be/"),
//         span(" et de "),
//         anchor("Energie Commune").set("href", "https://energiecommune.be"),
//     ])
//     .class("source");

//     let render_solar = div(vec![
//         div("Électricité photovoltaïque").class("main__title"),
//         render_solar_forecast(store, start, end, nb_days).await,
//         render_solar_update,
//         render_source.clone(),
//     ])
//     .class("content--body");

//     let render_wind = div(vec![
//         div("Électricité éolienne").class("main__title"),
//         render_wind_forecast(store, start, end, nb_days).await,
//         render_wind_update,
//         render_source,
//     ])
//     .class("content--body");

//     let render_all = div(vec![render_solar.clone(), render_wind.clone()]);

//     let forecast = match energy {
//         Ok(EnergyType::Solar) => render_solar,
//         Ok(EnergyType::Wind) => render_wind,
//         Ok(EnergyType::All) => render_all,
//         Err(_) => render_error("forecast error"),
//     };

//     let content = body(vec![div(vec![
//         // forecast_header,
//         div(forecast).class("content"), // TO DO: hour as parameter
//                                         // forecast_footer,
//     ])
//     .class("widget")]);

//     Ok(html([header, content]))
// }
