use std::{collections::HashMap, str::FromStr};
use warp::reject::Reject;

#[derive(Debug)]
pub enum FormError {
    MissingField(String),
    Convert(String),
}

impl Reject for FormError {}

pub fn field<T>(form: &HashMap<String, String>, key: &str) -> Result<T, FormError>
where
    T: FromStr,
{
    form.get(key)
        .ok_or(FormError::MissingField(key.into()))
        .and_then(|i| {
            i.parse::<T>().or(Err(FormError::Convert(format!(
                "'{}' expected to be {}",
                key,
                std::any::type_name::<T>()
            ))))
        })
}
