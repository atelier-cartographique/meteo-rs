use warp::{Rejection, Reply};

use crate::{
    html::div,
    news::render::render_latest_news,
    render_forecast::{forecast, ForecastQuery},
    render_production::{production, ProdQuery},
    store::ArcStore,
};

pub async fn onepage(
    store: &ArcStore,
    query_prod: ProdQuery,
    query_forecast: ForecastQuery,
) -> Result<impl Reply, Rejection> {
    let startprod = query_prod.start();
    let endprod = query_prod.end();
    let startfor = query_forecast.start();
    let endfor = query_forecast.end();
    let prod = production(store, &startprod, &endprod, query_prod.energy_type).await;
    let forecast = forecast(store, &startfor, &endfor, &query_forecast.energy_type).await;
    let note = render_latest_news(store, None).await?;
    Ok(div(vec![note, forecast, prod]))
}
