use crate::{
    forecast::jauges::{solar_jauge_svg, wind_jauge_svg},
    forecast::wind::wind_linechart_svg,
    map::{solar_map_svg_app, solar_map_svg_download},
    render::{MeteoImage, ReSVGImage, FONT},
    store::ArcStore,
};
use fontconfig::Fontconfig;
use fontdb::Database;
use image::GenericImageView;
use lazy_static::lazy_static;
use resvg::{render, Image};
use svg::node::element::Image as SvgImage;
use svg::Document;
use usvg::{ImageRendering, Options, ShapeRendering, TextRendering, Tree};
use warp::reject;
use warp::{Rejection, Reply};

lazy_static! {
    static ref SVG_TREE_OPTIONS: Options = {
        let fc = Fontconfig::new().expect("Could not initialize fontconfig");
        let font = fc
            .find(FONT, None)
            .expect(&format!("Fontconfig could not find {FONT}"));
        let mut db = Database::new();
        let path2 = font.path.clone();
        let font_path_name = path2.display();
        db.load_font_file(font.path)
            .expect(&format!("Could not load font \"{font_path_name}\""));

            let font_family = db.faces()
                .get(0)
                .map(|face| face.family.clone())
                .unwrap_or(FONT.to_string());

            Options {
                path: None,
                dpi: 96.0,
                // Default font is user-agent dependent so we can use whichever we like.
                // font_family: "Times New Roman".to_owned(),
                font_family: font_family,
                font_size: 12.0,
                languages: vec!["fr".to_string()],
                shape_rendering: ShapeRendering::default(),
                text_rendering: TextRendering::default(),
                image_rendering: ImageRendering::default(),
                keep_named_groups: true,
                fontdb: db,
            }
    };
}

pub fn svg_to_png(svg: Document) -> Option<Image> {
    let tree_res = Tree::from_str(&svg.to_string(), &*SVG_TREE_OPTIONS);
    // println!("==================================");
    // println!("{}", svg.to_string());
    // println!("==================================");
    match tree_res {
        Ok(t) => {
            // println!(
            //     "{}",
            //     t.to_string(usvg::XmlOptions {
            //         use_single_quote: false,
            //         indent: usvg::XmlIndent::Spaces(2),
            //         attributes_indent: usvg::XmlIndent::None,
            //     })
            // );
            render(&t, usvg::FitTo::Width(2000), None)
        }
        Err(e) => {
            println!("{}", e);
            None
        }
    }
}

pub fn dynimg_to_svgimg(img: MeteoImage) -> SvgImage {
    let mut out: Vec<u8> = Vec::new();
    let img0 = img.0;
    match img0.write_to(&mut out, image::ImageOutputFormat::Png) {
        Ok(_) => {
            let b64 = base64::encode(&mut out);
            SvgImage::new()
                .set("width", format!("{}", img0.width()))
                .set("height", format!("{}", img0.height()))
                .set("xlink:href", format!("data:image/png;base64,{}", b64))
        }
        Err(_) => SvgImage::new(),
    }
}

pub async fn wind_linechart_png(
    store: ArcStore,
    year: i32,
    month: u32,
    day: u32,
    nb_days: u32,
) -> Result<impl Reply, Rejection> {
    let svg = wind_linechart_svg(year, month, day, nb_days, &store).await;
    let png = svg_to_png(svg);
    match png {
        Some(img) => Ok(ReSVGImage(img)),
        None => Err(reject()),
    }
}

pub async fn solar_map_png_download(
    store: ArcStore,
    year: i32,
    month: u32,
    day: u32,
    hour: u32,
) -> Result<impl Reply, Rejection> {
    let svg = solar_map_svg_download(year, month, day, hour, &store).await;
    let png = svg_to_png(svg);
    match png {
        Some(img) => Ok(ReSVGImage(img)),
        None => {
            println!("ERROR: can't get map png");
            Err(reject())
        }
    }
}
pub async fn solar_map_png_app(
    store: ArcStore,
    year: i32,
    month: u32,
    day: u32,
    hour: u32,
) -> Result<impl Reply, Rejection> {
    let svg = solar_map_svg_app(year, month, day, hour, &store).await;
    let png = svg_to_png(svg);
    match png {
        Some(img) => Ok(ReSVGImage(img)),
        None => {
            println!("ERROR: can't get map png");
            Err(reject())
        }
    }
}

pub async fn wind_jauge_png(loadfactor: u32) -> Result<impl Reply, Rejection> {
    let svg = wind_jauge_svg(loadfactor as f64);
    let png = svg_to_png(svg);
    match png {
        Some(img) => Ok(ReSVGImage(img)),
        None => {
            println!("ERROR: can't get wind_jauge png");
            Err(reject())
        }
    }
}
pub async fn solar_jauge_png(loadfactor: u32) -> Result<impl Reply, Rejection> {
    let svg = solar_jauge_svg(loadfactor as f64);
    let png = svg_to_png(svg);
    match png {
        Some(img) => Ok(ReSVGImage(img)),
        None => {
            println!("ERROR: can't get wind_jauge png");
            Err(reject())
        }
    }
}
