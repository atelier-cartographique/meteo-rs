use chrono::{Date, DateTime, Datelike, Duration, NaiveDate, TimeZone, Utc};
use chrono_tz::Europe::Brussels;
use serde::Deserialize;
use std::f64;
use warp::{reject, Rejection};

use crate::{
    html::*,
    period_select::{render_date_input, render_select_month_year, render_select_year},
    production::{
        render_piechart::render_prod_piechart,
        solar::{render_solar_histogram, render_solar_year_histogram},
        wind::{render_wind_histogram, render_wind_year_histogram},
    },
    render::{render_error, EnergyType},
    store::ArcStore,
    util::{begin_of_month, format_date, last_month, year_of_last_month},
};

#[derive(Deserialize, Debug)]
pub struct ProdQuery {
    month: u32,
    year: u32,
    pub energy_type: EnergyType,
}

#[derive(Deserialize, Debug)]
pub struct PieChartQuery {
    start: String,
    end: String,
}

impl PieChartQuery {
    pub fn start(&self) -> Result<DateTime<Utc>, String> {
        match NaiveDate::parse_from_str(&self.start, "%Y-%m-%d") {
            Ok(date) => Ok(DateTime::from_utc(date.and_hms(0, 0, 0), Utc)),
            Err(e) => {
                println!("err {}", e);
                Err("Could not parse start date".to_string())
            }
        }
    }

    pub fn end(&self) -> Result<DateTime<Utc>, String> {
        match NaiveDate::parse_from_str(&self.end, "%Y-%m-%d") {
            Ok(date) => Ok(DateTime::from_utc(date.and_hms(0, 0, 0), Utc)),
            Err(_) => Err("Could not parse end date".to_string()),
        }
    }
    fn new(s: &DateTime<Utc>, e: &DateTime<Utc>) -> PieChartQuery {
        let start = format!("{}-{}-{}", s.year(), s.month(), s.day());
        let end = format!("{}-{}-{}", e.year(), e.month(), e.day());
        PieChartQuery { start, end }
    }
}

pub fn default_prod_query(energy_type: EnergyType) -> ProdQuery {
    let now = Utc::now();
    let month = now.month();
    ProdQuery::new(
        last_month(month),
        year_of_last_month(month, now.year() as u32),
        energy_type,
    )
}
pub fn default_piechart_query() -> PieChartQuery {
    let now = Utc::now();
    let end = begin_of_month(now - Duration::days(1));
    let day_in_last_month = now.with_month(last_month(now.month())).unwrap_or(end);
    let start = begin_of_month(day_in_last_month);

    PieChartQuery::new(&start, &end)
}
pub fn default_histogram_prod_query(energy_type: EnergyType) -> ProdQuery {
    let now = Utc::now();
    ProdQuery::new(1, now.year() as u32 - 1, energy_type)
}

fn get_end_of_month(month: u32, year: u32) -> DateTime<Utc> {
    let date: Date<Utc> = Utc.ymd(year as i32, month, 1);
    // we use the brussels begin of the day, then return UTC for manipulations
    let dt = date.with_timezone(&Brussels).and_hms(0, 0, 0);
    for i in 0..32 {
        let attempt = dt + Duration::days(i);
        if attempt.month() != month {
            return (attempt - Duration::hours(1)).with_timezone(&Utc);
        }
    }
    dt.with_timezone(&Utc)
}

impl ProdQuery {
    pub fn new(month: u32, year: u32, energy_type: EnergyType) -> ProdQuery {
        ProdQuery {
            month,
            year,
            energy_type,
        }
    }

    // pub fn month(&self) -> u32 {
    //     self.month
    // }
    pub fn year(&self) -> u32 {
        self.year
    }
    pub fn start(&self) -> DateTime<Utc> {
        let date: Date<Utc> = Utc.ymd(self.year as i32, self.month, 1);
        date.and_hms(0, 0, 0)
    }
    pub fn end(&self) -> DateTime<Utc> {
        get_end_of_month(self.month, self.year)
    }

    // pub fn start(&self) -> Result<DateTime<Utc>, String> {
    //     match NaiveDate::parse_from_str(&self.start, "%Y-%m-%d") {
    //         Ok(date) => Ok(DateTime::from_utc(date.and_hms(0, 0, 0), Utc)),
    //         Err(e) => {
    //             println!("err {}", e);
    //             Err("Could not parse start date".to_string())
    //         }
    //     }
    // }

    // pub fn end(&self) -> Result<DateTime<Utc>, String> {
    //     match NaiveDate::parse_from_str(&self.end, "%Y-%m-%d") {
    //         Ok(date) => Ok(DateTime::from_utc(date.and_hms(0, 0, 0), Utc)),
    //         Err(_) => Err("Could not parse end date".to_string()),
    //     }
    // }
}

pub enum TimePlace {
    Begin,
    End,
}

fn end_of_month(date: DateTime<Utc>) -> DateTime<Utc> {
    let starting_month = date.month();
    for i in 0..33 {
        let attempt = date + Duration::days(i);
        if attempt.month() != starting_month {
            return attempt;
        }
    }
    date
}
pub fn next_month(date: &DateTime<Utc>) -> DateTime<Utc> {
    let next = *date;
    if next.month() < 12 {
        next.with_month(next.month() + 1).unwrap_or(next)
    } else {
        next.with_month(1).unwrap_or(next)
    }
}

pub fn nb_days_included(date: &DateTime<Utc>, place: TimePlace) -> i64 {
    match place {
        TimePlace::Begin => Duration::num_days(&(end_of_month(*date) - *date)),
        TimePlace::End => Duration::num_days(&(*date - dbg!(date.with_day(1).unwrap_or(*date)))),
    }
}
pub fn nb_days_in_month(month: u32) -> u32 {
    if month == 2 {
        28
    } else if month == 4 || month == 6 || month == 9 || month == 11 {
        30
    } else {
        31
    }
}
pub async fn corr_for_month(corr: &Vec<(i32, f64)>, month: u32) -> f64 {
    match corr.iter().find(|tup| tup.0 == month as i32) {
        Some(t) => t.1 as f64,
        None => 1.,
    }
}

pub async fn production_reply(store: ArcStore, q: ProdQuery) -> Result<Element, Rejection> {
    let start = q.start();
    let end = q.end();
    println!("start:{:?}; end:{:?}", start, end);
    Ok(production_document(&store, &start, &end, q.energy_type).await)
}

pub async fn pie_reply(store: ArcStore, q: PieChartQuery) -> Result<Element, Rejection> {
    match (q.start(), q.end()) {
        (Ok(s), Ok(e)) => Ok(pie_document(&store, &s, &e).await),
        (_, _) => Err(reject()),
    }
}

pub async fn year_prod_reply(store: ArcStore, q: ProdQuery) -> Result<Element, Rejection> {
    Ok(year_histogram_document(&store, q.year() as i32, &q.energy_type).await)
}

fn make_document(elem: Element, title: &str) -> Element {
    html([
        head([
            meta().set("charset", "UTF-8"),
            meta().set("title", title),
            link()
                .set("rel", "stylesheet")
                .set("type", "text/css")
                .set("href", format!("/static/style/forecast.css")),
        ]),
        body(elem).class("content--body"),
        // div(vec![
        //     span("Energie Commune - "),
        //     anchor("www.energiecommune.be")
        //         .set("href", "https://energiecommune.be")
        //         .class("page-link"),
        //     span(" - "),
        //     anchor("info@energiecommune.be")
        //         .set("href", "mailto:info@energiecommune.be")
        //         .class("page-link"),
        // ])
        // .class("footer footer--prod"),
        div(vec![
            span("Valeurs calculées à partir de données de "),
            anchor("Elia").set("href", "https://www.elia.be/"),
            span(", de "),
            anchor("ENTSOe").set("href", "https://transparency.entsoe.eu/"),
            span(" et de "),
            anchor("Energie Commune").set("href", "https://energiecommune.be"),
        ])
        .class("source"),
    ])
}

fn prod_header(start: &DateTime<Utc>, end: &DateTime<Utc>) -> Element {
    div(vec![
        div(vec![
            anchor(img().set(
                "src",
                "https://atelier-cartographique.be/storage/apere/logo-ec.png",
            ))
            .set("href", "https://energiecommune.be")
            .class("logo--link"),
            anchor("Voir les prévisions >>")
                .set("href", "/forecast")
                .class("page-link"),
        ])
        .class("header--top"),
        div(format!(
            "Productions électriques du {} au {}",
            format_date(start),
            format_date(end)
        ))
        .class("sub__title"),
        // render_select_month_year(), // render_date_input(start, end),
    ])
    .class("header header--prod")
}

fn production_for(
    rendered: Result<Element, Rejection>,
    month: u32,
    year: i32,
    energy_type: &EnergyType,
    title: &str,
    error_message: &str,
) -> Element {
    let title = div(title).class("main__title");
    let select = render_select_month_year(month, year, &energy_type, &"/production");
    match rendered {
        Ok(element) => {
            div([div([title, select]).class("title-select__wrapper"), element]).class("production")
        }
        Err(err) => render_error(&format!("{}: {:?}", error_message, err)),
    }
}

fn pie_for(
    rendered: Result<Node, Rejection>,
    // month: u32,
    // year: i32,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    title: &str,
    error_message: &str,
) -> Element {
    let title = div(title).class("main__title");
    // let select = render_select_month_year(month, year, EnergyType::All, &"/conso_piechart");
    let select = render_date_input(start, end);
    match rendered {
        Ok(element) => div(div([title, select]).class("title-select__wrapper"))
            .append_node(element)
            .class("production"),
        Err(err) => render_error(&format!("{}: {:?}", error_message, err)),
    }
}

fn year_histogram_for(
    rendered: Result<Element, Rejection>,
    energy_type: &EnergyType,
    year: i32,
    title: &str,
    error_message: &str,
) -> Element {
    let title = div(title).class("main__title");
    let select = render_select_year(year, energy_type);
    match rendered {
        Ok(element) => {
            div([div([title, select]).class("title-select__wrapper"), element]).class("production")
        }
        Err(err) => render_error(&format!("{}: {:?}", error_message, err)),
    }
}

pub async fn production(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    energy_type: EnergyType,
) -> Element {
    let month = start.month();
    let year = start.year();

    match energy_type {
        EnergyType::Solar => production_for(
            render_solar_histogram(store, &start, &end).await,
            month,
            year,
            &energy_type,
            "Électricité photovoltaïque",
            "Error: solar histogram",
        ),
        EnergyType::Wind => production_for(
            render_wind_histogram(store, &start, &end).await,
            month,
            year,
            &energy_type,
            "Électricité éolienne",
            "Error: wind histogram",
        ),
        EnergyType::All => {
            let header = prod_header(&start, &end);
            let hist_solar = year_histogram_for(
                render_solar_year_histogram(store, year).await,
                &energy_type,
                year,
                "Productivité photovoltaïque",
                "Error: solar histogram",
            );
            let hist_wind = year_histogram_for(
                render_wind_year_histogram(store, year).await,
                &energy_type,
                year,
                "Productivité éolienne",
                "Error: wind histogram",
            );
            let solar = production_for(
                render_solar_histogram(store, &start, &end).await,
                month,
                year,
                &energy_type,
                "Électricité photovoltaïque",
                "Error: solar histogram",
            );
            let wind = production_for(
                render_wind_histogram(store, &start, &end).await,
                month,
                year,
                &energy_type,
                "Électricité éolienne",
                "Error: wind histogram",
            );
            let pie = pie_for(
                render_prod_piechart(store, &start, &end).await,
                start,
                end,
                // month,
                // year,
                "Consommation totale",
                "Error: piechart",
            );
            header
                .append(pie)
                .append(hist_solar)
                .append(hist_wind)
                .append(wind)
                .append(solar)
        }
    }
}

pub async fn production_document(
    store: &ArcStore,
    start: &DateTime<Utc>,
    end: &DateTime<Utc>,
    energy_type: EnergyType,
) -> Element {
    make_document(
        production(store, start, end, energy_type).await,
        "Productions électriques",
    )
}

pub async fn pie_document(store: &ArcStore, start: &DateTime<Utc>, end: &DateTime<Utc>) -> Element {
    // let month = start.month();
    // let year = start.year();

    make_document(
        pie_for(
            render_prod_piechart(store, &start, &end).await,
            start,
            end,
            // month,
            // year,
            "Consommation totale",
            "Error: piechart",
        ),
        "",
    )
}

pub async fn year_histogram_document(
    store: &ArcStore,
    year: i32,
    energy_type: &EnergyType,
) -> Element {
    match energy_type {
        EnergyType::Solar => make_document(
            year_histogram_for(
                render_solar_year_histogram(store, year).await,
                energy_type,
                year,
                "Productivité photovoltaïque",
                "Error: solar year histogram",
            ),
            "",
        ),
        EnergyType::Wind => make_document(
            year_histogram_for(
                render_wind_year_histogram(store, year).await,
                energy_type,
                year,
                "Productivité éolienne",
                "Error: wind year histogram",
            ),
            "",
        ),
        EnergyType::All => make_document(
            year_histogram_for(
                render_solar_year_histogram(store, year).await,
                energy_type,
                year,
                "Productivité photovoltaïque",
                "Error: solar year histogram",
            )
            .append(year_histogram_for(
                render_wind_year_histogram(store, year).await,
                energy_type,
                year,
                "Productivité éolienne",
                "Error: wind year histogram",
            )),
            "",
        ),
    }
}

// pub async fn production_all(store: &ArcStore, q: ProdQuery) -> Element {
//     // TODO: use production with "All" as energytype
//     let start = q.start();
//     let end = q.end();
//     let maybe_energy = q.energy_type;
//     let energy = EnergyType::try_from(maybe_energy).map_err(|_err| reject());

//     let prod_elem = Element::new("production");
//     make_document(
//         prod_elem
//             .append(prod_header(&start, &end))
//             .append_node(render_prod_piechart(store, &start, &end).await)
//             .append(production(store, &start, &end, EnergyType::Wind).await)
//             .append(production(store, &start, &end, EnergyType::Solar).await),
//         "Consommation totale",
//     )
// }

// pub async fn production_all(store: &ArcStore, q: ProdQuery) -> Result<Element, Rejection> {
//     let start = match q.start() {
//         Ok(d) => d,
//         Err(_) => Utc::now() - Duration::weeks(4),
//     };
//     let end = match q.end() {
//         Ok(d) => end_of_day(&d),
//         Err(_) => Utc::now() - Duration::days(1),
//     };

//     let solar_data = solar_prod(store, start, end).await?;

//     let header = head([
//         meta().set("charset", "UTF-8"),
//         link()
//             .set("rel", "stylesheet")
//             .set("type", "text/css")
//             .set("href", "/static/style/forecast.css"),
//     ]);

//     // HTML - PRODUCTION

//     let wind_data = wind_prod(store, start, end).await?;

//     let wind_histogram_node = Node::from(wind_histogram(700., 250., &wind_data));
//     let solar_histogram_month_node = Node::from(solar_histogram(700., 250., &solar_data));
//     let prod_pie_chart_node = Node::from(prod_pie_chart(past_prod(store, start, end).await?, 200.));

//     let prod_wind = div(vec![
//         div("Électricité éolienne").class("main__title"),
//         div(wind_histogram_node).class("line-chart"),
//     ])
//     .class("production-graph");

//     let prod_solar = div(vec![
//         div("Électricité photovoltaïque").class("main__title"),
//         div(solar_histogram_month_node).class("line-chart"),
//     ])
//     .class("production-graph");

//     let prod_footer = div(vec![
//         span("Energie Commune - "),
//         anchor("www.energiecommune.be")
//             .set("href", "https://energiecommune.be")
//             .class("page-link"),
//         span(" - "),
//         anchor("info@energiecommune.be")
//             .set("href", "mailto:info@energiecommune.be")
//             .class("page-link"),
//     ])
//     .class("footer footer--prod");

//     let input_date = form(vec![
//         input()
//             .set("type", "date")
//             .set("key", "start-date")
//             .set("name", "start")
//             .set("min", "2020-01-01")
//             .set("max", Utc::now().format("%Y-%m-%d").to_string())
//             .set("value", start.format("%Y-%m-%d").to_string()),
//         input()
//             .set("type", "date")
//             .set("key", "end-date")
//             .set("name", "end")
//             .set("min", "2020-01-01")
//             .set("max", Utc::now().format("%Y-%m-%d").to_string())
//             .set("value", end.format("%Y-%m-%d").to_string()),
//         input().set("type", "submit"),
//     ])
//     .set("action", "/production_at")
//     .set("method", "get");

//     let prod_header = div(vec![
//         div(vec![
//             anchor(img().set(
//                 "src",
//                 "https://atelier-cartographique.be/storage/apere/logo-ec.png",
//             ))
//             .set("href", "https://energiecommune.be")
//             .class("logo--link"),
//             anchor("Voir les prévisions >>")
//                 .set("href", "/forecast")
//                 .class("page-link"),
//         ])
//         .class("header--top"),
//         div(format!(
//             "Productions électriques du {} au {}",
//             format_date(&start),
//             format_date(&end)
//         ))
//         .class("sub__title"),
//         input_date,
//     ])
//     .class("header header--prod");

//     let render_source = div(vec![
//         span("Valeurs calculées à partir de données de "),
//         anchor("Elia").set("href", "https://www.elia.be/"),
//         span(", de "),
//         anchor("ENTSOe").set("href", "https://transparency.entsoe.eu/"),
//         span(" et de "),
//         anchor("Energie Commune").set("href", "https://energiecommune.be"),
//     ])
//     .class("source");
//     let prod_content_body = div(vec![
//         div("Consommation totale").class("main__title"),
//         div(prod_pie_chart_node).class("pie"),
//         prod_wind,
//         prod_solar,
//         render_source,
//     ])
//     .class("content--body");

//     let content = body(vec![div(vec![
//         prod_header,
//         div(vec![
//             // prod_sidebar,
//             prod_content_body,
//         ])
//         .class("content"),
//         prod_footer,
//     ])
//     .class("main main--production")]);

//     Ok(html([header, content]))
// }
