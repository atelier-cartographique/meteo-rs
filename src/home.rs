use crate::html::{anchor, body, div, h1, head, html, link, meta, Element};

pub fn render_home() -> Element {
    html([
        head([
            meta().set("charset", "UTF-8"),
            meta().set(
                "title",
                "énergie commune - météo des énergies renouvelables",
            ),
            link()
                .set("rel", "stylesheet")
                .set("type", "text/css")
                .set("href", format!("/static/style/base.css")),
        ]),
        body([
            h1("Énergie commune - météo des énergies renouvelables"),
            div(anchor("production passée").set("href", "/production")),
            div(anchor("prévisons").set("href", "/forecast")),
            h1("Widgets météo"),
            div(anchor("Dashboard des énergies renouvelables")
                .set("href", "/static/examples/demo.html")),
            div(anchor("Documentation des widgets météo")
                .set("href", "/static/examples/embed.html")),
        ]),
    ])
}
