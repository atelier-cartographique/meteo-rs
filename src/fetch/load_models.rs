use crate::fetch::child_filter_n;
use crate::model::{params, ToParams, WithSQL};
use crate::store::{Store, StoreError};
use crate::util::*;
use chrono::{DateTime, Utc};
use roxmltree::Node;

type NullableInt = Option<i32>;
type NullableFloat = Option<f64>;

// LOAD - forecast

#[derive(Clone)]
pub struct SolarLoad {
    pub cipu_load: NullableFloat,
    pub cipu_load_da_forecast: NullableFloat,
    pub cipu_load_forecast: NullableFloat,
    pub cipu_load_id_forecast: NullableFloat,
    pub date_time: Option<DateTime<Utc>>,
    pub tick: NullableInt,
    pub total_load: NullableFloat,
    pub total_load_da_forecast: NullableFloat,
    pub total_load_da_forecast_p10: NullableFloat,
    pub total_load_da_forecast_p90: NullableFloat,
    pub total_load_forecast: NullableFloat,
    pub total_load_forecast_p10: NullableFloat,
    pub total_load_forecast_p90: NullableFloat,
    pub total_load_id_forecast: NullableFloat,
    pub total_load_id_forecast_p10: NullableFloat,
    pub total_load_id_forecast_p90: NullableFloat,
    pub total_load_wa_forecast: NullableFloat,
    pub total_load_wa_forecast_p10: NullableFloat,
    pub total_load_wa_forecast_p90: NullableFloat,
}

impl ToParams for SolarLoad {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        (params()
            + &self.cipu_load
            + &self.cipu_load_da_forecast
            + &self.cipu_load_forecast
            + &self.cipu_load_id_forecast
            + &self.date_time
            + &self.tick
            + &self.total_load
            + &self.total_load_da_forecast
            + &self.total_load_da_forecast_p10
            + &self.total_load_da_forecast_p90
            + &self.total_load_forecast
            + &self.total_load_forecast_p10
            + &self.total_load_forecast_p90
            + &self.total_load_id_forecast
            + &self.total_load_id_forecast_p10
            + &self.total_load_id_forecast_p90
            + &self.total_load_wa_forecast
            + &self.total_load_wa_forecast_p10
            + &self.total_load_wa_forecast_p90)
            .inner()
    }
}

impl WithSQL for SolarLoad {
    fn sql() -> &'static str {
        "
                    INSERT INTO load (
                        cipu_load,
                        cipu_load_da_forecast,
                        cipu_load_forecast,
                        cipu_load_id_forecast,
                        ts,
                        tick,
                        total_load,
                        total_load_da_forecast,
                        total_load_da_forecast_p10,
                        total_load_da_forecast_p90,
                        total_load_forecast,
                        total_load_forecast_p10,
                        total_load_forecast_p90,
                        total_load_id_forecast,
                        total_load_id_forecast_p10,
                        total_load_id_forecast_p90,
                        total_load_wa_forecast,
                        total_load_wa_forecast_p10,
                        total_load_wa_forecast_p90
                      )
                    VALUES
                      (
                        $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19
                      );
                    "
    }
}

impl SolarLoad {
    pub fn from_node(n: &Node) -> SolarLoad {
        let cipu_load = child_filter_n("CipuLoad", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let cipu_load_da_forecast = child_filter_n("CipuLoadDAForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let cipu_load_forecast = child_filter_n("CipuLoadForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let cipu_load_id_forecast = child_filter_n("CipuLoadIDForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let date_time = child_filter_n("StartsOn", n)
            .and_then(|c| c.text())
            .and_then(str_to_utc);
        let tick = child_filter_n("Tick", n)
            .and_then(|n| n.text())
            .and_then(str_to_i32);
        let total_load = child_filter_n("TotalLoad", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let total_load_da_forecast = child_filter_n("TotalLoadDAForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let total_load_da_forecast_p10 = child_filter_n("TotalLoadDAForecastP10", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let total_load_da_forecast_p90 = child_filter_n("TotalLoadDAForecastP90", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let total_load_forecast = child_filter_n("TotalLoadForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let total_load_forecast_p10 = child_filter_n("TotalLoadForecastP10", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let total_load_forecast_p90 = child_filter_n("TotalLoadForecastP90", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let total_load_id_forecast = child_filter_n("TotalLoadIDForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let total_load_id_forecast_p10 = child_filter_n("TotalLoadIDForecastP10", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let total_load_id_forecast_p90 = child_filter_n("TotalLoadIDForecastP90", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let total_load_wa_forecast = child_filter_n("TotalLoadWAForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let total_load_wa_forecast_p10 = child_filter_n("TotalLoadWAForecastP10", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let total_load_wa_forecast_p90 = child_filter_n("TotalLoadWAForecastP90", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        SolarLoad {
            cipu_load,
            cipu_load_da_forecast,
            cipu_load_forecast,
            cipu_load_id_forecast,
            date_time,
            tick,
            total_load,
            total_load_da_forecast,
            total_load_da_forecast_p10,
            total_load_da_forecast_p90,
            total_load_forecast,
            total_load_forecast_p10,
            total_load_forecast_p90,
            total_load_id_forecast,
            total_load_id_forecast_p10,
            total_load_id_forecast_p90,
            total_load_wa_forecast,
            total_load_wa_forecast_p10,
            total_load_wa_forecast_p90,
        }
    }
}

impl Store {
    pub async fn save_solar_load(&self, record: &SolarLoad) -> Result<u64, StoreError> {
        self.save(record).await
    }
}

// LOAD - past measures

#[derive(Clone)]
pub struct SolarLoadPast {
    pub cipu_load: NullableFloat,
    pub date_time: Option<DateTime<Utc>>,
    pub tick: NullableInt,
    pub total_load: NullableFloat,
}

impl SolarLoadPast {
    pub fn from_node(n: &Node) -> SolarLoadPast {
        let cipu_load = child_filter_n("CipuLoad", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let date_time = child_filter_n("StartsOn", n)
            .and_then(|c| c.text())
            .and_then(str_to_utc);
        let tick = child_filter_n("Tick", n)
            .and_then(|n| n.text())
            .and_then(str_to_i32);
        let total_load = child_filter_n("TotalLoad", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        SolarLoadPast {
            cipu_load,
            date_time,
            tick,
            total_load,
        }
    }
}

impl ToParams for SolarLoadPast {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        (params() + &self.cipu_load + &self.date_time + &self.tick + &self.total_load).inner()
    }
}

impl WithSQL for SolarLoadPast {
    fn sql() -> &'static str {
        "
        INSERT INTO load_past (
            cipu_load,
            ts,
            tick,
            total_load
          )
        VALUES
          ($1, $2, $3, $4);
                    "
    }
}

impl Store {
    pub async fn save_solar_load_past(&self, record: &SolarLoadPast) -> Result<u64, StoreError> {
        self.save(record).await
    }
}
