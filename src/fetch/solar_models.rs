use crate::fetch::child_filter_n;
use crate::model::{params, ToParams, WithSQL};
use crate::store::{Region, Store, StoreError};
use crate::util::*;
use chrono::{DateTime, NaiveDate, Utc};
use const_format::concatcp;
use paste::paste;
use roxmltree::Node;

type NullableFloat = Option<f64>;

//SOLAR - forecast

const fn table_name_forecast(region: Region) -> &'static str {
    match region {
        Region::Belgique => "solar",
        Region::Bruxelles => "solar_bxl",
        Region::Antw => "solar_ant",
        Region::Hainaut => "solar_hai",
        Region::Limburg => "solar_lim",
        Region::Liege => "solar_lie",
        Region::Luxembourg => "solar_lux",
        Region::Namur => "solar_nam",
        Region::OVlaanderen => "solar_o_vl",
        Region::BrabantW => "solar_br_w",
        Region::WVlaanderen => "solar_w_vl",
        Region::VlBrabant => "solar_vl_b",
    }
}

#[derive(Clone)]
pub struct SolarForecastRecord {
    pub day_ahead_forecast: NullableFloat,
    pub monitored_capacity: NullableFloat,
    pub most_recent_forecast: NullableFloat,
    pub date_time: Option<DateTime<Utc>>,
    pub week_ahead_forecast: NullableFloat,
}

// macro_rules! insert_solar_forecast_sql {
//     ($region:path) => {
//         concatcp!(
//             "INSERT INTO ",
//             table_name_forecast($region),
//             "  (
//                 day_ahead_forecast,
//                 monitored_capacity,
//                 most_recent_forecast,
//                 ts,
//                 week_ahead_forecast
//               )
//               VALUES
//                 ($1, $2, $3, $4, $5);"
//         )
//     };
// }

macro_rules! upsert_solar_forecast_sql {
    ($region:path) => {
        concatcp!(
            "INSERT INTO ",
            table_name_forecast($region),
            "  (
                day_ahead_forecast,
                monitored_capacity,
                most_recent_forecast,
                ts,
                week_ahead_forecast
              )
              VALUES
                ($1, $2, $3, $4, $5)
              ON CONFLICT 
                  (ts)
                  DO UPDATE SET 
                  (
                    day_ahead_forecast,
                    monitored_capacity,
                    most_recent_forecast,
                    ts,
                    week_ahead_forecast
                  ) = ($1, $2, $3, $4, $5);"
        )
    }
}

// macro_rules! delete_solar_forecast_sql {
//     ($region:path) => {
//         concatcp!(
//             "DELETE FROM ",
//             table_name_forecast($region),
//             " WHERE ts > $1"
//         )
//     };
// }

macro_rules! forecast_region {
    ($region:ident) => {
        paste! {

            #[derive(Clone)]
            pub struct [< SolarForecast $region >](SolarForecastRecord);

            impl<'a> Into<&'a SolarForecastRecord> for &'a [< SolarForecast $region >] {
                fn into(self) -> &'a SolarForecastRecord {
                    &self.0
                }
            }

            impl ToParams for [< SolarForecast $region >] {
                fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
                    (params()
                        + &self.0.day_ahead_forecast
                        + &self.0.monitored_capacity
                        + &self.0.most_recent_forecast
                        + &self.0.date_time
                        + &self.0.week_ahead_forecast)
                    .inner()
                }
            }

            impl WithSQL for [< SolarForecast $region >] {
                fn sql() -> &'static str {
                    upsert_solar_forecast_sql!(Region::$region)
                }
            }

            // pub struct [< DeleteSolarForecast $region >] {
            //     from: DateTime<Utc>,
            // }

            // impl ToParams for [< DeleteSolarForecast $region >] {
            //     fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
            //         (params() + &self.from ).inner()
            //     }
            // }

            // impl WithSQL for [< DeleteSolarForecast $region >] {
            //     fn sql() -> &'static str {
            //         delete_solar_forecast_sql!(Region::$region)
            //     }
            // }
        }
    };
}

forecast_region!(Belgique);
forecast_region!(Bruxelles);
forecast_region!(Antw);
forecast_region!(Hainaut);
forecast_region!(Limburg);
forecast_region!(Liege);
forecast_region!(Luxembourg);
forecast_region!(Namur);
forecast_region!(OVlaanderen);
forecast_region!(VlBrabant);
forecast_region!(BrabantW);
forecast_region!(WVlaanderen);

#[derive(Clone)]
pub enum SolarForecast {
    Belgique(SolarForecastBelgique),
    Bruxelles(SolarForecastBruxelles),
    Antw(SolarForecastAntw),
    Hainaut(SolarForecastHainaut),
    Limburg(SolarForecastLimburg),
    Liege(SolarForecastLiege),
    Luxembourg(SolarForecastLuxembourg),
    Namur(SolarForecastNamur),
    OVlaanderen(SolarForecastOVlaanderen),
    VlBrabant(SolarForecastVlBrabant),
    BrabantW(SolarForecastBrabantW),
    WVlaanderen(SolarForecastWVlaanderen),
}

impl SolarForecast {
    fn record(&self) -> &SolarForecastRecord {
        match self {
            SolarForecast::Belgique(s) => s.into(),
            SolarForecast::Bruxelles(s) => s.into(),
            SolarForecast::Antw(s) => s.into(),
            SolarForecast::Hainaut(s) => s.into(),
            SolarForecast::Limburg(s) => s.into(),
            SolarForecast::Liege(s) => s.into(),
            SolarForecast::Luxembourg(s) => s.into(),
            SolarForecast::Namur(s) => s.into(),
            SolarForecast::OVlaanderen(s) => s.into(),
            SolarForecast::VlBrabant(s) => s.into(),
            SolarForecast::BrabantW(s) => s.into(),
            SolarForecast::WVlaanderen(s) => s.into(),
        }
    }
}

impl SolarForecast {
    // pub fn day_ahead_forecast(&self) -> NullableFloat {
    //     self.record().day_ahead_forecast
    // }
    // pub fn monitored_capacity(&self) -> NullableFloat {
    //     self.record().monitored_capacity
    // }
    pub fn most_recent_forecast(&self) -> NullableFloat {
        self.record().most_recent_forecast
    }
    pub fn date_time(&self) -> Option<DateTime<Utc>> {
        self.record().date_time
    }
    // pub fn week_ahead_forecast(&self) -> NullableFloat {
    //     self.record().week_ahead_forecast
    // }
}

impl SolarForecast {
    pub fn from_node(n: &Node, region: Region) -> SolarForecast {
        let day_ahead_forecast = child_filter_n("DayAheadForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let monitored_capacity = child_filter_n("MonitoredCapacity", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let most_recent_forecast = child_filter_n("MostRecentForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let date_time = child_filter_n("StartsOn", n).and_then(|c| {
            child_filter_n(
                ("http://schemas.datacontract.org/2004/07/System", "DateTime"),
                &c,
            )
            .and_then(|c| c.text())
            .and_then(str_to_utc)
        });
        let week_ahead_forecast = child_filter_n("WeekAheadForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let record = SolarForecastRecord {
            day_ahead_forecast,
            monitored_capacity,
            most_recent_forecast,
            date_time,
            week_ahead_forecast,
        };

        match region {
            Region::Belgique => SolarForecast::Belgique(SolarForecastBelgique(record)),
            Region::Bruxelles => SolarForecast::Bruxelles(SolarForecastBruxelles(record)),
            Region::Antw => SolarForecast::Antw(SolarForecastAntw(record)),
            Region::Hainaut => SolarForecast::Hainaut(SolarForecastHainaut(record)),
            Region::Limburg => SolarForecast::Limburg(SolarForecastLimburg(record)),
            Region::Liege => SolarForecast::Liege(SolarForecastLiege(record)),
            Region::Luxembourg => SolarForecast::Luxembourg(SolarForecastLuxembourg(record)),
            Region::Namur => SolarForecast::Namur(SolarForecastNamur(record)),
            Region::OVlaanderen => SolarForecast::OVlaanderen(SolarForecastOVlaanderen(record)),
            Region::VlBrabant => SolarForecast::VlBrabant(SolarForecastVlBrabant(record)),
            Region::BrabantW => SolarForecast::BrabantW(SolarForecastBrabantW(record)),
            Region::WVlaanderen => SolarForecast::WVlaanderen(SolarForecastWVlaanderen(record)),
        }
    }
}

// pub enum DeleteSolarForecast {
//     Belgique(DeleteSolarForecastBelgique),
//     Bruxelles(DeleteSolarForecastBruxelles),
//     Antw(DeleteSolarForecastAntw),
//     Hainaut(DeleteSolarForecastHainaut),
//     Limburg(DeleteSolarForecastLimburg),
//     Liege(DeleteSolarForecastLiege),
//     Luxembourg(DeleteSolarForecastLuxembourg),
//     Namur(DeleteSolarForecastNamur),
//     OVlaanderen(DeleteSolarForecastOVlaanderen),
//     VlBrabant(DeleteSolarForecastVlBrabant),
//     BrabantW(DeleteSolarForecastBrabantW),
//     WVlaanderen(DeleteSolarForecastWVlaanderen),
// }

// impl DeleteSolarForecast {
//     pub fn new(from: DateTime<Utc>, region: Region) -> DeleteSolarForecast {
//         match region {
//             Region::Belgique => DeleteSolarForecast::Belgique(DeleteSolarForecastBelgique { from }),
//             Region::Bruxelles => {
//                 DeleteSolarForecast::Bruxelles(DeleteSolarForecastBruxelles { from })
//             }
//             Region::Antw => DeleteSolarForecast::Antw(DeleteSolarForecastAntw { from }),
//             Region::Hainaut => DeleteSolarForecast::Hainaut(DeleteSolarForecastHainaut { from }),
//             Region::Limburg => DeleteSolarForecast::Limburg(DeleteSolarForecastLimburg { from }),
//             Region::Liege => DeleteSolarForecast::Liege(DeleteSolarForecastLiege { from }),
//             Region::Luxembourg => {
//                 DeleteSolarForecast::Luxembourg(DeleteSolarForecastLuxembourg { from })
//             }
//             Region::Namur => DeleteSolarForecast::Namur(DeleteSolarForecastNamur { from }),
//             Region::OVlaanderen => {
//                 DeleteSolarForecast::OVlaanderen(DeleteSolarForecastOVlaanderen { from })
//             }
//             Region::VlBrabant => {
//                 DeleteSolarForecast::VlBrabant(DeleteSolarForecastVlBrabant { from })
//             }
//             Region::BrabantW => DeleteSolarForecast::BrabantW(DeleteSolarForecastBrabantW { from }),
//             Region::WVlaanderen => {
//                 DeleteSolarForecast::WVlaanderen(DeleteSolarForecastWVlaanderen { from })
//             }
//         }
//     }
// }

impl Store {
    pub async fn save_solar_forecast(&self, record: &SolarForecast) -> Result<u64, StoreError> {
        match record {
            SolarForecast::Belgique(record) => self.save(record).await,
            SolarForecast::Bruxelles(record) => self.save(record).await,
            SolarForecast::Antw(record) => self.save(record).await,
            SolarForecast::Hainaut(record) => self.save(record).await,
            SolarForecast::Limburg(record) => self.save(record).await,
            SolarForecast::Liege(record) => self.save(record).await,
            SolarForecast::Luxembourg(record) => self.save(record).await,
            SolarForecast::Namur(record) => self.save(record).await,
            SolarForecast::OVlaanderen(record) => self.save(record).await,
            SolarForecast::VlBrabant(record) => self.save(record).await,
            SolarForecast::BrabantW(record) => self.save(record).await,
            SolarForecast::WVlaanderen(record) => self.save(record).await,
        }
    }
    // pub async fn delete_solar_forecast(
    //     &self,
    //     record: &DeleteSolarForecast,
    // ) -> Result<u64, StoreError> {
    //     match record {
    //         DeleteSolarForecast::Belgique(record) => self.delete(record).await,
    //         DeleteSolarForecast::Bruxelles(record) => self.delete(record).await,
    //         DeleteSolarForecast::Antw(record) => self.delete(record).await,
    //         DeleteSolarForecast::Hainaut(record) => self.delete(record).await,
    //         DeleteSolarForecast::Limburg(record) => self.delete(record).await,
    //         DeleteSolarForecast::Liege(record) => self.delete(record).await,
    //         DeleteSolarForecast::Luxembourg(record) => self.delete(record).await,
    //         DeleteSolarForecast::Namur(record) => self.delete(record).await,
    //         DeleteSolarForecast::OVlaanderen(record) => self.delete(record).await,
    //         DeleteSolarForecast::VlBrabant(record) => self.delete(record).await,
    //         DeleteSolarForecast::BrabantW(record) => self.delete(record).await,
    //         DeleteSolarForecast::WVlaanderen(record) => self.delete(record).await,
    //     }
    // }
}

// SOLAR - production

const fn table_name_prod(region: Region) -> &'static str {
    match region {
        Region::Belgique => "solar_past",
        Region::Bruxelles => "solar_bxl_prod",
        Region::Antw => "solar_ant_prod",
        Region::Hainaut => "solar_hai_prod",
        Region::Limburg => "solar_lim_prod",
        Region::Liege => "solar_lie_prod",
        Region::Luxembourg => "solar_lux_prod",
        Region::Namur => "solar_nam_prod",
        Region::OVlaanderen => "solar_o_vl_prod",
        Region::BrabantW => "solar_br_w_prod",
        Region::WVlaanderen => "solar_w_vl_prod",
        Region::VlBrabant => "solar_vl_b_prod",
    }
}

#[derive(Clone)]
pub struct SolarProdRecord {
    pub load_factor: NullableFloat,
    pub monitored_capacity: NullableFloat,
    pub realtime: NullableFloat,
    pub date_time: Option<DateTime<Utc>>,
}

// macro_rules! insert_solar_prod_sql {
//     ($region:path) => {
//         concatcp!(
//             "INSERT INTO ",
//             table_name_prod($region),
//             "  (
//                 load_factor,
//                 monitored_capacity,
//                 real_time,
//                 ts
//               )
//               VALUES
//                 ($1, $2, $3, $4);"
//         )
//     };
// }

macro_rules! upsert_solar_prod_sql {
    ($region:path) => {
        concatcp!(
            "INSERT INTO ",
            table_name_prod($region),
            "  (
                load_factor,
                monitored_capacity,
                real_time,
                ts
              )
              VALUES
                ($1, $2, $3, $4)
              ON CONFLICT 
                  (ts)
                  DO UPDATE SET 
                  (
                    load_factor,
                    monitored_capacity,
                    real_time,
                    ts
                  ) = ($1, $2, $3, $4);"
        )
    };
}

// macro_rules! delete_solar_prod_sql {
//     ($region:path) => {
//         concatcp!("DELETE FROM ", table_name_prod($region), " WHERE ts > $1")
//     };
// }

macro_rules! prod_region {
    ($region:ident) => {
        paste! {

            #[derive(Clone)]
            pub struct [< SolarProd $region >](SolarProdRecord);

            impl<'a> Into<&'a SolarProdRecord> for &'a [< SolarProd $region >] {
                fn into(self) -> &'a SolarProdRecord {
                    &self.0
                }
            }

            impl ToParams for [< SolarProd $region >] {
                fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
                    (params()
                        + &self.0.load_factor
                        + &self.0.monitored_capacity
                        + &self.0.realtime
                        + &self.0.date_time)
                    .inner()
                }
            }

            impl WithSQL for [< SolarProd $region >] {
                fn sql() -> &'static str {
                    upsert_solar_prod_sql!(Region::$region)
                }
            }

            // pub struct [< DeleteSolarProd $region >] {
            //     from: DateTime<Utc>,
            // }

            // impl ToParams for [< DeleteSolarProd $region >] {
            //     fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
            //         (params() + &self.from ).inner()
            //     }
            // }

            // impl WithSQL for [< DeleteSolarProd $region >] {
            //     fn sql() -> &'static str {
            //         delete_solar_prod_sql!(Region::$region)
            //     }
            // }
        }
    };
}

prod_region!(Belgique);
prod_region!(Bruxelles);
prod_region!(Antw);
prod_region!(Hainaut);
prod_region!(Limburg);
prod_region!(Liege);
prod_region!(Luxembourg);
prod_region!(Namur);
prod_region!(OVlaanderen);
prod_region!(VlBrabant);
prod_region!(BrabantW);
prod_region!(WVlaanderen);

#[derive(Clone)]
pub struct SolarDailyProd {
    pub date: Option<NaiveDate>,
    pub production_obs_kwh: NullableFloat,
    pub productivite_kwh_by_kwc: NullableFloat,
}

impl SolarDailyProd {
    // pub fn date(&self) -> Option<NaiveDate> {
    //     self.date
    // }
    // pub fn production_obs_kwh(&self) -> NullableFloat {
    //     self.production_obs_kwh
    // }
    // pub fn productivite_kwh_by_kwc(&self) -> NullableFloat {
    //     self.productivite_kwh_by_kwc
    // }
}

impl WithSQL for SolarDailyProd {
    // fn sql() -> &'static str {
    //     "INSERT INTO solar_daily_past (date,production_obs_kwh, productivite_kwh_by_kwc)
    //     SELECT ts::timestamp::date as date, sum(most_recent_forecast/4000) as production_obs_kwh, sum(most_recent_forecast/monitored_capacity)/4 as productivite_kwh_by_kwc
    //     FROM solar
    //     WHERE ts>='2021-01-01 02:00:00+02'
    //     GROUP BY date;"
    // }
    fn sql() -> &'static str {
        "INSERT INTO solar_daily_past
            (date, production_obs_kwh, productivite_kwh_by_kwc)
            VALUES
                ($1, $2, $3)
            ON CONFLICT 
                (date)
                DO UPDATE SET 
                (date,production_obs_kwh, productivite_kwh_by_kwc) = ($1, $2, $3);"
    }
}

impl ToParams for SolarDailyProd {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        (params() + &self.date + &self.production_obs_kwh + &self.productivite_kwh_by_kwc).inner()
    }
}

impl Store {
    pub async fn save_solar_daily_prod(&self, record: &SolarDailyProd) -> Result<u64, StoreError> {
        self.save(record).await
    }
}

#[derive(Clone)]
pub enum SolarProd {
    Belgique(SolarProdBelgique),
    Bruxelles(SolarProdBruxelles),
    Antw(SolarProdAntw),
    Hainaut(SolarProdHainaut),
    Limburg(SolarProdLimburg),
    Liege(SolarProdLiege),
    Luxembourg(SolarProdLuxembourg),
    Namur(SolarProdNamur),
    OVlaanderen(SolarProdOVlaanderen),
    VlBrabant(SolarProdVlBrabant),
    BrabantW(SolarProdBrabantW),
    WVlaanderen(SolarProdWVlaanderen),
}

impl SolarProd {
    // pub fn load_factor(&self) -> NullableFloat {
    //     self.record().load_factor
    // }
    pub fn monitored_capacity(&self) -> NullableFloat {
        self.record().monitored_capacity
    }
    pub fn realtime(&self) -> NullableFloat {
        self.record().realtime
    }
    pub fn date_time(&self) -> Option<DateTime<Utc>> {
        self.record().date_time
    }
}

impl SolarProd {
    fn record(&self) -> &SolarProdRecord {
        match self {
            SolarProd::Belgique(s) => s.into(),
            SolarProd::Bruxelles(s) => s.into(),
            SolarProd::Antw(s) => s.into(),
            SolarProd::Hainaut(s) => s.into(),
            SolarProd::Limburg(s) => s.into(),
            SolarProd::Liege(s) => s.into(),
            SolarProd::Luxembourg(s) => s.into(),
            SolarProd::Namur(s) => s.into(),
            SolarProd::OVlaanderen(s) => s.into(),
            SolarProd::VlBrabant(s) => s.into(),
            SolarProd::BrabantW(s) => s.into(),
            SolarProd::WVlaanderen(s) => s.into(),
        }
    }
}

impl SolarProd {
    pub fn from_node(n: &Node, region: Region) -> SolarProd {
        let load_factor = child_filter_n("LoadFactor", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let monitored_capacity = child_filter_n("MonitoredCapacity", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let realtime = child_filter_n("RealTime", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let date_time = child_filter_n("StartsOn", n).and_then(|c| {
            child_filter_n(
                ("http://schemas.datacontract.org/2004/07/System", "DateTime"),
                &c,
            )
            .and_then(|c| c.text())
            .and_then(str_to_utc)
        });

        let record = SolarProdRecord {
            load_factor,
            monitored_capacity,
            realtime,
            date_time,
        };

        match region {
            Region::Belgique => SolarProd::Belgique(SolarProdBelgique(record)),
            Region::Bruxelles => SolarProd::Bruxelles(SolarProdBruxelles(record)),
            Region::Antw => SolarProd::Antw(SolarProdAntw(record)),
            Region::Hainaut => SolarProd::Hainaut(SolarProdHainaut(record)),
            Region::Limburg => SolarProd::Limburg(SolarProdLimburg(record)),
            Region::Liege => SolarProd::Liege(SolarProdLiege(record)),
            Region::Luxembourg => SolarProd::Luxembourg(SolarProdLuxembourg(record)),
            Region::Namur => SolarProd::Namur(SolarProdNamur(record)),
            Region::OVlaanderen => SolarProd::OVlaanderen(SolarProdOVlaanderen(record)),
            Region::VlBrabant => SolarProd::VlBrabant(SolarProdVlBrabant(record)),
            Region::BrabantW => SolarProd::BrabantW(SolarProdBrabantW(record)),
            Region::WVlaanderen => SolarProd::WVlaanderen(SolarProdWVlaanderen(record)),
        }
    }
}

// pub enum DeleteSolarProd {
//     Belgique(DeleteSolarProdBelgique),
//     Bruxelles(DeleteSolarProdBruxelles),
//     Antw(DeleteSolarProdAntw),
//     Hainaut(DeleteSolarProdHainaut),
//     Limburg(DeleteSolarProdLimburg),
//     Liege(DeleteSolarProdLiege),
//     Luxembourg(DeleteSolarProdLuxembourg),
//     Namur(DeleteSolarProdNamur),
//     OVlaanderen(DeleteSolarProdOVlaanderen),
//     VlBrabant(DeleteSolarProdVlBrabant),
//     BrabantW(DeleteSolarProdBrabantW),
//     WVlaanderen(DeleteSolarProdWVlaanderen),
// }

// impl DeleteSolarProd {
//     pub fn new(from: DateTime<Utc>, region: Region) -> DeleteSolarProd {
//         match region {
//             Region::Belgique => DeleteSolarProd::Belgique(DeleteSolarProdBelgique { from }),
//             Region::Bruxelles => DeleteSolarProd::Bruxelles(DeleteSolarProdBruxelles { from }),
//             Region::Antw => DeleteSolarProd::Antw(DeleteSolarProdAntw { from }),
//             Region::Hainaut => DeleteSolarProd::Hainaut(DeleteSolarProdHainaut { from }),
//             Region::Limburg => DeleteSolarProd::Limburg(DeleteSolarProdLimburg { from }),
//             Region::Liege => DeleteSolarProd::Liege(DeleteSolarProdLiege { from }),
//             Region::Luxembourg => DeleteSolarProd::Luxembourg(DeleteSolarProdLuxembourg { from }),
//             Region::Namur => DeleteSolarProd::Namur(DeleteSolarProdNamur { from }),
//             Region::OVlaanderen => {
//                 DeleteSolarProd::OVlaanderen(DeleteSolarProdOVlaanderen { from })
//             }
//             Region::VlBrabant => DeleteSolarProd::VlBrabant(DeleteSolarProdVlBrabant { from }),
//             Region::BrabantW => DeleteSolarProd::BrabantW(DeleteSolarProdBrabantW { from }),
//             Region::WVlaanderen => {
//                 DeleteSolarProd::WVlaanderen(DeleteSolarProdWVlaanderen { from })
//             }
//         }
//     }
// }

impl Store {
    pub async fn save_solar_production(&self, record: &SolarProd) -> Result<u64, StoreError> {
        match record {
            SolarProd::Belgique(record) => self.save(record).await,
            SolarProd::Bruxelles(record) => self.save(record).await,
            SolarProd::Antw(record) => self.save(record).await,
            SolarProd::Hainaut(record) => self.save(record).await,
            SolarProd::Limburg(record) => self.save(record).await,
            SolarProd::Liege(record) => self.save(record).await,
            SolarProd::Luxembourg(record) => self.save(record).await,
            SolarProd::Namur(record) => self.save(record).await,
            SolarProd::OVlaanderen(record) => self.save(record).await,
            SolarProd::VlBrabant(record) => self.save(record).await,
            SolarProd::BrabantW(record) => self.save(record).await,
            SolarProd::WVlaanderen(record) => self.save(record).await,
        }
    }

    // pub async fn delete_solar_prod(&self, record: &DeleteSolarProd) -> Result<u64, StoreError> {
    //     match record {
    //         DeleteSolarProd::Belgique(record) => self.delete(record).await,
    //         DeleteSolarProd::Bruxelles(record) => self.delete(record).await,
    //         DeleteSolarProd::Antw(record) => self.delete(record).await,
    //         DeleteSolarProd::Hainaut(record) => self.delete(record).await,
    //         DeleteSolarProd::Limburg(record) => self.delete(record).await,
    //         DeleteSolarProd::Liege(record) => self.delete(record).await,
    //         DeleteSolarProd::Luxembourg(record) => self.delete(record).await,
    //         DeleteSolarProd::Namur(record) => self.delete(record).await,
    //         DeleteSolarProd::OVlaanderen(record) => self.delete(record).await,
    //         DeleteSolarProd::VlBrabant(record) => self.delete(record).await,
    //         DeleteSolarProd::BrabantW(record) => self.delete(record).await,
    //         DeleteSolarProd::WVlaanderen(record) => self.delete(record).await,
    //     }
    // }
}
