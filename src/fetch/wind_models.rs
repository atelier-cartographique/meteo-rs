use crate::fetch::child_filter_n;
use crate::model::{params, ToParams, WithSQL};
use crate::store::{Offshore, Store, StoreError};
use crate::util::*;
use chrono::{DateTime, Utc};
use roxmltree::Node;

type NullableInt = Option<i32>;
type NullableFloat = Option<f64>;

//WIND - forecast
#[derive(Clone)]
pub struct WindRecord {
    pub bid: NullableInt,
    pub day_ahead_forecast: NullableFloat,
    pub monitored_capacity: NullableFloat,
    pub most_recent_forecast: NullableFloat,
    pub date_time: Option<DateTime<Utc>>,
    pub week_ahead_forecast: NullableFloat,
    pub day_ahead_confidence10: NullableFloat,
    pub day_ahead_confidence90: NullableFloat,
    pub most_recent_confidence10: NullableFloat,
    pub most_recent_confidence90: NullableFloat,
    pub week_ahead_confidence10: NullableFloat,
    pub week_ahead_confidence90: NullableFloat,
}

#[derive(Clone)]
pub struct WindRecordAll(WindRecord);

#[derive(Clone)]
pub struct WindRecordOffShore(WindRecord);

#[derive(Clone)]
pub struct WindRecordOnShore(WindRecord);

// macro_rules! insert_wind_sql {
//     ($table:expr) => {
//         concat!(
//             "INSERT INTO ",
//             $table,
//             "  (
//                 bid,
//                 day_ahead_forecast,
//                 monitored_capacity,
//                 most_recent_forecast,
//                 ts,
//                 week_ahead_forecast,
//                 day_ahead_confidence10,
//                 day_ahead_confidence90,
//                 most_recent_confidence10,
//                 most_recent_confidence90,
//                 week_ahead_confidence10,
//                 week_ahead_confidence90
//               )
//             VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12 );"
//         )
//     };
// }
macro_rules! upsert_wind_sql {
    ($table:expr) => {
        concat!(
            "INSERT INTO ",
            $table,
            "  (
                bid,
                day_ahead_forecast,
                monitored_capacity,
                most_recent_forecast,
                ts,
                week_ahead_forecast,
                day_ahead_confidence10,
                day_ahead_confidence90,
                most_recent_confidence10,
                most_recent_confidence90,
                week_ahead_confidence10,
                week_ahead_confidence90
              )
            VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12 )
            ON CONFLICT 
                  (ts)
                  DO UPDATE SET 
                  (
                    bid,
                    day_ahead_forecast,
                    monitored_capacity,
                    most_recent_forecast,
                    ts,
                    week_ahead_forecast,
                    day_ahead_confidence10,
                    day_ahead_confidence90,
                    most_recent_confidence10,
                    most_recent_confidence90,
                    week_ahead_confidence10,
                    week_ahead_confidence90
                  ) = ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12 );"
        )
    };
}

impl WithSQL for WindRecordAll {
    fn sql() -> &'static str {
        upsert_wind_sql!("wind")
    }
}

impl<'a> Into<&'a WindRecord> for &'a WindRecordAll {
    fn into(self) -> &'a WindRecord {
        &self.0
    }
}

impl WithSQL for WindRecordOffShore {
    fn sql() -> &'static str {
        upsert_wind_sql!("wind_offshore")
    }
}

impl<'a> Into<&'a WindRecord> for &'a WindRecordOffShore {
    fn into(self) -> &'a WindRecord {
        &self.0
    }
}

impl WithSQL for WindRecordOnShore {
    fn sql() -> &'static str {
        upsert_wind_sql!("wind_onshore")
    }
}

impl<'a> Into<&'a WindRecord> for &'a WindRecordOnShore {
    fn into(self) -> &'a WindRecord {
        &self.0
    }
}

fn wind_record_to_params<'a>(
    record: &'a WindRecord,
) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
    (params()
        + &record.bid
        + &record.day_ahead_forecast
        + &record.monitored_capacity
        + &record.most_recent_forecast
        + &record.date_time
        + &record.week_ahead_forecast
        + &record.day_ahead_confidence10
        + &record.day_ahead_confidence90
        + &record.most_recent_confidence10
        + &record.most_recent_confidence90
        + &record.week_ahead_confidence10
        + &record.week_ahead_confidence90)
        .inner()
}

impl ToParams for WindRecordAll {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        wind_record_to_params(self.into())
    }
}

impl ToParams for WindRecordOffShore {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        wind_record_to_params(self.into())
    }
}

impl ToParams for WindRecordOnShore {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        wind_record_to_params(self.into())
    }
}

impl Store {
    async fn save_wind_record_all(&self, record: &WindRecordAll) -> Result<u64, StoreError> {
        self.save(record).await
    }
    async fn save_wind_record_offshore(
        &self,
        record: &WindRecordOffShore,
    ) -> Result<u64, StoreError> {
        self.save(record).await
    }
    async fn save_wind_record_onshore(
        &self,
        record: &WindRecordOnShore,
    ) -> Result<u64, StoreError> {
        self.save(record).await
    }
}

#[derive(Clone)]
pub enum WindForecast {
    All(WindRecordAll),
    OffShore(WindRecordOffShore),
    OnShore(WindRecordOnShore),
}

impl WindForecast {
    fn record(&self) -> &WindRecord {
        match self {
            WindForecast::All(w) => w.into(),
            WindForecast::OffShore(w) => w.into(),
            WindForecast::OnShore(w) => w.into(),
        }
    }
}

impl WindForecast {
    // pub fn bid(&self) -> NullableInt {
    //     self.record().bid
    // }
    // pub fn day_ahead_forecast(&self) -> NullableFloat {
    //     self.record().day_ahead_forecast
    // }
    // pub fn monitored_capacity(&self) -> NullableFloat {
    //     self.record().monitored_capacity
    // }
    pub fn most_recent_forecast(&self) -> NullableFloat {
        self.record().most_recent_forecast
    }
    pub fn date_time(&self) -> Option<DateTime<Utc>> {
        self.record().date_time
    }
    // pub fn week_ahead_forecast(&self) -> NullableFloat {
    //     self.record().week_ahead_forecast
    // }
    // pub fn day_ahead_confidence10(&self) -> NullableFloat {
    //     self.record().day_ahead_confidence10
    // }
    // pub fn day_ahead_confidence90(&self) -> NullableFloat {
    //     self.record().day_ahead_confidence90
    // }
    // pub fn most_recent_confidence10(&self) -> NullableFloat {
    //     self.record().most_recent_confidence10
    // }
    // pub fn most_recent_confidence90(&self) -> NullableFloat {
    //     self.record().most_recent_confidence90
    // }
    // pub fn week_ahead_confidence10(&self) -> NullableFloat {
    //     self.record().week_ahead_confidence10
    // }
    // pub fn week_ahead_confidence90(&self) -> NullableFloat {
    //     self.record().week_ahead_confidence90
    // }
}

impl WindForecast {
    pub fn from_node(n: &Node, tag: Offshore) -> WindForecast {
        let bid = child_filter_n("Bid", n)
            .and_then(|n| n.text())
            .and_then(str_to_i32);

        let day_ahead_forecast = child_filter_n("DayAheadForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        // let load_factor = child_filter_n("LoadFactor", n)
        //     .and_then(|n| n.text())
        //     .and_then(str_to_f64);

        let monitored_capacity = child_filter_n("MonitoredCapacity", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let most_recent_forecast = child_filter_n("MostRecentForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        // let realtime = child_filter_n("Realtime", n)
        //     .and_then(|n| n.text())
        //     .and_then(str_to_f64);

        let date_time = child_filter_n("StartsOn", n).and_then(|c| {
            child_filter_n(
                ("http://schemas.datacontract.org/2004/07/System", "DateTime"),
                &c,
            )
            .and_then(|c| c.text())
            .and_then(str_to_utc)
        });
        let week_ahead_forecast = child_filter_n("WeekAheadForecast", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let day_ahead_confidence10 = child_filter_n("DayAheadConfidence10", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let day_ahead_confidence90 = child_filter_n("DayAheadConfidence90", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let most_recent_confidence10 = child_filter_n("MostRecentConfidence10", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let most_recent_confidence90 = child_filter_n("MostRecentConfidence90", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let week_ahead_confidence10 = child_filter_n("WeekAheadConfidence10", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);
        let week_ahead_confidence90 = child_filter_n("WeekAheadConfidence90", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let inner_record = WindRecord {
            bid,
            day_ahead_forecast,
            // load_factor,
            monitored_capacity,
            most_recent_forecast,
            // realtime,
            date_time,
            week_ahead_forecast,
            day_ahead_confidence10,
            day_ahead_confidence90,
            most_recent_confidence10,
            most_recent_confidence90,
            week_ahead_confidence10,
            week_ahead_confidence90,
        };
        match tag {
            Offshore::None => WindForecast::All(WindRecordAll(inner_record)),
            Offshore::True => WindForecast::OffShore(WindRecordOffShore(inner_record)),
            Offshore::False => WindForecast::OnShore(WindRecordOnShore(inner_record)),
        }
    }
}

// pub struct DeleteWindForecastAll {
//     from: DateTime<Utc>,
// }

// impl WithSQL for DeleteWindForecastAll {
//     fn sql() -> &'static str {
//         " DELETE FROM wind WHERE ts > $1;"
//     }
// }

// impl ToParams for DeleteWindForecastAll {
//     fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
//         (params() + &self.from).inner()
//     }
// }

// pub struct DeleteWindForecastOffShore {
//     from: DateTime<Utc>,
// }

// impl WithSQL for DeleteWindForecastOffShore {
//     fn sql() -> &'static str {
//         " DELETE FROM wind_offshore WHERE ts > $1;"
//     }
// }

// impl ToParams for DeleteWindForecastOffShore {
//     fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
//         (params() + &self.from).inner()
//     }
// }
// pub struct DeleteWindForecastOnShore {
//     from: DateTime<Utc>,
// }
// impl WithSQL for DeleteWindForecastOnShore {
//     fn sql() -> &'static str {
//         " DELETE FROM wind_onshore WHERE ts > $1;"
//     }
// }

// impl ToParams for DeleteWindForecastOnShore {
//     fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
//         (params() + &self.from).inner()
//     }
// }

// pub enum DeleteWindForecast {
//     All(DeleteWindForecastAll),
//     OffShore(DeleteWindForecastOffShore),
//     OnShore(DeleteWindForecastOnShore),
// }

// impl DeleteWindForecast {
//     pub fn new(from: DateTime<Utc>, o: Offshore) -> DeleteWindForecast {
//         match o {
//             Offshore::None => DeleteWindForecast::All(DeleteWindForecastAll { from }),
//             Offshore::True => DeleteWindForecast::OffShore(DeleteWindForecastOffShore { from }),
//             Offshore::False => DeleteWindForecast::OnShore(DeleteWindForecastOnShore { from }),
//         }
//     }
// }

// impl Store {
//     pub async fn delete_wind_forecast(&self, del: &DeleteWindForecast) -> Result<u64, StoreError> {
//         match del {
//             DeleteWindForecast::All(del) => self.delete(del).await,
//             DeleteWindForecast::OffShore(del) => self.delete(del).await,
//             DeleteWindForecast::OnShore(del) => self.delete(del).await,
//         }
//     }
// }

// Wind - production

#[derive(Clone, Debug)]
pub struct WindProdRecord {
    pub bid: NullableInt,
    pub load_factor: NullableFloat,
    pub monitored_capacity: NullableFloat,
    pub realtime: NullableFloat,
    pub date_time: Option<DateTime<Utc>>,
}

#[derive(Clone)]
pub struct WindProdRecordAll(WindProdRecord);
#[derive(Clone)]
pub struct WindProdRecordOffShore(WindProdRecord);
#[derive(Clone)]
pub struct WindProdRecordOnShore(WindProdRecord);

// macro_rules! insert_wind_prod_sql {
//     ($table:expr) => {
//         concat!(
//             "INSERT INTO ",
//             $table,
//             "  (
//                 bid,
//                 load_factor,
//                 monitored_capacity,
//                 real_time,
//                 ts
//               )
//             VALUES
//               ($1, $2, $3, $4, $5);"
//         )
//     };
// }
macro_rules! upsert_wind_prod_sql {
    ($table:expr) => {
        concat!(
            "INSERT INTO ",
            $table,
            "  (
                bid,
                load_factor,
                monitored_capacity,
                real_time,
                ts
              )
            VALUES
              ($1, $2, $3, $4, $5)
            ON CONFLICT 
                (ts)
                DO UPDATE SET
                (
                    bid,
                    load_factor,
                    monitored_capacity,
                    real_time,
                    ts
                  )
                = ($1, $2, $3, $4, $5);"
        )
    };
}

impl WithSQL for WindProdRecordAll {
    fn sql() -> &'static str {
        upsert_wind_prod_sql!("wind_past")
    }
}

impl<'a> Into<&'a WindProdRecord> for &'a WindProdRecordAll {
    fn into(self) -> &'a WindProdRecord {
        &self.0
    }
}

impl From<WindProdRecord> for WindProdRecordAll {
    fn from(record: WindProdRecord) -> Self {
        WindProdRecordAll(record)
    }
}

impl WithSQL for WindProdRecordOffShore {
    fn sql() -> &'static str {
        upsert_wind_prod_sql!("wind_past_offshore")
    }
}

impl<'a> Into<&'a WindProdRecord> for &'a WindProdRecordOffShore {
    fn into(self) -> &'a WindProdRecord {
        &self.0
    }
}

impl From<WindProdRecord> for WindProdRecordOffShore {
    fn from(record: WindProdRecord) -> Self {
        WindProdRecordOffShore(record)
    }
}

impl WithSQL for WindProdRecordOnShore {
    fn sql() -> &'static str {
        upsert_wind_prod_sql!("wind_past_onshore")
    }
}

impl<'a> Into<&'a WindProdRecord> for &'a WindProdRecordOnShore {
    fn into(self) -> &'a WindProdRecord {
        &self.0
    }
}

impl From<WindProdRecord> for WindProdRecordOnShore {
    fn from(record: WindProdRecord) -> Self {
        WindProdRecordOnShore(record)
    }
}

fn wind_prod_record_to_params<'a>(
    record: &'a WindProdRecord,
) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
    (params()
        + &record.bid
        + &record.load_factor
        + &record.monitored_capacity
        + &record.realtime
        + &record.date_time)
        .inner()
}

impl ToParams for WindProdRecordAll {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        wind_prod_record_to_params(self.into())
    }
}

impl ToParams for WindProdRecordOffShore {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        wind_prod_record_to_params(self.into())
    }
}

impl ToParams for WindProdRecordOnShore {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        wind_prod_record_to_params(self.into())
    }
}

impl Store {
    async fn save_wind_prod_record_all(
        &self,
        record: &WindProdRecordAll,
    ) -> Result<u64, StoreError> {
        self.save(record).await
    }
    async fn save_wind_prod_record_offshore(
        &self,
        record: &WindProdRecordOffShore,
    ) -> Result<u64, StoreError> {
        self.save(record).await
    }
    async fn save_wind_prod_record_onshore(
        &self,
        record: &WindProdRecordOnShore,
    ) -> Result<u64, StoreError> {
        self.save(record).await
    }
}

// pub struct DeleteWindProdAll {
//     from: DateTime<Utc>,
// }

// impl WithSQL for DeleteWindProdAll {
//     fn sql() -> &'static str {
//         " DELETE FROM wind_past WHERE ts > $1;"
//     }
// }

// impl ToParams for DeleteWindProdAll {
//     fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
//         (params() + &self.from).inner()
//     }
// }

// pub struct DeleteWindProdOffShore {
//     from: DateTime<Utc>,
// }

// impl WithSQL for DeleteWindProdOffShore {
//     fn sql() -> &'static str {
//         " DELETE FROM wind_past_offshore WHERE ts > $1;"
//     }
// }

// impl ToParams for DeleteWindProdOffShore {
//     fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
//         (params() + &self.from).inner()
//     }
// }
// pub struct DeleteWindProdOnShore {
//     from: DateTime<Utc>,
// }
// impl WithSQL for DeleteWindProdOnShore {
//     fn sql() -> &'static str {
//         " DELETE FROM wind_past_onshore WHERE ts > $1;"
//     }
// }

// impl ToParams for DeleteWindProdOnShore {
//     fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
//         (params() + &self.from).inner()
//     }
// }

// pub enum DeleteWindProd {
//     All(DeleteWindProdAll),
//     OffShore(DeleteWindProdOffShore),
//     OnShore(DeleteWindProdOnShore),
// }

// impl DeleteWindProd {
//     pub fn new(from: DateTime<Utc>, o: Offshore) -> DeleteWindProd {
//         match o {
//             Offshore::None => DeleteWindProd::All(DeleteWindProdAll { from }),
//             Offshore::True => DeleteWindProd::OffShore(DeleteWindProdOffShore { from }),
//             Offshore::False => DeleteWindProd::OnShore(DeleteWindProdOnShore { from }),
//         }
//     }
// }

// impl Store {
//     pub async fn delete_wind_prod(&self, del: &DeleteWindProd) -> Result<u64, StoreError> {
//         match del {
//             DeleteWindProd::All(del) => self.delete(del).await,
//             DeleteWindProd::OffShore(del) => self.delete(del).await,
//             DeleteWindProd::OnShore(del) => self.delete(del).await,
//         }
//     }
// }

#[derive(Clone)]
pub enum WindProduction {
    All(WindProdRecordAll),
    OffShore(WindProdRecordOffShore),
    OnShore(WindProdRecordOnShore),
}

impl WindProduction {
    fn record(&self) -> &WindProdRecord {
        match self {
            WindProduction::All(w) => w.into(),
            WindProduction::OffShore(w) => w.into(),
            WindProduction::OnShore(w) => w.into(),
        }
    }
}

impl WindProduction {
    // pub fn bid(&self) -> NullableInt {
    //     self.record().bid
    // }
    // pub fn load_factor(&self) -> NullableFloat {
    //     self.record().load_factor
    // }
    // pub fn monitored_capacity(&self) -> NullableFloat {
    //     self.record().monitored_capacity
    // }
    // pub fn realtime(&self) -> NullableFloat {
    //     self.record().realtime
    // }
    pub fn date_time(&self) -> Option<DateTime<Utc>> {
        self.record().date_time
    }
}

impl WindProduction {
    pub fn from_node(n: &Node, tag: Offshore) -> WindProduction {
        let bid = child_filter_n("Bid", n)
            .and_then(|n| n.text())
            .and_then(str_to_i32);
        let load_factor = child_filter_n("LoadFactor", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let monitored_capacity = child_filter_n("MonitoredCapacity", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let realtime = child_filter_n("Realtime", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let date_time = child_filter_n("StartsOn", n).and_then(|c| {
            child_filter_n(
                ("http://schemas.datacontract.org/2004/07/System", "DateTime"),
                &c,
            )
            .and_then(|c| c.text())
            .and_then(str_to_utc)
        });

        let inner_record = WindProdRecord {
            bid,
            load_factor,
            monitored_capacity,
            realtime,
            date_time,
        };
        match tag {
            Offshore::None => WindProduction::All(WindProdRecordAll(inner_record)),
            Offshore::True => WindProduction::OffShore(WindProdRecordOffShore(inner_record)),
            Offshore::False => WindProduction::OnShore(WindProdRecordOnShore(inner_record)),
        }
    }
}

// All at once

impl Store {
    pub async fn save_wind_forecast(&self, record: &WindForecast) -> Result<u64, StoreError> {
        match record {
            WindForecast::All(record) => self.save_wind_record_all(record).await,
            WindForecast::OffShore(record) => self.save_wind_record_offshore(record).await,
            WindForecast::OnShore(record) => self.save_wind_record_onshore(record).await,
        }
    }

    pub async fn save_wind_production(&self, record: &WindProduction) -> Result<u64, StoreError> {
        match record {
            WindProduction::All(record) => self.save_wind_prod_record_all(record).await,
            WindProduction::OffShore(record) => self.save_wind_prod_record_offshore(record).await,
            WindProduction::OnShore(record) => self.save_wind_prod_record_onshore(record).await,
        }
    }
}
