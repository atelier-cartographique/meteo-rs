use crate::fetch::child_filter_n;
use crate::model::{params, ToParams, WithSQL};
use crate::store::{Store, StoreError};
use crate::util::*;
use chrono::{DateTime, Duration, TimeZone, Utc};
use roxmltree::Node;

type NullableInt = Option<i32>;
type NullableFloat = Option<f64>;

//Waste - past measures
#[derive(Clone, Debug)]
pub struct WasteProd {
    pub start: Option<DateTime<Utc>>,
    pub date: Option<DateTime<Utc>>,
    pub position: NullableInt,
    pub quantity: NullableFloat,
}

impl WasteProd {
    pub fn date_from_period(n: &Node) -> Option<DateTime<Utc>> {
        child_filter_n("timeInterval", n).and_then(|c| {
            child_filter_n("start", &c)
                .and_then(|c| c.text())
                .and_then(|s| Result::ok(Utc.datetime_from_str(s, "%Y-%m-%dT%H:%MZ")))
        })
    }

    pub fn from_node(n: &Node, start: Option<DateTime<Utc>>) -> WasteProd {
        let position = child_filter_n("position", n)
            .and_then(|n| n.text())
            .and_then(str_to_i32);
        let quantity = child_filter_n("quantity", n)
            .and_then(|n| n.text())
            .and_then(str_to_f64);

        let date = start.and_then(|s| position.map(|p| s + Duration::hours(p as i64)));
        WasteProd {
            start,
            date,
            position,
            quantity,
        }
    }
}

impl ToParams for WasteProd {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        (params() + &self.date + &self.quantity).inner()
    }
}

impl WithSQL for WasteProd {
    fn sql() -> &'static str {
        "
        INSERT INTO waste (ts, production)
        VALUES ($1, $2);
        "
    }
}
impl Store {
    pub async fn save_waste(&self, record: &WasteProd) -> Result<u64, StoreError> {
        self.save(record).await
    }
}
