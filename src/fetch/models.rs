use crate::model::{params, ToParams, WithSQL};
use crate::store::{Store, StoreError};
use chrono::{DateTime, Utc};

type NullableString = Option<String>;

// FETCH EVENTS
#[derive(Clone)]
pub struct FetchEvent {
    pub ts: Option<DateTime<Utc>>,
    pub description: NullableString,
    pub begin: Option<DateTime<Utc>>,
    pub url: NullableString,
    pub error: NullableString,
}

impl ToParams for FetchEvent {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        (params() + &self.ts + &self.description + &self.begin + &self.url + &self.error).inner()
    }
}

impl WithSQL for FetchEvent {
    fn sql() -> &'static str {
        "
        INSERT INTO fetch_events (ts, \"description\", \"begin\", \"url\", error)
        VALUES ($1, $2, $3, $4, $5);
        "
    }
}
impl Store {
    pub async fn save_fetch_event(&self, record: &FetchEvent) -> Result<u64, StoreError> {
        self.save(record).await
    }
}
