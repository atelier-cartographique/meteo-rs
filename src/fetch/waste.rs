use super::{map_xml_resource, tag_filter, FetchError, FetchResult};
use crate::{
    fetch::{models::FetchEvent, waste_models::WasteProd},
    store::ArcStore,
};
use chrono::{DateTime, Duration, TimeZone, Utc};

async fn production_last_date(store: &ArcStore) -> FetchResult<DateTime<Utc>> {
    let last_update = store.waste_last_past_update().await?;
    match last_update.value() {
        Some(last_update) if *last_update >= Utc::now() => Err(FetchError {
            inner: String::from("waste last_update > now"),
        }),
        Some(last_update) => Ok(*last_update),
        None => Ok(Utc.ymd(2020, 1, 1).and_hms(0, 0, 0)),
    }
}

async fn waste_past_url(last_update: &DateTime<Utc>, token: &String) -> FetchResult<String> {
    let next_step = Utc::now() + Duration::days(1);

    Ok(
        // format!("https://transparency.entsoe.eu/api?securityToken={}&PsrType=B17&documentType=A75&processType=A16&in_Domain=10YBE----------2&periodStart={}0000&periodEnd={}0000",
        format!("https://web-api.tp.entsoe.eu/api?securityToken={}&PsrType=B17&documentType=A75&processType=A16&in_Domain=10YBE----------2&periodStart={}0000&periodEnd={}0000",
            token,
            last_update.format("%Y%m%d"),
            next_step.format("%Y%m%d"),
            )
    )
}

pub async fn store_past_waste(store: &ArcStore, token: String) -> FetchResult<()> {
    let description = "InsertWasteProd";
    let mut last_update = production_last_date(store).await?;
    let mut url = waste_past_url(&last_update, &token).await?;
    println!("store_past_waste {}", url);

    while last_update < Utc::now() - Duration::days(1) {
        let records_res = map_xml_resource(&url, |doc| {
            tag_filter("Period", &doc)
                .map_children("Point", |period, point| {
                    WasteProd::from_node(&point, WasteProd::date_from_period(&period))
                })
                .iter()
                .filter(|r| {
                    if let Some(q) = r.quantity {
                        q >= 0.0
                    } else {
                        false
                    }
                })
                .filter(|r| match (r.start, r.position) {
                    (Some(s), Some(p)) => {
                        let date = s + Duration::hours(p as i64);
                        date > last_update && date < Utc::now()
                    }
                    (_, _) => false,
                })
                .cloned()
                .collect()
        })
        .await;
        let records: Vec<WasteProd> = match records_res {
            Ok(vec) => {
                store
                    .save_fetch_event(&FetchEvent {
                        ts: Some(Utc::now()),
                        description: Some(description.into()),
                        begin: Some(last_update),
                        url: Some(url.to_string()),
                        error: None,
                    })
                    .await?;
                vec
            }
            Err(e) => {
                store
                    .save_fetch_event(&FetchEvent {
                        ts: Some(Utc::now()),
                        description: Some(description.into()),
                        begin: Some(last_update),
                        url: Some(url.to_string()),
                        error: Some(format!("{:?}", e)),
                    })
                    .await?;
                // Vec::new()
                return Err(e);
            }
        };

        for rec in records.iter() {
            store.save_waste(rec).await?;
        }
        last_update = production_last_date(store).await?;
        url = waste_past_url(&last_update, &token).await?;
    }
    Ok(())
}
