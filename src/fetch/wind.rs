use super::wind_models::WindProduction;
use super::{map_xml_resource, tag_filter, FetchError, FetchResult};
use crate::fetch::models::FetchEvent;
use crate::fetch::wind_models::WindForecast;
use crate::store::{ArcStore, Offshore};
use chrono::{DateTime, Duration, TimeZone, Utc};

// WIND - forecast

fn offshore_string(offshore: Offshore) -> String {
    match offshore {
        Offshore::None => String::from(""),
        Offshore::True => String::from("true"),
        Offshore::False => String::from("false"),
    }
}
async fn forecast_last_date(store: &ArcStore, is_offshore: Offshore) -> FetchResult<DateTime<Utc>> {
    let last_update_opt = store.wind_forecast_last_date(is_offshore).await?;
    // Option date to result
    match last_update_opt.value() {
        Some(last_update_opt) if *last_update_opt >= Utc::now() + Duration::days(8) => {
            Err(FetchError {
                inner: String::from("last_update > now + 8 days"),
            })
        }
        Some(last_update_opt) => Ok(*last_update_opt),
        None => Ok(Utc::now() - Duration::days(8)),
    }
}

async fn wind_forecast_url(is_offshore: Offshore) -> FetchResult<String> {
    let now = Utc::now();
    let next_step = Utc::now() + Duration::days(8);

    Ok(format!("https://publications.elia.be/Publications/Publications/WindForecasting.v2.svc/GetForecastData?beginDate={}&endDate={}&isOffshore={}",
    now.format("%Y-%m-%d"),
    next_step.format("%Y-%m-%d"),
    offshore_string(is_offshore,)))
}

async fn store_wind_forecast(store: &ArcStore, is_offshore: Offshore) -> FetchResult<()> {
    let description = match is_offshore {
        Offshore::None => "InsertWind",
        Offshore::True => "InsertWindOffshore",
        Offshore::False => "InsertWindOnshore",
    };

    let url = wind_forecast_url(is_offshore).await?;
    println!("url wind forecast: {}", url);
    let records_res = map_xml_resource(&url, |doc| {
        tag_filter("WindForecastingGraphItem", &doc)
            .map(|node| WindForecast::from_node(&node, is_offshore))
            .filter(|r| Option::is_some(&r.most_recent_forecast()))
            .collect()
    })
    .await;
    let records: Vec<WindForecast> = match records_res {
        Ok(vec) => {
            store
                .save_fetch_event(&FetchEvent {
                    ts: Some(Utc::now()),
                    begin: Some(Utc::now()),
                    description: Some(description.into()),
                    url: Some(url.to_string()),
                    error: None,
                })
                .await?;
            vec
        }
        Err(e) => {
            store
                .save_fetch_event(&FetchEvent {
                    ts: Some(Utc::now()),
                    begin: Some(Utc::now()),
                    description: Some(description.into()),
                    url: Some(url.to_string()),
                    error: Some(format!("{:?}", e)),
                })
                .await?;
            Vec::new()
        }
    };
    for rec in records.iter() {
        store.save_wind_forecast(rec).await?;
    }
    Ok(())
}

pub async fn store_all_wind_forecast(store: &ArcStore) -> FetchResult<()> {
    // All wind data forecast
    let offshore_values = [Offshore::None, Offshore::True, Offshore::False];
    for val in offshore_values.iter() {
        store_wind_forecast(store, *val).await?;
    }
    Ok(())
}
// async fn delete_wind_forecast(store: &ArcStore, is_offshore: Offshore) -> FetchResult<()> {
//     let date = Utc::now();
//     let description = match is_offshore {
//         Offshore::None => "DeleteWindForecast",
//         Offshore::True => "DeleteWindOffshoreForecast",
//         Offshore::False => "DeleteWindOnshoreForecast",
//     };
//     let del = DeleteWindForecast::new(date, is_offshore);
//     match store.delete_wind_forecast(&del).await {
//         Ok(_) => {
//             store
//                 .save_fetch_event(&FetchEvent {
//                     ts: Some(Utc::now()),
//                     description: Some(description.into()),
//                     begin: Some(date),
//                     url: None,
//                     error: None,
//                 })
//                 .await?;
//             Ok(())
//         }
//         Err(e) => {
//             store
//                 .save_fetch_event(&FetchEvent {
//                     ts: Some(Utc::now()),
//                     description: Some(description.into()),
//                     begin: Some(date),
//                     url: None,
//                     error: Some(format!("{:?}", e)),
//                 })
//                 .await?;
//             Ok(())
//         }
//     }
// }

// pub async fn update_all_wind_forecast(store: &ArcStore) -> FetchResult<()> {
//     let offshore_values = [Offshore::None, Offshore::True, Offshore::False];
//     for val in offshore_values.iter() {
//         delete_wind_forecast(store, *val).await?;
//     }
//     Ok(())
// }

// WIND - production

async fn production_last_date(
    store: &ArcStore,
    is_offshore: Offshore,
) -> FetchResult<DateTime<Utc>> {
    let last_update_opt = store.wind_production_last_date(is_offshore).await?;
    match last_update_opt.value() {
        Some(last_update) if *last_update >= Utc::now() => Err(FetchError {
            inner: String::from("Wind last_update > now"),
        }),
        Some(last_update) => Ok(*last_update),
        None => Ok(Utc.ymd(2020, 1, 1).and_hms(0, 0, 0)),
        // None => Ok(Utc::now() - Duration::days(90)),
    }
}

// pub async fn wind_prod_last_del_date(
//     store: &ArcStore,
//     description: String,
// ) -> FetchResult<DateTime<Utc>> {
//     let last_delete_opt = store.last_del_date(description).await?;
//     match last_delete_opt {
//         Some(date) => Ok(date),
//         None => Ok(Utc::now() - Duration::weeks(9)), // to make sure we do it the first time
//     }
// }
// async fn delete_wind_production(store: &ArcStore, is_offshore: Offshore) -> FetchResult<()> {
//     let date = Utc::now() - Duration::weeks(8);
//     let description = match is_offshore {
//         Offshore::None => "DeleteWindProd",
//         Offshore::True => "DeleteWindOffshoreProd",
//         Offshore::False => "DeleteWindOnshoreProd",
//     };
//     let del = DeleteWindProd::new(date, is_offshore);

//     match store.delete_wind_prod(&del).await {
//         Ok(_) => {
//             store
//                 .save_fetch_event(&FetchEvent {
//                     ts: Some(Utc::now()),
//                     description: Some(description.into()),
//                     begin: Some(date),
//                     url: None,
//                     error: None,
//                 })
//                 .await?;
//             Ok(())
//         }
//         Err(e) => {
//             store
//                 .save_fetch_event(&FetchEvent {
//                     ts: Some(Utc::now()),
//                     description: Some(description.into()),
//                     begin: Some(date),
//                     url: None,
//                     error: Some(format!("{:?}", e)),
//                 })
//                 .await?;
//             Ok(())
//         }
//     }
// }
// pub async fn update_all_wind_prod(store: &ArcStore) -> FetchResult<()> {
//     let offshore_values = [Offshore::None, Offshore::True, Offshore::False];
//     for val in offshore_values.iter() {
//         let name = match *val {
//             Offshore::None => "DeleteWindProd",
//             Offshore::True => "DeleteWindOffshoreProd",
//             Offshore::False => "DeleteWindOnshoreProd",
//         };
//         let date = get_last_del_date(store, name).await?;
//         if Utc::now() > date + Duration::weeks(8) {
//             delete_wind_production(store, *val).await?;
//         }
//     }
//     Ok(())
// }

async fn wind_production_url(
    last_update: &DateTime<Utc>,
    is_offshore: Offshore,
) -> FetchResult<String> {
    let next_step = Utc::now() + Duration::days(1);

    let url = format!("https://publications.elia.be/Publications/Publications/WindForecasting.v2.svc/GetForecastData?beginDate={}&endDate={}&isOffshore={}",
    last_update.format("%Y-%m-%d"),
    next_step.format("%Y-%m-%d"),
    offshore_string(is_offshore));
    println!("url wind prod:  {}", url);
    Ok(url)
}

async fn store_wind_production(store: &ArcStore, is_offshore: Offshore) -> FetchResult<()> {
    let description = match is_offshore {
        Offshore::None => "InsertWindProd",
        Offshore::True => "InsertWindOffshoreProd",
        Offshore::False => "InsertWindOnshoreProd",
    };
    let mut last_update = production_last_date(store, is_offshore).await?;
    let mut url = wind_production_url(&last_update, is_offshore).await?;

    while last_update < Utc::now() - Duration::days(1) {
        let records_res = map_xml_resource(&url, |doc| {
            tag_filter("WindForecastingGraphItem", &doc)
                .map(|node| WindProduction::from_node(&node, is_offshore))
                .filter(|r| {
                    if let Some(d) = r.date_time() {
                        d > last_update && d < Utc::now() - Duration::hours(1)
                    } else {
                        false
                    }
                })
                .collect()
        })
        .await;
        let records: Vec<WindProduction> = match records_res {
            Ok(vec) => {
                store
                    .save_fetch_event(&FetchEvent {
                        ts: Some(Utc::now()),
                        description: Some(description.into()),
                        begin: Some(last_update),
                        url: Some(url.to_string()),
                        error: None,
                    })
                    .await?;
                vec
            }
            Err(e) => {
                store
                    .save_fetch_event(&FetchEvent {
                        ts: Some(Utc::now()),
                        description: Some(description.into()),
                        begin: Some(last_update),
                        url: Some(url.to_string()),
                        error: Some(format!("{:?}", e)),
                    })
                    .await?;
                // Vec::new()
                return Err(e);
            }
        };

        for rec in records.iter() {
            // rec.save(store, name).await?;
            store.save_wind_production(rec).await?;
        }
        last_update = production_last_date(store, is_offshore).await?;
        url = wind_production_url(&last_update, is_offshore).await?;
    }
    Ok(())
}

pub async fn store_all_wind_production(store: &ArcStore) -> FetchResult<()> {
    // All production
    let offshore_values = [Offshore::None, Offshore::True, Offshore::False];
    for offshore in offshore_values.iter() {
        store_wind_production(store, *offshore).await?;
    }

    Ok(())
}

// #[cfg(test)]
// mod wind_test {
//     use super::*;
//     use roxmltree::Document;

//     #[test]
//     fn offshore_xml_parse() {
//         let resource = String::from(include_str!("../../test-data/offshore.xml"));
//         let doc = Document::parse(&resource).unwrap();
//         let records = tag_filter("WindForecastingGraphItem", &doc)
//             .map(|node| WindRecord::from_node(&node))
//             .collect::<Vec<_>>();

//         assert_eq!(records.len(), 768);
//     }
// }
