use crate::fetch::biomass::store_past_biomass;
use crate::fetch::load::{store_forecast_load, store_past_load};
use crate::fetch::solar::{store_all_solar_forecast, store_all_solar_production};
use crate::fetch::waste::store_past_waste;
use crate::fetch::wind::{store_all_wind_forecast, store_all_wind_production};
use crate::store::{ArcStore, Store, StoreError};
use crate::util::wait_start_time;

use futures::stream::StreamExt;
use tokio_stream::wrappers::IntervalStream;

use roxmltree::{Descendants, Document, ExpandedName, Node};
use std::fmt::Display;
use std::time::Duration as TimeDuration;
use tokio::time;

mod biomass;
mod biomass_models;
mod load;
mod load_models;
mod models;
mod solar;
mod solar_models;
mod waste;
mod waste_models;
mod wind;
mod wind_models;

#[derive(Clone)]
pub struct DocumentTagFilter<'a, 'input>
where
    'input: 'a,
{
    tag: ExpandedName<'a>,
    ds: Descendants<'a, 'input>,
}

impl<'a, 'input> std::iter::Iterator for DocumentTagFilter<'a, 'input>
where
    'input: 'a,
{
    type Item = Node<'a, 'input>;

    fn next(&mut self) -> Option<Self::Item> {
        let tag = self.tag;
        loop {
            match self.ds.next() {
                Some(n) if n.has_tag_name(tag) => return Some(n),
                None => return None,
                _ => {}
            }
        }
    }
}

impl<'a, 'input> DocumentTagFilter<'a, 'input> {
    fn map_children<'b, F, R>(&self, tag: &str, f: F) -> Vec<R>
    where
        R: Clone,
        F: Fn(Node, Node) -> R,
    {
        self.clone()
            .flat_map(|parent| {
                let mut result: Vec<R> = Vec::new();
                let mut children = parent.descendants();
                loop {
                    match children.next() {
                        Some(node) => {
                            if node.has_tag_name(tag) {
                                result.push(f(parent, node))
                            }
                        }
                        _ => return result,
                    };
                }
            })
            .collect::<Vec<R>>()
    }
}

pub fn tag_filter<'a, 'input, N>(tag: N, doc: &'input Document) -> DocumentTagFilter<'a, 'input>
where
    'input: 'a,
    N: Into<ExpandedName<'a>>,
{
    DocumentTagFilter {
        tag: tag.into(),
        ds: doc.descendants(),
    }
}

// pub fn tag_filter_n<'a, 'input, N>(tag: N, n: &'input Node) -> DocumentTagFilter<'a, 'input>
// where
//     'input: 'a,
//     N: Into<ExpandedName<'a>>,
// {
//     DocumentTagFilter {
//         tag: tag.into(),
//         ds: n.descendants(),
//     }
// }

pub fn child_filter_n<'a, 'input, N>(tag: N, n: &'input Node) -> Option<Node<'a, 'input>>
where
    N: Into<ExpandedName<'a>>,
{
    let tag = tag.into();
    for c in n.children() {
        if c.has_tag_name(tag) {
            return Some(c);
        }
    }
    None
}

#[derive(Debug)]
pub struct FetchError {
    inner: String,
}

impl Display for FetchError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.inner)
    }
}

impl From<reqwest::Error> for FetchError {
    fn from(err: reqwest::Error) -> FetchError {
        FetchError {
            inner: format!("reqwets::error: {}", err),
        }
    }
}

impl From<roxmltree::Error> for FetchError {
    fn from(err: roxmltree::Error) -> FetchError {
        FetchError {
            inner: format!("roxmltree::error: {}", err),
        }
    }
}

impl From<StoreError> for FetchError {
    fn from(err: StoreError) -> FetchError {
        FetchError {
            inner: format!("storeerror: {}", err),
        }
    }
}

pub type FetchResult<T> = Result<T, FetchError>;

async fn get_resource(url: &str) -> FetchResult<String> {
    use reqwest::{header, header::HeaderValue, Client};

    let response = Client::new()
        .get(url)
        .timeout(TimeDuration::from_secs(60))
        .header(
            header::ACCEPT,
            HeaderValue::from_static("application/xml,application/xhtml+xml"),
        )
        .send()
        .await?;

    Ok(response.text().await?)
}

pub async fn map_xml_resource<F, R>(url: &str, f: F) -> FetchResult<R>
where
    R: Clone,
    F: Fn(roxmltree::Document) -> R,
{
    let resource = get_resource(url).await?;
    let doc = Document::parse(&resource).map_err(Into::<FetchError>::into)?;
    Ok(f(doc))
}

// pub async fn map_xml_resource_vec<F, R>(urls: &Vec<&str>, f: F) -> FetchResult<Vec<R>>
// where
//     R: Clone,
//     F: Fn(roxmltree::Document) -> R,
// {
//     use futures::stream::{self};
//     let s = stream::iter(urls.clone());
//     let t = s
//         .map(|u| String::from(u))
//         .fold(Ok(Vec::new()), |acc, url| async {
//             let url = String::from(url);
//             let fut = get_resource(&url);
//             let r = match acc {
//                 Err(e) => Err(e),
//                 Ok(values) => match fut.await {
//                     Err(e) => Err(e),
//                     Ok(resource) => {
//                         if let Ok(doc) = Document::parse(&resource) {
//                             let result = f(doc);
//                             Ok([values, vec![result]].concat())
//                         } else {
//                             Err(FetchError {
//                                 inner: String::from("parse error"),
//                             })
//                         }
//                     }
//                 },
//             };
//             r
//         })
//         .await?;

//     Ok(t)
// }

async fn fetch_all_inner(dsn: String, t: u64, st: chrono::NaiveTime, token: String) {
    match Store::new(&dsn).await {
        Err(err) => println!("Failed to get a store:\n{:?} \n\nexiting", err),
        Ok(store) => {
            wait_start_time(st).await;
            let arc_store = ArcStore::new(store);
            let interval = time::interval(time::Duration::from_secs(t));
            let interval = IntervalStream::new(interval);
            interval
                .for_each(|_instant| async {
                    // match update_all_wind_prod(&arc_store).await {
                    //     Ok(_) => println!("Ok: update_wind_production"),
                    //     Err(e) => println!("Error::update_wind_prod: {e}"),
                    // };
                    // match update_all_solar_prod(&arc_store).await {
                    //     Ok(_) => println!("Ok: update_solar_production"),
                    //     Err(e) => println!("Error::update_solar_prod: {e}"),
                    // };
                    // match update_all_wind_forecast(&arc_store).await {
                    //     Ok(_) => println!("Ok: delete_old_wind_forecast"),
                    //     Err(e) => println!("Error::update_wind_forecast: {e}"),
                    // };
                    // match update_all_solar_forecast(&arc_store).await {
                    //     Ok(_) => println!("Ok: delete_old_solar_forecast"),
                    //     Err(e) => println!("Error::update_solar_forecast: {e}"),
                    // };
                    match store_all_wind_forecast(&arc_store).await {
                        Ok(_) => println!("Ok::store_wind_forecast"),
                        Err(e) => println!("Error::store_wind_forecast: {e}"),
                    };
                    match store_all_wind_production(&arc_store).await {
                        Ok(_) => println!("Ok::store_wind_production"),
                        Err(e) => println!("Error::store_wind_production: {e}"),
                    };
                    match store_all_solar_forecast(&arc_store).await {
                        Ok(_) => println!("Ok::store_solar_forecast"),
                        Err(e) => println!("Error::store_solar_forecast: {e}"),
                    };
                    match store_all_solar_production(&arc_store).await {
                        Ok(_) => println!("Ok::store_solar_production"),
                        Err(e) => println!("Error::store_solar_production: {e}"),
                    };
                    match store_past_biomass(&arc_store, token.clone()).await {
                        Ok(_) => println!("Ok::store_biomass_production"),
                        Err(e) => println!("Error::store_biomass_production: {e}"),
                    }
                    match store_past_waste(&arc_store, token.clone()).await {
                        Ok(_) => println!("Ok::store_waste_production"),
                        Err(e) => println!("Error::store_waste_production: {e}"),
                    }
                    match store_forecast_load(&arc_store).await {
                        Ok(_) => println!("Ok::store_forecast_load"),
                        Err(e) => println!("Error::store_forecast_load: {e}"),
                    };
                    match store_past_load(&arc_store).await {
                        Ok(_) => println!("Ok::store_past_load"),
                        Err(e) => println!("Error::store_past_load: {e}"),
                    };
                })
                .await;
        }
    }
}

pub fn fetch_all(dsn: String, t: u64, st: chrono::NaiveTime, token: String) {
    let rt = tokio::runtime::Runtime::new().unwrap();
    rt.block_on(fetch_all_inner(dsn, t, st, token));
}
