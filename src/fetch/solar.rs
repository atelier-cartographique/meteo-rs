use std::collections::HashMap;

use super::{map_xml_resource, tag_filter, FetchError, FetchResult};
use crate::fetch::models::FetchEvent;
use crate::fetch::solar_models::{SolarDailyProd, SolarForecast, SolarProd};
use crate::store::{ArcStore, Region};
use chrono::{DateTime, Duration, NaiveDate, TimeZone, Utc};

// FORECAST

fn region_code(region: Region) -> String {
    match region {
        Region::Belgique => String::from("1"),
        Region::Bruxelles => String::from("4"),
        Region::Antw => String::from("5"),
        Region::Hainaut => String::from("6"),
        Region::Limburg => String::from("7"),
        Region::Liege => String::from("8"),
        Region::Luxembourg => String::from("9"),
        Region::Namur => String::from("10"),
        Region::OVlaanderen => String::from("11"),
        Region::VlBrabant => String::from("12"),
        Region::BrabantW => String::from("13"),
        Region::WVlaanderen => String::from("14"),
    }
}

async fn forecast_last_date(store: &ArcStore, region: Region) -> FetchResult<DateTime<Utc>> {
    let last_update_opt = store.solar_last_forecast_date(region).await?;

    match last_update_opt.value() {
        Some(last_update) if *last_update >= Utc::now() + Duration::days(8) => Err(FetchError {
            inner: format!("Solar last_update > now + 7 days: last_update = {last_update}; "),
        }),
        Some(last_update) => Ok(*last_update),
        None => Ok(Utc::now() - Duration::days(8)),
    }
}

async fn solar_forecast_url(last_update: DateTime<Utc>, region: Region) -> FetchResult<String> {
    let next_step = Utc::now() + Duration::days(8);
    let source_id = region_code(region); //geographic zone code (1 = "Belgium")
    Ok(format!("https://publications.elia.be/Publications/publications/solarforecasting.v4.svc/GetChartDataForZoneXml?dateFrom={}&dateTo={}&sourceId={}",
    last_update.format("%Y-%m-%d"),
    next_step.format("%Y-%m-%d"),
    source_id))
}

async fn store_solar_forecast(store: &ArcStore, region: Region) -> FetchResult<()> {
    let description = match region {
        Region::Belgique => "InsertSolar",
        Region::Bruxelles => "InsertSolarBxl",
        Region::Antw => "InsertSolarAnt",
        Region::Hainaut => "InsertSolarHai",
        Region::Limburg => "InsertSolarLim",
        Region::Liege => "InsertSolarLie",
        Region::Luxembourg => "InsertSolarLux",
        Region::Namur => "InsertSolarNam",
        Region::OVlaanderen => "InsertSolarOVl",
        Region::VlBrabant => "InsertSolarVlB",
        Region::BrabantW => "InsertSolarBrW",
        Region::WVlaanderen => "InsertSolarWVl",
    };
    let last_update = forecast_last_date(store, region).await?;
    let url = solar_forecast_url(last_update, region).await?;
    println!("store_solar_forecast {}", url);

    let records_res = map_xml_resource(&url, |doc| {
        tag_filter("SolarForecastingChartDataForZoneItem", &doc)
            .map(|node| SolarForecast::from_node(&node, region))
            .filter(|r| {
                if let Some(d) = r.date_time() {
                    d > last_update
                } else {
                    false
                }
            })
            //Filter no realtime values
            .filter(|r| {
                if let Some(p) = r.most_recent_forecast() {
                    p >= 0.0
                } else {
                    false
                }
            })
            .collect()
    })
    .await;
    let records: Vec<SolarForecast> = match records_res {
        Ok(vec) => {
            store
                .save_fetch_event(&FetchEvent {
                    ts: Some(Utc::now()),
                    begin: Some(last_update),
                    description: Some(description.into()),
                    url: Some(url.to_string()),
                    error: None,
                })
                .await?;
            vec
        }
        Err(e) => {
            store
                .save_fetch_event(&FetchEvent {
                    ts: Some(Utc::now()),
                    begin: Some(last_update),
                    description: Some(description.into()),
                    url: Some(url.to_string()),
                    error: Some(format!("{:?}", e)),
                })
                .await?;
            Vec::new()
        }
    };

    for rec in records.iter() {
        store.save_solar_forecast(rec).await?;
    }
    Ok(())
}

pub async fn store_all_solar_forecast(store: &ArcStore) -> FetchResult<()> {
    let regions = [
        Region::Belgique,
        Region::Bruxelles,
        Region::Antw,
        Region::Hainaut,
        Region::Limburg,
        Region::Liege,
        Region::Luxembourg,
        Region::Namur,
        Region::OVlaanderen,
        Region::VlBrabant,
        Region::BrabantW,
        Region::WVlaanderen,
    ];
    for region in regions {
        store_solar_forecast(store, region).await?;
    }
    Ok(())
}

// async fn delete_solar_forecast(store: &ArcStore, region: Region) -> FetchResult<()> {
//     let date = Utc::now();
//     let description = match region {
//         Region::Belgique => "DeleteSolar",
//         Region::Bruxelles => "DeleteSolarBxl",
//         Region::Antw => "DeleteSolarAnt",
//         Region::Hainaut => "DeleteSolarHai",
//         Region::Limburg => "DeleteSolarLim",
//         Region::Liege => "DeleteSolarLie",
//         Region::Luxembourg => "DeleteSolarLux",
//         Region::Namur => "DeleteSolarNam",
//         Region::OVlaanderen => "DeleteSolarOVl",
//         Region::VlBrabant => "DeleteSolarVlB",
//         Region::BrabantW => "DeleteSolarBrW",
//         Region::WVlaanderen => "DeleteSolarWVl",
//     };
//     match store
//         .delete_solar_forecast(&DeleteSolarForecast::new(date, region))
//         .await
//     {
//         Ok(_) => {
//             store
//                 .save_fetch_event(&FetchEvent {
//                     ts: Some(Utc::now()),
//                     description: Some(description.into()),
//                     begin: Some(date),
//                     url: None,
//                     error: None,
//                 })
//                 .await?;
//             Ok(())
//         }
//         Err(e) => {
//             store
//                 .save_fetch_event(&FetchEvent {
//                     ts: Some(Utc::now()),
//                     description: Some(description.into()),
//                     begin: Some(date),
//                     url: None,
//                     error: Some(format!("{:?}", e)),
//                 })
//                 .await?;
//             Ok(())
//         }
//     }
// }

// pub async fn update_all_solar_forecast(store: &ArcStore) -> FetchResult<()> {
//     let regions = [
//         Region::Bruxelles,
//         Region::Antw,
//         Region::Hainaut,
//         Region::Limburg,
//         Region::Liege,
//         Region::Luxembourg,
//         Region::Namur,
//         Region::OVlaanderen,
//         Region::VlBrabant,
//         Region::BrabantW,
//         Region::WVlaanderen,
//         Region::Belgique,
//     ];
//     for r in regions {
//         delete_solar_forecast(store, r).await?;
//     }
//     Ok(())
// }

// PRODUCTION

async fn production_last_date(store: &ArcStore, region: Region) -> FetchResult<DateTime<Utc>> {
    let last_update_opt = store.solar_last_prod_date(region).await?;
    match last_update_opt.value() {
        Some(last_update) if *last_update >= Utc::now() => Err(FetchError {
            inner: format!("Solar last_update > now : last_update = {last_update}; "),
        }),
        Some(last_update) => Ok(*last_update),
        None => Ok(Utc.ymd(2020, 1, 1).and_hms(0, 0, 0)),
    }
}

async fn solar_production_url(last_update: DateTime<Utc>, region: Region) -> FetchResult<String> {
    let next_step = Utc::now() + Duration::days(1);
    let source_id = region_code(region); //geographic zone code
    Ok(format!("https://publications.elia.be/Publications/publications/solarforecasting.v4.svc/GetChartDataForZoneXml?dateFrom={}&dateTo={}&sourceId={}",
    last_update.format("%Y-%m-%d"),
    next_step.format("%Y-%m-%d"),
    source_id))
}

fn with_daily_production(val: &SolarProd, running: f64) -> Option<f64> {
    val.realtime().map(|rt| rt / 4000. + running)
}

fn get_daily_productivity(production: Option<f64>, val: &SolarProd) -> Option<f64> {
    production.and_then(|p| val.monitored_capacity().map(|mc| p / (mc / 1000.)))
}

async fn store_solar_production(store: &ArcStore, region: Region) -> FetchResult<()> {
    let description = match region {
        Region::Belgique => "InsertSolarProd",
        Region::Bruxelles => "InsertSolarProdBxl",
        Region::Antw => "InsertSolarProdAnt",
        Region::Hainaut => "InsertSolarProdHai",
        Region::Limburg => "InsertSolarProdLim",
        Region::Liege => "InsertSolarProdLie",
        Region::Luxembourg => "InsertSolarProdLux",
        Region::Namur => "InsertSolarProdNam",
        Region::OVlaanderen => "InsertSolarProdOVl",
        Region::VlBrabant => "InsertSolarProdVlB",
        Region::BrabantW => "InsertSolarProdBrW",
        Region::WVlaanderen => "InsertSolarProdWVl",
    };
    let last_update = production_last_date(store, region).await?;
    let url = solar_production_url(last_update, region).await?;
    println!("store_solar_production {}", url);

    let records_res = map_xml_resource(&url, |doc| {
        tag_filter("SolarForecastingChartDataForZoneItem", &doc)
            .map(|node| SolarProd::from_node(&node, region))
            .filter(|r| {
                if let Some(d) = r.date_time() {
                    d > last_update && d < Utc::now() - Duration::hours(1)
                } else {
                    false
                }
            })
            .collect()
    })
    .await;
    let records: Vec<SolarProd> = match records_res {
        Ok(vec) => {
            store
                .save_fetch_event(&FetchEvent {
                    ts: Some(Utc::now()),
                    begin: Some(last_update),
                    description: Some(description.into()),
                    url: Some(url.to_string()),
                    error: None,
                })
                .await?;
            vec
        }
        Err(e) => {
            store
                .save_fetch_event(&FetchEvent {
                    ts: Some(Utc::now()),
                    begin: Some(last_update),
                    description: Some(description.into()),
                    url: Some(url.to_string()),
                    error: Some(format!("{:?}", e)),
                })
                .await?;
            Vec::new()
        }
    };

    for rec in records.iter() {
        store.save_solar_production(rec).await?;
    }

    let daily_records = records.iter().fold(
        HashMap::<NaiveDate, SolarDailyProd>::new(),
        |mut acc, val| {
            if let Some(dt) = val.date_time() {
                let key = dt.date_naive();
                let production = with_daily_production(
                    val,
                    acc.get(&key)
                        .and_then(|solar_prod| solar_prod.production_obs_kwh)
                        .unwrap_or(0.),
                );
                let productivity = get_daily_productivity(production, val);

                acc.insert(
                    key,
                    SolarDailyProd {
                        date: Some(key),
                        production_obs_kwh: production,
                        productivite_kwh_by_kwc: productivity,
                    },
                );
                acc
            } else {
                acc
            }
        },
    );

    for (_, v) in daily_records.iter() {
        store.save_solar_daily_prod(v).await?;
    }

    Ok(())
}

pub async fn store_all_solar_production(store: &ArcStore) -> FetchResult<()> {
    let regions = [
        Region::Bruxelles,
        Region::Antw,
        Region::Hainaut,
        Region::Limburg,
        Region::Liege,
        Region::Luxembourg,
        Region::Namur,
        Region::OVlaanderen,
        Region::VlBrabant,
        Region::BrabantW,
        Region::WVlaanderen,
        Region::Belgique,
    ];
    for region in regions {
        store_solar_production(store, region).await?;
    }
    Ok(())
}

// async fn delete_solar_production(store: &ArcStore, region: Region) -> FetchResult<()> {
//     let date = Utc::now() - Duration::weeks(8);
//     let description = match region {
//         Region::Belgique => "DeleteSolarProd",
//         Region::Bruxelles => "DeleteSolarProdBxl",
//         Region::Antw => "DeleteSolarProdAnt",
//         Region::Hainaut => "DeleteSolarProdHai",
//         Region::Limburg => "DeleteSolarProdLim",
//         Region::Liege => "DeleteSolarProdLie",
//         Region::Luxembourg => "DeleteSolarProdLux",
//         Region::Namur => "DeleteSolarProdNam",
//         Region::OVlaanderen => "DeleteSolarProdOVl",
//         Region::VlBrabant => "DeleteSolarProdVlB",
//         Region::BrabantW => "DeleteSolarProdBrW",
//         Region::WVlaanderen => "DeleteSolarProdWVl",
//     };
//     match store
//         .delete_solar_prod(&DeleteSolarProd::new(date, region))
//         .await
//     {
//         Ok(_) => {
//             store
//                 .save_fetch_event(&FetchEvent {
//                     ts: Some(Utc::now()),
//                     description: Some(description.into()),
//                     begin: Some(date),
//                     url: None,
//                     error: None,
//                 })
//                 .await?;
//             Ok(())
//         }
//         Err(e) => {
//             store
//                 .save_fetch_event(&FetchEvent {
//                     ts: Some(Utc::now()),
//                     description: Some(description.into()),
//                     begin: Some(date),
//                     url: None,
//                     error: Some(format!("{:?}", e)),
//                 })
//                 .await?;
//             Ok(())
//         }
//     }
// }
// pub async fn update_all_solar_prod(store: &ArcStore) -> FetchResult<()> {
//     let regions = [
//         Region::Bruxelles,
//         Region::Antw,
//         Region::Hainaut,
//         Region::Limburg,
//         Region::Liege,
//         Region::Luxembourg,
//         Region::Namur,
//         Region::OVlaanderen,
//         Region::VlBrabant,
//         Region::BrabantW,
//         Region::WVlaanderen,
//         Region::Belgique,
//     ];
//     for r in regions {
//         let name = match r {
//             Region::Belgique => "DeleteSolarProd",
//             Region::Bruxelles => "DeleteSolarProdBxl",
//             Region::Antw => "DeleteSolarProdAnt",
//             Region::Hainaut => "DeleteSolarProdHai",
//             Region::Limburg => "DeleteSolarProdLim",
//             Region::Liege => "DeleteSolarProdLie",
//             Region::Luxembourg => "DeleteSolarProdLux",
//             Region::Namur => "DeleteSolarProdNam",
//             Region::OVlaanderen => "DeleteSolarProdOVl",
//             Region::VlBrabant => "DeleteSolarProdVlB",
//             Region::BrabantW => "DeleteSolarProdBrW",
//             Region::WVlaanderen => "DeleteSolarProdWVl",
//         };
//         let date = get_last_del_date(store, name).await?;
//         if Utc::now() > date + Duration::weeks(8) {
//             delete_solar_production(store, r).await?;
//         }
//     }
//     Ok(())
// }
