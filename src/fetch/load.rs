use super::{map_xml_resource, tag_filter, FetchError, FetchResult};
use crate::fetch::load_models::{SolarLoad, SolarLoadPast};
use crate::fetch::models::FetchEvent;
use crate::store::ArcStore;
use chrono::{DateTime, Duration, TimeZone, Utc};

async fn load_forecast_url(store: &ArcStore) -> FetchResult<(DateTime<Utc>, String)> {
    let last_update = store.load_last_update().await?;

    let (start, next_step) = {
        match last_update.value() {
            Some(last_update) if *last_update >= Utc::now() + Duration::days(8) => {
                return Err(FetchError {
                    inner: String::from("Load last_update > now + 7 days"),
                });
            }
            Some(last_update) => (
                *last_update + Duration::days(1),
                Utc::now() + Duration::days(8),
            ),
            None => {
                let now = Utc::now();
                (now, now + Duration::days(8))
            }
        }
    };
    Ok((start, format!("https://griddata.elia.be/eliabecontrols.prod/interface/stlforecasting/stlforecastinggraph/1/from-{}/to-{}",
    start.format("%Y-%m-%d"),
    next_step.format("%Y-%m-%d"),
)))
}

pub async fn store_forecast_load(store: &ArcStore) -> FetchResult<()> {
    let (last_update, url) = load_forecast_url(store).await?;
    println!("store_totalload {}", url);

    let records_res = map_xml_resource(&url, |doc| {
        tag_filter("STLForecastingGraphDto", &doc)
            .map(|node| SolarLoad::from_node(&node))
            .filter(|r| {
                if let Some(d) = r.date_time {
                    d > last_update
                } else {
                    false
                }
            })
            .collect()
    })
    .await;
    let records: Vec<SolarLoad> = match records_res {
        Ok(vec) => {
            store
                .save_fetch_event(&FetchEvent {
                    ts: Some(Utc::now()),
                    begin: Some(last_update),
                    description: Some("InsertLoad".into()),
                    url: Some(url.to_string()),
                    error: None,
                })
                .await?;
            vec
        }
        Err(e) => {
            store
                .save_fetch_event(&FetchEvent {
                    ts: Some(Utc::now()),
                    begin: Some(last_update),
                    description: Some("InsertLoad".into()),
                    url: Some(url.to_string()),
                    error: Some(format!("{:?}", e)),
                })
                .await?;
            Vec::new()
        }
    };

    for rec in records.iter() {
        store.save_solar_load(rec).await?;
    }
    Ok(())
}

async fn load_past_url(store: &ArcStore) -> FetchResult<(DateTime<Utc>, String)> {
    let last_update = store.load_last_past_update().await?;
    let next_step = Utc::now() + Duration::days(1);

    let start = {
        match last_update.value() {
            Some(last_update) if *last_update >= Utc::now() => {
                return Err(FetchError {
                    inner: String::from("Load last_update > now"),
                });
            }
            Some(last_update) => *last_update,
            None => Utc.ymd(2020, 1, 1).and_hms(0, 0, 0),
        }
    };
    Ok((start, format!("https://griddata.elia.be/eliabecontrols.prod/interface/stlforecasting/stlforecastinggraph/1/from-{}/to-{}",
    start.format("%Y-%m-%d"),
    next_step.format("%Y-%m-%d"),
)))
}

pub async fn store_past_load(store: &ArcStore) -> FetchResult<()> {
    let (last_update, url) = load_past_url(store).await?;
    println!("store_past_totalload {}", url);

    let records_res = map_xml_resource(&url, |doc| {
        tag_filter("STLForecastingGraphDto", &doc)
            .map(|node| SolarLoadPast::from_node(&node))
            .filter(|r| {
                if let Some(d) = r.date_time {
                    d > last_update
                } else {
                    false
                }
            })
            .filter(|r| {
                if let Some(p) = r.total_load {
                    p >= 0.0
                } else {
                    false
                }
            })
            .collect()
    })
    .await;
    let records: Vec<SolarLoadPast> = match records_res {
        Ok(vec) => {
            store
                .save_fetch_event(&FetchEvent {
                    ts: Some(Utc::now()),
                    begin: Some(last_update),
                    description: Some("InsertLoadPast".into()),
                    url: Some(url.to_string()),
                    error: None,
                })
                .await?;
            vec
        }
        Err(e) => {
            store
                .save_fetch_event(&FetchEvent {
                    ts: Some(Utc::now()),
                    begin: Some(last_update),
                    description: Some("InsertLoadPast".into()),
                    url: Some(url.to_string()),
                    error: Some(format!("{:?}", e)),
                })
                .await?;
            Vec::new()
        }
    };

    for rec in records.iter() {
        store.save_solar_load_past(rec).await?;
    }
    Ok(())
}
