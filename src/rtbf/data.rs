use crate::{
    forecast::{
        jauges::{solar_jauge_svg, wind_jauge_svg},
        wind::wind_linechart_svg,
    },
    models::regional::{SelectForecast, SelectLoadFactor},
    models::shore::{WindForecast, WindLoadFactorForecast, WindMonitoredCapacityForecast},
    models::solar_indicators::{
        SolarCapacityCorrection, SolarFamilyNbForecast, SolarMonitoredCapacityForecast,
    },
    store::{ArcStore, Offshore, Region},
    util::begin_of_day,
    {map::solar_map_svg_download, raster::svg_to_png},
};
use futures::join;
use resvg::Image;

use chrono::{DateTime, Datelike, Duration, Timelike, Utc};
use chrono_tz::Europe::Brussels;

pub struct RTBFData {
    pub solar_loadfactor: Result<SelectLoadFactor, String>,
    pub wind_loadfactor: Result<WindLoadFactorForecast, String>,
    pub solar_forecast: Result<SelectForecast, String>,
    pub solar_mon_cap: Result<SolarMonitoredCapacityForecast, String>,
    pub solar_family_nb: Result<SolarFamilyNbForecast, String>,
    pub wind_tot_forecast: Result<f64, String>,
    pub wind_off_forecast: Result<WindForecast, String>,
    pub wind_on_forecast: Result<WindForecast, String>,
    pub wind_mon_cap: Result<WindMonitoredCapacityForecast, String>,
    pub wind_family_nb: Result<f64, String>,
    // + images: carte solaire, graphe éolien (et jauges??)
}

pub async fn rtbf_data_from_day(store: &ArcStore, start_date: DateTime<Utc>) -> RTBFData {
    let start = &begin_of_day(&start_date);
    let end_date = start_date + Duration::days(1);
    let end = &begin_of_day(&end_date);

    // solar load_factor (-> jauge)
    // take avg of loadfactors between 13h and 14h (see SQL), for all days between start and end
    let solar_loadfactor = store
        .solar_loadfactor_forecast(start, end, Region::Belgique)
        .await
        .map_err(|_| "Solar loadfactors are missing".to_string());

    // wind_loadfactor (-> jauge)
    let wind_loadfactor = store
        .wind_loadfactor_forecast(start, end, Offshore::None)
        .await
        .map(|x| WindLoadFactorForecast::new(x.value().round()))
        .map_err(|_| "Wind loadfactor is missing".to_string());

    // solar forecast
    let solar_forecast = store
        .solar_forecast(start, end, Region::Belgique)
        .await
        .map_err(|_| "Solar forecast value is missing".to_string());

    let solar_correction = store
        .solar_capacity_correction(end)
        .await
        .unwrap_or(SolarCapacityCorrection::new(0.));

    let solar_mon_cap = store
        .solar_monitored_capacity_forecast(start, end)
        .await
        .map(|cap| cap.with_correction(solar_correction))
        .map_err(|_| "Solar monitored capacity is missing".to_string());

    let solar_family_nb = store
        .solar_familly_nb_forecast(start, end, Region::Belgique)
        .await
        .map_err(|_| "Solar family number is missing".to_string());

    // wind forecast
    let wind_off_forecast = store
        .wind_forecast(start, end, Offshore::True)
        .await
        .map_err(|_| "Wind offshore forecast value is missing".to_string());

    let wind_on_forecast = store
        .wind_forecast(start, end, Offshore::False)
        .await
        .map_err(|_| "Wind onshore forecast value is missing".to_string());

    let wind_tot_forecast = match (&wind_off_forecast, &wind_on_forecast) {
        (Ok(off), Ok(on)) => Ok(off.value() + on.value()),
        (_, _) => Err("Offshore or onshore value is missing".to_string()),
    };

    let wind_mon_cap = store
        .wind_monitored_capacity_forecast(start, end, Offshore::None)
        .await
        .map_err(|_| "Wind monitored capacity is missing".to_string());

    let wind_family_nb = store
        .wind_familly_nb_forecast(start, end, Offshore::None)
        .await
        .map_err(|_| "Wind family number is missing".to_string());

    RTBFData {
        solar_loadfactor,
        wind_loadfactor,
        solar_forecast,
        solar_mon_cap,
        solar_family_nb,
        wind_tot_forecast,
        wind_off_forecast,
        wind_on_forecast,
        wind_mon_cap,
        wind_family_nb,
    }
}

pub type NamedImage = (&'static str, Option<Image>);

pub async fn rtbf_images_between_dates(
    store: &ArcStore,
    start_date: DateTime<Utc>,
    end_date: DateTime<Utc>,
) -> Vec<NamedImage> {
    let start = &begin_of_day(&start_date);
    let end_date = end_date + Duration::days(1);
    let end = &begin_of_day(&end_date);

    let solar_jauge = match store //take avg of loadfactors between 13h and 14h (see SQL), for all days between start and end
        .solar_loadfactor_forecast(start, end, Region::Belgique)
        .await
    {
        Ok(lf) => {
            let slf = lf.value().round();
            svg_to_png(solar_jauge_svg(slf))
        }
        Err(_) => None,
    };

    let wind_jauge = match store
        .wind_loadfactor_forecast(start, end, Offshore::None)
        .await
    {
        Ok(wlf) => svg_to_png(wind_jauge_svg(wlf.value().round())),
        Err(_) => None,
    };
    let start_date_brussels = start_date.with_timezone(&Brussels);

    let year = start_date_brussels.date().year();
    let month = start_date_brussels.date().month();
    let day = start_date_brussels.date().day();

    // ACTUALLY WE DON'T NEED TO PICK 12H - IT'S ALREADY DONE IN SQL REQUEST ('select_solar_loadfactor_forecast.sql')
    let hour = start_date_brussels
        .with_hour(12)
        .unwrap_or(start_date_brussels)
        .hour();

    // wind linechart
    let wind_svg = wind_linechart_svg(year, month, day, 1, store).await;
    let wind_png = svg_to_png(wind_svg);

    // solar map
    let solar_svg = solar_map_svg_download(year, month, day, hour, store).await;
    let solar_png = svg_to_png(solar_svg);

    vec![
        ("solar_jauge", solar_jauge),
        ("wind_jauge", wind_jauge),
        ("solar_forecast", solar_png),
        ("wind_forecast", wind_png),
    ]
}

pub async fn rtbf_data(store: &ArcStore) -> (RTBFData, RTBFData) {
    let today = Utc::now();
    let tomorrow = Utc::now() + Duration::days(1);
    futures::future::join(
        rtbf_data_from_day(store, today),
        rtbf_data_from_day(store, tomorrow),
    )
    .await
}

pub async fn rtbf_images(
    store: &ArcStore,
) -> (
    Vec<NamedImage>,
    Vec<NamedImage>,
    Vec<NamedImage>,
    Vec<NamedImage>,
) {
    let today = Utc::now();
    let tomorrow = Utc::now() + Duration::days(1);

    let in5days = Utc::now() + Duration::days(4);
    let in7days = Utc::now() + Duration::days(5);
    join!(
        rtbf_images_between_dates(store, today, today),
        rtbf_images_between_dates(store, tomorrow, tomorrow),
        rtbf_images_between_dates(store, today, in5days),
        rtbf_images_between_dates(store, today, in7days),
    )
}

// pub async fn rtbf_all_data(
//     store: &ArcStore,
// ) -> (RTBFData, RTBFData, Vec<Option<Image>>, Vec<Option<Image>>) {
//     let today = Utc::now();
//     let tomorrow = Utc::now() + Duration::days(1);
//     futures::future::join4(
//         rtbf_data_from_day(store, today),
//         rtbf_data_from_day(store, tomorrow),
//         rtbf_images_for_date(store, today),
//         rtbf_images_for_date(store, tomorrow),
//     )
//     .await
// }
