use crate::store::{ArcStore, Store};
use crate::util::wait_start_time;
use suppaftp::{
    types::{FileType, FormatControl},
    FtpError, FtpStream,
};
use futures::stream::StreamExt;
use std::io::Cursor;
use tokio::time;
use tokio_stream::wrappers::IntervalStream;

use self::data::rtbf_images;

mod data;
mod xml;

#[derive(Debug, Clone)]
pub struct FTPConfig {
    pub host: String,
    pub port: u32,
    pub username: String,
    pub password: String,
    pub directory: String,
    pub time_of_day: chrono::NaiveTime,
}

#[derive(Clone)]
struct NamedData {
    filename: String,
    data: Vec<u8>,
    mode: FileType,
}

impl NamedData {
    fn new(filename: impl Into<String>, data: Vec<u8>, mode: FileType) -> NamedData {
        NamedData {
            filename: filename.into(),
            data,
            mode,
        }
    }

    fn from_string(filename: impl Into<String>, data: String) -> NamedData {
        NamedData {
            filename: filename.into(),
            data: data.into_bytes(),
            mode: FileType::Ascii(FormatControl::Default),
        }
    }
}

fn put_ftp(config: &FTPConfig, files: Vec<NamedData>) -> Result<(), FtpError> {
    let host = format!("{}:{}", config.host, config.port);
    let mut ftp_stream = FtpStream::connect(host)?;
    let _ = ftp_stream.login(&config.username, &config.password)?;
    let _ = ftp_stream.cwd(&config.directory)?;

    for nd in files {
        let mut reader = Cursor::new(nd.data.as_slice());
        let _ = ftp_stream.transfer_type(nd.mode);
        let _ = ftp_stream.put_file(&nd.filename, &mut reader)?;
    }

    ftp_stream.quit()
}

fn image_to_bytes(img: resvg::Image) -> Option<Vec<u8>> {
    let mut out: Vec<u8> = Vec::new();

    let mut encoder = png::Encoder::new(&mut out, img.width(), img.height());
    encoder.set_color(png::ColorType::RGBA);
    encoder.set_depth(png::BitDepth::Eight);

    match encoder
        .write_header()
        .and_then(|mut writer| writer.write_image_data(img.data()))
    {
        Ok(_) => Some(out),
        Err(_) => None,
    }
}

async fn uploader_inner(dsn: String, config: FTPConfig) {
    match Store::new(&dsn).await {
        Err(err) => println!("Failed to get a store:\n{:?} \n\nexiting", err),
        Ok(store) => {
            wait_start_time(config.time_of_day).await;
            let arc_store = ArcStore::new(store);
            let interval = time::interval(time::Duration::from_secs(43200));
            let interval = IntervalStream::new(interval);
            interval
                .for_each(|_| async {
                    let (day0_data, day1_data) = data::rtbf_data(&arc_store).await;
                    let xml_string = xml::apply_context(day0_data, day1_data);
                    let xml_files = vec![NamedData::from_string("meteo.xml", xml_string)];

                    let (day0_io, day1_io, day5_io, day7_io) = rtbf_images(&arc_store).await;
                    let day0_images: Vec<NamedData> = day0_io
                        .into_iter()
                        .filter_map(|(name, o)| {
                            o.and_then(image_to_bytes).map(|data| {
                                NamedData::new(
                                    format!("{}_today.png", name),
                                    data,
                                    FileType::Binary,
                                )
                            })
                        })
                        .collect();
                    let day1_images: Vec<NamedData> = day1_io
                        .into_iter()
                        .filter_map(|(name, o)| {
                            o.and_then(image_to_bytes).map(|data| {
                                NamedData::new(
                                    format!("{}_tomorrow.png", name),
                                    data,
                                    FileType::Binary,
                                )
                            })
                        })
                        .collect();
                    let day5_images: Vec<NamedData> = day5_io
                        .into_iter()
                        .filter_map(|(name, o)| {
                            o.and_then(image_to_bytes).map(|data| {
                                NamedData::new(
                                    format!("{}_5days.png", name),
                                    data,
                                    FileType::Binary,
                                )
                            })
                        })
                        .collect();
                    let day7_images: Vec<NamedData> = day7_io
                        .into_iter()
                        .filter_map(|(name, o)| {
                            o.and_then(image_to_bytes).map(|data| {
                                NamedData::new(
                                    format!("{}_7days.png", name),
                                    data,
                                    FileType::Binary,
                                )
                            })
                        })
                        .collect();

                    let files = [
                        day0_images,
                        day1_images,
                        day5_images,
                        day7_images,
                        xml_files,
                    ]
                    .concat();

                    match put_ftp(&config, files) {
                        Ok(_) => println!("Upload OK"),
                        Err(e) => println!("Upload FTP Error: {}", e),
                    }
                })
                .await;
        }
    }
}

pub fn uploader(dsn: String, config: FTPConfig) {
    let rt = tokio::runtime::Runtime::new().unwrap();
    rt.block_on(uploader_inner(dsn, config));
}
