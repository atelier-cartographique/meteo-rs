use crate::html::{as_xml, element, Element};
use crate::util::{format_datetime, format_family_nb};

use super::data::RTBFData;

fn el<V>(tag: &'static str, data: Result<V, String>) -> Element
where
    V: Into<f64>,
{
    match data {
        Ok(n) => element(tag, format!("{}", n.into())),
        Err(e) => element(tag, e),
    }
}
fn str_el(tag: &'static str, data: Result<String, String>) -> Element {
    match data {
        Ok(n) => element(tag, n),
        Err(e) => element(tag, e),
    }
}

pub fn make_children(tag: &'static str, data: RTBFData) -> Element {
    element(
        tag,
        vec![
            element(
                "Solar",
                vec![
                    el("SolarLoadFactor", data.solar_loadfactor),
                    el("SolarForecast", data.solar_forecast),
                    el("SolarMonitoredCapacity", data.solar_mon_cap),
                    str_el(
                        "SolarFamilyNumber",
                        Ok(data
                            .solar_family_nb
                            .map_or("inconnu".to_string(), format_family_nb)),
                    ),
                    // el("SolarFamilyNumber", data.solar_family_nb),
                ],
            ),
            element(
                "Wind",
                vec![
                    el("WindLoadFactor", data.wind_loadfactor),
                    el("WindTotalForecast", data.wind_tot_forecast),
                    el("WindOffshoreForecast", data.wind_off_forecast),
                    el("WindOnshoreForecast", data.wind_on_forecast),
                    el("WindMonitoredCapacity", data.wind_mon_cap),
                    str_el(
                        "WindFamilyNumber",
                        Ok(data
                            .wind_family_nb
                            .map_or("inconnu".to_string(), format_family_nb)),
                    ),
                ],
            ),
        ],
    )
}

pub fn apply_context(day0: RTBFData, day1: RTBFData) -> String {
    let root = element(
        "rtbf",
        vec![make_children("Day0", day0), make_children("Day1", day1)],
    )
    .set("datetime", format_datetime(&chrono::Utc::now()));
    as_xml(root)
}
