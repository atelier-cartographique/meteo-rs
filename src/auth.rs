use std::{collections::HashMap, convert::TryInto};

use crate::{
    form::field,
    html::{body, div, form, head, html, input, link, meta, Element},
    session::{ReplyWithSession, Session, SessionDelete},
    store::ArcStore,
    user::{check_password, UserRecord},
};
use warp::{Rejection, Reply};

const ROOT: &str = "/";
pub const LOGIN_URL: &str = "/auth/login";

fn make_document(elem: Element, title: &str) -> Element {
    html([
        head([
            meta().set("charset", "UTF-8"),
            meta().set("title", title),
            link()
                .set("rel", "stylesheet")
                .set("type", "text/css")
                .set("href", "/static/style/login.css"),
        ]),
        body(elem),
    ])
}

async fn render_logout(session: &Session, store: &ArcStore) -> Result<Element, Rejection> {
    let _ = store
        .delete_session(&SessionDelete::new(session.username().to_string()))
        .await?;

    Ok(make_document(div("bye"), "bye"))
}

async fn render_login_inner() -> Result<Element, Rejection> {
    let form_elem = form([
        input()
            .set("type", "hidden")
            .set("name", "next")
            .set("value", "/news"),
        div([
            div("username"),
            input().set("type", "text").set("name", "username"),
        ]),
        div([
            div("password"),
            input().set("type", "password").set("name", "password"),
        ]),
        div(input().set("type", "submit").set("value", "login")),
    ])
    .set("action", LOGIN_URL)
    .set("method", "post");

    Ok(make_document(form_elem, "login"))
}

pub async fn render_login(
    session: Option<Session>,
    store: ArcStore,
) -> Result<impl Reply, Rejection> {
    match session {
        Some(session) => render_logout(&session, &store).await,
        None => render_login_inner().await,
    }
}

pub async fn post_login(
    store: ArcStore,
    form: HashMap<String, String>,
) -> Result<impl Reply, Rejection> {
    let username = field::<String>(&form, "username")?;
    let password = field::<String>(&form, "password")?;
    let next_string = field::<String>(&form, "next").unwrap_or(ROOT.to_string());
    let next: warp::http::Uri = next_string
        .try_into()
        .unwrap_or(warp::http::Uri::from_static(ROOT));
    let user: UserRecord = store.find_user(&username).await?.into();
    if check_password(&password, user.password()) {
        let session: Session = store.create_session(&username).await?.into();
        Ok(ReplyWithSession::new(warp::redirect::see_other(next)).with_session(session))
    } else {
        Ok(ReplyWithSession::new(warp::redirect::see_other(
            warp::http::Uri::from_static(LOGIN_URL),
        )))
    }
}
