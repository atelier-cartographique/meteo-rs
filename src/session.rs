use chrono::{DateTime, Duration, Utc};
use model_wrapper::Wrapper0;
use rand_chacha::ChaCha8Rng;
use rand_core::{OsRng, RngCore, SeedableRng};
use warp::{http, reply::Response, Filter, Reply};

use crate::{
    model::{params, FromRow, ModelError, ToParams, WithSQL},
    store::{ArcStore, Store, StoreError},
};

pub const SESSION_COOKIE_NAME: &str = "session_token";
const LIFETIME: i64 = 30;

#[derive(Wrapper0, Clone, Debug)]
pub struct Session {
    username: String,
    token: String,
    created_at: DateTime<Utc>,
}

fn from_row(row: &tokio_postgres::Row) -> Result<Session, ModelError> {
    Ok(Session {
        username: row
            .try_get("username")
            .map_err(|err| ModelError::Wrapped(format!("username: {err}")))?,
        token: row
            .try_get("token")
            .map_err(|err| ModelError::Wrapped(format!("token: {err}")))?,
        created_at: row
            .try_get("created_at")
            .map_err(|err| ModelError::Wrapped(format!("created_at: {err}")))?,
    })
}

// GET

#[derive(Wrapper0, Clone, Debug)]
pub struct SessionGet(Session);

impl WithSQL for SessionGet {
    fn sql() -> &'static str {
        "
        SELECT token, username, created_at
        FROM sessions
        WHERE token = $1;
        "
    }
}

impl FromRow for SessionGet {
    fn from_row(row: &tokio_postgres::Row) -> Result<Self, ModelError>
    where
        Self: Sized,
    {
        from_row(row).map(Self)
    }
}

// CREATE

#[derive(Wrapper0, Clone, Debug)]
pub struct SessionCreate(Session);

impl WithSQL for SessionCreate {
    fn sql() -> &'static str {
        "INSERT INTO  sessions (username, token, created_at) VALUES ($1, $2, $3) RETURNING *; "
    }
}

impl FromRow for SessionCreate {
    fn from_row(row: &tokio_postgres::Row) -> Result<Self, ModelError>
    where
        Self: Sized,
    {
        from_row(row).map(Self)
    }
}

impl ToParams for SessionCreate {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        (params() + self.value().username() + self.value().token() + self.value().created_at())
            .inner()
    }
}

// DELETE

#[derive(Wrapper0, Clone, Debug)]
pub struct SessionDelete(String);

impl WithSQL for SessionDelete {
    fn sql() -> &'static str {
        "DELETE FROM sessions WHERE username = $1;"
    }
}
impl ToParams for SessionDelete {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        (params() + self.value()).inner()
    }
}

impl Store {
    pub async fn get_session(&self, token: &str) -> Result<SessionGet, StoreError> {
        self.fetch_one(&[&token]).await
    }

    pub async fn create_session(&self, username: &str) -> Result<SessionCreate, StoreError> {
        // FIXME users should be able to have multiple sessions
        let _ = self
            .delete_session(&SessionDelete::new(username.to_string()))
            .await?;
        let mut random = ChaCha8Rng::seed_from_u64(OsRng.next_u64());
        let mut token = [0u8; 16];
        random.fill_bytes(&mut token);
        let token = format!("{}", u128::from_le_bytes(token));
        self.create(&[&username, &token, &Utc::now()]).await
    }

    pub async fn delete_session(&self, del: &SessionDelete) -> Result<u64, StoreError> {
        self.delete(del).await
    }
}

pub async fn get_session(store: &ArcStore, session_token: String) -> Option<Session> {
    if let Ok(session) = store.get_session(&session_token).await {
        let session: Session = session.into();
        if session.created_at > Utc::now() + Duration::days(LIFETIME) {
            let _ = store
                .delete_session(&SessionDelete::new(session.username().to_string()))
                .await;
            return None;
        } else {
            return Some(session);
        }
    }

    None
}

pub async fn session_from_token(session_opt: Option<String>, store: ArcStore) -> Option<Session> {
    if let Some(session_token) = session_opt {
        return get_session(&store, session_token).await;
    }
    None
}

pub fn filter_session(store: ArcStore) -> warp::filters::BoxedFilter<(ArcStore, Option<Session>)> {
    let store = store.clone();
    warp::any()
        .map(move || (&store).clone())
        .and(warp::cookie::optional(SESSION_COOKIE_NAME))
        .then(|store, session_opt: Option<String>| async {
            if let Some(session_token) = session_opt {
                let sess = get_session(&store, session_token.clone()).await;
                return (store, sess);
            } else {
                return (store, None);
            }
        })
        .untuple_one()
        .boxed()

    // warp::any()
    //     .map(move || (&store).clone())

    //     .and(warp::cookie::optional(SESSION_COOKIE_NAME).then(
    //         move |session_opt: Option<String>| async move {
    //             if let Some(session_token) = session_opt {
    //                 return get_session(&store, session_token.clone()).await;
    //             } else {
    //                 return None;
    //             }
    //         },
    //     ))
}

pub struct ReplyWithSession<R>
where
    R: Reply,
{
    reply: R,
    session: Option<Session>,
}

impl<R> ReplyWithSession<R>
where
    R: Reply,
{
    pub fn new(reply: R) -> Self {
        ReplyWithSession {
            reply,
            session: None,
        }
    }

    pub fn with_session(self, session: Session) -> Self {
        ReplyWithSession {
            reply: self.reply,
            session: Some(session),
        }
    }
}

impl<R> Reply for ReplyWithSession<R>
where
    R: Reply,
{
    fn into_response(self) -> Response {
        let mut res = self.reply.into_response();
        if let Some(Session { token, .. }) = self.session {
            let headers = res.headers_mut();
            let cookie = format!(
                "{}={}; Max-Age={}; Path=/",
                SESSION_COOKIE_NAME,
                token,
                LIFETIME * 24 * 3600
            );
            if let Ok(cookie_value) = http::HeaderValue::from_str(&cookie) {
                headers.insert(http::header::SET_COOKIE, cookie_value);
            }
        }
        res
    }
}

// pub struct SessionStore {
//     random: ChaCha8Rng,
//     sessions: HashMap<u128, Session>,
//     lifetime: i64, // days
// }

// impl SessionStore {
//     pub fn with_lifetime(lifetime: i64) -> Self {
//         SessionStore {
//             random: ChaCha8Rng::seed_from_u64(OsRng.next_u64()),
//             sessions: HashMap::new(),
//             lifetime,
//         }
//     }
//     pub fn new() -> Self {
//         SessionStore {
//             random: ChaCha8Rng::seed_from_u64(OsRng.next_u64()),
//             sessions: HashMap::new(),
//             lifetime: 30,
//         }
//     }

//     pub fn get_session(&mut self, session_id: u128) -> Option<&Session> {
//         self.sessions.get(&session_id).and_then(|session| {
//             if session.created_at > Utc::now() + Duration::days(self.lifetime) {
//                 self.sessions.remove(&session_id);
//                 None
//             } else {
//                 Some(session)
//             }
//         })
//     }

//     pub fn create_session(&mut self, username: String) -> Session {
//         let mut u128_pool = [0u8; 16];
//         self.random.fill_bytes(&mut u128_pool);
//         let token = u128::from_le_bytes(u128_pool);
//         let session = Session {
//             token,
//             username: None,
//             created_at: Utc::now(),
//         };
//         self.sessions.insert(token, session.clone());
//         session
//     }
// }

// pub type ArcSessionStore = ::std::sync::Arc<SessionStore>;

// struct SessionWrapper {
//     store: ArcSessionStore,
// }

// impl<F, T, U> WrapSealed<T> for SessionWrapper<F>
// where
//     F: Fn(T) -> U,
//     T: Filter,
//     U: Filter,
// {
//     type Wrapped = U;

//     fn wrap(&self, filter: T) -> Self::Wrapped {
//         // (self.func)(filter)
//         filter
//             .and(warp::cookie::optional(SESSION_COOKIE_NAME))
//             .map(|r, session_opt| {})
//     }
// }

// #[macro_export]
// macro_rules! session_wrap {
//     ($orig:ident, $sess:ident) => {{
//         // use tap::Pipe;

//         // let credentials =
//         //     Some("Basic ".to_string() + &base64::encode(format!("{}:{}", $user, $pass)));

//         // let check_creds = move |auth: Option<String>| {
//         //     if auth == credentials {
//         //         Ok(())
//         //     } else {
//         //         Err(reject::custom($crate::auth::AuthorizationRequired))
//         //     }
//         //     .pipe(|ret| async { ret })
//         // };
//         let sess_cloned = $sess.clone();
//         $orig.wrap(
//             wrap_fn(move |filter| {

//                 filter
//                 .and(warp::cookie::optional(SESSION_COOKIE_NAME))
//                 .map(|session_opt, response| {
//                     match session_opt
//                 })

//                 // header::optional("authorization")
//                 //     .and_then(check_creds.clone())
//                 //     .and(filter)
//                 //     .map(|(), res| res)
//                 //     .recover(|rej: Rejection| async {
//                 //         if let Some($crate::auth::AuthorizationRequired) = rej.find() {
//                 //             Ok(hyper::Response::builder()
//                 //                 .status(401)
//                 //                 .header("WWW-Authenticate", "Basic")
//                 //                 .body("")
//                 //                 .unwrap())
//                 //         } else {
//                 //             Err(rej)
//                 //         }
//                 //     })
//             })
//         )
//     }};
// }
