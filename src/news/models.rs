use std::{convert::TryInto, fmt::Display, str::FromStr};

use chrono::{DateTime, Utc};
use model_wrapper::Wrapper0;

use crate::{
    model::{params, FromRow, ToParams, WithSQL},
    store::{Store, StoreError},
};

#[derive(Debug, PartialEq)]
pub enum NewsStatus {
    Draft,
    Published,
    Deleted,
}

impl Display for NewsStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            NewsStatus::Draft => write!(f, "brouillon"),
            NewsStatus::Published => write!(f, "publié"),
            NewsStatus::Deleted => write!(f, "supprimé"),
        }
    }
}

impl std::convert::TryFrom<i32> for NewsStatus {
    type Error = String;
    fn try_from(value: i32) -> Result<Self, Self::Error> {
        match value {
            1 => Ok(NewsStatus::Draft),
            2 => Ok(NewsStatus::Published),
            3 => Ok(NewsStatus::Deleted),
            _ => Err(format!("NewsStatus must be of 1, 2 or 3. Not {value}")),
        }
    }
}

impl FromStr for NewsStatus {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse::<i32>()
            .or(Err(format!("'{s}' does not parse as an i32")))
            .and_then(|code| code.try_into())
    }
}

impl Into<i32> for NewsStatus {
    fn into(self) -> i32 {
        match self {
            NewsStatus::Draft => 1,
            NewsStatus::Published => 2,
            NewsStatus::Deleted => 3,
        }
    }
}
impl Into<i32> for &NewsStatus {
    fn into(self) -> i32 {
        match self {
            NewsStatus::Draft => 1,
            NewsStatus::Published => 2,
            NewsStatus::Deleted => 3,
        }
    }
}

fn from_row(row: &tokio_postgres::Row) -> Result<NewsItem, crate::model::ModelError> {
    let status_code: i32 = row.try_get("status")?;
    Ok(NewsItem {
        id: row.try_get("id")?,
        pub_date: row.try_get("pub_date").ok(),
        summary: row.try_get("summary")?,
        content: row.try_get("content")?,
        status: status_code
            .try_into()
            .map_err(crate::model::ModelError::Wrapped)?,
    })
}

#[derive(Wrapper0, Debug)]
pub struct NewsItem {
    id: i32,
    pub_date: Option<DateTime<Utc>>,
    summary: String,
    content: String,
    status: NewsStatus,
}

// SELECT ALL

#[derive(Wrapper0, Debug)]
pub struct NewsItemSelectAll(NewsItem);

impl WithSQL for NewsItemSelectAll {
    fn sql() -> &'static str {
        "
        SELECT id, pub_date, summary, content, \"status\" 
        FROM news ORDER BY pub_date DESC NULLS LAST; 
        "
    }
}

impl FromRow for NewsItemSelectAll {
    fn from_row(row: &tokio_postgres::Row) -> Result<Self, crate::model::ModelError>
    where
        Self: Sized,
    {
        from_row(row).map(Self)
    }
}

impl Store {
    pub async fn get_all_news(&self) -> Result<Vec<NewsItemSelectAll>, StoreError> {
        self.fetch(&[]).await
    }
}

// SELECT LATEST

#[derive(Wrapper0, Debug)]
pub struct NewsItemSelectLatest(NewsItem);

impl WithSQL for NewsItemSelectLatest {
    fn sql() -> &'static str {
        "
        SELECT id, pub_date, summary, content, status 
        FROM news
        WHERE \"status\" = 2 AND pub_date < now()
        ORDER BY  pub_date DESC
        LIMIT 1
        ; 
        "
    }
}

impl FromRow for NewsItemSelectLatest {
    fn from_row(row: &tokio_postgres::Row) -> Result<Self, crate::model::ModelError>
    where
        Self: Sized,
    {
        from_row(row).map(Self)
    }
}

impl Store {
    pub async fn get_latest_news(&self) -> Result<NewsItemSelectLatest, StoreError> {
        self.fetch_one(&[]).await
    }
}

// CREATE

#[derive(Wrapper0, Debug)]
pub struct NewsItemCreate(NewsItem);

impl WithSQL for NewsItemCreate {
    fn sql() -> &'static str {
        " INSERT INTO  news (summary, content, \"status\") VALUES ($1, $2, $3) RETURNING *; "
    }
}

impl ToParams for NewsItemCreate {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        let status_code: i32 = self.0.status().into();
        (params() + self.0.pub_date() + self.0.summary() + self.0.content() + status_code).inner()
    }
}

impl FromRow for NewsItemCreate {
    fn from_row(row: &tokio_postgres::Row) -> Result<Self, crate::model::ModelError>
    where
        Self: Sized,
    {
        from_row(row).map(Self)
    }
}

impl Store {
    pub async fn create_news(&self) -> Result<NewsItemCreate, StoreError> {
        let summary = "";
        let content = "";
        let status: i32 = NewsStatus::Draft.into();
        self.create::<NewsItemCreate>(&[&summary, &content, &status])
            .await
    }
}

// UPDATE

#[derive(Wrapper0, Debug)]
pub struct NewsItemUpdate(NewsItem);

impl WithSQL for NewsItemUpdate {
    fn sql() -> &'static str {
        "UPDATE news  
        SET pub_date = $2, 
            summary = $3, 
            content = $4, 
            \"status\" = $5
        WHERE id = $1; "
    }
}

impl ToParams for NewsItemUpdate {
    fn params<'a>(&'a self) -> Vec<Box<dyn tokio_postgres::types::ToSql + Sync + Send + 'a>> {
        let status_code: i32 = self.0.status().into();
        (params()
            + self.0.id()
            + self.0.pub_date()
            + self.0.summary()
            + self.0.content()
            + status_code)
            .inner()
    }
}

impl Store {
    pub async fn update_news(&self, record: &NewsItemUpdate) -> Result<u64, StoreError> {
        self.save(record).await
    }
}
