use pulldown_cmark::{HeadingLevel, LinkType, Tag, Event};

use crate::html::{element, Children, Node, div};

fn heading_str(level: HeadingLevel) -> &str {
    match level {
        HeadingLevel::H1 => "h1",
        HeadingLevel::H2 => "h2",
        HeadingLevel::H3 => "h3",
        HeadingLevel::H4 => "h4",
        HeadingLevel::H5 => "h5",
        HeadingLevel::H6 => "h6",
    }
}

fn tag_to_node(tag: Tag) -> Node {
    match tag {
        Tag::Paragraph => element("p", Children::Empty),
        Tag::Heading(level, _, _) => element(heading_str(heading), c),
        Tag::BlockQuote => element("blockquote", Children::Empty),
        Tag::CodeBlock(_) => element("code", Children::Empty),
        Tag::List(Some(start)) => element("ol", Children::Empty).set("start", start),
        Tag::List(None) => element("ul", Children::Empty),
        Tag::Item => element("li", Children::Empty),
        Tag::FootnoteDefinition(c) => element("div", Children::Empty).set("title", c),
        Tag::Table(_) => element("table", Children::Empty),
        Tag::TableHead => element("thead", Children::Empty),
        Tag::TableRow => element("tr", Children::Empty),
        Tag::TableCell => element("td", Children::Empty),
        Tag::Emphasis => element("em", Children::Empty),
        Tag::Strong => element("strong", Children::Empty),
        Tag::Strikethrough => element("s", Children::Empty),
        Tag::Link(LinkType::Inline, url, title) => element("a", title).set("href", url),
        Tag::Link(LinkType::Autolink, url, title) => element("a", title).set("href", url),
        Tag::Link(LinkType::Email, url, title) => element("a", title).set("href", url),
        Tag::Link(_, _, title) => element("span", title),
        Tag::Image(LinkType, src, title) => element("img", Children::Empty)
            .set("src", src)
            .set("title", title),
    }
}



// Start(Tag<'a>),
// End(Tag<'a>),
// Text(CowStr<'a>),
// Code(CowStr<'a>),
// Html(CowStr<'a>),
// FootnoteReference(CowStr<'a>),
// SoftBreak,
// HardBreak,
// Rule,
// TaskListMarker(bool),


struct Markdown<'a>{
    root_node: Node,
    heap: Vec<&'a Node>,
    input: &'a str,
    head: usize,
}

impl<'a> Markdown<'a> {
    pub fn new(input: &'a str) -> Markdown<'a> {
        Markdown { root_node , heap: Vec::new(), input, head: 0 }
    }

    pub fn push_node(&mut self, node: Node) -> Option<&Node>{

    }
} 



pub fn visit(event:Event, md: &mut Markdown) {
match event {
    Start(tag) => ,
// End(Tag<'a>),
// Text(CowStr<'a>),
// Code(CowStr<'a>),
// Html(CowStr<'a>),
// FootnoteReference(CowStr<'a>),
// SoftBreak,
// HardBreak,
// Rule,
// TaskListMarker(bool),
}    

}