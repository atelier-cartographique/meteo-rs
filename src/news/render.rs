use std::collections::HashMap;

use super::models::{NewsItem, NewsItemUpdate, NewsStatus};
use crate::{
    form::field,
    html::{
        anchor, body, div, form, h1, head, html, img, input, link, meta, option, select, text_area,
        Children, Element, Empty,
    },
    session::Session,
    store::{ArcStore, StoreError},
    util::format_date_iso,
};
use chrono::{DateTime, Utc};

use pulldown_cmark::{html, Options, Parser};
use warp::{reject, Rejection};

impl NewsItem {
    fn rendered(&self) -> String {
        let parser = Parser::new_ext(self.content(), Options::empty());
        let mut html_output = String::new();
        html::push_html(&mut html_output, parser);

        html_output
    }
    fn render_pub_date(&self) -> String {
        self.pub_date()
            .map(|date| format_date_iso(&date))
            .unwrap_or("".into())
    }
}

pub async fn render_latest_news(
    store: &ArcStore,
    _session_opt: Option<Session>,
) -> Result<Element, StoreError> {
    let news: NewsItem = store.get_latest_news().await?.into();
    // let id = format!("{}", news.id());
    let link: Element =
        div(anchor("Liste des news précédentes").set("href", format!("/news/")).set("target", "_top")).class("link-list");
    let logo: Element = anchor(img().set(
        "src",
        "https://atelier-cartographique.be/storage/apere/logo-ec.png",
    ))
    .set("href", "https://energiecommune.be")
    .class("logo--link");
    Ok(div([
        div([
            div(news.render_pub_date()).class("pub_date"),
            div(news.summary()).class("pub_title"),
            div(Children::Empty)
                .inner_html(news.rendered())
                .class("inner-text"),
        ])
        .class("content"),
        div([logo, link]).class("footer"),
    ])
    .class("news-item"))
}

pub async fn render_latest_news_reply(store: ArcStore) -> Result<Element, Rejection> {
    match render_latest_news(&store, None).await {
        Ok(e) => Ok(make_document(e, "latest news", "news_latest")),
        Err(_) => Err(reject()),
    }
}

fn make_document(elem: Element, title: &str, stylesheet: &str) -> Element {
    html([
        head([
            meta().set("charset", "UTF-8"),
            meta().set("title", title),
            link()
                .set("rel", "stylesheet")
                .set("type", "text/css")
                .set("href", format!("/static/style/{stylesheet}.css")),
        ]),
        body(elem),
    ])
}

fn render_need_session() -> Element {
    make_document(
        div([
            h1("you must be logged in to access this page"),
            anchor("login page").set("href", crate::auth::LOGIN_URL),
        ]),
        "not authorized",
        "news",
    )
}

fn render_news_item_admin(record: &NewsItem) -> Element {
    // let record: &NewsItem = news_item.value();
    let id = format!("{}", record.id());
    div([
        div(anchor(&id).set("href", format!("/news/admin/change/{id}"))).class("id"),
        div(record.render_pub_date()).class("pub_date"),
        div(format!("{}", record.status())).class("status"),
        div(record.summary()).class("summary"),
    ])
    .class("news-item")
}

fn render_create_news(session_opt: Option<Session>) -> Element {
    match session_opt {
        None => div(anchor("login").set("href", crate::auth::LOGIN_URL)),
        Some(_) => form(input().set("type", "submit").set("value", "créer une news"))
            .set("action", "/news/admin/create")
            .set("method", "post"),
    }
}

pub async fn render_list_admin(
    store: ArcStore,
    session_opt: Option<Session>,
) -> Result<Element, Rejection> {
    let all_news = store.get_all_news().await.map_err(warp::reject::custom)?;
    let news_nodes: Vec<Element> = all_news
        .iter()
        .map(|ni| render_news_item_admin(ni.value()))
        .collect();
    Ok(make_document(
        div([
            h1("Admin : Liste des news"),
            render_create_news(session_opt),
            div(news_nodes).class("content"),
        ])
        .class("news-list"),
        "news",
        "news_admin",
    ))
}

pub async fn create_news(
    store: ArcStore,
    session_opt: Option<Session>,
) -> Result<Element, Rejection> {
    let new = store.create_news().await.map_err(warp::reject::custom)?;
    render_single_edit(store, session_opt, *new.value().id()).await
}

fn render_single_edit_element(news: &NewsItem) -> Element {
    make_document(
        div([
            h1("Admin : Création / Édition d'une news"),
            anchor("retour à la liste").set("href", "/news/admin/"),
            form([
                input()
                    .set("name", "id")
                    .set_num("value", *news.id())
                    .set_bool("hidden", true),
                div([
                    div("Statut"),
                    div(select([
                        option("brouillon")
                            .set_bool("selected", *news.status() == NewsStatus::Draft)
                            .set_num("value", 1),
                        option("publié")
                            .set_bool("selected", *news.status() == NewsStatus::Published)
                            .set_num("value", 2),
                        option("supprimé")
                            .set_bool("selected", *news.status() == NewsStatus::Deleted)
                            .set_num("value", 3),
                    ])
                    .set("name", "status")),
                ]),
                div([
                    div("Date de publication"),
                    input()
                        .set("type", "date")
                        .set(
                            "value",
                            news.pub_date()
                                .map(|date| format_date_iso(&date))
                                .unwrap_or("".into()),
                        )
                        .set("name", "pub_date"),
                ]),
                div([
                    div("Titre"),
                    input().set("name", "summary").set("value", news.summary()),
                ]),
                div([
                    div("Corps"),
                    text_area(news.content()).set("name", "content"),
                ]),
                input().set("type", "submit").set("value", "valider"),
            ])
            .set("action", format!("/news/admin/change/{}", news.id()))
            .set("method", "post"),
        ])
        .class("news-edit"),
        "Édition",
        "news_admin",
    )
}

pub async fn update_news(
    store: ArcStore,
    _session_opt: Option<Session>,
    _id: i32,
    form: HashMap<String, String>,
) -> Result<Element, Rejection> {
    // let all_news = store.get_all_news().await?;
    // let news_nodes: Vec<Element> = all_news.iter().map(render_news_item).collect();
    // Ok(div([h1("News"), div(news_nodes).class("content")])
    //     .class("news-list")
    //     .into())
    let item_id = field::<i32>(&form, "id")?;
    let pub_date = field::<String>(&form, "pub_date")
        .ok()
        .and_then(|date_string| {
            format!("{date_string}T08:00:00Z")
                .parse::<DateTime<Utc>>()
                .ok()
        }); // news du matin
    let summary = field::<String>(&form, "summary")?;
    let content = field::<String>(&form, "content")?;
    let status = field::<NewsStatus>(&form, "status")?;
    let news = NewsItem::new((item_id, pub_date, summary, content, status));
    let element = render_single_edit_element(&news);
    let record = NewsItemUpdate::new(news);
    store.update_news(&record).await?;
    Ok(element)
}

pub async fn render_single_edit(
    store: ArcStore,
    session_opt: Option<Session>,
    id: i32,
) -> Result<Element, Rejection> {
    if session_opt.is_none() {
        return Ok(render_need_session());
    }
    let all_news = store.get_all_news().await.map_err(warp::reject::custom)?;
    let news = all_news
        .iter()
        .find(|n| *n.value().id() == id)
        .ok_or(StoreError::MissingRow)?
        .value();
    Ok(render_single_edit_element(news))
}

fn render_news_item(record: &NewsItem) -> Element {
    let id = format!("{}", record.id());
    div([
        div(record.render_pub_date()).class("pub_date"),
        div(anchor(record.summary()).set("href", format!("/news/{id}"))).class("pub_title"),
        div(Empty).inner_html(record.rendered()).class("content"),
    ])
    .class("news-item")
}

pub async fn render_list_all(
    store: ArcStore,
    _session_opt: Option<Session>,
) -> Result<Element, Rejection> {
    let all_news = store.get_all_news().await.map_err(warp::reject::custom)?;
    let news_nodes: Vec<Element> = all_news
        .iter()
        .map(|nisa| nisa.value())
        .filter(|ni| {
            if let Some(dt) = ni.pub_date() {
                *ni.status() == NewsStatus::Published && *dt < Utc::now()
            } else {
                false
            }
        })
        .map(render_news_item)
        .collect();
    Ok(make_document(
        div([h1("News précédentes"), div(news_nodes).class("content")])
            .class("news-list previous-news"),
        "All the news",
        "news",
    ))
}

fn render_single_element(news: &NewsItem) -> Element {
    make_document(
        div([
            div(news.render_pub_date()).class("pub_date"),
            div(news.summary()).class("pub_title"),
            div(Empty).inner_html(news.rendered()).class("content"),
            div([
                anchor(img().set(
                    "src",
                    "https://atelier-cartographique.be/storage/apere/logo-ec.png",
                ))
                .set("href", "https://energiecommune.be")
                .class("logo--link"),
                div(anchor("Liste des news précédentes").class("link-list").set("href", "/news/")),
            ])
            .class("footer"),
        ])
        .class("news-item"),
        &format!("{}", news.summary()),
        "news",
    )
}

pub async fn render_news_single(
    store: ArcStore,
    session_opt: Option<Session>,
    id: i32,
) -> Result<Element, Rejection> {
    let all_news = store.get_all_news().await.map_err(warp::reject::custom)?;
    let news = all_news
        .iter()
        .find(|n| *n.value().id() == id)
        .ok_or(StoreError::MissingRow)?
        .value();
    match (session_opt, news.pub_date(), news.status()) {
        (Some(_), _, _) => Ok(render_single_element(news)),
        (None, Some(dt), NewsStatus::Published) if *dt < Utc::now() => {
            Ok(render_single_element(news))
        }
        _ => Err(warp::reject()),
    }
}
