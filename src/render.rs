use chrono::{DateTime, Datelike, Timelike, Utc};
use image::{DynamicImage, ImageOutputFormat};
use serde::Deserialize;
use std::{convert::TryFrom, f64, path::PathBuf};
use warp::{filters, http, reply::Response, Filter, Rejection, Reply};

use crate::{
    auth::{post_login, render_login},
    forecast::jauges::{jauges_default_reply, jauges_reply},
    forecast::solar::image_legend,
    home::render_home,
    html::*,
    news::render::{
        create_news, render_latest_news_reply, render_list_admin, render_list_all,
        render_news_single, render_single_edit, update_news,
    },
    raster::solar_jauge_png,
    raster::solar_map_png_app,
    raster::solar_map_png_download,
    raster::wind_jauge_png,
    raster::wind_linechart_png,
    render_forecast::forecast_reply,
    render_forecast::ForecastQuery,
    render_production::{
        default_histogram_prod_query, default_piechart_query, default_prod_query, pie_reply,
        production_reply, year_prod_reply, PieChartQuery, ProdQuery,
    },
    session::filter_session,
    session::session_from_token,
    store::{ArcStore, Store},
};

pub const FONT: &str = "nunito";
pub const FONT_REGULAR: &str = "nunito";

#[derive(Debug)]
pub struct CustomError {
    _cause: String,
}

pub fn render_error(err: &str) -> Element {
    div(format!("ERROR: {}", err)).class("error-message")
}
// pub fn cerr(name: &str, err: impl ::std::error::Error) -> CustomError {
//     CustomError {
//         cause: format!("{}: {}", name, err),
//     }
// }

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "lowercase")]
pub enum EnergyType {
    Solar,
    Wind,
    All,
}

impl TryFrom<String> for EnergyType {
    type Error = String;
    fn try_from(value: String) -> Result<Self, Self::Error> {
        match value.as_str() {
            "solar" => Ok(EnergyType::Solar),
            "wind" => Ok(EnergyType::Wind),
            "all" => Ok(EnergyType::All),
            &_ => Err(format!("Expected solar or wind: got {value}")),
        }
    }
}
impl ToString for EnergyType {
    fn to_string(&self) -> String {
        match self {
            EnergyType::Solar => "solar".to_string(),
            EnergyType::Wind => "wind".to_string(),
            EnergyType::All => "all".to_string(),
        }
    }
}

impl warp::reject::Reject for CustomError {}

impl Reply for Element {
    fn into_response(self) -> Response {
        let mut res = Response::new(with_doctype(self).into());
        res.headers_mut().insert(
            http::header::CONTENT_TYPE,
            http::HeaderValue::from_static("text/html"),
        );
        res
    }
}

pub struct MeteoImage(pub DynamicImage);
impl Reply for MeteoImage {
    fn into_response(self) -> Response {
        let mut out: Vec<u8> = Vec::new();
        match self.0.write_to(&mut out, ImageOutputFormat::Png) {
            Ok(_) => {
                let mut res = Response::new(out.into());
                res.headers_mut().insert(
                    http::header::CONTENT_TYPE,
                    http::HeaderValue::from_static("image/png"),
                );
                res
            }
            Err(_) => http::StatusCode::INTERNAL_SERVER_ERROR.into_response(),
        }
    }
}

pub struct ReSVGImage(pub resvg::Image);
impl Reply for ReSVGImage {
    fn into_response(self) -> Response {
        let mut out: Vec<u8> = Vec::new();

        let mut encoder = png::Encoder::new(&mut out, self.0.width(), self.0.height());
        encoder.set_color(png::ColorType::RGBA);
        encoder.set_depth(png::BitDepth::Eight);

        match encoder
            .write_header()
            .and_then(|mut writer| writer.write_image_data(self.0.data()))
        {
            Ok(_) => {
                let mut res = Response::new(out.into());
                res.headers_mut().insert(
                    http::header::CONTENT_TYPE,
                    http::HeaderValue::from_static("image/png"),
                );
                res.headers_mut().insert(
                    http::header::CONTENT_DISPOSITION,
                    http::HeaderValue::from_static("attachment"),
                );
                res
            }
            Err(_) => http::StatusCode::INTERNAL_SERVER_ERROR.into_response(),
        }
    }
}
#[derive(Debug)]
pub struct PastProduction {
    pub wind_off: f64,
    pub wind_on: f64,
    pub solar: f64,
    pub hydro: f64,
    pub biomass: f64,
    pub waste: f64,
    pub total_load: f64,
}
pub struct WindData {
    pub offshore: Vec<(DateTime<Utc>, f64)>,
    pub onshore: Vec<(DateTime<Utc>, f64)>,
    pub monitored_capacity: Option<f64>,
}

pub fn render_item(val: &str, title: &str, subtitle: &str) -> Element {
    div(vec![
        span(val).class("item__value"),
        span(title),
        span(subtitle).class("item__subtitle"),
    ])
    .class("item")
}

// fn render_row(title: &str, items: Vec<Element>) -> Element {
//     div([
//         div(title).class("row__title"),
//         div(items).class("row__content"),
//     ])
//     .class("row")
// }

async fn default_production(store: ArcStore) -> Result<impl Reply, Rejection> {
    Ok(production_reply(store, default_histogram_prod_query(EnergyType::All)).await?)
}
async fn default_production_solar(store: ArcStore) -> Result<impl Reply, Rejection> {
    Ok(production_reply(store, default_prod_query(EnergyType::Solar)).await?)
}
async fn default_production_wind(store: ArcStore) -> Result<impl Reply, Rejection> {
    Ok(production_reply(store, default_prod_query(EnergyType::Wind)).await?)
}
async fn default_piechart(store: ArcStore) -> Result<impl Reply, Rejection> {
    Ok(pie_reply(store, default_piechart_query()).await?)
}
async fn default_year_prod_solar(store: ArcStore) -> Result<impl Reply, Rejection> {
    Ok(year_prod_reply(store, default_histogram_prod_query(EnergyType::Solar)).await?)
}
async fn default_year_prod_wind(store: ArcStore) -> Result<impl Reply, Rejection> {
    Ok(year_prod_reply(store, default_histogram_prod_query(EnergyType::Wind)).await?)
}
async fn default_solar_map(store: ArcStore) -> Result<impl Reply, Rejection> {
    let now = Utc::now();
    Ok(solar_map_png_download(store, now.year(), now.month(), now.day(), now.hour()).await?)
}
async fn default_wind_linechart(store: ArcStore) -> Result<impl Reply, Rejection> {
    let now = Utc::now();
    Ok(wind_linechart_png(store, now.year(), now.month(), now.day(), now.hour()).await?)
}

async fn default_forecast(store: ArcStore) -> Result<impl Reply, Rejection> {
    println!("default_forecast : {:?}h", Utc::now().hour());
    if Utc::now().hour() > 12 {
        Ok(forecast_reply(store.clone(), ForecastQuery::new(1, EnergyType::All)).await?)
    } else {
        Ok(forecast_reply(store, ForecastQuery::new(0, EnergyType::All)).await?)
    }
}

fn inject_context(store: ArcStore) -> filters::BoxedFilter<(ArcStore,)> {
    warp::any().map(move || store.clone()).boxed()
}

fn print_log(info: warp::filters::log::Info) {
    eprintln!(
        "{} {} {} {} {} {}",
        Utc::now().to_rfc3339(),
        info.remote_addr()
            .map(|a| format!("{}", a.ip()))
            .unwrap_or("-".into()),
        info.method(),
        info.path(),
        info.status(),
        info.elapsed().as_millis(),
    );
}

pub async fn serve(dsn: String, address: &str, static_directory: &str) {
    pretty_env_logger::init();
    match Store::new(&dsn).await {
        Err(err) => println!(" [render]:\n{:?} \n\nexiting", err),
        Ok(store) => {
            let arc_store = ArcStore::new(store);
            let base = || warp::any().and(inject_context(arc_store.clone()));
            let logger = warp::log::custom(print_log);

            //TODO
            // let home = base()
            //     .and(warp::path(""))
            //     .and(warp::query::<ProdQuery>)
            //     .and(warp::query::<ForecastQuery>)
            //     .and_then(onepage);

            let default_forecast = base()
                .and(warp::path("forecast"))
                .and_then(default_forecast);

            let forecast_at = base()
                .and(warp::path!("forecast"))
                .and(warp::query::<ForecastQuery>())
                .and_then(forecast_reply);
            let forecast = forecast_at.or(default_forecast);

            let default_jauges = base()
                .and(warp::path!("jauges"))
                // .and(warp::query::<ForecastQuery>())
                .and_then(jauges_default_reply);
            let jauges_with_arg = base()
                .and(warp::path!("jauges"))
                .and(warp::query::<ForecastQuery>())
                .and_then(jauges_reply);
            let jauges = jauges_with_arg.or(default_jauges);

            let default_prod_solar = base()
                .and(warp::path("production_solar"))
                .and_then(default_production_solar);
            let default_prod_wind = base()
                .and(warp::path("production_wind"))
                .and_then(default_production_wind);
            let default_prod = base()
                .and(warp::path("production"))
                .and_then(default_production);
            let prod_at = base()
                .and(warp::path("production"))
                .and(warp::query::<ProdQuery>())
                .and_then(production_reply);
            let production = prod_at.or(default_prod);

            let default_piechart = base()
                .and(warp::path("conso_piechart"))
                .and_then(default_piechart);
            let piechart_at = base()
                .and(warp::path("conso_piechart"))
                .and(warp::query::<PieChartQuery>())
                .and_then(pie_reply);
            let piechart = piechart_at.or(default_piechart);

            let default_year_prod_solar = base()
                .and(warp::path("year_production_solar"))
                .and_then(default_year_prod_solar);
            let default_year_prod_wind = base()
                .and(warp::path("year_production_wind"))
                .and_then(default_year_prod_wind);
            let year_prod = base()
                .and(warp::path("year_production"))
                .and(warp::query::<ProdQuery>())
                .and_then(year_prod_reply);

            let img = base()
                .and(warp::path!("image" / i32 / u32 / u32 / u32))
                .and_then(solar_map_png_app);

            let img_legend = warp::path!("image_legend").and_then(image_legend);

            let default_solar_map_png = base()
                .and(warp::path("solar_map"))
                .and_then(default_solar_map);
            let solar_map_png_at = base()
                .and(warp::path!("solar_map" / i32 / u32 / u32 / u32))
                .and_then(solar_map_png_download);
            let solar_map_png = solar_map_png_at.or(default_solar_map_png);

            let default_wind_linechart_png = base()
                .and(warp::path("wind_linechart"))
                .and_then(default_wind_linechart);
            let wind_linechart_at = base()
                .and(warp::path!("wind_linechart" / i32 / u32 / u32 / u32))
                .and_then(wind_linechart_png);
            let wind_linechart = wind_linechart_at.or(default_wind_linechart_png);

            let wind_jauge = warp::path!("wind_jauge" / u32).and_then(wind_jauge_png);

            let solar_jauge = warp::path!("solar_jauge" / u32).and_then(solar_jauge_png);

            // let widget = base()
            //     .and(warp::path!("widget" / String))
            //     .and(warp::query::<WidgetQuery>())
            //     .and_then(render_widget);

            let news_admin = || {
                filter_session(arc_store.clone()).and(warp::path("news").and(warp::path("admin")))
            };
            let news = || filter_session(arc_store.clone()).and(warp::path("news"));
            let news_admin_list = news_admin()
                .and(warp::path::end())
                .and_then(render_list_admin);
            let news_create = news_admin()
                .and(warp::post())
                .and(warp::path!("create"))
                .and_then(create_news);
            let news_edit = news_admin()
                .and(warp::get())
                .and(warp::path!("change" / i32))
                .and_then(render_single_edit);
            let news_update = news_admin()
                .and(
                    warp::path!("change" / i32)
                        .and(warp::post())
                        .and(warp::body::form()),
                )
                .and_then(update_news);

            let news_list = news().and(warp::path::end()).and_then(render_list_all);
            let news_single = news().and(warp::path!(i32)).and_then(render_news_single);
            let latest_news = base()
                .and(warp::path!("news" / "latest"))
                .and_then(render_latest_news_reply);

            let statics = warp::path("static").and(warp::fs::dir(PathBuf::from(static_directory)));

            let home = warp::path::end().map(render_home);

            let auth = || {
                warp::path("auth")
                    .and(warp::cookie::optional(crate::session::SESSION_COOKIE_NAME))
                    .and(inject_context(arc_store.clone()))
                    .then(session_from_token)
                    .and(inject_context(arc_store.clone()))
            };

            let login_page = auth().and(warp::get()).and_then(render_login);
            let login_submit = warp::path!("auth" / "login")
                .and(warp::post())
                .and(inject_context(arc_store.clone()))
                .and(warp::body::form())
                .and_then(post_login);

            let service = warp::serve(
                home.or(statics)
                    .or(login_page)
                    .or(login_submit)
                    .or(forecast)
                    .or(default_prod_solar)
                    .or(default_prod_wind)
                    .or(production)
                    .or(piechart)
                    .or(default_year_prod_solar)
                    .or(default_year_prod_wind)
                    .or(year_prod)
                    .or(img)
                    .or(img_legend)
                    .or(wind_linechart)
                    .or(solar_map_png)
                    .or(jauges)
                    .or(wind_jauge)
                    .or(solar_jauge)
                    // .or(widget)
                    .or(latest_news)
                    .or(news_admin_list)
                    .or(news_create)
                    .or(news_edit)
                    .or(news_update)
                    .or(news_list)
                    .or(news_single)
                    .with(logger),
            );

            let addr: std::net::SocketAddr = address.parse().expect("not a valid address");
            service.run(addr).await;
        }
    }
}
