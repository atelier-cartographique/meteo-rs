use chrono::{DateTime, Datelike, Timelike, Utc};
use std::f64;
use svg::{
    node::element::path::Data,
    node::element::{Group, Path as SvgPath, Text, Title},
    node::Text as TextNode,
    Document,
};

use super::{
    axis::{month_x_axis, x_axis, y_axis},
    base::{
        get_max_month, GraphType, HistogramValue, Interval, LOGO_HEIGHT, LOGO_WIDTH, OFFSET_X,
        OFFSHORE_COLOR, ONSHORE_COLOR, SOLAR_COLOR, SOLAR_EMPTY_COLOR, TEXT_COLOR, WIND_COLOR,
        WIND_EMPTY_COLOR,
    },
    legend::legend,
    linechart::linechart,
};
use crate::{
    plot::base::{first_year, logo_apere},
    render::{EnergyType, WindData},
    util::{format_nb, usize_to_f64},
};

// #[derive(Clone, Debug)]
// struct LegendItem {
//     _name: String,
//     _color: String,
// }

fn nb_days_between(start: &DateTime<Utc>, end: &DateTime<Utc>) -> f64 {
    ((DateTime::timestamp(end) - DateTime::timestamp(start)) as f64 / (3600. * 24.)).ceil()
}

pub fn wind_histogram(width: f64, height: f64, data: &WindData) -> Document {
    if !data.onshore.is_empty() && !data.offshore.is_empty() {
        //graph dimensions
        let x_width = width - OFFSET_X;
        let y_height = height - 50.;
        // sum of off_data and on_data, term by term
        let sum_data: Vec<(DateTime<Utc>, f64)> = data
            .offshore
            .iter()
            .zip(&data.onshore)
            .map(|((dt1, a), (_dt2, b))| (*dt1, a + b))
            .collect();
        let sum_vec_interval = vec_by_interval(&sum_data, Interval::Day);
        let onshore_vec_interval = vec_by_interval(&data.onshore, Interval::Day);
        let offshore_vec_interval = vec_by_interval(&data.offshore, Interval::Day);
        let offshore_list = sum_vec_interval
            .iter()
            .zip(&onshore_vec_interval)
            .zip(&offshore_vec_interval)
            .map(|(((dt1, a), (_dt2, b)), (_dt3, c))| {
                (
                    *dt1,
                    *a,
                    format!(
                        "Total: {} GWh \n Onshore: {} GWh \n Offshore: {} GWh",
                        a.round() / 1000.,
                        b.round() / 1000.,
                        c.round() / 1000.,
                    ),
                )
            })
            .collect::<Vec<_>>();
        let onshore_list = sum_vec_interval
            .iter()
            .zip(&onshore_vec_interval)
            .zip(&offshore_vec_interval)
            .map(|(((dt1, a), (_dt2, b)), (_dt3, c))| {
                (
                    *dt1,
                    *b,
                    format!(
                        "Total: {} GWh \n Onshore: {} GWh \n Offshore: {} GWh",
                        a.round() / 1000.,
                        b.round() / 1000.,
                        c.round() / 1000.,
                    ),
                )
            })
            .collect::<Vec<_>>();
        let start = sum_vec_interval[0].0;
        let end = sum_vec_interval[sum_vec_interval.len() - 1].0;
        let nb_days = nb_days_between(&start, &end) + 1.; //"+1" to get place at begin and end histogram

        // let y_max = get_max_y(&sum_vec_interval);
        let y_max = 150000.;
        // let y_factor = subdivision(y_max);
        let y_factor = 25000.;
        let x_unit = x_width / nb_days; // nb px for 1 day
        let y_unit = y_height / y_max; // nb of px for 1 MWh
        let y_nb_unit = std::cmp::max(1, (y_height / (y_unit * y_factor).round()) as usize) + 1; // nb intervals for 1000 units
        let (x_axis, x_labels) = x_axis(
            sum_data.to_vec()[0].0,
            x_width,
            nb_days,
            Interval::Day,
            GraphType::Histogram,
        );
        // let y_nb_unit = 30;
        // let y_factor = y_height / (y_unit * y_nb_unit as f64);
        let (y_axis, y_labels) = y_axis(y_height, y_unit, y_nb_unit, y_factor, "GWh");
        let items = vec![
            ("Offshore".to_string(), OFFSHORE_COLOR),
            ("Onshore".to_string(), ONSHORE_COLOR),
        ];
        let offshore_paths = histogram(x_unit, y_unit, &offshore_list, OFFSHORE_COLOR);
        let onshore_paths = histogram(x_unit, y_unit, &onshore_list, ONSHORE_COLOR);
        Document::new()
            .set("viewBox", (-40., -height + 25., width, height + 10.))
            .add(offshore_paths)
            .add(onshore_paths)
            .add(y_axis)
            .add(y_labels)
            .add(x_axis)
            .add(x_labels)
            .add(logo_apere(
                x_width - LOGO_WIDTH,
                -y_height - LOGO_HEIGHT / 2.,
            ))
            .add(legend(items, y_height))
    // svg::save("medias/line-chart.svg", &document).unwrap();
    } else {
        let err_text = Text::new()
            .add(TextNode::new("Données éoliennes manquantes"))
            .set("x", width / 2. - 30.)
            .set("y", -height / 2. + 25.)
            .set("text-anchor", "end")
            .set("fill", TEXT_COLOR);
        Document::new()
            .set("viewBox", (-40., -height + 15., width, height))
            .add(err_text)
    }
}

pub fn solar_histogram(width: f64, height: f64, data: &Vec<(DateTime<Utc>, f64)>) -> Document {
    if !data.is_empty() {
        //graph dimensions
        let x_width = width - OFFSET_X;
        let y_height = height - 50.;

        let vec_interval = vec_by_interval(data, Interval::Day);
        let vec_histogram = vec_interval
            .iter()
            .map(|(dt, a)| (*dt, *a, format!("{} GWh", a.round() / 1000.)))
            .collect::<Vec<_>>();

        let nb_days = nb_days_between(
            &vec_histogram[0].0,
            &vec_histogram[vec_histogram.len() - 1].0,
        ) + 1.; //"+1" to get place at begin and end histogram
                // let x_max = nb_days_in_month_of(data[0].0);
                // let y_max = get_max_y(&vec_interval);
        let y_max = 52000.;
        // let y_factor = subdivision(y_max);
        let y_factor = 5000.;
        let x_unit = x_width / (nb_days as f64); // nb px for 1 time unit
        let y_unit = y_height / y_max; // nb of px for 1 unit of production
        let y_nb_unit = std::cmp::max(1, (y_height / (y_unit * y_factor).round()) as usize) + 1; // nb intervals for 1000 units
        let (x_axis, x_labels) = x_axis(
            vec_histogram[0].0,
            x_width,
            nb_days as f64,
            Interval::Day,
            GraphType::Histogram,
        );
        let (y_axis, y_labels) = y_axis(y_height, y_unit, y_nb_unit, y_factor, "GWh");

        let solar_paths = histogram(x_unit, y_unit, &vec_histogram, SOLAR_COLOR);
        Document::new()
            .set("viewBox", (-40., -height + 25., width, height + 10.))
            .add(solar_paths)
            .add(y_axis)
            .add(y_labels)
            .add(x_axis)
            .add(x_labels)
            .add(logo_apere(
                x_width - LOGO_WIDTH,
                -y_height - LOGO_HEIGHT / 2.,
            ))
    // svg::save("medias/solar-line-chart.svg", &document).unwrap();
    } else {
        let err_text = Text::new()
            .add(TextNode::new("Données solaires manquantes"))
            .set("x", width / 2. - 30.)
            .set("y", -height / 2. + 25.)
            .set("text-anchor", "end")
            .set("fill", TEXT_COLOR);
        Document::new()
            .set("viewBox", (-40., -height + 15., width, height))
            .add(err_text)
    }
}

// fn nb_days_in_month_of(d: DateTime<Utc>) -> u32 {
//     let month = d.date().month();
//     if month == 2 {
//         return 29; // TO DO!!
//     } else if month == 4 || month == 6 || month == 9 || month == 11 {
//         return 30;
//     } else {
//         return 31;
//     }
// }
// fn avg(vec: &Vec<(DateTime<Utc>, f64)>) -> (DateTime<Utc>, f64) {
//     let mut avg = 0.;
//     for v in vec {
//         avg = avg + v.1;
//     }
//     (vec[0].0, avg / (vec.len() as f64))
// }

struct IntervalIterator<T>
where
    T: Clone,
{
    idx: usize,
    step: usize,
    source: Vec<T>,
}
// impl<T> IntervalIterator<T>
// where
//     T: Clone,
// {
//     pub fn new(step: usize, source: Vec<T>) -> IntervalIterator<T> {
//         IntervalIterator {
//             idx: 0,
//             step,
//             source,
//         }
//     }
// }

impl<T> Iterator for IntervalIterator<T>
where
    T: Clone,
{
    type Item = Vec<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.idx + self.step >= self.source.len() {
            return None;
        }
        let sub_it = self.source.iter().skip(self.idx);
        let sub_vec: Self::Item = sub_it.take(self.step).cloned().collect();
        self.idx += self.step;
        Some(sub_vec)
    }
}

// impl<T> crate::cal::CalendarT<T> for &Vec<(DateTime<Utc>, f64)>
// where
//     T: Clone,
// {
//     fn find(
//         &self,
//         start: DateTime<Local>,
//         end: DateTime<Local>,
//     ) -> Vec<crate::cal::CalendarEvent<T>> {
//         self.clone()
//             .into_iter()
//             .filter(|(dt, val)| {
//                 (start < e.start_time && e.start_time < end)
//                     || (start < e.end_time && e.end_time < end)
//                     || (start < e.start_time && e.end_time < end)
//             })
//             .collect()
//     }
// }

//If interval is Day, gives the day number, if Month the month number, etc.
fn interval_date_value(date: DateTime<Utc>, interval: Interval) -> i32 {
    match interval {
        Interval::Hour => date.time().hour() as i32,
        Interval::Day => date.date().day() as i32,
        // Interval::Month => date.date().month() as i32,
        // Interval::Year => date.date().year(),
    }
}

//sum values of each interval (each day for example) and returns a new vector with this sums, associated with the first day of the interval.
fn vec_by_interval(
    vec: &Vec<(DateTime<Utc>, f64)>,
    interval: Interval,
) -> Vec<(DateTime<Utc>, f64)> {
    let mut i = 1;
    let mut new_vec = Vec::new();
    while i < vec.len() + 1 {
        let actual_date_value = interval_date_value(vec[i - 1].0, interval);
        let date = vec[i - 1].0;
        let mut sum = vec[i - 1].1;
        while i < vec.len() && actual_date_value == interval_date_value(vec[i].0, interval) {
            sum += vec[i].1;
            i += 1;
        }
        // println!("INTERVAL VALUE: {}", sum / 4.);
        new_vec.push((date, sum / 4.));
        i += 1;
    }
    new_vec
}

// fn x_by_interval(v: &(DateTime<Utc>, f64, String), interval: Interval) -> f64 {
//     match interval {
//         Interval::Hour => {
//             v.0.time().hour() as f64
//             // + v.0.time().minute() as f64 / 60.,
//         }
//         Interval::Day => {
//             v.0.date().day() as f64
//             // + (v.0.time().hour() as f64 + v.0.time().minute() as f64 / 60.) / 24.
//         }
//     }
// }
// fn max_line(x_max: f64, height: f64, color: &str) -> SvgPath {
//     let data = Data::new().move_to((0., height)).line_to((x_max, height));
//     SvgPath::new()
//         .set("transform", "scale(1,-1)")
//         .set("stroke", format!("{}", color))
//         .set("fill", "red")
//         .set("stroke-width", 1)
//         .set("d", data)
// }

pub fn histogram(
    x_unit: f64,
    y_unit: f64,
    values: &Vec<(DateTime<Utc>, f64, String)>,
    color: &str,
) -> Group {
    // let new_values = vec_by_interval(values, interval);

    let stick_width = x_unit / 1.5;

    let mut paths = Group::new();
    // for v in values.iter() {
    for i in 0..values.len() {
        let x = x_unit / 2. + x_unit * i as f64 - stick_width / 2.;
        // let x = x_unit * x_by_interval(&values[i], interval) - stick_width / 2.;
        let y = y_unit * values[i].1;
        let data = Data::new()
            .move_to((x, 0.))
            .line_to((x, y))
            .line_to((x + stick_width, y))
            .line_to((x + stick_width, 0.));
        let title = Title::new().add(TextNode::new(values[i].clone().2));
        paths = paths.clone().add(
            SvgPath::new()
                .set("transform", "scale(1,-1)")
                .set("fill", color.to_string())
                .set("stroke", color.to_string())
                .set("stroke-width", 0.6)
                .set("d", data)
                .add(title),
        )
    }
    paths
}

fn histogram_values(all_data: &Vec<(i32, [f64; 12])>, year: i32) -> Option<[HistogramValue; 12]> {
    let month_data = all_data.iter().find(|el| el.0 == year).map(|el| el.1);
    month_data.map(|data| {
        data.map(|val| HistogramValue {
            value: val,
            title: format!("{} kWh/kWc", format_nb(val)),
        })
    })
}

pub fn month_histogram(
    width: f64,
    height: f64,
    data: &Vec<(i32, [f64; 12])>,
    energy_type: &EnergyType,
    year: i32,
) -> Document {
    if !data.is_empty() {
        // Selected year values
        let comp_hist_values = histogram_values(data, year);

        // Current year values
        let current_year = Utc::now().year();
        let curr_hist_values = histogram_values(data, current_year);

        const EMPTY_STRING: String = String::new();
        let mut minimums = vec![(EMPTY_STRING, f64::MAX); 12]; // list of tuples (year, value), one tuple per month
        let mut maximums = vec![(EMPTY_STRING, -1.); 12]; // list of tuples (year, value)
        let mut avg = vec![(EMPTY_STRING, 0.); 12];
        let nb_of_years = usize_to_f64(data.len());

        for i in 0..12 {
            let mut nb_non_zero_values = nb_of_years;
            // for each month, chech which year has the min, which the max and which the avg.
            for d in data {
                if d.1[i] > 0. && d.1[i] < minimums[i].1 {
                    // let min_label = format!("min en {}", d.0);
                    minimums[i] = (format!("min ({})", d.0), d.1[i]);
                }
                if d.1[i] > maximums[i].1 {
                    // let max_label = format!("max en {}", d.0);
                    maximums[i] = (format!("max ({})", d.0), d.1[i]);
                }
                let tmp = avg[i].1;
                if d.1[i] > 0. {
                    avg[i] = (
                        format!("moy entre {} et {}", first_year(energy_type), current_year),
                        d.1[i] / nb_of_years + tmp,
                    );
                } else {
                    // if a value is missing (= is zero), the avg has to be ajusted to not consider it
                    let new_nb_val = nb_non_zero_values - 1.;
                    avg[i] = (
                        format!("moy entre {} et {}", first_year(energy_type), current_year),
                        tmp * nb_non_zero_values / new_nb_val,
                    );
                    nb_non_zero_values = new_nb_val;
                }
            }
        }

        //graph dimensions
        let x_width = width - OFFSET_X;
        let y_height = height - 50.;

        let y_max = get_max_month(&maximums);
        // let y_factor = subdivision(y_max);
        let y_factor = match energy_type {
            EnergyType::Solar => 50.,
            EnergyType::Wind => 50.,
            EnergyType::All => 500., // no use of this last
        };

        let x_unit = x_width / 12.; // nb px for 1 time unit
        let y_unit = y_height / y_max; // nb of px for 1 unit of production
        let y_nb_unit = std::cmp::max(1, (y_height / (y_unit * y_factor).round()) as usize) + 1;
        let (x_axis, x_labels) = month_x_axis(x_width);
        let (y_axis, y_labels) = y_axis(y_height, y_unit, y_nb_unit, y_factor, "kWh/kWc");

        let rectangles = curr_hist_values
            .and_then(|current| {
                comp_hist_values.map(|compared| {
                    multiple_histogram(x_unit, y_unit, &vec![compared, current], energy_type)
                })
            })
            .unwrap_or(Group::new());

        let min_color = get_color(&EnergyType::All, 3);
        let max_color = get_color(&EnergyType::All, 4);
        let avg_color = get_color(&EnergyType::All, 5);

        let min_points = linechart(x_unit, y_unit, &minimums, min_color);
        let max_points = linechart(x_unit, y_unit, &maximums, max_color);
        let avg_points = linechart(x_unit, y_unit, &avg, avg_color);

        let items = vec![
            (format!("{}", year), get_color(energy_type, 0)),
            (format!("{}", current_year), get_color(energy_type, 1)),
            (
                format!("min (de {} à {})", first_year(energy_type), current_year),
                min_color,
            ),
            (
                format!("max (de {} à {})", first_year(energy_type), current_year),
                max_color,
            ),
            (
                format!(
                    "moyenne (de {} à {})",
                    first_year(energy_type),
                    current_year
                ),
                avg_color,
            ),
            // (format!("min (2020-{})", year), min_color),
            // (format!("max (2020-{})", year), max_color),
            // (format!("moyenne (2020-{})", year), avg_color),
        ];
        Document::new()
            .set("viewBox", (-40., -height + 30., width, height + 10.))
            .add(rectangles)
            .add(min_points)
            .add(max_points)
            .add(avg_points)
            .add(y_axis)
            .add(y_labels)
            .add(x_axis)
            .add(x_labels)
            .add(logo_apere(
                x_width - LOGO_WIDTH,
                -y_height - LOGO_HEIGHT / 2.,
            ))
            .add(legend(items, y_height))
    // svg::save("medias/solar-line-chart.svg", &document).unwrap();
    } else {
        let err_text = Text::new()
            .add(TextNode::new("Données manquantes"))
            .set("x", width / 2. - 30.)
            .set("y", -height / 2. + 25.)
            .set("text-anchor", "end")
            .set("fill", TEXT_COLOR);
        Document::new()
            .set("viewBox", (-40., -height + 15., width, height))
            .add(err_text)
    }
}

fn get_color(energy_type: &EnergyType, i: usize) -> &'static str {
    let colors = match energy_type {
        EnergyType::All => vec![
            "#2ba6ff", "#335496", "#42dd73", "#ce3943", "#df37c3", "#ff9c3a", "#373e46", "#c8e6cf",
            "#191760", "#b3e5b5",
        ],
        EnergyType::Solar => vec![SOLAR_COLOR, SOLAR_EMPTY_COLOR],
        EnergyType::Wind => vec![WIND_COLOR, WIND_EMPTY_COLOR],
    };
    let color_index = i % colors.len();
    colors[color_index]
}

pub fn multiple_histogram(
    x_unit: f64,
    y_unit: f64,
    series: &Vec<[HistogramValue; 12]>,
    energy_type: &EnergyType,
) -> Group {
    let nb_series = usize_to_f64(series.len());
    let stick_width = x_unit / (1.5 * nb_series);

    series
        .iter()
        .enumerate()
        .fold(Group::new(), |mut acc, (i_ser, values)| {
            values
                .iter()
                .enumerate() // starts at 0
                .map(
                    |(
                        i,
                        HistogramValue {
                            // label,
                            value,
                            title,
                        },
                    )| {
                        let x_offset = stick_width * (-nb_series + 2. * usize_to_f64(i_ser)) / 2.;
                        let x = x_unit / 2. + x_unit * usize_to_f64(i) + x_offset;
                        let y = y_unit * value;
                        let data = Data::new()
                            .move_to((x, 0.))
                            .line_to((x, y))
                            .line_to((x + stick_width, y))
                            .line_to((x + stick_width, 0.));
                        let title = Title::new().add(TextNode::new(title));

                        acc = acc.clone().add(
                            SvgPath::new()
                                .set("transform", "scale(1,-1)")
                                .set("fill", get_color(energy_type, i_ser))
                                .set("stroke", get_color(energy_type, i_ser))
                                .set("stroke-width", 0.6)
                                .set("d", data)
                                .add(title),
                        )
                    },
                )
                .for_each(drop);
            acc
        })
}

// pub fn solar_year_histogram(width: f64, height: f64, year: u32) -> Document {
//     let data1 = solar_prod()
//     if !data.is_empty() {
//         //graph dimensions
//         let x_width = width - OFFSET_X;
//         let y_height = height - 50.;

//         let vec_interval = vec_by_interval(data, Interval::Day);
//         let vec_histogram = vec_interval
//             .iter()
//             .map(|(dt, a)| (*dt, *a, format!("{} GWh", a.round() / 1000.)))
//             .collect::<Vec<_>>();

//         let nb_days = nb_days_between(
//             &vec_histogram[0].0,
//             &vec_histogram[vec_histogram.len() - 1].0,
//         ) + 1.; //"+1" to get place at begin and end histogram
//                 // let x_max = nb_days_in_month_of(data[0].0);
//         let y_max = get_max_y(&vec_interval);
//         let y_factor = subdivision(y_max);
//         let x_unit = x_width / (nb_days as f64); // nb px for 1 time unit
//         let y_unit = y_height / y_max; // nb of px for 1 unit of production
//         let y_nb_unit = std::cmp::max(1, (y_height / (y_unit * y_factor).round()) as usize) + 1; // nb intervals for 1000 units
//         let (x_axis, x_labels) = x_axis(
//             vec_histogram[0].0,
//             x_width,
//             nb_days as f64,
//             Interval::Day,
//             GraphType::Histogram,
//         );
//         let (y_axis, y_labels) = y_axis(y_height, y_unit, y_nb_unit, y_factor);

//         let solar_paths = histogram(x_unit, y_unit, &vec_histogram, SOLAR_COLOR);
//         Document::new()
//             .set("viewBox", (-40., -height + 25., width, height + 10.))
//             .add(solar_paths)
//             .add(y_axis)
//             .add(y_labels)
//             .add(x_axis)
//             .add(x_labels)
//     // svg::save("medias/solar-line-chart.svg", &document).unwrap();
//     } else {
//         let err_text = Text::new()
//             .add(TextNode::new("Données solaires manquantes"))
//             .set("x", width / 2. - 30.)
//             .set("y", -height / 2. + 25.)
//             .set("text-anchor", "end")
//             .set("fill", TEXT_COLOR);
//         Document::new()
//             .set("viewBox", (-40., -height + 15., width, height))
//             .add(err_text)
//     }
// }
