use chrono::{DateTime, Utc};
use std::f64;
use svg::{
    node::element::{path::Data, Group, Path as SvgPath},
    node::{element::Title, Text as TextNode},
    Document,
};

use crate::{
    plot::{
        axis::{x_axis, y_axis},
        base::{logo_apere, GraphType, Interval, OFFSHORE_COLOR, ONSHORE_COLOR},
        legend::legend,
        smooth_curve::{points_to_curve, CurvedPoint},
    },
    util::{format_nb, usize_to_f64},
};

use super::base::{LOGO_HEIGHT, LOGO_WIDTH};

fn polyline(
    x_unit: f64,
    y_unit: f64,
    values: &Vec<(DateTime<Utc>, f64)>,
    _nb_in_unit: f64, // how much data in 1 unit
    color: &str,
) -> SvgPath {
    let x_unit_seconds = x_unit / 3600.;
    if values.len() > 2 {
        let ref_timestamp = values.to_vec()[0].0.timestamp(); //"zéro date" in seconds
        let points: Vec<(f64, f64)> = values
            .iter()
            .map(|(t, y)| {
                (
                    x_unit_seconds * (t.timestamp() - ref_timestamp) as f64,
                    y_unit * y,
                )
            })
            .collect();
        let mut maxx = 0.0;
        let line_data = points_to_curve(&points)
            .iter()
            .fold(
                Data::new(),
                |line_data,
                 CurvedPoint {
                     x,
                     y,
                     control1,
                     control2,
                     first,
                 }| {
                    maxx = *x;
                    if *first {
                        line_data.move_to((*x, *y))
                    } else {
                        line_data.cubic_curve_to((
                            control1.x, control1.y, control2.x, control2.y, *x, *y,
                        ))
                    }
                },
            )
            .line_to((maxx, 0.))
            .line_to((0., 0.))
            .close();
        SvgPath::new()
            .set("transform", "scale(1,-1)")
            .set("fill", color.to_string())
            .set("stroke", color.to_string())
            .set("stroke-width", 0.6)
            .set("d", line_data)
    } else {
        SvgPath::new()
    }
}

pub fn linechart(x_unit: f64, y_unit: f64, values: &Vec<(String, f64)>, color: &str) -> Group {
    let mut previous_data = Data::new().move_to((0., 0.));
    values
        .iter()
        .enumerate()
        .fold(Group::new(), |acc, (i, val)| {
            let x = x_unit * usize_to_f64(i);
            let y = y_unit * val.1;
            let data = previous_data
                .clone()
                .line_to((x, y))
                .line_to((x + x_unit, y));
            previous_data = previous_data.clone().move_to((x + x_unit, y));
            let title = Title::new().add(TextNode::new(format!(
                "{}: {} GWh",
                val.0,
                format_nb((val.1) / 1000.)
            )));

            acc.add(
                SvgPath::new()
                    .set("transform", "scale(1,-1)")
                    .set("stroke", color)
                    .set("stroke-width", 0.8)
                    .set("fill-opacity", 0)
                    .set("d", data)
                    .add(title),
            )
        })
}

pub fn wind_linechart(
    width: f64,
    height: f64,
    off_data: &Vec<(DateTime<Utc>, f64)>,
    on_data: &Vec<(DateTime<Utc>, f64)>,
) -> Document {
    //graph dimensions
    let x_width = width - 60.;
    let y_height = height - 50.;

    if !off_data.is_empty() && !on_data.is_empty() {
        // sum of off_data and on_data, term by term
        let sum_data: Vec<(DateTime<Utc>, f64)> = off_data
            .iter()
            .zip(on_data)
            .map(|((dt1, a), (_dt2, b))| (*dt1, a + b))
            .collect();
        let x_max = sum_data.len() as f64 / 4.;
        // let y_max = get_max_y(&sum_data);
        // let y_factor = subdivision(y_max);
        let y_max = 5000.;
        let y_factor = 500.;
        let x_unit = x_width / x_max; // nb px for 1 time unit
        let nb_in_unit_x = 4.;
        let y_unit = y_height / y_max; // nb of px for 1 unit
        let y_nb_unit = (y_height / (y_unit * y_factor).round()) as usize + 1; // nb intervals for y_factor units
        let (x_axis, x_labels) = x_axis(
            sum_data.to_vec()[0].0,
            x_width,
            x_max.round(),
            Interval::Hour,
            GraphType::Linechart,
        );
        let (y_axis, y_labels) = y_axis(y_height, y_unit, y_nb_unit, y_factor, "GWh");
        let items = vec![
            ("Offshore".to_string(), OFFSHORE_COLOR),
            ("Onshore".to_string(), ONSHORE_COLOR),
        ];

        // svg::save("medias/line-chart.svg", &document).unwrap();
        Document::new()
            .set("xmlns:xlink", "http://www.w3.org/1999/xlink")
            .set("viewBox", (-40., -height + 10., width, height + 30.))
            // .set("transform", "scale(1,-1)")
            .add(polyline(
                x_unit,
                y_unit,
                &sum_data,
                nb_in_unit_x,
                OFFSHORE_COLOR,
            ))
            .add(polyline(
                x_unit,
                y_unit,
                on_data,
                nb_in_unit_x,
                ONSHORE_COLOR,
            ))
            .add(y_axis)
            .add(y_labels)
            .add(x_axis)
            .add(x_labels)
            .add(logo_apere(
                x_width - LOGO_WIDTH - 5.,
                -y_height - LOGO_HEIGHT / 2.,
            ))
            .add(legend(items, y_height))
    } else {
        Document::new()
    }
}
