use crate::{
    plot::base::{TEXT_COLOR, TEXT_SIZE},
    util::{usize_to_f64},
};
use std::f64;
use svg::node::element::path::Data;
use svg::node::element::{Group, Path as SvgPath, Text};
use svg::node::Text as TextNode;

//TO DO: make a struct "(label, color)" for the legend
pub fn legend(items: Vec<(String, &str)>, height: f64) -> Group {
    //items contains labels associated with colors
    // let x = width + 30.;
    // let y = height / 2.;
    let x = 0.;
    let y = -35.;
    let s = height / 50.; //square side
    let mut dist = 0.;
    let mut group = Group::new();
    for i in items {
        let square = Data::new()
            .move_to((x + dist, y))
            .line_to((x + dist, y + s))
            .line_to((x + s + dist, y + s))
            .line_to((x + s + dist, y))
            .close();
        let path = SvgPath::new()
            .set("transform", "scale(1, -1)")
            .set("fill", i.1)
            .set("d", square);
        let label = Text::new()
            .add(TextNode::new(i.0.to_string()))
            .set("x", x + 10. + dist)
            .set("y", -y)
            .set("font-size", TEXT_SIZE)
            .set("fill", TEXT_COLOR);
        let text_length = i.0.len();
        group = group.clone().add(path).add(label);
        dist += s + 50. + usize_to_f64(text_length)*3.5;
    }
    group
}
