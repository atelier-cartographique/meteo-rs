// This code is a translation of
// Oleg V. Polikarpotchkin
// https://ovpwp.wordpress.com/2008/12/17/how-to-draw-a-smooth-curve-through-a-set-of-2d-points-with-bezier-methods/

#[derive(Clone)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

fn point(x: f64, y: f64) -> Point {
    Point { x, y }
}

type Control = (Vec<Point>, Vec<Point>);

fn get_first_control_points(rhs: &Vec<f64>) -> Vec<f64> {
    let n = rhs.len();
    let mut x: Vec<f64> = vec![0.0; n]; // Solution vector.
    let mut tmp: Vec<f64> = vec![0.0; n]; // Temp workspace.
    let mut b: f64 = 2.0;
    x[0] = rhs[0] / b;
    for i in 1..n {
        tmp[i] = 1.0 / b;
        let v = if i < n - 1 { 4.0 } else { 2.0 };
        b = v - tmp[i];
        x[i] = (rhs[i] - x[i - 1]) / b;
    }
    for i in 1..n {
        x[n - i - 1] -= tmp[n - i] * x[n - i]; // Backsubstitution.
    }
    x
}

pub fn get_curve_control_points(knots: Vec<Point>) -> Control {
    let n = knots.len() - 1;
    if n < 1 {
        return (vec![], vec![]);
    }

    // Calculate first Bezier control points
    // Right hand side vector
    let mut rhs: Vec<f64> = vec![0.0; n];

    // Set right hand side X values
    for i in 1..n - 1 {
        rhs[i] = 4.0 * knots[i].x + 2.0 * knots[i + 1].x;
    }
    rhs[0] = knots[0].x + 2.0 * knots[1].x;
    rhs[n - 1] = 3.0 * knots[n - 1].x;
    // Get first control points X-values
    let x = get_first_control_points(&rhs);

    // Set right hand side Y values
    for i in 1..n - 1 {
        rhs[i] = 4.0 * knots[i].y + 2.0 * knots[i + 1].y;
    }
    rhs[0] = knots[0].y + 2.0 * knots[1].y;
    rhs[n - 1] = 3.0 * knots[n - 1].y;
    // Get first control points Y-values
    let y = get_first_control_points(&rhs);

    // Fill output arrays.
    let mut first: Vec<Point> = Vec::with_capacity(n);
    let mut second: Vec<Point> = Vec::with_capacity(n);
    for i in 0..n {
        // First control point
        first.push(point(x[i], y[i]));
        // Second control point
        if i < n - 1 {
            second.push(point(
                2.0 * knots[i + 1].x - x[i + 1],
                2.0 * knots[i + 1].y - y[i + 1],
            ));
        } else {
            second.push(point(
                (knots[n].x + x[n - 1]) / 2.0,
                (knots[n].y + y[n - 1]) / 2.0,
            ));
        }
    }

    (first, second)
}

pub struct CurvedPoint {
    pub x: f64,
    pub y: f64,
    pub control1: Point,
    pub control2: Point,
    pub first: bool,
}

pub fn points_to_curve(points: &Vec<(f64, f64)>) -> Vec<CurvedPoint> {
    let (first, second) =
        get_curve_control_points(points.iter().map(|(x, y)| point(*x, *y)).collect());
    let null_point = point(0.0, 0.0);
    points
        .iter()
        .enumerate()
        .map(|(i, (x, y))| CurvedPoint {
            x: *x,
            y: *y,
            control1: if i > 0 {
                first[i - 1].clone()
            } else {
                null_point.clone()
            },
            control2: if i > 0 {
                second[i - 1].clone()
            } else {
                null_point.clone()
            },
            first: i == 0,
        })
        .collect()
}
