use chrono::{DateTime, Utc};
use std::f64;
use svg::node::element::path::Data;
use svg::node::element::{Group, Path as SvgPath, Text};
use svg::node::Text as TextNode;

use super::base::{increment_date, GraphType, Interval, GRAPH_COLOR, TEXT_COLOR, TEXT_SIZE};

pub fn x_axis(
    start: DateTime<Utc>,
    width: f64,
    nb_unit: f64,
    interval: Interval,
    graph_type: GraphType,
) -> (SvgPath, Group) {
    let unit = width / nb_unit;
    let mut dist = match graph_type {
        GraphType::Histogram => unit / 2.,
        GraphType::Linechart => 0.,
    };
    let max_x = width + 10.;
    let labels_font_size = TEXT_SIZE;

    //line and arrow
    let mut data = Data::new()
        .move_to((0., 0.))
        .line_to((max_x, 0.))
        .move_to((max_x - 3., 2.))
        .line_to((max_x, 0.))
        .move_to((max_x - 3., -2.))
        .line_to((max_x, 0.));

    //labels
    let mut labels = Group::new().set("id", "x_axis");
    let mut label_date = start;
    // let div = (nb_unit / 15.).ceil() as usize;

    for i in 0..nb_unit as usize {
        data = data.clone().move_to((dist, 0.)).line_to((dist, -2.));
        match interval {
            Interval::Day => {
                let label_dist = dist;
                if (label_dist) <= width {
                    data = data
                        .clone()
                        .move_to((label_dist, 0.))
                        .line_to((label_dist, -4.));
                    // if i % div == 0 {
                    let label = Text::new()
                        .add(TextNode::new(format!("{}", label_date.format("%d"))))
                        .set("x", label_dist)
                        .set("y", 15)
                        .set("text-anchor", "middle")
                        .set("font-size", labels_font_size)
                        .set("fill", TEXT_COLOR);
                    labels = labels.clone().add(label);
                    // }
                    label_date = increment_date(label_date, interval);
                }
            }
            Interval::Hour => {
                if nb_unit <= 24. {
                    let label = Text::new()
                        .add(TextNode::new(format!("{}", i)))
                        .set("x", dist)
                        .set("y", 15)
                        .set("text-anchor", "middle")
                        .set("font-size", labels_font_size)
                        .set("fill", TEXT_COLOR);
                    labels = labels.clone().add(label);
                } else {
                    let div = 12;
                    let label_dist = dist * div as f64;
                    if (label_dist) <= width {
                        data = data
                            .clone()
                            .move_to((label_dist, 0.))
                            .line_to((label_dist, -4.));
                        if ((i * div) % 24) == 0 {
                            let label = Text::new()
                                .add(TextNode::new(format!("{}", label_date.format("%d/%m"))))
                                .set("x", label_dist)
                                .set("y", 15)
                                .set("text-anchor", "middle")
                                .set("font-size", labels_font_size)
                                .set("fill", TEXT_COLOR);
                            labels = labels.clone().add(label);
                            label_date = increment_date(label_date, Interval::Day);
                        }
                    }
                }
            }
        }
        dist += unit;
    }
    // let axis_name = match interval {
    //     Interval::Hour => "heures",
    //     Interval::Day => "jours",
    // };
    // let name = Text::new()
    //     .add(TextNode::new(format!("{}", axis_name)))
    //     .set("x", dist + 15.)
    //     .set("y", 3)
    //     .set("font-size", "1em")
    //     .set("text-anchor", "start")
    //     .set("fill", TEXT_COLOR);
    // labels = labels.clone().add(name);

    (
        SvgPath::new()
            .set("stroke", GRAPH_COLOR)
            .set("stroke-width", 0.6)
            .set("transform", "scale(1,-1)")
            .set("fill", "none")
            .set("d", data),
        labels,
    )
}

pub fn month_x_axis(width: f64) -> (SvgPath, Group) {
    let unit = width / 12.;
    let mut dist = unit / 2.;
    let max_x = width + 10.;
    let labels_font_size = TEXT_SIZE;

    //line and arrow
    let mut data = Data::new()
        .move_to((0., 0.))
        .line_to((max_x, 0.))
        .move_to((max_x - 3., 2.))
        .line_to((max_x, 0.))
        .move_to((max_x - 3., -2.))
        .line_to((max_x, 0.));

    //labels
    let mut labels = Group::new().set("id", "x_axis");

    for i in 1..13 as usize {
        data = data.clone().move_to((dist, 0.)).line_to((dist, -2.));

        let label = Text::new()
            .add(TextNode::new(format!("{}", i)))
            .set("x", dist)
            .set("y", 15)
            .set("text-anchor", "middle")
            .set("font-size", labels_font_size)
            .set("fill", TEXT_COLOR);
        labels = labels.clone().add(label);
        dist += unit;
    }
    (
        SvgPath::new()
            .set("stroke", GRAPH_COLOR)
            .set("stroke-width", 0.6)
            .set("transform", "scale(1,-1)")
            .set("fill", "none")
            .set("d", data),
        labels,
    )
}

pub fn y_axis(
    height: f64,
    unit: f64,
    nb_unit: usize,
    factor: f64,
    axis_name: &str,
) -> (SvgPath, Group) {
    let mut dist = 0.;

    //line and arrow
    let mut data = Data::new()
        .move_to((0., 0.))
        .line_to((0., height + unit))
        .move_to((-2., height - 3.))
        .line_to((0., height))
        .move_to((2., height - 3.))
        .line_to((0., height));

    //labels
    let mut labels = Group::new();

    //set y_axis in GWh if range are bigger than 10000
    let mut label_divider = 1.;
    // let mut axis_name = "MW";
    if factor > 200. {
        label_divider = 1000.;
        // axis_name = "GWh";
    }

    for i in 0..nb_unit {
        data = data.clone().move_to((0., dist)).line_to((-2., dist));
        let label = Text::new()
            .add(TextNode::new(format!(
                "{}",
                (i as f64) * factor / label_divider
            )))
            .set("x", -5.)
            .set("y", -dist + 4.)
            .set("text-anchor", "end")
            .set("font-size", TEXT_SIZE)
            .set("fill", TEXT_COLOR);
        labels = labels.clone().add(label);
        dist += unit * factor;
    }
    let name = Text::new()
        .add(TextNode::new(axis_name.to_string()))
        .set("text-anchor", "middle")
        .set("x", 0)
        .set("y", -height - 8.)
        .set("font-size", TEXT_SIZE)
        .set("fill", TEXT_COLOR);
    labels = labels.clone().add(name);
    (
        SvgPath::new()
            .set("stroke", GRAPH_COLOR)
            .set("stroke-width", 0.6)
            .set("transform", "scale(1,-1)")
            .set("fill", "none")
            .set("d", data),
        labels,
    )
}
