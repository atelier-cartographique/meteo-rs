use std::path::Path;

use chrono::{DateTime, Duration, Utc};
use svg::node::element::Image as SVGImage;

use crate::{
    raster::dynimg_to_svgimg,
    render::{EnergyType, MeteoImage},
};

pub const FIRST_YEAR_SOLAR: i32 = 2010;
pub const FIRST_YEAR_WIND: i32 = 2020;

pub fn first_year(energy_type: &EnergyType) -> i32 {
    match energy_type {
        EnergyType::Solar => FIRST_YEAR_SOLAR,
        EnergyType::Wind => FIRST_YEAR_WIND,
        EnergyType::All => FIRST_YEAR_WIND,
    }
}

pub const SOLAR_EMPTY_COLOR: &str = "#fff1cc";
pub const WIND_EMPTY_COLOR: &str = "#d7ebad";
pub const WIND_COLOR: &str = "#95b359";

pub const OFFSHORE_COLOR: &str = "#80994D";
pub const ONSHORE_COLOR: &str = "#aacc66";
pub const SOLAR_COLOR: &str = "#FFCB46";

pub const OFFSET_X: f64 = 70.;
pub const VIEWBOX_WIDTH: f64 = 700.;
pub const VIEWBOX_HEIGHT: f64 = 300.;
pub const LOGO_WIDTH: f64 = VIEWBOX_WIDTH / 12.;
pub const LOGO_HEIGHT: f64 = VIEWBOX_HEIGHT / 8.;

pub const TEXT_SIZE: &str = "12px";
pub const TEXT_SIZE_SMALL: &str = "9px";
pub const TEXT_SIZE_BIG: &str = "20px";

pub const TEXT_COLOR: &str = "white"; // RTBF color - is overrided on energie-commune site by the css styles

pub const GRAPH_COLOR: &str = "#AAAAAA";

#[derive(Copy, Clone)]
pub enum Interval {
    Hour,
    Day,
    // Month,
}

#[derive(Copy, Clone)]
pub enum GraphType {
    Histogram,
    Linechart,
}

#[derive(Debug)]
pub struct HistogramValue {
    // pub label: String,
    pub value: f64,
    pub title: String,
}
// pub struct HistogramSerie {
//     pub values: Vec<HistogramValue>,
//     // pub color: String,
// }

pub fn logo_apere(x: f64, y: f64) -> SVGImage {
    let logo = image::open(Path::new("medias/logo-ec.png")).unwrap();
    dynimg_to_svgimg(MeteoImage(logo))
        .set("width", LOGO_WIDTH)
        .set("height", LOGO_HEIGHT)
        .set("x", x)
        // .set("y", height - 15.)
        .set("y", y)
}

// pub fn subdivision(range: f64) -> f64 {
//     println!("Range: {}", range);
//     // if range > 0. && range < 100. {
//     //     return ((range / 15.) / 0.05).round() * 0.05;
//     // } else {
//     //     return ((range / 15.) / 5.).round() * 5.;
//     // }
//     if range > 0. && range < 1. {
//         0.05
//     } else if range < 2. {
//         0.1
//     } else if range < 20. {
//         1.
//     } else if range < 100. {
//         5.
//     } else if range < 200. {
//         10.
//     } else if range < 500. {
//         20.
//     } else if range < 1000. {
//         50.
//     } else if range < 2000. {
//         100.
//     } else if range < 5000. {
//         200.
//     } else if range < 10000. {
//         500.
//     } else if range < 50000. {
//         2000.
//     } else if range < 100000. {
//         5000.
//     } else if range < 100000. {
//         100000.
//     } else if range < 500000. {
//         200000.
//     } else {
//         500000.
//     }
// }

// pub fn get_max_y(vec: &Vec<(DateTime<Utc>, f64)>) -> f64 {
//     vec.first()
//         .map(|first| vec.iter().fold(first.1, |acc, val| f64::max(acc, val.1)))
//         .unwrap_or(0.)
// }

pub fn get_max_month(vec: &Vec<(String, f64)>) -> f64 {
    vec.first()
        .map(|first| vec.iter().fold(first.1, |acc, val| f64::max(acc, val.1)))
        .unwrap_or(0.)
}
// pub fn get_max_month(vec: &Vec<[HistogramValue; 12]>) -> f64 {
//     let flattened: Vec<&HistogramValue> = vec.iter().flatten().collect();
//     flattened
//         .first()
//         .map(|first| {
//             flattened
//                 .iter()
//                 .fold(first.value, |acc, val| f64::max(acc, val.value))
//         })
//         .unwrap_or(0.)
// }

pub fn increment_date(date: DateTime<Utc>, interval: Interval) -> DateTime<Utc> {
    match interval {
        Interval::Hour => date + Duration::hours(1),
        Interval::Day => date + Duration::days(1),
        // Interval::Year => date.with_year(date.year() + 1).unwrap_or(date),
    }
}
