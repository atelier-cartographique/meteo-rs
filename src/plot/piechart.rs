use crate::plot::base::{OFFSHORE_COLOR, ONSHORE_COLOR, SOLAR_COLOR, TEXT_COLOR};
use crate::render::PastProduction;
use crate::util::str_to_f64;
use ::std::f64;
use svg::node::element::path::Data;
use svg::node::element::{Circle, Group, Path, Text};
use svg::node::Text as TextNode;
use svg::Document;

use super::base::{
    logo_apere, TEXT_SIZE, TEXT_SIZE_BIG, TEXT_SIZE_SMALL, VIEWBOX_HEIGHT, VIEWBOX_WIDTH,
};

const HYDRO_COLOR: &str = "#69A2AF";
const BIOMASS_COLOR: &str = "#DC602D";
const WASTE_COLOR: &str = "#808080";
const OTHERS_COLOR: &str = "#cccccc";

// #[derive(Clone, Debug)]
struct Slice {
    name: String,
    value: f64,
    percent: f64,
    color: String,
    _link: String,
}

fn angle_from_percent(percent: f64) -> f64 {
    2. * f64::consts::PI * percent
}

fn get_coord_from_percent(percent: f64, radius: f64) -> (f64, f64) {
    (
        (2. * f64::consts::PI * percent).cos() * radius,
        (2. * f64::consts::PI * percent).sin() * radius,
    )
}
fn get_coord_from_angle(angle: f64, radius: f64) -> (f64, f64) {
    (angle.cos() * radius, angle.sin() * radius)
}

fn percent_text(percent: f64) -> String {
    if percent < 0.5 {
        return "< 1".to_string();
    }
    return format!("{}", (percent).round());
}

fn pie_legend(cumul_percent: f64, radius: f64, s: &Slice, last_y: f64) -> (Path, Text, f64) {
    //make line by the middle of each slice
    let mid_angle = angle_from_percent(cumul_percent + s.percent / 2.);
    let (x_mid, y_mid) = get_coord_from_angle(mid_angle, radius);
    let (x_line, mut y_line) = get_coord_from_angle(mid_angle, radius + 15.);
    let diff = (last_y - y_line).abs();
    let readable_limit: f64 = str_to_f64(TEXT_SIZE).unwrap_or(15.) + 5.;
    if diff < readable_limit {
        y_line += readable_limit - diff;
    }
    let mut legend_x = x_line + 20.;
    let mut text_legend_x = legend_x + 5.;
    let mut text_align = "start";
    if mid_angle > f64::consts::PI / 2. {
        legend_x = x_line - 20.;
        text_legend_x = legend_x - 5.;
        text_align = "end";
    }
    //make connectors to legend
    let lines = Data::new()
        .move_to((x_mid, y_mid))
        .line_to((x_line, y_line))
        .line_to((legend_x, y_line));

    //write legend
    let text = Text::new()
        .add(TextNode::new(format!(
            "{}, {} GWh ({}%)",
            s.name.clone(),
            s.value.round(),
            percent_text(s.percent * 100.)
        )))
        .set("x", text_legend_x)
        .set("y", y_line + 2.)
        .set("text-anchor", text_align)
        .set("font-size", TEXT_SIZE)
        .set("color", TEXT_COLOR);

    (
        Path::new()
            .set("stroke", s.color.clone())
            .set("fill", "none")
            .set("d", lines),
        text,
        y_line,
    )
}
fn pie_center(center: (f64, f64), radius: f64, total_load: f64, cumul_percent: f64) -> Group {
    // let text = Text::new()
    //     .add(TextNode::new("Consommation totale"))
    //     .set("x", center.0)
    //     .set("y", center.1 - 10.)
    //     .set("text-anchor", "middle");
    let text = Text::new()
        .add(TextNode::new("Consommation électrique"))
        .set("x", center.0)
        .set("y", center.1 - 17.)
        .set("text-anchor", "middle")
        .set("font-size", TEXT_SIZE_SMALL)
        .set("fill", TEXT_COLOR);

    let conso = Text::new()
        .add(TextNode::new(format!("{} GWh", total_load.round())))
        .set("x", center.0)
        .set("y", center.1 + 10.)
        .set("text-anchor", "middle")
        .set("font-size", TEXT_SIZE_BIG)
        .set("fill", TEXT_COLOR);
    let percent = Text::new()
        .add(TextNode::new(format!(
            "{}% de renouvelable",
            cumul_percent.round()
        )))
        .set("x", center.0)
        .set("y", center.1 + 25.)
        .set("text-anchor", "middle")
        .set("font-size", TEXT_SIZE_SMALL)
        .set("fill", TEXT_COLOR);
    let circle = Circle::new()
        .set("cx", center.0)
        .set("cy", center.1)
        .set("r", radius)
        .set("fill", "white")
        .set("stroke", "none");
    Group::new().add(circle).add(text).add(conso).add(percent)
}

fn min(v1: f64, v2: f64) -> f64 {
    if v1 < v2 {
        v1
    } else {
        v2
    }
}

pub fn prod_pie_chart(past_prod: PastProduction) -> Document {
    // let width = radius * 2. + 700.;
    // let height = radius * 2. + 50.;
    let width = VIEWBOX_WIDTH;
    let height = VIEWBOX_HEIGHT;
    let radius = (min(width, height) - 50.) / 2.;
    let total = past_prod.total_load;
    let wind_off = past_prod.wind_off;
    let wind_on = past_prod.wind_on;
    let solar = past_prod.solar;
    let hydro = past_prod.hydro;
    let biomass = past_prod.biomass;
    let waste = past_prod.waste;

    // TO DO: pass slices as argument
    let slices = vec![
        Slice {
            name: "Eolien offshore".to_string(),
            value: wind_off,
            percent: wind_off / total,
            color: OFFSHORE_COLOR.to_string(),
            _link: "production-eolien".to_string(),
        },
        Slice {
            name: "Eolien onshore".to_string(),
            value: wind_on,
            percent: wind_on / total,
            color: ONSHORE_COLOR.to_string(),
            _link: "production-eolien".to_string(),
        },
        Slice {
            name: "Photovoltaïque".to_string(),
            value: solar,
            percent: solar / total,
            color: SOLAR_COLOR.to_string(),
            _link: "production".to_string(),
        },
        Slice {
            name: "Hydraulique".to_string(),
            value: hydro,
            percent: hydro / total,
            color: HYDRO_COLOR.to_string(),
            _link: "production".to_string(),
        },
        Slice {
            name: "Biomasse".to_string(),
            value: biomass,
            percent: biomass / total,
            color: BIOMASS_COLOR.to_string(),
            _link: "production".to_string(),
        },
        Slice {
            name: "Biomasse (incinération)".to_string(),
            value: waste,
            percent: waste / total,
            color: WASTE_COLOR.to_string(),
            _link: "production".to_string(),
        },
        Slice {
            name: "Autres".to_string(),
            value: (total - wind_off - wind_on - solar),
            percent: (total - wind_off - wind_on - solar - hydro - biomass - waste) / total,
            color: OTHERS_COLOR.to_string(),
            _link: "production".to_string(),
        },
    ];

    let mut cumul_percent = -0.25;
    let x_center = -width / 2. + 40.;
    let y_center = -height / 2.;
    let mut document = Document::new().set("viewBox", (x_center, y_center , width, height));
    let mut nb_err = 0.;
    let mut last_y = VIEWBOX_HEIGHT / 2.;
    for s in slices.iter() {
        if s.value > 0. {
            let (x, y) = get_coord_from_percent(cumul_percent, radius);
            let (line_path, text, slice_y) = pie_legend(cumul_percent, radius, s, last_y);
            //draw pie slices
            cumul_percent += s.percent;
            let (x_end, y_end) = get_coord_from_percent(cumul_percent, radius);
            let large_arc_flag = if s.percent > 0.5 { 1 } else { 0 };
            let data = Data::new()
                .move_to((x, y))
                .elliptical_arc_to(((radius, radius), 0, (large_arc_flag, 1), (x_end, y_end)))
                .line_to((0, 0))
                .close();
            let path = Path::new()
                .set("fill", s.color.clone())
                .set("stroke", s.color.clone())
                .set("stroke-width", 0.6)
                .set("d", data);
            // .set("href", s.link.clone());
            // let link = Link::new().set("href", s.link.clone()).add(path);
            document = document.clone().add(line_path).add(path).add(text);
            last_y = slice_y;
        } else {
            let err_text = Text::new()
                .add(TextNode::new(format!("{}: données manquantes", s.name)))
                .set("x", width / 2. - 30.)
                .set("y", height / 2. - 15. * nb_err - 10.)
                .set("text-anchor", "end")
                .set("font-size", TEXT_SIZE)
                .set("fill", TEXT_COLOR);
            nb_err += 1.;
            document = document.clone().add(err_text);
        }
    }
    let total_percent_renouvelable =
        (wind_off + wind_on + solar + hydro + biomass + waste) / total * 100.;
    let sub_circle = pie_center(
        (0., 0.),
        radius - 0.5 * radius,
        total,
        total_percent_renouvelable,
    );
    document
        .add(sub_circle)
        .add(logo_apere(x_center, y_center))
}
