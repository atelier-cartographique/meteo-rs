use std::collections::HashMap;
use std::convert::{From, Into};
use std::fmt::{self, Display};
use std::ops::Add;

use num_traits::Num;

#[derive(Clone)]
pub enum Node {
    HTML(Element),
    Text(String),
    InnerHTML(String),
}

impl fmt::Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Node::Text(s) => write!(f, "{}", s),
            Node::HTML(e) => write!(f, "{}", e.as_string()),
            Node::InnerHTML(s) => write!(f, "{}", s),
        }
    }
}

impl From<Element> for Node {
    fn from(e: Element) -> Node {
        Node::HTML(e)
    }
}

impl From<&Element> for Node {
    fn from(e: &Element) -> Node {
        Node::HTML(e.clone())
    }
}

impl From<String> for Node {
    fn from(s: String) -> Node {
        Node::Text(s)
    }
}

impl From<svg::Document> for Node {
    fn from(doc: svg::Document) -> Node {
        Node::InnerHTML(doc.to_string())
    }
}

#[derive(Clone)]
pub enum AttributeValue {
    Bool(bool),
    String(String),
}

pub type Attributes = HashMap<String, AttributeValue>;

impl Into<String> for &AttributeValue {
    fn into(self) -> String {
        match self {
            AttributeValue::Bool(v) => format!("{v}"),
            AttributeValue::String(v) => v.to_string(),
        }
    }
}

fn attrs_as_string(attrs: &Attributes) -> String {
    attrs
        .iter()
        .map(|(k, v)| {
            if let AttributeValue::Bool(value) = v {
                if *value {
                    format!(" {}", k)
                } else {
                    String::new()
                }
            } else {
                format!(" {}=\"{}\"", k, Into::<String>::into(v))
            }
        })
        .collect()
}

#[derive(Clone)]
pub struct Element {
    pub tag: &'static str,
    pub attrs: Attributes,
    pub children: Vec<Node>,
    empty: bool,
}

// https://developer.mozilla.org/en-US/docs/Glossary/empty_element
fn is_empty(tag: &'static str) -> bool {
    match tag {
        "area" | "base" | "br" | "col" | "embed" | "hr" | "img" | "input" | "link" | "meta"
        | "param" | "source" | "track" | "wbr" => true,
        _ => false,
    }
}

impl Element {
    pub fn new(tag: &'static str) -> Element {
        Element {
            tag,
            attrs: Attributes::new(),
            children: Vec::new(),
            empty: is_empty(tag),
        }
    }

    pub fn set_attribute<K>(self, key: K, val: AttributeValue) -> Element
    where
        K: Into<String>,
    {
        let mut new_attrs = self.attrs.clone();
        new_attrs.insert(key.into(), val);
        Element {
            tag: self.tag,
            attrs: new_attrs,
            children: self.children.clone(),
            empty: self.empty,
        }
    }

    pub fn set<K, V>(self, key: K, val: V) -> Element
    where
        K: Into<String>,
        V: Into<String>,
    {
        self.set_attribute(key, AttributeValue::String(val.into()))
    }

    pub fn set_num<K, N>(self, key: K, val: N) -> Element
    where
        K: Into<String>,
        N: Num + Display,
    {
        self.set_attribute(key, AttributeValue::String(format!("{val}")))
    }

    // pub fn set_u32<K>(self, key: K, val: u32) -> Element
    // where
    //     K: Into<String>,
    // {
    //     self.set_attribute(key, AttributeValue::U32(val))
    // }

    // pub fn set_float<K>(self, key: K, val: f64) -> Element
    // where
    //     K: Into<String>,
    // {
    //     self.set_attribute(key, AttributeValue::Float(val))
    // }

    pub fn set_bool<K>(self, key: K, val: bool) -> Element
    where
        K: Into<String>,
    {
        self.set_attribute(key, AttributeValue::Bool(val))
    }

    pub fn class<S>(self, val: S) -> Element
    where
        S: Into<String>,
    {
        self.set("class", val)
    }
    // pub fn style<S>(self, val: S) -> Element
    // where
    //     S: Into<String>,
    // {
    //     self.set("style", val)
    // }

    pub fn append_node(self, node: Node) -> Element {
        if self.empty {
            return self;
        }
        let mut new_children = self.children.clone();
        new_children.push(node);
        Element {
            tag: self.tag,
            attrs: self.attrs.clone(),
            children: new_children,
            empty: self.empty,
        }
    }

    pub fn append(self, e: Element) -> Element {
        self.append_node(Node::HTML(e))
    }

    pub fn inner_html(self, inner: String) -> Element {
        self.append_node(Node::InnerHTML(inner))
    }

    // pub fn append_text<S>(self, t: S) -> Element
    // where
    //     S: Into<String>,
    // {
    //     self.append_node(Node::Text(t.into()))
    // }

    pub fn as_string(&self) -> String {
        let tag = self.tag;
        if !self.children.is_empty() {
            let cs = self
                .children
                .iter()
                .map(|c| match c {
                    Node::HTML(e) => e.as_string(),
                    Node::Text(s) => s.clone(),
                    Node::InnerHTML(s) => s.clone(),
                })
                .collect::<Vec<String>>()
                .join("\n");
            format!("<{}{}>{}</{}>", tag, attrs_as_string(&self.attrs), cs, tag)
        } else if self.empty {
            format!("<{}{}>", tag, attrs_as_string(&self.attrs))
        } else {
            format!("<{}{}></{}>", tag, attrs_as_string(&self.attrs), tag)
        }
    }
}

impl Add for Element {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        self.append(other)
    }
}

// impl From<svg::node::element::Element> for Element {
//     fn from(svg_element: svg::node::element::Element) -> Element {
// let attrs: Attributes = svg_element.attributes.iter().map(|(k,v)| (k.clone(), String::from(v))).collect();

//         Element {
//             tag: svg_element.name.clone(),
//             attrs,

//         }
//     }
// }

pub enum Children {
    Empty,
    One(Node),
    Many(Vec<Node>),
}

pub struct Empty;

impl From<Empty> for Children {
    fn from(_: Empty) -> Children {
        Children::Empty
    }
}

impl From<Node> for Children {
    fn from(n: Node) -> Children {
        Children::One(n)
    }
}
impl From<Vec<Node>> for Children {
    fn from(ns: Vec<Node>) -> Children {
        Children::Many(ns)
    }
}

impl From<Element> for Children {
    fn from(e: Element) -> Children {
        Children::One(e.into())
    }
}
impl From<Vec<Element>> for Children {
    fn from(es: Vec<Element>) -> Children {
        Children::Many(es.iter().map(|e| e.into()).collect())
    }
}

impl From<[Element; 2]> for Children {
    fn from(es: [Element; 2]) -> Children {
        Children::Many(es.iter().map(|e| e.into()).collect())
    }
}

impl From<[Element; 3]> for Children {
    fn from(es: [Element; 3]) -> Children {
        Children::Many(es.iter().map(|e| e.into()).collect())
    }
}

impl From<[Element; 4]> for Children {
    fn from(es: [Element; 4]) -> Children {
        Children::Many(es.iter().map(|e| e.into()).collect())
    }
}

impl From<[Element; 5]> for Children {
    fn from(es: [Element; 5]) -> Children {
        Children::Many(es.iter().map(|e| e.into()).collect())
    }
}

impl From<[Element; 6]> for Children {
    fn from(es: [Element; 6]) -> Children {
        Children::Many(es.iter().map(|e| e.into()).collect())
    }
}

// From<[Element; N]> ... add as you need it, keep it sorted

impl From<String> for Children {
    fn from(s: String) -> Children {
        Children::One(Node::Text(s))
    }
}

impl From<&String> for Children {
    fn from(s: &String) -> Children {
        Children::One(Node::Text(s.clone()))
    }
}

impl From<&str> for Children {
    fn from(s: &str) -> Children {
        Children::One(Node::Text(String::from(s)))
    }
}

// impl<T> From<T> for Children
// where
//     T: Display,
// {
//     fn from(x: T) -> Children {
//         Children::One(Node::Text(format!("{x}")))
//     }
// }

pub fn element<C>(tag: &'static str, c: C) -> Element
where
    C: Into<Children>,
{
    let children: Children = c.into();
    match children {
        Children::Empty => Element::new(tag),
        Children::One(n) => Element::new(tag).append_node(n),
        Children::Many(ns) => Element {
            tag,
            attrs: Attributes::new(),
            children: ns,
            empty: is_empty(tag),
        },
    }
}

pub fn html<C>(c: C) -> Element
where
    C: Into<Children>,
{
    element("html", c)
}

pub fn head<C>(c: C) -> Element
where
    C: Into<Children>,
{
    element("head", c)
}

pub fn meta() -> Element {
    element("meta", Empty)
}

pub fn link() -> Element {
    element("link", Empty)
}

pub fn body<C>(c: C) -> Element
where
    C: Into<Children>,
{
    element("body", c)
}

pub fn div<C>(c: C) -> Element
where
    C: Into<Children>,
{
    element("div", c)
}

pub fn span<C>(c: C) -> Element
where
    C: Into<Children>,
{
    element("span", c)
}

pub fn img() -> Element {
    element("img", Empty)
}

pub fn input() -> Element {
    element("input", Empty)
}

pub fn text_area<C>(c: C) -> Element
where
    C: Into<Children>,
{
    element("textarea", c)
}

pub fn form<C>(c: C) -> Element
where
    C: Into<Children>,
{
    element("form", c)
}
pub fn select<C>(c: C) -> Element
where
    C: Into<Children>,
{
    element("select", c)
}
pub fn option<C>(c: C) -> Element
where
    C: Into<Children>,
{
    element("option", c)
}

// pub fn table<C>(c: C) -> Element
// where
//     C: Into<Children>,
// {
//     element("table", c)
// }

// pub fn tr<C>(c: C) -> Element
// where
//     C: Into<Children>,
// {
//     element("tr", c)
// }

// pub fn td<C>(c: C) -> Element
// where
//     C: Into<Children>,
// {
//     element("td", c)
// }

// pub fn code<C>(c: C) -> Element
// where
//     C: Into<Children>,
// {
//     element("code", c)
// }

pub fn h1<C>(c: C) -> Element
where
    C: Into<Children>,
{
    element("h1", c)
}

// pub fn h2<C>(c: C) -> Element
// where
//     C: Into<Children>,
// {
//     element("h2", c)
// }

// pub fn style<C>(c: C) -> Element
// where
//     C: Into<Children>,
// {
//     element("style", c)
// }

pub fn anchor<C>(c: C) -> Element
where
    C: Into<Children>,
{
    element("a", c)
}

pub fn with_doctype(e: Element) -> String {
    format!("<!DOCTYPE html>\n{}", e.as_string())
}

pub fn as_xml(e: Element) -> String {
    format!(
        r#"<?xml version="1.0" encoding="utf-8" ?>
{}"#,
        e.as_string()
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn create_element_with_attrs() {
        let e = div(Empty).set("foo", "bar");
        assert_eq!(e.as_string(), "<div foo=\"bar\"></div>");
    }
    #[test]
    fn create_element_with_child() {
        let c = div(Empty);
        let e = div(c);
        assert_eq!(e.as_string(), "<div><div></div></div>");
    }
}
